<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = json_decode('[{
            "id": "1",
            "name": "Kadarsah Suryadi",
            "prefix": "Prof.Dr.Ir.",
            "suffix": "DEA",
            "nim": null
        }, {
            "id": "2",
            "name": "Deddy Kurniadi",
            "prefix": "Prof.Dr.Ir.",
            "suffix": "",
            "nim": null
        }, {
            "id": "3",
            "name": "Fx. Nugroho Soelami",
            "prefix": "Dr.Ir.",
            "suffix": "M.B.Env.",
            "nim": null
        }, {
            "id": "4",
            "name": "Bermawi Priyatna Iskandar",
            "prefix": "Prof.Ir.",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "5",
            "name": "Wawan Gunawan A. Kadir",
            "prefix": "Prof.Dr.",
            "suffix": "MS",
            "nim": null
        }, {
            "id": "6",
            "name": "Miming Miharja",
            "prefix": "Dr.",
            "suffix": "ST,M.Sc.Eng.",
            "nim": null
        }, {
            "id": "7",
            "name": "Bambang Riyanto Trilaksono",
            "prefix": "Prof.Dr.Ir",
            "suffix": "",
            "nim": null
        }, {
            "id": "8",
            "name": "Irawati",
            "prefix": "Prof.Dr.",
            "suffix": "MS",
            "nim": null
        }, {
            "id": "9",
            "name": "Hasanuddin Z.A",
            "prefix": "Prof.Ir",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "10",
            "name": "Edy Tri Baskoro",
            "prefix": "Prof.Dr.",
            "suffix": "M.Sc.",
            "nim": null
        }, {
            "id": "11",
            "name": "Imam Santosa",
            "prefix": "Dr.",
            "suffix": "M.Sn.",
            "nim": null
        }, {
            "id": "12",
            "name": "Hari Muhammad",
            "prefix": "Ir.",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "13",
            "name": "Ade Sjafruddin",
            "prefix": "Prof.Ir.",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "14",
            "name": "Sri Widiyantoro",
            "prefix": "Prof.",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "15",
            "name": "Widjaja Martokusumo",
            "prefix": "Prof.Dr.Ing.Ir.",
            "suffix": "",
            "nim": null
        }, {
            "id": "16",
            "name": "Sudarso Kaderi Wiryono",
            "prefix": "Prof.Dr.Ir",
            "suffix": "DEA",
            "nim": null
        }, {
            "id": "17",
            "name": "Daryono Hadi",
            "prefix": "Prof.Dr.",
            "suffix": "Tj. Apt.,M.Sc.",
            "nim": null
        }, {
            "id": "18",
            "name": "I Nyoman Pugeg Aryantha",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "19",
            "name": "Jaka Sembiring",
            "prefix": "Dr.Ir.",
            "suffix": "M.Eng",
            "nim": null
        }, {
            "id": "20",
            "name": "Pudji Astuti Waluyo",
            "prefix": "Prof.Dr.",
            "suffix": "MS",
            "nim": null
        }, {
            "id": "21",
            "name": "Mindriany Syafila",
            "prefix": "Prof.",
            "suffix": "MS, Ph.D",
            "nim": null
        }, {
            "id": "22",
            "name": "Fida Madayanti Warganegara",
            "prefix": "Prof.Dra.",
            "suffix": "MS, Ph.D",
            "nim": null
        }, {
            "id": "23",
            "name": "Achmad Syarief",
            "prefix": "Dr.",
            "suffix": "S.Sn., M.Sc",
            "nim": null
        }, {
            "id": "24",
            "name": "Wedyanto",
            "prefix": "Dr.Ir.",
            "suffix": "M.Sc.",
            "nim": null
        }, {
            "id": "25",
            "name": "Woejantari Kartidjo",
            "prefix": "Dr.Ir.",
            "suffix": "MT",
            "nim": null
        }, {
            "id": "26",
            "name": "Taufikurahman",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "27",
            "name": "Asep Gana Suganda",
            "prefix": "Prof.Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "28",
            "name": "Wahyudi W. Parnadi",
            "prefix": "Dr.rer.nat.Ir.",
            "suffix": "MS",
            "nim": null
        }, {
            "id": "29",
            "name": "Ario Dewanto QIA",
            "prefix": "Drs.",
            "suffix": "",
            "nim": null
        }, {
            "id": "30",
            "name": "Ichsan Setya Putra",
            "prefix": "Prof.Dr.Ir.",
            "suffix": "",
            "nim": null
        }, {
            "id": "31",
            "name": "Pepen Arifin",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "32",
            "name": "Mary Handoko Wijoyo",
            "prefix": "Ir.",
            "suffix": "M.Sc.",
            "nim": null
        }, {
            "id": "33",
            "name": "Dasapta Erwin Irawan",
            "prefix": "Dr.",
            "suffix": "ST, MT.",
            "nim": null
        }, {
            "id": "34",
            "name": "Betti S. Alisjahbana",
            "prefix": "Ir.",
            "suffix": "",
            "nim": null
        }, {
            "id": "35",
            "name": "Jann Hidajat Tjakraatmadja",
            "prefix": "Prof.",
            "suffix": "",
            "nim": null
        }, {
            "id": "36",
            "name": "Made Emmy Relawati S",
            "prefix": "Prof.Dr.Ir",
            "suffix": "",
            "nim": null
        }, {
            "id": "37",
            "name": "Intan Ahmad",
            "prefix": "Prof. Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "39",
            "name": "Ir. Benyamin Sapiie",
            "prefix": "Ir.",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "40",
            "name": "Dina Anggreni Sarsito",
            "prefix": "Dr.Ir. ",
            "suffix": "MT",
            "nim": null
        }, {
            "id": "41",
            "name": "Abdul Waris",
            "prefix": "Prof.Drs.",
            "suffix": "M.Eng.,Ph.D.",
            "nim": null
        }, {
            "id": "42",
            "name": "Indra Noviandri",
            "prefix": "Dr.",
            "suffix": "MS",
            "nim": null
        }, {
            "id": "43",
            "name": "Hafiz Aziz Ahmad,",
            "prefix": "Dr.",
            "suffix": "S.Sn.,M.Desg.",
            "nim": null
        }, {
            "id": "44",
            "name": "Achmad1 Haldani Destiarmand",
            "prefix": "Dr.",
            "suffix": "M.Sn.",
            "nim": null
        }, {
            "id": "45",
            "name": "Tjokorde Walmiki Samadhi",
            "prefix": "Dr.",
            "suffix": "ST.MT",
            "nim": null
        }, {
            "id": "46",
            "name": "Suprayogi",
            "prefix": "",
            "suffix": "ST,MT,Ph.D.",
            "nim": null
        }, {
            "id": "47",
            "name": "Zainal Abidin",
            "prefix": "Prof.Dr.Ir.",
            "suffix": "",
            "nim": null
        }, {
            "id": "48",
            "name": "Ignatius Pulung Nurprasetio",
            "prefix": "Dr.Ir.",
            "suffix": "MSME",
            "nim": null
        }, {
            "id": "49",
            "name": "Irsan Soemantri Brodjonegoro",
            "prefix": "Ir.",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "50",
            "name": "Indah Rachmatiah Siti Salami",
            "prefix": "Ir.",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "51",
            "name": "Ridho Kresna Wattimena",
            "prefix": "Prof.Ir",
            "suffix": "MT,Ph.D.",
            "nim": null
        }, {
            "id": "52",
            "name": "Susanti Alawiyah",
            "prefix": "Dr.",
            "suffix": "ST, MT.",
            "nim": null
        }, {
            "id": "53",
            "name": "Denny Zulkaidi",
            "prefix": "Dr.Ir.",
            "suffix": "MUP",
            "nim": null
        }, {
            "id": "54",
            "name": "Sri2 Maryati",
            "prefix": "Dr.",
            "suffix": "MIP",
            "nim": null
        }, {
            "id": "55",
            "name": "Utomo Sardjono Putro",
            "prefix": "Prof.Dr.Ir.",
            "suffix": "M.Eng",
            "nim": null
        }, {
            "id": "56",
            "name": "Aurik Gustomo",
            "prefix": "Dr.",
            "suffix": "ST, MT.",
            "nim": null
        }, {
            "id": "57",
            "name": "Kusnandar Anggadiredja",
            "prefix": "Dr.",
            "suffix": "S.Si.,M.Si.",
            "nim": null
        }, {
            "id": "58",
            "name": "Marlia Singgih",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "59",
            "name": "Tjandra Anggraeni",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "60",
            "name": "Iriawati",
            "prefix": "Dr.",
            "suffix": "M.Sc.",
            "nim": null
        }, {
            "id": "61",
            "name": "Iwan Kridasantausa",
            "prefix": "Prof.Ir",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "62",
            "name": "Nining Sari Ningsih",
            "prefix": "Dr.Eng",
            "suffix": "MS",
            "nim": null
        }, {
            "id": "63",
            "name": "Dwi Hendratmo Widyantoro",
            "prefix": "Ir.",
            "suffix": "M.Sc.,Ph.D.",
            "nim": null
        }, {
            "id": "64",
            "name": "Nana Rachmana Syambas",
            "prefix": "Dr.Ir.",
            "suffix": "M.Eng",
            "nim": null
        }, {
            "id": "65",
            "name": "Estiyanti Ekawati",
            "prefix": "Ir.",
            "suffix": "MT. Ph.D.",
            "nim": null
        }, {
            "id": "66",
            "name": "Eko Mursito Budi",
            "prefix": "Dr.Ir.",
            "suffix": "MT",
            "nim": null
        }, {
            "id": "67",
            "name": "Brian Yuliarto",
            "prefix": "",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "68",
            "name": "Agus Samsi",
            "prefix": "Dr.Ir.",
            "suffix": "MT",
            "nim": null
        }, {
            "id": "69",
            "name": "Ahmad Nuruddin",
            "prefix": "Ir.",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "70",
            "name": "I.B. Ardhana Putra",
            "prefix": "Ir.",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "71",
            "name": "Augie Widyotriatmo",
            "prefix": "",
            "suffix": "ST. MT. Ph.D.",
            "nim": null
        }, {
            "id": "72",
            "name": "Ayu Garetha Risangtuni",
            "prefix": "",
            "suffix": "ST MT",
            "nim": null
        }, {
            "id": "73",
            "name": "Endra Joelianto",
            "prefix": "Ir.",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "74",
            "name": "Muhammad Rifki K",
            "prefix": "",
            "suffix": "",
            "nim": "13310089"
        }, {
            "id": "75",
            "name": "Pradito Bintang Wicaksono",
            "prefix": "",
            "suffix": "",
            "nim": "13311044"
        }, {
            "id": "76",
            "name": "Asri Nur Zahirah",
            "prefix": "",
            "suffix": "",
            "nim": "13311053"
        }, {
            "id": "77",
            "name": "Asra Wijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13311058"
        }, {
            "id": "78",
            "name": "Raras Handwiyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13311092"
        }, {
            "id": "79",
            "name": "R Alf Fajrus S",
            "prefix": "",
            "suffix": "",
            "nim": "13312051"
        }, {
            "id": "80",
            "name": "Albert Wilson",
            "prefix": "",
            "suffix": "",
            "nim": "13312052"
        }, {
            "id": "81",
            "name": "Oki Matra Prakasa",
            "prefix": "",
            "suffix": "",
            "nim": "13312053"
        }, {
            "id": "82",
            "name": "Rizkiawan Fauzan",
            "prefix": "",
            "suffix": "",
            "nim": "13312054"
        }, {
            "id": "83",
            "name": "Shabrina Hapsaryta Aufari",
            "prefix": "",
            "suffix": "",
            "nim": "13312055"
        }, {
            "id": "84",
            "name": "Kevin Christian",
            "prefix": "",
            "suffix": "",
            "nim": "13312056"
        }, {
            "id": "85",
            "name": "Ivan Adela Sulistyawan",
            "prefix": "",
            "suffix": "",
            "nim": "13312057"
        }, {
            "id": "86",
            "name": "Fajrin Firmansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13312058"
        }, {
            "id": "87",
            "name": "Sofiya Habiba",
            "prefix": "",
            "suffix": "",
            "nim": "13312059"
        }, {
            "id": "88",
            "name": "Kevin Soetomo",
            "prefix": "",
            "suffix": "",
            "nim": "13312060"
        }, {
            "id": "89",
            "name": "Indah Inayati",
            "prefix": "",
            "suffix": "",
            "nim": "13312061"
        }, {
            "id": "90",
            "name": "Rezzy Yolanda Wulandhari",
            "prefix": "",
            "suffix": "",
            "nim": "13312062"
        }, {
            "id": "91",
            "name": "Auditio Mandhany",
            "prefix": "",
            "suffix": "",
            "nim": "13312063"
        }, {
            "id": "92",
            "name": "Hadi Parmana",
            "prefix": "",
            "suffix": "",
            "nim": "13312064"
        }, {
            "id": "93",
            "name": "Rafid Fikri",
            "prefix": "",
            "suffix": "",
            "nim": "13312065"
        }, {
            "id": "94",
            "name": "Masitoh",
            "prefix": "",
            "suffix": "",
            "nim": "13312066"
        }, {
            "id": "95",
            "name": "Afif Fauziyyah",
            "prefix": "",
            "suffix": "",
            "nim": "13312067"
        }, {
            "id": "96",
            "name": "Dena Karunianto Wibowo",
            "prefix": "",
            "suffix": "",
            "nim": "13312068"
        }, {
            "id": "97",
            "name": "Simon Boy Pardomuan",
            "prefix": "",
            "suffix": "",
            "nim": "13312069"
        }, {
            "id": "98",
            "name": "Giovanni",
            "prefix": "",
            "suffix": "",
            "nim": "13312070"
        }, {
            "id": "99",
            "name": "K Rangga Wiratama",
            "prefix": "",
            "suffix": "",
            "nim": "13312071"
        }, {
            "id": "100",
            "name": "Aflin Tris Hibatullah",
            "prefix": "",
            "suffix": "",
            "nim": "13312072"
        }, {
            "id": "101",
            "name": "Nadya Komara Putri",
            "prefix": "",
            "suffix": "",
            "nim": "13312073"
        }, {
            "id": "102",
            "name": "Genki Imam Prayogo",
            "prefix": "",
            "suffix": "",
            "nim": "13312074"
        }, {
            "id": "103",
            "name": "Adam Yanuro",
            "prefix": "",
            "suffix": "",
            "nim": "13312075"
        }, {
            "id": "104",
            "name": "Dovi Nurdiana Azizah",
            "prefix": "",
            "suffix": "",
            "nim": "13312076"
        }, {
            "id": "105",
            "name": "Puti Audia Fatima",
            "prefix": "",
            "suffix": "",
            "nim": "13312078"
        }, {
            "id": "106",
            "name": "Hilmy Hanif",
            "prefix": "",
            "suffix": "",
            "nim": "13312079"
        }, {
            "id": "107",
            "name": "Teguh Aditanoyo",
            "prefix": "",
            "suffix": "",
            "nim": "13312080"
        }, {
            "id": "108",
            "name": "Amron Naibaho",
            "prefix": "",
            "suffix": "",
            "nim": "13312081"
        }, {
            "id": "109",
            "name": "Mhd Akbar Anthony Siregar",
            "prefix": "",
            "suffix": "",
            "nim": "13312082"
        }, {
            "id": "110",
            "name": "Yosha Yoana Putri",
            "prefix": "",
            "suffix": "",
            "nim": "13312083"
        }, {
            "id": "111",
            "name": "Thomas Milano Setiawan",
            "prefix": "",
            "suffix": "",
            "nim": "13312084"
        }, {
            "id": "112",
            "name": "Nadia Hanif",
            "prefix": "",
            "suffix": "",
            "nim": "13312085"
        }, {
            "id": "113",
            "name": "Rizky Sanditya Arief",
            "prefix": "",
            "suffix": "",
            "nim": "13312086"
        }, {
            "id": "114",
            "name": "Rahimatul Yusra Mardiah",
            "prefix": "",
            "suffix": "",
            "nim": "13312087"
        }, {
            "id": "115",
            "name": "Qanun Miladial H",
            "prefix": "",
            "suffix": "",
            "nim": "13312088"
        }, {
            "id": "116",
            "name": "Hardistya Ayu Agustiani Putri",
            "prefix": "",
            "suffix": "",
            "nim": "13312089"
        }, {
            "id": "117",
            "name": "Talitha Fauzia Rahma",
            "prefix": "",
            "suffix": "",
            "nim": "13312090"
        }, {
            "id": "118",
            "name": "Muhammad Bagus Abdani Syukron",
            "prefix": "",
            "suffix": "",
            "nim": "13312091"
        }, {
            "id": "119",
            "name": "Siti Maisaroh",
            "prefix": "",
            "suffix": "",
            "nim": "13312092"
        }, {
            "id": "120",
            "name": "Dhea Rineka Ramdhini",
            "prefix": "",
            "suffix": "",
            "nim": "13312093"
        }, {
            "id": "121",
            "name": "Keswara Hariyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13312094"
        }, {
            "id": "122",
            "name": "Bagus Dewangga",
            "prefix": "",
            "suffix": "",
            "nim": "13312095"
        }, {
            "id": "123",
            "name": "Ichsan Aulya",
            "prefix": "",
            "suffix": "",
            "nim": "13312096"
        }, {
            "id": "124",
            "name": "Azmi Khoiri",
            "prefix": "",
            "suffix": "",
            "nim": "13312097"
        }, {
            "id": "125",
            "name": "I Putu Darma Ruswara",
            "prefix": "",
            "suffix": "",
            "nim": "13312098"
        }, {
            "id": "126",
            "name": "Maulana Murtadha",
            "prefix": "",
            "suffix": "",
            "nim": "13312099"
        }, {
            "id": "127",
            "name": "Nabella Adjani",
            "prefix": "",
            "suffix": "",
            "nim": "13312100"
        }, {
            "id": "128",
            "name": "Nova Mahendra Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13312101"
        }, {
            "id": "130",
            "name": "Abdul Ghofar",
            "prefix": "",
            "suffix": "",
            "nim": "13309112"
        }, {
            "id": "131",
            "name": "Abdul Rachman Sanjaya",
            "prefix": "",
            "suffix": "",
            "nim": "13313057"
        }, {
            "id": "132",
            "name": "Abdurrahman Saleh Adijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13313076"
        }, {
            "id": "133",
            "name": "Addin Dinda Rinata",
            "prefix": "",
            "suffix": "",
            "nim": "13312048"
        }, {
            "id": "134",
            "name": "Adfialdho Rehanda",
            "prefix": "",
            "suffix": "",
            "nim": "13314077"
        }, {
            "id": "135",
            "name": "Adhe Khresna Pustiadi",
            "prefix": "",
            "suffix": "",
            "nim": "13314049"
        }, {
            "id": "136",
            "name": "Adhika Widya Sena",
            "prefix": "",
            "suffix": "",
            "nim": "13313054"
        }, {
            "id": "137",
            "name": "Adi Suparyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13314091"
        }, {
            "id": "138",
            "name": "Adlian Rahmadita S A",
            "prefix": "",
            "suffix": "",
            "nim": "13310053"
        }, {
            "id": "139",
            "name": "Adrian Akbar Untoro",
            "prefix": "",
            "suffix": "",
            "nim": "13310096"
        }, {
            "id": "140",
            "name": "Adriana Enge",
            "prefix": "",
            "suffix": "",
            "nim": "13312001"
        }, {
            "id": "141",
            "name": "Afina Hasna Ghaida Taufik",
            "prefix": "",
            "suffix": "",
            "nim": "13712021"
        }, {
            "id": "142",
            "name": "Agil Nawa Irawan Putro",
            "prefix": "",
            "suffix": "",
            "nim": "13310091"
        }, {
            "id": "143",
            "name": "Agustinus Leonardo Lie",
            "prefix": "",
            "suffix": "",
            "nim": "13309049"
        }, {
            "id": "144",
            "name": "Ahmad Kharies H",
            "prefix": "",
            "suffix": "",
            "nim": "13314080"
        }, {
            "id": "145",
            "name": "Ahmad Rodik Wijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13314015"
        }, {
            "id": "146",
            "name": "Ahmad Zahi Ulul Azmi",
            "prefix": "",
            "suffix": "",
            "nim": "13313004"
        }, {
            "id": "147",
            "name": "Ahmad Zainul Ihsan",
            "prefix": "",
            "suffix": "",
            "nim": "13312017"
        }, {
            "id": "148",
            "name": "Ahmad Zaki",
            "prefix": "",
            "suffix": "",
            "nim": "13312033"
        }, {
            "id": "149",
            "name": "Ahsan Ismail Anwar",
            "prefix": "",
            "suffix": "",
            "nim": "13312046"
        }, {
            "id": "150",
            "name": "Aidia Margiansyah Fajrin",
            "prefix": "",
            "suffix": "",
            "nim": "13310018"
        }, {
            "id": "151",
            "name": "Aishanura Handina Primanti",
            "prefix": "",
            "suffix": "",
            "nim": "13312002"
        }, {
            "id": "152",
            "name": "Aisyah Shabrina",
            "prefix": "",
            "suffix": "",
            "nim": "13314070"
        }, {
            "id": "153",
            "name": "Akbar Badriansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13308053"
        }, {
            "id": "154",
            "name": "Akhmad Ridlo Iqbal Mukaffi",
            "prefix": "",
            "suffix": "",
            "nim": "13312040"
        }, {
            "id": "155",
            "name": "Akhmad Zein Eko Mustofa",
            "prefix": "",
            "suffix": "",
            "nim": "13712012"
        }, {
            "id": "156",
            "name": "Albertus Wida Wiratama",
            "prefix": "",
            "suffix": "",
            "nim": "13313083"
        }, {
            "id": "157",
            "name": "Alderine Kuswardhani",
            "prefix": "",
            "suffix": "",
            "nim": "13310005"
        }, {
            "id": "158",
            "name": "Aldo Fadhila",
            "prefix": "",
            "suffix": "",
            "nim": "13312027"
        }, {
            "id": "159",
            "name": "Alexander Septian",
            "prefix": "",
            "suffix": "",
            "nim": "13313019"
        }, {
            "id": "160",
            "name": "Alfian Tirta Diputra",
            "prefix": "",
            "suffix": "",
            "nim": "13308094"
        }, {
            "id": "161",
            "name": "Alfiz Muhammad Qizwini",
            "prefix": "",
            "suffix": "",
            "nim": "13712054"
        }, {
            "id": "162",
            "name": "Althaf Aprilmar",
            "prefix": "",
            "suffix": "",
            "nim": "13314006"
        }, {
            "id": "163",
            "name": "Alvin Ekaputra W",
            "prefix": "",
            "suffix": "",
            "nim": "13311033"
        }, {
            "id": "164",
            "name": "Alvin Nurhadi Wijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13311069"
        }, {
            "id": "165",
            "name": "Amanda Dwisstya Utami",
            "prefix": "",
            "suffix": "",
            "nim": "13312043"
        }, {
            "id": "166",
            "name": "Amanda Putri Ismu Chumairoh",
            "prefix": "",
            "suffix": "",
            "nim": "13314022"
        }, {
            "id": "167",
            "name": "Amin Yahya Zefiansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13314051"
        }, {
            "id": "168",
            "name": "Amirul Ihsan",
            "prefix": "",
            "suffix": "",
            "nim": "13314082"
        }, {
            "id": "169",
            "name": "Andika Anugrah T",
            "prefix": "",
            "suffix": "",
            "nim": "13308008"
        }, {
            "id": "170",
            "name": "Andika Nur Fadhilah",
            "prefix": "",
            "suffix": "",
            "nim": "13314039"
        }, {
            "id": "171",
            "name": "Andreas",
            "prefix": "",
            "suffix": "",
            "nim": "13310034"
        }, {
            "id": "172",
            "name": "Andreas Aditya",
            "prefix": "",
            "suffix": "",
            "nim": "13312006"
        }, {
            "id": "173",
            "name": "Andreas Julianto Sutrisno",
            "prefix": "",
            "suffix": "",
            "nim": "13309091"
        }, {
            "id": "174",
            "name": "Andrew Winata",
            "prefix": "",
            "suffix": "",
            "nim": "13314018"
        }, {
            "id": "175",
            "name": "Andyaningrum Fauziah",
            "prefix": "",
            "suffix": "",
            "nim": "13312008"
        }, {
            "id": "176",
            "name": "Angela Dian Andrini",
            "prefix": "",
            "suffix": "",
            "nim": "13314014"
        }, {
            "id": "177",
            "name": "Anggita Putri Sumarna",
            "prefix": "",
            "suffix": "",
            "nim": "10212006"
        }, {
            "id": "178",
            "name": "Angku Rai",
            "prefix": "",
            "suffix": "",
            "nim": "13310015"
        }, {
            "id": "179",
            "name": "Anisa Azizah",
            "prefix": "",
            "suffix": "",
            "nim": "13313073"
        }, {
            "id": "180",
            "name": "Anisah",
            "prefix": "",
            "suffix": "",
            "nim": "13312021"
        }, {
            "id": "181",
            "name": "Anni Nuril Hidayati",
            "prefix": "",
            "suffix": "",
            "nim": "10212049"
        }, {
            "id": "182",
            "name": "Annisa Amalia Martiano",
            "prefix": "",
            "suffix": "",
            "nim": "13712044"
        }, {
            "id": "183",
            "name": "Annisa Azalia Herwandani",
            "prefix": "",
            "suffix": "",
            "nim": "13314071"
        }, {
            "id": "184",
            "name": "Anton Wijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13310106"
        }, {
            "id": "185",
            "name": "Apresio Kefin Fajrial",
            "prefix": "",
            "suffix": "",
            "nim": "13313042"
        }, {
            "id": "186",
            "name": "Araaf Dinullah Recta",
            "prefix": "",
            "suffix": "",
            "nim": "13310067"
        }, {
            "id": "187",
            "name": "Arda Putra Ryandika",
            "prefix": "",
            "suffix": "",
            "nim": "13313082"
        }, {
            "id": "188",
            "name": "Ardani Cesario Zuhri",
            "prefix": "",
            "suffix": "",
            "nim": "13313038"
        }, {
            "id": "189",
            "name": "Ardinda Kartikaningtyas",
            "prefix": "",
            "suffix": "",
            "nim": "13313068"
        }, {
            "id": "190",
            "name": "Ardityo Giantra",
            "prefix": "",
            "suffix": "",
            "nim": "13313031"
        }, {
            "id": "191",
            "name": "Aresti Likafia",
            "prefix": "",
            "suffix": "",
            "nim": "13313095"
        }, {
            "id": "192",
            "name": "Aretha Fieradiella Pahrevi",
            "prefix": "",
            "suffix": "",
            "nim": "13314017"
        }, {
            "id": "193",
            "name": "Arief Satrio Rachman",
            "prefix": "",
            "suffix": "",
            "nim": "13312036"
        }, {
            "id": "194",
            "name": "Arief Surya Wijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13313046"
        }, {
            "id": "195",
            "name": "Arif Sutikno",
            "prefix": "",
            "suffix": "",
            "nim": "13309011"
        }, {
            "id": "196",
            "name": "Arifin Luthfi Maulana",
            "prefix": "",
            "suffix": "",
            "nim": "13314044"
        }, {
            "id": "197",
            "name": "Arimurti Adinegoro",
            "prefix": "",
            "suffix": "",
            "nim": "13311052"
        }, {
            "id": "198",
            "name": "Arinda Puspita Rachman",
            "prefix": "",
            "suffix": "",
            "nim": "13310008"
        }, {
            "id": "199",
            "name": "Ariva Permatasari",
            "prefix": "",
            "suffix": "",
            "nim": "13313061"
        }, {
            "id": "200",
            "name": "Arjun Sumarlan",
            "prefix": "",
            "suffix": "",
            "nim": "13314081"
        }, {
            "id": "201",
            "name": "Arumingtyas Silmi H",
            "prefix": "",
            "suffix": "",
            "nim": "13310050"
        }, {
            "id": "202",
            "name": "Athira Fajrina",
            "prefix": "",
            "suffix": "",
            "nim": "13314032"
        }, {
            "id": "203",
            "name": "Atindriyo Kusumo Pamososuryo",
            "prefix": "",
            "suffix": "",
            "nim": "13310104"
        }, {
            "id": "204",
            "name": "Avi Melati",
            "prefix": "",
            "suffix": "",
            "nim": "13310087"
        }, {
            "id": "205",
            "name": "Ayuning Fauziyah",
            "prefix": "",
            "suffix": "",
            "nim": "13313074"
        }, {
            "id": "206",
            "name": "Aziz Muzaki",
            "prefix": "",
            "suffix": "",
            "nim": "13313066"
        }, {
            "id": "207",
            "name": "Azizah Syifalianti Noor",
            "prefix": "",
            "suffix": "",
            "nim": "13312018"
        }, {
            "id": "208",
            "name": "Bagas Aulia Kautsar",
            "prefix": "",
            "suffix": "",
            "nim": "13314075"
        }, {
            "id": "209",
            "name": "Bellinda",
            "prefix": "",
            "suffix": "",
            "nim": "13313058"
        }, {
            "id": "210",
            "name": "Benedictus Aristyo Nugroho",
            "prefix": "",
            "suffix": "",
            "nim": "13313096"
        }, {
            "id": "211",
            "name": "Billy Nikodemus Max",
            "prefix": "",
            "suffix": "",
            "nim": "13310013"
        }, {
            "id": "212",
            "name": "Billy Suyapmo",
            "prefix": "",
            "suffix": "",
            "nim": "13314064"
        }, {
            "id": "213",
            "name": "Boby Anditio",
            "prefix": "",
            "suffix": "",
            "nim": "13314089"
        }, {
            "id": "214",
            "name": "Cecilia Tities Ginalih",
            "prefix": "",
            "suffix": "",
            "nim": "13310007"
        }, {
            "id": "215",
            "name": "Cornelia Andini Norma Gupita",
            "prefix": "",
            "suffix": "",
            "nim": "13314068"
        }, {
            "id": "216",
            "name": "Dafi Adinegoro Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13313085"
        }, {
            "id": "217",
            "name": "DamarwulanEka Agustina",
            "prefix": "",
            "suffix": "",
            "nim": "13313089"
        }, {
            "id": "218",
            "name": "Daniel Sanjaya",
            "prefix": "",
            "suffix": "",
            "nim": "13314031"
        }, {
            "id": "219",
            "name": "Davin Barito",
            "prefix": "",
            "suffix": "",
            "nim": "13310023"
        }, {
            "id": "220",
            "name": "Davindra Giovanno Airulla",
            "prefix": "",
            "suffix": "",
            "nim": "13312035"
        }, {
            "id": "221",
            "name": "Dea Lana Sari",
            "prefix": "",
            "suffix": "",
            "nim": "13313001"
        }, {
            "id": "222",
            "name": "Deasty Kusuma Dewi",
            "prefix": "",
            "suffix": "",
            "nim": "13314063"
        }, {
            "id": "223",
            "name": "Deni Sutisna Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13314007"
        }, {
            "id": "224",
            "name": "Devi Nurhanivah",
            "prefix": "",
            "suffix": "",
            "nim": "10212071"
        }, {
            "id": "225",
            "name": "Devianti Jakub",
            "prefix": "",
            "suffix": "",
            "nim": "13311002"
        }, {
            "id": "226",
            "name": "Devy Astari Siregar",
            "prefix": "",
            "suffix": "",
            "nim": "13310026"
        }, {
            "id": "227",
            "name": "Dewi Nurfitri",
            "prefix": "",
            "suffix": "",
            "nim": "13313034"
        }, {
            "id": "228",
            "name": "Dewi Retno Andriani",
            "prefix": "",
            "suffix": "",
            "nim": "13314054"
        }, {
            "id": "229",
            "name": "Dhimas Riyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13314013"
        }, {
            "id": "230",
            "name": "Dhimas Rizqi Aji Sudarno",
            "prefix": "",
            "suffix": "",
            "nim": "13309059"
        }, {
            "id": "231",
            "name": "Dian Adi Prastowo",
            "prefix": "",
            "suffix": "",
            "nim": "13314086"
        }, {
            "id": "232",
            "name": "Diana Pinasthika",
            "prefix": "",
            "suffix": "",
            "nim": "13314033"
        }, {
            "id": "233",
            "name": "Diansi Proborini Azarya",
            "prefix": "",
            "suffix": "",
            "nim": "13712025"
        }, {
            "id": "234",
            "name": "Dicky Adhitya D",
            "prefix": "",
            "suffix": "",
            "nim": "13309103"
        }, {
            "id": "235",
            "name": "Diina Qiyaman Mushoddiqoh",
            "prefix": "",
            "suffix": "",
            "nim": "13310056"
        }, {
            "id": "236",
            "name": "Dimas Indra Cahaya Ramadhan",
            "prefix": "",
            "suffix": "",
            "nim": "13313081"
        }, {
            "id": "237",
            "name": "Dina Puspita Sari",
            "prefix": "",
            "suffix": "",
            "nim": "13314005"
        }, {
            "id": "238",
            "name": "Dinda Putri Thalia",
            "prefix": "",
            "suffix": "",
            "nim": "13314073"
        }, {
            "id": "239",
            "name": "Dita Ayu Banjarnahor",
            "prefix": "",
            "suffix": "",
            "nim": "13312032"
        }, {
            "id": "240",
            "name": "Dita Novizayanti",
            "prefix": "",
            "suffix": "",
            "nim": "10212038"
        }, {
            "id": "241",
            "name": "Dwi Hizzki Hanissa",
            "prefix": "",
            "suffix": "",
            "nim": "13313048"
        }, {
            "id": "242",
            "name": "Dwita Nitoayu Astari",
            "prefix": "",
            "suffix": "",
            "nim": "13311054"
        }, {
            "id": "243",
            "name": "Edelweis Martha Jelita",
            "prefix": "",
            "suffix": "",
            "nim": "13313021"
        }, {
            "id": "244",
            "name": "Edgar Adirga Firmanditya",
            "prefix": "",
            "suffix": "",
            "nim": "13712016"
        }, {
            "id": "245",
            "name": "Edwin Maesaro Rahman",
            "prefix": "",
            "suffix": "",
            "nim": "13310093"
        }, {
            "id": "246",
            "name": "Ega Risandy Imam Pramadhani",
            "prefix": "",
            "suffix": "",
            "nim": "13310049"
        }, {
            "id": "247",
            "name": "Eghan Dharmmabuddhi Pramodana",
            "prefix": "",
            "suffix": "",
            "nim": "13312034"
        }, {
            "id": "248",
            "name": "Egi Baio Kohara",
            "prefix": "",
            "suffix": "",
            "nim": "13712037"
        }, {
            "id": "249",
            "name": "Enggar Nanggalih Esa Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13312016"
        }, {
            "id": "250",
            "name": "Erhan Oressa",
            "prefix": "",
            "suffix": "",
            "nim": "13313060"
        }, {
            "id": "251",
            "name": "Fadhlurrohman",
            "prefix": "",
            "suffix": "",
            "nim": "13314074"
        }, {
            "id": "252",
            "name": "Faizatuzzahrah Rahmaniah",
            "prefix": "",
            "suffix": "",
            "nim": "13314010"
        }, {
            "id": "253",
            "name": "Fakhri Ridho Dhiyamatra",
            "prefix": "",
            "suffix": "",
            "nim": "13313010"
        }, {
            "id": "254",
            "name": "Farah Tsabitha",
            "prefix": "",
            "suffix": "",
            "nim": "13314016"
        }, {
            "id": "255",
            "name": "Fariza Ardelia Alifah",
            "prefix": "",
            "suffix": "",
            "nim": "13313062"
        }, {
            "id": "256",
            "name": "Fathurrahman Feradi",
            "prefix": "",
            "suffix": "",
            "nim": "13313018"
        }, {
            "id": "257",
            "name": "Fauzan Muzaki",
            "prefix": "",
            "suffix": "",
            "nim": "13313020"
        }, {
            "id": "258",
            "name": "Fedriz Revanisa",
            "prefix": "",
            "suffix": "",
            "nim": "13309056"
        }, {
            "id": "259",
            "name": "Fikri Rizky Al Fadhil",
            "prefix": "",
            "suffix": "",
            "nim": "13312045"
        }, {
            "id": "260",
            "name": "Filza Munir",
            "prefix": "",
            "suffix": "",
            "nim": "13312003"
        }, {
            "id": "261",
            "name": "Firman Gusti",
            "prefix": "",
            "suffix": "",
            "nim": "13310052"
        }, {
            "id": "262",
            "name": "Fizri Adiyesa",
            "prefix": "",
            "suffix": "",
            "nim": "13314050"
        }, {
            "id": "263",
            "name": "Fransisca Viviyanti Susastra",
            "prefix": "",
            "suffix": "",
            "nim": "13313045"
        }, {
            "id": "264",
            "name": "Fransiscus Xaverius Johan",
            "prefix": "",
            "suffix": "",
            "nim": "13309005"
        }, {
            "id": "265",
            "name": "Gamal Luckman Sudibya",
            "prefix": "",
            "suffix": "",
            "nim": "13309066"
        }, {
            "id": "266",
            "name": "Gary Sebastian Pangihutan",
            "prefix": "",
            "suffix": "",
            "nim": "13314083"
        }, {
            "id": "267",
            "name": "Gde Bimananda Mahardika Wisna",
            "prefix": "",
            "suffix": "",
            "nim": "13311007"
        }, {
            "id": "268",
            "name": "Gewinner S.C. S. Sinaga",
            "prefix": "",
            "suffix": "",
            "nim": "13313072"
        }, {
            "id": "269",
            "name": "Ghesa Mardhatillah",
            "prefix": "",
            "suffix": "",
            "nim": "13314040"
        }, {
            "id": "270",
            "name": "Gifarie Effendy",
            "prefix": "",
            "suffix": "",
            "nim": "13312011"
        }, {
            "id": "271",
            "name": "Gita Andhika Swastanto",
            "prefix": "",
            "suffix": "",
            "nim": "13310085"
        }, {
            "id": "272",
            "name": "Gradi Desendra",
            "prefix": "",
            "suffix": "",
            "nim": "13313075"
        }, {
            "id": "273",
            "name": "Grandyasa Assami",
            "prefix": "",
            "suffix": "",
            "nim": "13310046"
        }, {
            "id": "274",
            "name": "Greaty Kristanto",
            "prefix": "",
            "suffix": "",
            "nim": "13313028"
        }, {
            "id": "275",
            "name": "Gunachandra",
            "prefix": "",
            "suffix": "",
            "nim": "13310071"
        }, {
            "id": "276",
            "name": "Hafid Sidqi Nur Muzwar",
            "prefix": "",
            "suffix": "",
            "nim": "13310030"
        }, {
            "id": "277",
            "name": "Hafidz Bahtiar",
            "prefix": "",
            "suffix": "",
            "nim": "13311013"
        }, {
            "id": "278",
            "name": "Hakim Ginanjar",
            "prefix": "",
            "suffix": "",
            "nim": "13711040"
        }, {
            "id": "279",
            "name": "Haris Suwignyo",
            "prefix": "",
            "suffix": "",
            "nim": "13312025"
        }, {
            "id": "280",
            "name": "Haritsa Hammami",
            "prefix": "",
            "suffix": "",
            "nim": "13314025"
        }, {
            "id": "281",
            "name": "Hariyadi",
            "prefix": "",
            "suffix": "",
            "nim": "13314035"
        }, {
            "id": "282",
            "name": "Harry Handoko Halim",
            "prefix": "",
            "suffix": "",
            "nim": "13314061"
        }, {
            "id": "283",
            "name": "Helma Devina",
            "prefix": "",
            "suffix": "",
            "nim": "13314038"
        }, {
            "id": "284",
            "name": "Herjuno Rah Nindhito",
            "prefix": "",
            "suffix": "",
            "nim": "13309009"
        }, {
            "id": "285",
            "name": "Hilman Hafizhan",
            "prefix": "",
            "suffix": "",
            "nim": "13313025"
        }, {
            "id": "286",
            "name": "Hilmy Adam Jieta P",
            "prefix": "",
            "suffix": "",
            "nim": "13314067"
        }, {
            "id": "287",
            "name": "Hizkia Natanael",
            "prefix": "",
            "suffix": "",
            "nim": "13312010"
        }, {
            "id": "288",
            "name": "I Komang Ananta Aryadinata",
            "prefix": "",
            "suffix": "",
            "nim": "13313044"
        }, {
            "id": "289",
            "name": "I Wayan Hari Sukmaranu",
            "prefix": "",
            "suffix": "",
            "nim": "13314019"
        }, {
            "id": "290",
            "name": "Ias Azhari Rezkyarno",
            "prefix": "",
            "suffix": "",
            "nim": "13311093"
        }, {
            "id": "291",
            "name": "Ikhya Ulumudin Imam Mujahid",
            "prefix": "",
            "suffix": "",
            "nim": "13310040"
        }, {
            "id": "292",
            "name": "Imra Della Ochtavyani",
            "prefix": "",
            "suffix": "",
            "nim": "13313084"
        }, {
            "id": "293",
            "name": "Indah Yuliriana",
            "prefix": "",
            "suffix": "",
            "nim": "13310070"
        }, {
            "id": "294",
            "name": "Inneke Wulandari Setiadi",
            "prefix": "",
            "suffix": "",
            "nim": "13314009"
        }, {
            "id": "295",
            "name": "Irfan Hanafiah",
            "prefix": "",
            "suffix": "",
            "nim": "13310048"
        }, {
            "id": "296",
            "name": "Irfan Naufaldi",
            "prefix": "",
            "suffix": "",
            "nim": "13712043"
        }, {
            "id": "297",
            "name": "Irham Tanjung",
            "prefix": "",
            "suffix": "",
            "nim": "13312015"
        }, {
            "id": "298",
            "name": "Ismail Faruqi",
            "prefix": "",
            "suffix": "",
            "nim": "13314002"
        }, {
            "id": "299",
            "name": "Isna Rizkydianita Septrima",
            "prefix": "",
            "suffix": "",
            "nim": "10212035"
        }, {
            "id": "300",
            "name": "Isni Rofiani",
            "prefix": "",
            "suffix": "",
            "nim": "13313007"
        }, {
            "id": "301",
            "name": "Iva Rofiatun Nisa Azzahra",
            "prefix": "",
            "suffix": "",
            "nim": "13310014"
        }, {
            "id": "302",
            "name": "Ivan Wirawan",
            "prefix": "",
            "suffix": "",
            "nim": "13313030"
        }, {
            "id": "303",
            "name": "Jaka Kelana Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13313092"
        }, {
            "id": "304",
            "name": "Jeremiah Bintang",
            "prefix": "",
            "suffix": "",
            "nim": "13313003"
        }, {
            "id": "305",
            "name": "Jessica",
            "prefix": "",
            "suffix": "",
            "nim": "13314092"
        }, {
            "id": "306",
            "name": "Jireh Fessenden",
            "prefix": "",
            "suffix": "",
            "nim": "13313065"
        }, {
            "id": "307",
            "name": "Josavin Simanjuntak",
            "prefix": "",
            "suffix": "",
            "nim": "13314020"
        }, {
            "id": "308",
            "name": "Joshua Julian Damanik",
            "prefix": "",
            "suffix": "",
            "nim": "13314004"
        }, {
            "id": "309",
            "name": "Julius",
            "prefix": "",
            "suffix": "",
            "nim": "13312029"
        }, {
            "id": "310",
            "name": "Julya Fransisca",
            "prefix": "",
            "suffix": "",
            "nim": "13313090"
        }, {
            "id": "311",
            "name": "Karima Fadla",
            "prefix": "",
            "suffix": "",
            "nim": "13313067"
        }, {
            "id": "312",
            "name": "Kelvin Valentino",
            "prefix": "",
            "suffix": "",
            "nim": "13312009"
        }, {
            "id": "313",
            "name": "Kesia Minorie",
            "prefix": "",
            "suffix": "",
            "nim": "13313002"
        }, {
            "id": "314",
            "name": "Kevin Pradityo",
            "prefix": "",
            "suffix": "",
            "nim": "13313022"
        }, {
            "id": "315",
            "name": "Khalid Nur Fahman",
            "prefix": "",
            "suffix": "",
            "nim": "13313055"
        }, {
            "id": "316",
            "name": "Kiki Intan Mayangsari",
            "prefix": "",
            "suffix": "",
            "nim": "13311014"
        }, {
            "id": "317",
            "name": "Kinanti Aprilia Kurnia",
            "prefix": "",
            "suffix": "",
            "nim": "13312039"
        }, {
            "id": "318",
            "name": "Laksmana Hanif N",
            "prefix": "",
            "suffix": "",
            "nim": "13310097"
        }, {
            "id": "319",
            "name": "Lathifa Nur Ramdhania",
            "prefix": "",
            "suffix": "",
            "nim": "13310081"
        }, {
            "id": "320",
            "name": "Laudita Natasha Tamrin",
            "prefix": "",
            "suffix": "",
            "nim": "13314069"
        }, {
            "id": "321",
            "name": "Lely Firda Anggraeni",
            "prefix": "",
            "suffix": "",
            "nim": "13314036"
        }, {
            "id": "322",
            "name": "Lucas Elbert Suryana",
            "prefix": "",
            "suffix": "",
            "nim": "13312024"
        }, {
            "id": "323",
            "name": "Luhung Pravubinantaka",
            "prefix": "",
            "suffix": "",
            "nim": "13310092"
        }, {
            "id": "324",
            "name": "Luthfi Kartika Dewi",
            "prefix": "",
            "suffix": "",
            "nim": "13313011"
        }, {
            "id": "325",
            "name": "M Farhan F",
            "prefix": "",
            "suffix": "",
            "nim": "13314011"
        }, {
            "id": "326",
            "name": "M Firdaus Hermansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13712003"
        }, {
            "id": "327",
            "name": "M Ghilman Oemar",
            "prefix": "",
            "suffix": "",
            "nim": "13314053"
        }, {
            "id": "328",
            "name": "M Rifqy Maulana",
            "prefix": "",
            "suffix": "",
            "nim": "13311015"
        }, {
            "id": "329",
            "name": "M Sidik Arsyadi",
            "prefix": "",
            "suffix": "",
            "nim": "13711065"
        }, {
            "id": "330",
            "name": "M Unggul Karami",
            "prefix": "",
            "suffix": "",
            "nim": "13312020"
        }, {
            "id": "331",
            "name": "M. Akmalul Ihsan",
            "prefix": "",
            "suffix": "",
            "nim": "13313052"
        }, {
            "id": "332",
            "name": "Mabelle Budi Sekarwangi",
            "prefix": "",
            "suffix": "",
            "nim": "13314058"
        }, {
            "id": "333",
            "name": "Made Olivia Gravisi Juniari Kaesar",
            "prefix": "",
            "suffix": "",
            "nim": "13313080"
        }, {
            "id": "334",
            "name": "Maesy",
            "prefix": "",
            "suffix": "",
            "nim": "13313047"
        }, {
            "id": "335",
            "name": "Malik Astar AL Kaadzimi",
            "prefix": "",
            "suffix": "",
            "nim": "13314047"
        }, {
            "id": "336",
            "name": "Maristya Rahmadiansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13312038"
        }, {
            "id": "337",
            "name": "Maya Nurlaila Putri",
            "prefix": "",
            "suffix": "",
            "nim": "13712014"
        }, {
            "id": "338",
            "name": "Megan Graciela Nauli",
            "prefix": "",
            "suffix": "",
            "nim": "13313008"
        }, {
            "id": "339",
            "name": "Meike Kania Dewi",
            "prefix": "",
            "suffix": "",
            "nim": "13314043"
        }, {
            "id": "340",
            "name": "Melissa Novembrianty Hermanto",
            "prefix": "",
            "suffix": "",
            "nim": "13313041"
        }, {
            "id": "341",
            "name": "Merwyn Sebastian",
            "prefix": "",
            "suffix": "",
            "nim": "13309048"
        }, {
            "id": "342",
            "name": "Miqdad Hanif",
            "prefix": "",
            "suffix": "",
            "nim": "13314008"
        }, {
            "id": "343",
            "name": "Mochamad Mahendra Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13313053"
        }, {
            "id": "344",
            "name": "Mochamad Rizky Pradana",
            "prefix": "",
            "suffix": "",
            "nim": "13312028"
        }, {
            "id": "345",
            "name": "Moehammad Dzaky Fauzan Maas",
            "prefix": "",
            "suffix": "",
            "nim": "13313086"
        }, {
            "id": "346",
            "name": "Mohamad Hanifan",
            "prefix": "",
            "suffix": "",
            "nim": "13312004"
        }, {
            "id": "347",
            "name": "Mohamad Naufal Nafian",
            "prefix": "",
            "suffix": "",
            "nim": "13313071"
        }, {
            "id": "348",
            "name": "Mohammad Ilham Bayquni",
            "prefix": "",
            "suffix": "",
            "nim": "13312041"
        }, {
            "id": "349",
            "name": "Mohammad Lutfi Arifin H",
            "prefix": "",
            "suffix": "",
            "nim": "13313014"
        }, {
            "id": "350",
            "name": "Muhamad Akbar Dwiyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13313035"
        }, {
            "id": "351",
            "name": "Muhamad Fahruroji",
            "prefix": "",
            "suffix": "",
            "nim": "13310011"
        }, {
            "id": "352",
            "name": "Muhamad Maulanal Haq",
            "prefix": "",
            "suffix": "",
            "nim": "13312005"
        }, {
            "id": "353",
            "name": "Muhammad Afif",
            "prefix": "",
            "suffix": "",
            "nim": "13311085"
        }, {
            "id": "354",
            "name": "Muhammad Ahsanuddin Abdurrohim",
            "prefix": "",
            "suffix": "",
            "nim": "13313091"
        }, {
            "id": "355",
            "name": "Muhammad Akmalul Ihsan",
            "prefix": "",
            "suffix": "",
            "nim": "13313052a"
        }, {
            "id": "356",
            "name": "Muhammad Bagir",
            "prefix": "",
            "suffix": "",
            "nim": "13308015"
        }, {
            "id": "357",
            "name": "Muhammad Brahma Waluya",
            "prefix": "",
            "suffix": "",
            "nim": "13314041"
        }, {
            "id": "358",
            "name": "Muhammad Fadhil Abdulkarim",
            "prefix": "",
            "suffix": "",
            "nim": "13313005"
        }, {
            "id": "359",
            "name": "Muhammad Haekal Budiman",
            "prefix": "",
            "suffix": "",
            "nim": "13313015"
        }, {
            "id": "360",
            "name": "Muhammad Ilham Bayquni",
            "prefix": "",
            "suffix": "",
            "nim": "13312041a"
        }, {
            "id": "361",
            "name": "Muhammad Irfandi",
            "prefix": "",
            "suffix": "",
            "nim": "13313050"
        }, {
            "id": "362",
            "name": "Muhammad Raka Putranda",
            "prefix": "",
            "suffix": "",
            "nim": "13311094"
        }, {
            "id": "363",
            "name": "Muhammad Reza Alfatika",
            "prefix": "",
            "suffix": "",
            "nim": "13309093"
        }, {
            "id": "364",
            "name": "Muhammad Reza Arifandie",
            "prefix": "",
            "suffix": "",
            "nim": "13314065"
        }, {
            "id": "365",
            "name": "Muhammad Reza Firdaus",
            "prefix": "",
            "suffix": "",
            "nim": "13310063"
        }, {
            "id": "366",
            "name": "Muhammad Riandhy Anindika Yudh",
            "prefix": "",
            "suffix": "",
            "nim": "13309075"
        }, {
            "id": "367",
            "name": "Muhammad Rizqi Abdullah",
            "prefix": "",
            "suffix": "",
            "nim": "13313033"
        }, {
            "id": "368",
            "name": "Muhammad Sadeli Amli",
            "prefix": "",
            "suffix": "",
            "nim": "13308072"
        }, {
            "id": "369",
            "name": "Muhammad Syamsul Arifin",
            "prefix": "",
            "suffix": "",
            "nim": "13313037"
        }, {
            "id": "370",
            "name": "Muhammad Unggul Karami",
            "prefix": "",
            "suffix": "",
            "nim": "13312020a"
        }, {
            "id": "371",
            "name": "Muhammad Wahid Adam",
            "prefix": "",
            "suffix": "",
            "nim": "13314060"
        }, {
            "id": "372",
            "name": "Muhammad Zaki",
            "prefix": "",
            "suffix": "",
            "nim": "13314079"
        }, {
            "id": "373",
            "name": "Muhammad Zaky",
            "prefix": "",
            "suffix": "",
            "nim": "13312049"
        }, {
            "id": "374",
            "name": "Muhammad Zulfan Fahreza",
            "prefix": "",
            "suffix": "",
            "nim": "13313006"
        }, {
            "id": "375",
            "name": "Mutia Purnama Batubara",
            "prefix": "",
            "suffix": "",
            "nim": "13309014"
        }, {
            "id": "376",
            "name": "Nabila Lotus Amala",
            "prefix": "",
            "suffix": "",
            "nim": "13314062"
        }, {
            "id": "377",
            "name": "Nadya Febiani",
            "prefix": "",
            "suffix": "",
            "nim": "13313069"
        }, {
            "id": "378",
            "name": "Nahdia Nurul Hikmah",
            "prefix": "",
            "suffix": "",
            "nim": "13313024"
        }, {
            "id": "379",
            "name": "Nanda Widiansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13310100"
        }, {
            "id": "380",
            "name": "Nanda Youleany Dalillya",
            "prefix": "",
            "suffix": "",
            "nim": "13312047"
        }, {
            "id": "381",
            "name": "Nathaniel Chandra Harjanto",
            "prefix": "",
            "suffix": "",
            "nim": "13310083"
        }, {
            "id": "382",
            "name": "Naufal Aulia Aziz",
            "prefix": "",
            "suffix": "",
            "nim": "13313009"
        }, {
            "id": "383",
            "name": "Nida Nurmadi Hamdani",
            "prefix": "",
            "suffix": "",
            "nim": "13313093"
        }, {
            "id": "384",
            "name": "Nina Margareth Theresia",
            "prefix": "",
            "suffix": "",
            "nim": "13310066"
        }, {
            "id": "385",
            "name": "Nina Nugraheni",
            "prefix": "",
            "suffix": "",
            "nim": "13313056"
        }, {
            "id": "386",
            "name": "Nita Tri Nugraheni",
            "prefix": "",
            "suffix": "",
            "nim": "13314028"
        }, {
            "id": "387",
            "name": "Nizar Ghozali",
            "prefix": "",
            "suffix": "",
            "nim": "13313094"
        }, {
            "id": "388",
            "name": "Nova Rani Widyagita Anata",
            "prefix": "",
            "suffix": "",
            "nim": "13313078"
        }, {
            "id": "389",
            "name": "Nur Azimah Salehah",
            "prefix": "",
            "suffix": "",
            "nim": "13312030"
        }, {
            "id": "390",
            "name": "Nur Havid Yulianto",
            "prefix": "",
            "suffix": "",
            "nim": "13310080"
        }, {
            "id": "391",
            "name": "Nur Shadrina Amalina",
            "prefix": "",
            "suffix": "",
            "nim": "13312026"
        }, {
            "id": "392",
            "name": "Nurhuda Sulistiyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13313032"
        }, {
            "id": "393",
            "name": "Nurul Hanifah",
            "prefix": "",
            "suffix": "",
            "nim": "13313077"
        }, {
            "id": "394",
            "name": "Nurul Hidayah",
            "prefix": "",
            "suffix": "",
            "nim": "13313017"
        }, {
            "id": "395",
            "name": "Nurul Mukarromah",
            "prefix": "",
            "suffix": "",
            "nim": "13312014"
        }, {
            "id": "396",
            "name": "Nurul Rahmi Ramadhani",
            "prefix": "",
            "suffix": "",
            "nim": "13313027"
        }, {
            "id": "397",
            "name": "Nuurrisya Artinamuti",
            "prefix": "",
            "suffix": "",
            "nim": "13314029"
        }, {
            "id": "398",
            "name": "Okie Fauzi Rachman",
            "prefix": "",
            "suffix": "",
            "nim": "13311079"
        }, {
            "id": "399",
            "name": "Okmatiar Dasati",
            "prefix": "",
            "suffix": "",
            "nim": "13314024"
        }, {
            "id": "400",
            "name": "Okta Nurwidyas Amalia",
            "prefix": "",
            "suffix": "",
            "nim": "13314045"
        }, {
            "id": "401",
            "name": "Paula Kristi Septiani",
            "prefix": "",
            "suffix": "",
            "nim": "13314087"
        }, {
            "id": "402",
            "name": "Pedrick Pratama",
            "prefix": "",
            "suffix": "",
            "nim": "13312007"
        }, {
            "id": "403",
            "name": "Pierre Jobel",
            "prefix": "",
            "suffix": "",
            "nim": "13314042"
        }, {
            "id": "404",
            "name": "Prakas L S",
            "prefix": "",
            "suffix": "",
            "nim": "13314030"
        }, {
            "id": "405",
            "name": "Pramuditha Utami",
            "prefix": "",
            "suffix": "",
            "nim": "13310060"
        }, {
            "id": "406",
            "name": "Prasetiyo Hadi Purwoko",
            "prefix": "",
            "suffix": "",
            "nim": "13310058"
        }, {
            "id": "407",
            "name": "Putu Handre Kertha Utama",
            "prefix": "",
            "suffix": "",
            "nim": "13314088"
        }, {
            "id": "408",
            "name": "Rachel Sinondang",
            "prefix": "",
            "suffix": "",
            "nim": "10212097"
        }, {
            "id": "409",
            "name": "Rachmat Amandi R",
            "prefix": "",
            "suffix": "",
            "nim": "13314003"
        }, {
            "id": "410",
            "name": "Rahmat Maulana",
            "prefix": "",
            "suffix": "",
            "nim": "13313023"
        }, {
            "id": "411",
            "name": "Raihan Krishna",
            "prefix": "",
            "suffix": "",
            "nim": "13312050"
        }, {
            "id": "412",
            "name": "Ramadhan Akmal Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13309033"
        }, {
            "id": "413",
            "name": "Refaldi Intri Dwi Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13314072"
        }, {
            "id": "414",
            "name": "Rendy Nurmansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13307094"
        }, {
            "id": "415",
            "name": "Reza Dwicahyo",
            "prefix": "",
            "suffix": "",
            "nim": "13314026"
        }, {
            "id": "416",
            "name": "Reza Prasetyo",
            "prefix": "",
            "suffix": "",
            "nim": "13312042"
        }, {
            "id": "417",
            "name": "Rhea Aqmarina",
            "prefix": "",
            "suffix": "",
            "nim": "13310032"
        }, {
            "id": "418",
            "name": "Rhinocho Fathano Ilham",
            "prefix": "",
            "suffix": "",
            "nim": "13310001"
        }, {
            "id": "419",
            "name": "Rialdi Eka Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13313051"
        }, {
            "id": "420",
            "name": "Rian Bernard",
            "prefix": "",
            "suffix": "",
            "nim": "13314084"
        }, {
            "id": "421",
            "name": "Richard Andika",
            "prefix": "",
            "suffix": "",
            "nim": "13312044"
        }, {
            "id": "422",
            "name": "Ridho Kurniawan",
            "prefix": "",
            "suffix": "",
            "nim": "13314001"
        }, {
            "id": "423",
            "name": "Ridlo Iqbal",
            "prefix": "",
            "suffix": "",
            "nim": "13312040a"
        }, {
            "id": "424",
            "name": "Rifqi Faiz Argiansyah",
            "prefix": "",
            "suffix": "",
            "nim": "13313036"
        }, {
            "id": "425",
            "name": "Rifqi Rafifandi",
            "prefix": "",
            "suffix": "",
            "nim": "13313013"
        }, {
            "id": "426",
            "name": "Rilis Eka Perkasa",
            "prefix": "",
            "suffix": "",
            "nim": "13711053"
        }, {
            "id": "427",
            "name": "Rini Nur Fatimah",
            "prefix": "",
            "suffix": "",
            "nim": "13314085"
        }, {
            "id": "428",
            "name": "Riska Susanti",
            "prefix": "",
            "suffix": "",
            "nim": "13310073"
        }, {
            "id": "429",
            "name": "Rita Debora Uli",
            "prefix": "",
            "suffix": "",
            "nim": "13712041"
        }, {
            "id": "430",
            "name": "Ritomi Ardi Kuslanjar",
            "prefix": "",
            "suffix": "",
            "nim": "13314057"
        }, {
            "id": "431",
            "name": "Rival Aji Santosa",
            "prefix": "",
            "suffix": "",
            "nim": "13313029"
        }, {
            "id": "432",
            "name": "Robi Juniardi",
            "prefix": "",
            "suffix": "",
            "nim": "13310038"
        }, {
            "id": "433",
            "name": "Romi Sangaji",
            "prefix": "",
            "suffix": "",
            "nim": "13314023"
        }, {
            "id": "434",
            "name": "Ronald Alyanto Pradana",
            "prefix": "",
            "suffix": "",
            "nim": "13307027"
        }, {
            "id": "435",
            "name": "Rudolf Jason Kwaria",
            "prefix": "",
            "suffix": "",
            "nim": "13310025"
        }, {
            "id": "436",
            "name": "Ryo Christopher B",
            "prefix": "",
            "suffix": "",
            "nim": "13314012"
        }, {
            "id": "437",
            "name": "Said Gegas Syahwani",
            "prefix": "",
            "suffix": "",
            "nim": "13314055"
        }, {
            "id": "438",
            "name": "Saiful Rijal",
            "prefix": "",
            "suffix": "",
            "nim": "13311039"
        }, {
            "id": "439",
            "name": "Sakina Wihantari Sahara Putri",
            "prefix": "",
            "suffix": "",
            "nim": "13712030"
        }, {
            "id": "440",
            "name": "Samuel Grady",
            "prefix": "",
            "suffix": "",
            "nim": "13312031"
        }, {
            "id": "441",
            "name": "Sefina Rahmatunisa",
            "prefix": "",
            "suffix": "",
            "nim": "13313012"
        }, {
            "id": "442",
            "name": "Septian",
            "prefix": "",
            "suffix": "",
            "nim": "13313019a"
        }, {
            "id": "443",
            "name": "Shadiq Hassan Heyder",
            "prefix": "",
            "suffix": "",
            "nim": "13314046"
        }, {
            "id": "444",
            "name": "Sherry Almira Puspita",
            "prefix": "",
            "suffix": "",
            "nim": "13311048"
        }, {
            "id": "445",
            "name": "Shinta Rohmatika Kosmaga",
            "prefix": "",
            "suffix": "",
            "nim": "13310003"
        }, {
            "id": "446",
            "name": "Sigit Prasetyo Winardi",
            "prefix": "",
            "suffix": "",
            "nim": "13711002"
        }, {
            "id": "447",
            "name": "Sigit Yudanto",
            "prefix": "",
            "suffix": "",
            "nim": "13310044"
        }, {
            "id": "448",
            "name": "Sinta",
            "prefix": "",
            "suffix": "",
            "nim": "13313026"
        }, {
            "id": "449",
            "name": "Sri Indah Ihsani",
            "prefix": "",
            "suffix": "",
            "nim": "13310017"
        }, {
            "id": "450",
            "name": "Sri Utami Lestari",
            "prefix": "",
            "suffix": "",
            "nim": "13310061"
        }, {
            "id": "451",
            "name": "Stephani Edwina Lucia",
            "prefix": "",
            "suffix": "",
            "nim": "13314034"
        }, {
            "id": "452",
            "name": "Stephen Adrian B S",
            "prefix": "",
            "suffix": "",
            "nim": "13312013"
        }, {
            "id": "453",
            "name": "Subhan Alimul Haq",
            "prefix": "",
            "suffix": "",
            "nim": "13313059"
        }, {
            "id": "454",
            "name": "Syahidah S M",
            "prefix": "",
            "suffix": "",
            "nim": "13314052"
        }, {
            "id": "455",
            "name": "Syauqi Abdurrahman Abrori",
            "prefix": "",
            "suffix": "",
            "nim": "13313040"
        }, {
            "id": "456",
            "name": "Syifa Mediana Putri Erwiyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13314059"
        }, {
            "id": "457",
            "name": "Tasya Nadya Hartati",
            "prefix": "",
            "suffix": "",
            "nim": "13314037"
        }, {
            "id": "458",
            "name": "Taufik Banu Setyawan",
            "prefix": "",
            "suffix": "",
            "nim": "13313097"
        }, {
            "id": "459",
            "name": "Tb M Yusuf Yuda P",
            "prefix": "",
            "suffix": "",
            "nim": "13311075"
        }, {
            "id": "460",
            "name": "Thomas Febyanto",
            "prefix": "",
            "suffix": "",
            "nim": "13312019"
        }, {
            "id": "461",
            "name": "Titik Dwi Kurniawati",
            "prefix": "",
            "suffix": "",
            "nim": "13310077"
        }, {
            "id": "462",
            "name": "Tommy Adwitiya",
            "prefix": "",
            "suffix": "",
            "nim": "13308037"
        }, {
            "id": "463",
            "name": "Toni Subagja",
            "prefix": "",
            "suffix": "",
            "nim": "13712040"
        }, {
            "id": "464",
            "name": "Trendy Prima Wijaya",
            "prefix": "",
            "suffix": "",
            "nim": "13314027"
        }, {
            "id": "465",
            "name": "Tri Diah Wulandari Siantury",
            "prefix": "",
            "suffix": "",
            "nim": "13314056"
        }, {
            "id": "466",
            "name": "Uswatun Khasanah",
            "prefix": "",
            "suffix": "",
            "nim": "13313064"
        }, {
            "id": "467",
            "name": "Venettia Olga",
            "prefix": "",
            "suffix": "",
            "nim": "13314076"
        }, {
            "id": "468",
            "name": "Vianka Hardi",
            "prefix": "",
            "suffix": "",
            "nim": "13313087"
        }, {
            "id": "469",
            "name": "Victor Sinaga",
            "prefix": "",
            "suffix": "",
            "nim": "13313043"
        }, {
            "id": "470",
            "name": "Virgiawan Listanto Rahagung",
            "prefix": "",
            "suffix": "",
            "nim": "13313079"
        }, {
            "id": "471",
            "name": "Wahyu Abdi Pranata",
            "prefix": "",
            "suffix": "",
            "nim": "13312012"
        }, {
            "id": "472",
            "name": "Wahyu Muqsita Wardana",
            "prefix": "",
            "suffix": "",
            "nim": "13312037"
        }, {
            "id": "473",
            "name": "Wenda Nuridahissan",
            "prefix": "",
            "suffix": "",
            "nim": "13310069"
        }, {
            "id": "474",
            "name": "Werdi Wedana Gunawan",
            "prefix": "",
            "suffix": "",
            "nim": "13313088"
        }, {
            "id": "475",
            "name": "Widya Hastuti",
            "prefix": "",
            "suffix": "",
            "nim": "10212068"
        }, {
            "id": "476",
            "name": "Winda Kirana Marantika",
            "prefix": "",
            "suffix": "",
            "nim": "13314021"
        }, {
            "id": "477",
            "name": "Yeremia Hamonangan",
            "prefix": "",
            "suffix": "",
            "nim": "13308014"
        }, {
            "id": "478",
            "name": "Yogi Aditya Putra",
            "prefix": "",
            "suffix": "",
            "nim": "13311095"
        }, {
            "id": "479",
            "name": "Yogi Andrinianto",
            "prefix": "",
            "suffix": "",
            "nim": "13314078"
        }, {
            "id": "480",
            "name": "Yokanan Wigar Satwika",
            "prefix": "",
            "suffix": "",
            "nim": "13313016"
        }, {
            "id": "481",
            "name": "Yudha Esa Ramadhan",
            "prefix": "",
            "suffix": "",
            "nim": "13310009"
        }, {
            "id": "482",
            "name": "Yudistira AW",
            "prefix": "",
            "suffix": "",
            "nim": "13310022"
        }, {
            "id": "483",
            "name": "Yulia Rahmawati",
            "prefix": "",
            "suffix": "",
            "nim": "13310101"
        }, {
            "id": "484",
            "name": "Yunadi",
            "prefix": "",
            "suffix": "",
            "nim": "13309003"
        }, {
            "id": "485",
            "name": "Yunan Hilmi",
            "prefix": "",
            "suffix": "",
            "nim": "13711021"
        }, {
            "id": "486",
            "name": "Yuyut Rima Jemira",
            "prefix": "",
            "suffix": "",
            "nim": "13309113"
        }, {
            "id": "487",
            "name": "Zahid Abdush Shomad",
            "prefix": "",
            "suffix": "",
            "nim": "13712009"
        }, {
            "id": "488",
            "name": "Zainie Rizal Abdillah",
            "prefix": "",
            "suffix": "",
            "nim": "13314066"
        }, {
            "id": "489",
            "name": "Naftali",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "490",
            "name": "Andika Rizki",
            "prefix": null,
            "suffix": null,
            "nim": "13311043"
        }, {
            "id": "491",
            "name": "Agung Prasdianto",
            "prefix": null,
            "suffix": null,
            "nim": "13311029"
        }, {
            "id": "492",
            "name": "Dhika Permana",
            "prefix": null,
            "suffix": null,
            "nim": "13309111"
        }, {
            "id": "493",
            "name": "Achmad Fauzan",
            "prefix": null,
            "suffix": null,
            "nim": "13311009"
        }, {
            "id": "494",
            "name": "Bill Harison",
            "prefix": null,
            "suffix": null,
            "nim": "13311087"
        }, {
            "id": "495",
            "name": "Yosa Fendra",
            "prefix": null,
            "suffix": null,
            "nim": "13311049"
        }, {
            "id": "496",
            "name": "Amanda Castolina",
            "prefix": null,
            "suffix": null,
            "nim": "13311005"
        }, {
            "id": "497",
            "name": "Niko Eka Putra",
            "prefix": null,
            "suffix": null,
            "nim": "13311045"
        }, {
            "id": "498",
            "name": "Balqis Afifah",
            "prefix": null,
            "suffix": null,
            "nim": "13311061"
        }, {
            "id": "499",
            "name": "Aji Setyawan",
            "prefix": null,
            "suffix": null,
            "nim": "13311011"
        }, {
            "id": "500",
            "name": "Reza Raditya Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "13311051"
        }, {
            "id": "501",
            "name": "Arkanty Septyvergy",
            "prefix": null,
            "suffix": null,
            "nim": "13311055"
        }, {
            "id": "502",
            "name": "Rizky H. Sungguh",
            "prefix": null,
            "suffix": null,
            "nim": "13311067"
        }, {
            "id": "503",
            "name": "Steve",
            "prefix": null,
            "suffix": null,
            "nim": "13311021"
        }, {
            "id": "504",
            "name": "Fathur Ridha",
            "prefix": null,
            "suffix": null,
            "nim": "13311081"
        }, {
            "id": "505",
            "name": "Kevin Bagus Christianto",
            "prefix": null,
            "suffix": null,
            "nim": "13311037"
        }, {
            "id": "506",
            "name": "M. Nazar Rizqi R.",
            "prefix": null,
            "suffix": null,
            "nim": "13310037"
        }, {
            "id": "507",
            "name": "Nurul Fitrianti",
            "prefix": null,
            "suffix": null,
            "nim": "13311071"
        }, {
            "id": "508",
            "name": "Nadia Rizky Vindiazhari",
            "prefix": null,
            "suffix": null,
            "nim": "13311089"
        }, {
            "id": "509",
            "name": "Ika Khoirun Nisa",
            "prefix": null,
            "suffix": null,
            "nim": "13311023"
        }, {
            "id": "510",
            "name": "Hario Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "13311041"
        }, {
            "id": "511",
            "name": "Tyffani Meirnadias",
            "prefix": null,
            "suffix": null,
            "nim": "13311003"
        }, {
            "id": "512",
            "name": "Agnes Farida",
            "prefix": null,
            "suffix": null,
            "nim": "13311097"
        }, {
            "id": "513",
            "name": "Mohammad Arif Shalihuddin",
            "prefix": null,
            "suffix": null,
            "nim": "13311027"
        }, {
            "id": "514",
            "name": "Alfian Fernandhika",
            "prefix": null,
            "suffix": null,
            "nim": "13311099"
        }, {
            "id": "515",
            "name": "Natsir Habibullah",
            "prefix": null,
            "suffix": null,
            "nim": "13311101"
        }, {
            "id": "516",
            "name": "Gitta Arsika Septiani",
            "prefix": null,
            "suffix": null,
            "nim": "13311001"
        }, {
            "id": "517",
            "name": "M. Taufik Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "13311031"
        }, {
            "id": "518",
            "name": "Syarif Hidayatullah",
            "prefix": null,
            "suffix": null,
            "nim": "13311083"
        }, {
            "id": "519",
            "name": "Dilla Annisa",
            "prefix": null,
            "suffix": null,
            "nim": "13311017"
        }, {
            "id": "520",
            "name": "Mohammad Gilang Permana",
            "prefix": null,
            "suffix": null,
            "nim": "13311063"
        }, {
            "id": "521",
            "name": "Ezra Theodorus Tandian",
            "prefix": null,
            "suffix": null,
            "nim": "13311047"
        }, {
            "id": "522",
            "name": "Reza Levi Fauzi",
            "prefix": null,
            "suffix": null,
            "nim": "13311025"
        }, {
            "id": "523",
            "name": "Maria Sri Suratmi",
            "prefix": null,
            "suffix": null,
            "nim": "13311035"
        }, {
            "id": "524",
            "name": "Bayu Pamungkas",
            "prefix": null,
            "suffix": null,
            "nim": "13311077"
        }, {
            "id": "525",
            "name": "Danang Driyartono",
            "prefix": null,
            "suffix": null,
            "nim": "13311059"
        }, {
            "id": "526",
            "name": "Elfi Yulia",
            "prefix": null,
            "suffix": null,
            "nim": "13311091"
        }, {
            "id": "527",
            "name": "Ibnu Widyatmoko ",
            "prefix": null,
            "suffix": null,
            "nim": "13311057"
        }, {
            "id": "528",
            "name": "Muhammad Akbar Arrozak ",
            "prefix": null,
            "suffix": null,
            "nim": "13311073"
        }, {
            "id": "529",
            "name": "Ahmad Fadhil Reviansyah",
            "prefix": null,
            "suffix": null,
            "nim": "13310004"
        }, {
            "id": "530",
            "name": "Ahmad Ibrahim",
            "prefix": null,
            "suffix": null,
            "nim": "13310006"
        }, {
            "id": "531",
            "name": "Nanda Anugrah Zikrullah",
            "prefix": null,
            "suffix": null,
            "nim": "13310010"
        }, {
            "id": "532",
            "name": "Intan Aprilia",
            "prefix": null,
            "suffix": null,
            "nim": "13310012"
        }, {
            "id": "533",
            "name": "Faishal Al Rasyid",
            "prefix": null,
            "suffix": null,
            "nim": "13310019"
        }, {
            "id": "534",
            "name": "Ferdian Hutomo",
            "prefix": null,
            "suffix": null,
            "nim": "13310020"
        }, {
            "id": "535",
            "name": "Lingga Anugrah",
            "prefix": null,
            "suffix": null,
            "nim": "13310036"
        }, {
            "id": "536",
            "name": "Sylvester Chrisander",
            "prefix": null,
            "suffix": null,
            "nim": "13310039"
        }, {
            "id": "537",
            "name": "Kumowarih Trisno Aji",
            "prefix": null,
            "suffix": null,
            "nim": "13310042"
        }, {
            "id": "538",
            "name": "Umar Hanif Ramadhani",
            "prefix": null,
            "suffix": null,
            "nim": "13310043"
        }, {
            "id": "539",
            "name": "Coni Marsha Sofyna",
            "prefix": null,
            "suffix": null,
            "nim": "13310047"
        }, {
            "id": "540",
            "name": "Putu Fedri Krisna Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": "13310054"
        }, {
            "id": "541",
            "name": "Fahim Hadi Maula",
            "prefix": null,
            "suffix": null,
            "nim": "13310059"
        }, {
            "id": "542",
            "name": "William Suriana",
            "prefix": null,
            "suffix": null,
            "nim": "13310062"
        }, {
            "id": "543",
            "name": "Ikhsanudin Amri Prakosa",
            "prefix": null,
            "suffix": null,
            "nim": "13310065"
        }, {
            "id": "544",
            "name": "Nugroho Hari Wibowo",
            "prefix": null,
            "suffix": null,
            "nim": "13310068"
        }, {
            "id": "545",
            "name": "Zeffantri",
            "prefix": null,
            "suffix": null,
            "nim": "13310072"
        }, {
            "id": "546",
            "name": "Gina Fauziah Akasum",
            "prefix": null,
            "suffix": null,
            "nim": "13310084"
        }, {
            "id": "547",
            "name": "Robet Fransiska  M A",
            "prefix": null,
            "suffix": null,
            "nim": "13310088"
        }, {
            "id": "548",
            "name": "Nur Afifah Juneldi",
            "prefix": null,
            "suffix": null,
            "nim": "13310090"
        }, {
            "id": "549",
            "name": "Fadhel Adlansyah",
            "prefix": null,
            "suffix": null,
            "nim": "13310102"
        }, {
            "id": "550",
            "name": "Zalman Fariz",
            "prefix": null,
            "suffix": null,
            "nim": "13310103"
        }, {
            "id": "551",
            "name": "Irval Mayiendra",
            "prefix": null,
            "suffix": null,
            "nim": "13308105"
        }, {
            "id": "552",
            "name": "Alexander Christian Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "13310002"
        }, {
            "id": "553",
            "name": "Stephen Andronicus Yanto",
            "prefix": null,
            "suffix": null,
            "nim": "13310016"
        }, {
            "id": "554",
            "name": "Mohammed Ismail Abdullah P",
            "prefix": null,
            "suffix": null,
            "nim": "13310021"
        }, {
            "id": "555",
            "name": "Iqbal Ansyori",
            "prefix": null,
            "suffix": null,
            "nim": "13310024"
        }, {
            "id": "556",
            "name": "Muhammad Malik Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "13310027"
        }, {
            "id": "557",
            "name": "Krisnina Anggraini",
            "prefix": null,
            "suffix": null,
            "nim": "13310028"
        }, {
            "id": "558",
            "name": "Ganang Bagaswara",
            "prefix": null,
            "suffix": null,
            "nim": "13310029"
        }, {
            "id": "559",
            "name": "Galih Herprasetyo Aji",
            "prefix": null,
            "suffix": null,
            "nim": "13310033"
        }, {
            "id": "560",
            "name": "Adio Perwira",
            "prefix": null,
            "suffix": null,
            "nim": "13310041"
        }, {
            "id": "561",
            "name": "Frederick Adhi Putranto",
            "prefix": null,
            "suffix": null,
            "nim": "13310045"
        }, {
            "id": "562",
            "name": "Amalia Nurkhasanah",
            "prefix": null,
            "suffix": null,
            "nim": "13310051"
        }, {
            "id": "563",
            "name": "Hayyu Widiatma Sakya",
            "prefix": null,
            "suffix": null,
            "nim": "13310057"
        }, {
            "id": "564",
            "name": "Adinda Bunga Juwita",
            "prefix": null,
            "suffix": null,
            "nim": "13310074"
        }, {
            "id": "565",
            "name": "Novianto Nur Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "13310075"
        }, {
            "id": "566",
            "name": "Shawny",
            "prefix": null,
            "suffix": null,
            "nim": "13310076"
        }, {
            "id": "567",
            "name": "Astra Goldie Aulia",
            "prefix": null,
            "suffix": null,
            "nim": "13310078"
        }, {
            "id": "568",
            "name": "Primadhya Arif Ekaputra",
            "prefix": null,
            "suffix": null,
            "nim": "13310079"
        }, {
            "id": "569",
            "name": "Rafael Jeremia Jonathans",
            "prefix": null,
            "suffix": null,
            "nim": "13310082"
        }, {
            "id": "570",
            "name": "William Anthony",
            "prefix": null,
            "suffix": null,
            "nim": "13310086"
        }, {
            "id": "571",
            "name": "Muhammad Fathahillah Zuhri",
            "prefix": null,
            "suffix": null,
            "nim": "13310094"
        }, {
            "id": "572",
            "name": "Rachmat Cahyono Lasama",
            "prefix": null,
            "suffix": null,
            "nim": "13310095"
        }, {
            "id": "573",
            "name": "Evan Wijaya Tanotogono",
            "prefix": null,
            "suffix": null,
            "nim": "13310098"
        }, {
            "id": "574",
            "name": "Gibran Erlangga",
            "prefix": null,
            "suffix": null,
            "nim": "13310099"
        }, {
            "id": "575",
            "name": "Shani Safarah Rohmah",
            "prefix": null,
            "suffix": null,
            "nim": "13310105"
        }, {
            "id": "576",
            "name": "Keisuke Yamada",
            "prefix": null,
            "suffix": null,
            "nim": "73314701"
        }, {
            "id": "577",
            "name": "Supriyanto",
            "prefix": "",
            "suffix": "Ph.D.",
            "nim": null
        }, {
            "id": "578",
            "name": "Endang Yuliastuti",
            "prefix": "Dr. Ir.",
            "suffix": "MT",
            "nim": null
        }, {
            "id": "579",
            "name": "Muhammad Nur Faqih",
            "prefix": null,
            "suffix": null,
            "nim": "13310035"
        }, {
            "id": "580",
            "name": "Ricky Adityanto",
            "prefix": null,
            "suffix": null,
            "nim": "13313063"
        }, {
            "id": "581",
            "name": "Fauza Karomatul Masyhuroh",
            "prefix": null,
            "suffix": null,
            "nim": "13312022"
        }, {
            "id": "582",
            "name": "Kristiawan Ariwibawa",
            "prefix": null,
            "suffix": null,
            "nim": "13310031"
        }, {
            "id": "583",
            "name": "M Irham Hudaya",
            "prefix": null,
            "suffix": null,
            "nim": "13309090"
        }, {
            "id": "584",
            "name": "Victor Rusfandy",
            "prefix": null,
            "suffix": null,
            "nim": "13311096"
        }, {
            "id": "585",
            "name": "Arthur M Tielung",
            "prefix": null,
            "suffix": null,
            "nim": "13011112"
        }, {
            "id": "586",
            "name": "Ismadina",
            "prefix": null,
            "suffix": null,
            "nim": "13311004"
        }, {
            "id": "587",
            "name": "Ratih Rizki Dahlia",
            "prefix": null,
            "suffix": null,
            "nim": "13311006"
        }, {
            "id": "588",
            "name": "Sugeng Santoso",
            "prefix": null,
            "suffix": null,
            "nim": "13311008"
        }, {
            "id": "589",
            "name": "Rivon Tridesman",
            "prefix": null,
            "suffix": null,
            "nim": "13311010"
        }, {
            "id": "590",
            "name": "Gisela Vania Aline",
            "prefix": null,
            "suffix": null,
            "nim": "13311012"
        }, {
            "id": "591",
            "name": "Muhammad Faizal Sofyan",
            "prefix": null,
            "suffix": null,
            "nim": "13311016"
        }, {
            "id": "592",
            "name": "William Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "13311020"
        }, {
            "id": "593",
            "name": "R Oktanio Sitradewa Ettyatno",
            "prefix": null,
            "suffix": null,
            "nim": "13311022"
        }, {
            "id": "594",
            "name": "Rohendy Michael",
            "prefix": null,
            "suffix": null,
            "nim": "13311030"
        }, {
            "id": "595",
            "name": "Esther Kezia",
            "prefix": null,
            "suffix": null,
            "nim": "13311032"
        }, {
            "id": "596",
            "name": "Firda Fitria",
            "prefix": null,
            "suffix": null,
            "nim": "13311038"
        }, {
            "id": "597",
            "name": "Joseph Ramos P D H",
            "prefix": null,
            "suffix": null,
            "nim": "13311042"
        }, {
            "id": "598",
            "name": "Nurvita Aji",
            "prefix": null,
            "suffix": null,
            "nim": "13311064"
        }, {
            "id": "599",
            "name": "Trisfianto Prasetio",
            "prefix": null,
            "suffix": null,
            "nim": "13311068"
        }, {
            "id": "600",
            "name": "Matsani",
            "prefix": null,
            "suffix": null,
            "nim": "13311070"
        }, {
            "id": "601",
            "name": "Deby Suandi",
            "prefix": null,
            "suffix": null,
            "nim": "13311080"
        }, {
            "id": "602",
            "name": "Nur Afifah",
            "prefix": null,
            "suffix": null,
            "nim": "13311024"
        }, {
            "id": "603",
            "name": "Eka Permana Putra",
            "prefix": null,
            "suffix": null,
            "nim": "13311028"
        }, {
            "id": "604",
            "name": "Ivan Stefanus",
            "prefix": null,
            "suffix": null,
            "nim": "13311076"
        }, {
            "id": "605",
            "name": "Dimitri Hans Cahya Kencana",
            "prefix": null,
            "suffix": null,
            "nim": "13309030"
        }, {
            "id": "606",
            "name": "Michael Martin",
            "prefix": null,
            "suffix": null,
            "nim": "13311018"
        }, {
            "id": "607",
            "name": "Anindya Dian Asri",
            "prefix": null,
            "suffix": null,
            "nim": "13311026"
        }, {
            "id": "608",
            "name": "Fadya Syahnariza Nan Bareno",
            "prefix": null,
            "suffix": null,
            "nim": "13311034"
        }, {
            "id": "609",
            "name": "Faris Abdala",
            "prefix": null,
            "suffix": null,
            "nim": "13311036"
        }, {
            "id": "610",
            "name": "Rima Kurnia Putri",
            "prefix": null,
            "suffix": null,
            "nim": "13311040"
        }, {
            "id": "611",
            "name": "Mardliyahtur Rohmah",
            "prefix": null,
            "suffix": null,
            "nim": "13311046"
        }, {
            "id": "612",
            "name": "Adie Tri Hanindriyo",
            "prefix": null,
            "suffix": null,
            "nim": "13311050"
        }, {
            "id": "613",
            "name": "Sofia Tharrannisa",
            "prefix": null,
            "suffix": null,
            "nim": "13311056"
        }, {
            "id": "614",
            "name": "Setu Kurnianto Putra",
            "prefix": null,
            "suffix": null,
            "nim": "13311060"
        }, {
            "id": "615",
            "name": "Rezzal Andryan R A",
            "prefix": null,
            "suffix": null,
            "nim": "13311062"
        }, {
            "id": "616",
            "name": "Muhamad Refky Kamajaya",
            "prefix": null,
            "suffix": null,
            "nim": "13311066"
        }, {
            "id": "617",
            "name": "Nikita Pradnya Paramita S",
            "prefix": null,
            "suffix": null,
            "nim": "13311072"
        }, {
            "id": "618",
            "name": "Siti Aminah Fairuz Annisa",
            "prefix": null,
            "suffix": null,
            "nim": "13311074"
        }, {
            "id": "619",
            "name": "Reynaldo Adinata",
            "prefix": null,
            "suffix": null,
            "nim": "13311078"
        }, {
            "id": "620",
            "name": "Ryan Muhammad Khawarizmi",
            "prefix": null,
            "suffix": null,
            "nim": "13311082"
        }, {
            "id": "621",
            "name": "Jeffry Omega Prima",
            "prefix": null,
            "suffix": null,
            "nim": "13311084"
        }, {
            "id": "622",
            "name": "Kelvin Wongso",
            "prefix": null,
            "suffix": null,
            "nim": "13311086"
        }, {
            "id": "623",
            "name": "Firda Fairuz Zakiah",
            "prefix": null,
            "suffix": null,
            "nim": "13311088"
        }, {
            "id": "624",
            "name": "Desy Nur Anisa",
            "prefix": null,
            "suffix": null,
            "nim": "13311090"
        }, {
            "id": "625",
            "name": "Steven Lukas Goentoro",
            "prefix": null,
            "suffix": null,
            "nim": "13311098"
        }, {
            "id": "626",
            "name": "David Marchio Purba",
            "prefix": null,
            "suffix": null,
            "nim": "13311100"
        }, {
            "id": "627",
            "name": "Luhur Setyo Pambudi",
            "prefix": null,
            "suffix": null,
            "nim": "13309008"
        }, {
            "id": "628",
            "name": "Irfan Julian Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "13309015"
        }, {
            "id": "629",
            "name": "Romi Hardiyansyah",
            "prefix": null,
            "suffix": null,
            "nim": "13309041"
        }, {
            "id": "630",
            "name": "Rinomulat Sembhada",
            "prefix": null,
            "suffix": null,
            "nim": "13309058"
        }, {
            "id": "631",
            "name": "Nur Muhammad Azis",
            "prefix": null,
            "suffix": null,
            "nim": "13309022"
        }, {
            "id": "632",
            "name": "Gunariyo",
            "prefix": null,
            "suffix": null,
            "nim": "13309100"
        }, {
            "id": "633",
            "name": "Anis Saffira W",
            "prefix": null,
            "suffix": null,
            "nim": "13308004"
        }, {
            "id": "634",
            "name": "Yohanes Donni Priyatna",
            "prefix": null,
            "suffix": null,
            "nim": "13308010"
        }, {
            "id": "635",
            "name": "Vina Putri Virgiani",
            "prefix": null,
            "suffix": null,
            "nim": "13309001"
        }, {
            "id": "636",
            "name": "Anandya Putri",
            "prefix": null,
            "suffix": null,
            "nim": "13309006"
        }, {
            "id": "637",
            "name": "Ashri Rahmatia Salma",
            "prefix": null,
            "suffix": null,
            "nim": "13309019"
        }, {
            "id": "638",
            "name": "Mediane Nurul Fuadah",
            "prefix": null,
            "suffix": null,
            "nim": "13309027"
        }, {
            "id": "639",
            "name": "Mita Aprianti",
            "prefix": null,
            "suffix": null,
            "nim": "13309031"
        }, {
            "id": "640",
            "name": "Azwar Fikri Yudhi",
            "prefix": null,
            "suffix": null,
            "nim": "13309045"
        }, {
            "id": "641",
            "name": "M Lutfi Marjan",
            "prefix": null,
            "suffix": null,
            "nim": "13309065"
        }, {
            "id": "642",
            "name": "Embun Marintan",
            "prefix": null,
            "suffix": null,
            "nim": "13309071"
        }, {
            "id": "643",
            "name": "Melati Nurguritno",
            "prefix": null,
            "suffix": null,
            "nim": "13309081"
        }, {
            "id": "644",
            "name": "Arvino Adrian Hadinata",
            "prefix": null,
            "suffix": null,
            "nim": "13309088"
        }, {
            "id": "645",
            "name": "Debby Chairubby Lubis",
            "prefix": null,
            "suffix": null,
            "nim": "13309092"
        }, {
            "id": "646",
            "name": "Adhyatmika Dwi W",
            "prefix": null,
            "suffix": null,
            "nim": "13309101"
        }, {
            "id": "647",
            "name": "Novi Kesumaningtyas",
            "prefix": null,
            "suffix": null,
            "nim": "13309102"
        }, {
            "id": "648",
            "name": "Fadhli Ibnu Qayyim",
            "prefix": null,
            "suffix": null,
            "nim": "13309104"
        }, {
            "id": "649",
            "name": "Irfan Askandari",
            "prefix": null,
            "suffix": null,
            "nim": "13309107"
        }, {
            "id": "650",
            "name": "Abghi Ilman Arif",
            "prefix": null,
            "suffix": null,
            "nim": "13309108"
        }, {
            "id": "651",
            "name": "Catherine Hadibowo",
            "prefix": null,
            "suffix": null,
            "nim": "17310010"
        }, {
            "id": "652",
            "name": "Edirza Ardiansyah Eddin",
            "prefix": null,
            "suffix": null,
            "nim": "13309062"
        }, {
            "id": "653",
            "name": "Rosy Andreas",
            "prefix": null,
            "suffix": null,
            "nim": "12109004"
        }, {
            "id": "654",
            "name": "Ricky Samuel",
            "prefix": null,
            "suffix": null,
            "nim": "12109020"
        }, {
            "id": "655",
            "name": "Ken Indiardi",
            "prefix": null,
            "suffix": null,
            "nim": "12109035"
        }, {
            "id": "656",
            "name": "Alfin Revanov",
            "prefix": null,
            "suffix": null,
            "nim": "12110023"
        }, {
            "id": "657",
            "name": "Edo Syawaludin",
            "prefix": null,
            "suffix": null,
            "nim": "12110027"
        }, {
            "id": "658",
            "name": "Muhammad Yusuf Puji Atmoko",
            "prefix": null,
            "suffix": null,
            "nim": "12110053"
        }, {
            "id": "659",
            "name": "Fahmi Maulana Kamil",
            "prefix": null,
            "suffix": null,
            "nim": "12110055"
        }, {
            "id": "660",
            "name": "Nizam Hawa",
            "prefix": null,
            "suffix": null,
            "nim": "12111008"
        }, {
            "id": "661",
            "name": "Akira Elika",
            "prefix": null,
            "suffix": null,
            "nim": "12111030"
        }, {
            "id": "662",
            "name": "Adi Setiadi",
            "prefix": null,
            "suffix": null,
            "nim": "12111042"
        }, {
            "id": "663",
            "name": "Rachel Lindro",
            "prefix": null,
            "suffix": null,
            "nim": "12111054"
        }, {
            "id": "664",
            "name": "Ainal Muchlis",
            "prefix": null,
            "suffix": null,
            "nim": "12112001"
        }, {
            "id": "665",
            "name": "Simon Ferez Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": "12112002"
        }, {
            "id": "666",
            "name": "Febryan Surya Selsus",
            "prefix": null,
            "suffix": null,
            "nim": "12112003"
        }, {
            "id": "667",
            "name": "Budi Seftafiandra",
            "prefix": null,
            "suffix": null,
            "nim": "12112004"
        }, {
            "id": "668",
            "name": "Rosario Tobias",
            "prefix": null,
            "suffix": null,
            "nim": "12112005"
        }, {
            "id": "669",
            "name": "Rifki Azizar Ilmiadi",
            "prefix": null,
            "suffix": null,
            "nim": "12112006"
        }, {
            "id": "670",
            "name": "Dwi Mukti Yulianto",
            "prefix": null,
            "suffix": null,
            "nim": "12112007"
        }, {
            "id": "671",
            "name": "Johannes Kennedy",
            "prefix": null,
            "suffix": null,
            "nim": "12112008"
        }, {
            "id": "672",
            "name": "Yohanes Tulus Tri Utama",
            "prefix": null,
            "suffix": null,
            "nim": "12112009"
        }, {
            "id": "673",
            "name": "Eko Sunarso",
            "prefix": null,
            "suffix": null,
            "nim": "12112010"
        }, {
            "id": "674",
            "name": "Bayu Mandala Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12112011"
        }, {
            "id": "675",
            "name": "I Made Robertho Hutajulu",
            "prefix": null,
            "suffix": null,
            "nim": "12112012"
        }, {
            "id": "676",
            "name": "Dominicus Vincent",
            "prefix": null,
            "suffix": null,
            "nim": "12112013"
        }, {
            "id": "677",
            "name": "I Made Mahendrayana",
            "prefix": null,
            "suffix": null,
            "nim": "12112014"
        }, {
            "id": "678",
            "name": "Yogi Firmansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12112015"
        }, {
            "id": "679",
            "name": "Rafi Khoery",
            "prefix": null,
            "suffix": null,
            "nim": "12112016"
        }, {
            "id": "680",
            "name": "Muhammad Ridho",
            "prefix": null,
            "suffix": null,
            "nim": "12112017"
        }, {
            "id": "681",
            "name": "Soma Herningtiyas",
            "prefix": null,
            "suffix": null,
            "nim": "12112018"
        }, {
            "id": "682",
            "name": "Yosua Posma Tambunan",
            "prefix": null,
            "suffix": null,
            "nim": "12112019"
        }, {
            "id": "683",
            "name": "Resti Amelia",
            "prefix": null,
            "suffix": null,
            "nim": "12112020"
        }, {
            "id": "684",
            "name": "Komang Yogatama Widhiyanto",
            "prefix": null,
            "suffix": null,
            "nim": "12112021"
        }, {
            "id": "685",
            "name": "Rinaldhi Fauzhi Herlan",
            "prefix": null,
            "suffix": null,
            "nim": "12112022"
        }, {
            "id": "686",
            "name": "Farah Susanti",
            "prefix": null,
            "suffix": null,
            "nim": "12112023"
        }, {
            "id": "687",
            "name": "Habib Mailul Afif",
            "prefix": null,
            "suffix": null,
            "nim": "12112024"
        }, {
            "id": "688",
            "name": "Haidar",
            "prefix": null,
            "suffix": null,
            "nim": "12112025"
        }, {
            "id": "689",
            "name": "Rizky Abdillah Nst",
            "prefix": null,
            "suffix": null,
            "nim": "12112026"
        }, {
            "id": "690",
            "name": "Putu Nayaka Rahadita Diana",
            "prefix": null,
            "suffix": null,
            "nim": "12112027"
        }, {
            "id": "691",
            "name": "Marty Ramadhan",
            "prefix": null,
            "suffix": null,
            "nim": "12112028"
        }, {
            "id": "692",
            "name": "Ahmad Arsyad Bosar Hasibuan",
            "prefix": null,
            "suffix": null,
            "nim": "12112029"
        }, {
            "id": "693",
            "name": "Randy Arion Duke",
            "prefix": null,
            "suffix": null,
            "nim": "12112030"
        }, {
            "id": "694",
            "name": "Iqbal Bramanthya",
            "prefix": null,
            "suffix": null,
            "nim": "12112031"
        }, {
            "id": "695",
            "name": "Benhard Saut Tahi",
            "prefix": null,
            "suffix": null,
            "nim": "12112032"
        }, {
            "id": "696",
            "name": "Siswo Afrianto",
            "prefix": null,
            "suffix": null,
            "nim": "12112033"
        }, {
            "id": "697",
            "name": "Dian Amalia Khusna",
            "prefix": null,
            "suffix": null,
            "nim": "12112034"
        }, {
            "id": "698",
            "name": "Johan Nur Rahman Treeseff",
            "prefix": null,
            "suffix": null,
            "nim": "12112035"
        }, {
            "id": "699",
            "name": "Niko Abednego",
            "prefix": null,
            "suffix": null,
            "nim": "12112036"
        }, {
            "id": "700",
            "name": "Osvaldo Dio Odearma",
            "prefix": null,
            "suffix": null,
            "nim": "12112037"
        }, {
            "id": "701",
            "name": "Diajeng Larasati P",
            "prefix": null,
            "suffix": null,
            "nim": "12112038"
        }, {
            "id": "702",
            "name": "Wiji Astuti",
            "prefix": null,
            "suffix": null,
            "nim": "12112039"
        }, {
            "id": "703",
            "name": "Faisal Dimas Raditya Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12112040"
        }, {
            "id": "704",
            "name": "Muhamad Lucky Fernando",
            "prefix": null,
            "suffix": null,
            "nim": "12112041"
        }, {
            "id": "705",
            "name": "Ahmad Zaenal",
            "prefix": null,
            "suffix": null,
            "nim": "12112042"
        }, {
            "id": "706",
            "name": "Zulis Septian",
            "prefix": null,
            "suffix": null,
            "nim": "12112043"
        }, {
            "id": "707",
            "name": "Bagus Satya",
            "prefix": null,
            "suffix": null,
            "nim": "12112044"
        }, {
            "id": "708",
            "name": "Dani Rahmawati",
            "prefix": null,
            "suffix": null,
            "nim": "12112045"
        }, {
            "id": "709",
            "name": "Niken Safira",
            "prefix": null,
            "suffix": null,
            "nim": "12112046"
        }, {
            "id": "710",
            "name": "Samuel",
            "prefix": null,
            "suffix": null,
            "nim": "12112047"
        }, {
            "id": "711",
            "name": "Rahmi Putri Utami",
            "prefix": null,
            "suffix": null,
            "nim": "12112048"
        }, {
            "id": "712",
            "name": "Hendra Handriatno Kabau",
            "prefix": null,
            "suffix": null,
            "nim": "12112049"
        }, {
            "id": "713",
            "name": "Fran Sanjaya Lumbangaol",
            "prefix": null,
            "suffix": null,
            "nim": "12112050"
        }, {
            "id": "714",
            "name": "Dian Setiawan Damanik",
            "prefix": null,
            "suffix": null,
            "nim": "12112051"
        }, {
            "id": "715",
            "name": "Anzhari Wadiyan",
            "prefix": null,
            "suffix": null,
            "nim": "12112052"
        }, {
            "id": "716",
            "name": "Arif Yurahman",
            "prefix": null,
            "suffix": null,
            "nim": "12112053"
        }, {
            "id": "717",
            "name": "Lewi Nisi Simamora",
            "prefix": null,
            "suffix": null,
            "nim": "12112054"
        }, {
            "id": "718",
            "name": "Merdyanto",
            "prefix": null,
            "suffix": null,
            "nim": "12112055"
        }, {
            "id": "719",
            "name": "Muhammad Irham Rahadhian",
            "prefix": null,
            "suffix": null,
            "nim": "12112056"
        }, {
            "id": "720",
            "name": "Muhamad Aditya Abdurachman",
            "prefix": null,
            "suffix": null,
            "nim": "12112057"
        }, {
            "id": "721",
            "name": "Hifdzul Fikri",
            "prefix": null,
            "suffix": null,
            "nim": "12112058"
        }, {
            "id": "722",
            "name": "Khairul Afandi",
            "prefix": null,
            "suffix": null,
            "nim": "12112059"
        }, {
            "id": "723",
            "name": "Qodri Hadi Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12112060"
        }, {
            "id": "724",
            "name": "Bany Adam",
            "prefix": null,
            "suffix": null,
            "nim": "12112061"
        }, {
            "id": "725",
            "name": "Arruya Ashadiqa",
            "prefix": null,
            "suffix": null,
            "nim": "12112062"
        }, {
            "id": "726",
            "name": "Ervan Balie",
            "prefix": null,
            "suffix": null,
            "nim": "12112063"
        }, {
            "id": "727",
            "name": "Adhytia Rian Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12112064"
        }, {
            "id": "728",
            "name": "Astri Muhammad Reza",
            "prefix": null,
            "suffix": null,
            "nim": "12112065"
        }, {
            "id": "729",
            "name": "Muhammad Faisal Rahman",
            "prefix": null,
            "suffix": null,
            "nim": "12112066"
        }, {
            "id": "730",
            "name": "Novi Tri Yana",
            "prefix": null,
            "suffix": null,
            "nim": "12112067"
        }, {
            "id": "731",
            "name": "Valdo T Olivert Butar-butar",
            "prefix": null,
            "suffix": null,
            "nim": "12112068"
        }, {
            "id": "732",
            "name": "Pandu Zea Ardiansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12112069"
        }, {
            "id": "733",
            "name": "Fitri Rahmadhani",
            "prefix": null,
            "suffix": null,
            "nim": "12112070"
        }, {
            "id": "734",
            "name": "Qodri Nur Fasholi",
            "prefix": null,
            "suffix": null,
            "nim": "12112071"
        }, {
            "id": "735",
            "name": "Benny Sumaryono",
            "prefix": null,
            "suffix": null,
            "nim": "12112072"
        }, {
            "id": "736",
            "name": "Dwi Syafiar Bayu Aji",
            "prefix": null,
            "suffix": null,
            "nim": "12112073"
        }, {
            "id": "737",
            "name": "Morgan M Brilliant Simanjuntak",
            "prefix": null,
            "suffix": null,
            "nim": "12112074"
        }, {
            "id": "738",
            "name": "Gifari Nitya Mukhlis",
            "prefix": null,
            "suffix": null,
            "nim": "12112075"
        }, {
            "id": "739",
            "name": "Irham Mochamad Hambali",
            "prefix": null,
            "suffix": null,
            "nim": "12112076"
        }, {
            "id": "740",
            "name": "Elvin Luvian",
            "prefix": null,
            "suffix": null,
            "nim": "12112077"
        }, {
            "id": "741",
            "name": "Hilda Meistra Sari Naibaho",
            "prefix": null,
            "suffix": null,
            "nim": "12112078"
        }, {
            "id": "742",
            "name": "Rezza Danidiputra",
            "prefix": null,
            "suffix": null,
            "nim": "12112079"
        }, {
            "id": "743",
            "name": "Adhitya Barkah Arvi",
            "prefix": null,
            "suffix": null,
            "nim": "12112080"
        }, {
            "id": "744",
            "name": "Ayu Kusuma Hastuti",
            "prefix": null,
            "suffix": null,
            "nim": "12112081"
        }, {
            "id": "745",
            "name": "Rahmat Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "12112082"
        }, {
            "id": "746",
            "name": "Hermas Puntodewo",
            "prefix": null,
            "suffix": null,
            "nim": "12112083"
        }, {
            "id": "747",
            "name": "Syachrial",
            "prefix": null,
            "suffix": null,
            "nim": "12112084"
        }, {
            "id": "748",
            "name": "Ari Trigar Timotius Hutahaean",
            "prefix": null,
            "suffix": null,
            "nim": "12112085"
        }, {
            "id": "749",
            "name": "Slavito Shan",
            "prefix": null,
            "suffix": null,
            "nim": "12112086"
        }, {
            "id": "750",
            "name": "Trie Prasetyo W",
            "prefix": null,
            "suffix": null,
            "nim": "12112087"
        }, {
            "id": "751",
            "name": "Tiara Andrianie Putri",
            "prefix": null,
            "suffix": null,
            "nim": "12112088"
        }, {
            "id": "752",
            "name": "R Bg Arwin N E",
            "prefix": null,
            "suffix": null,
            "nim": "12112089"
        }, {
            "id": "753",
            "name": "Hizha Gashira W",
            "prefix": null,
            "suffix": null,
            "nim": "12112090"
        }, {
            "id": "754",
            "name": "Bambang Jaya Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": "12112091"
        }, {
            "id": "755",
            "name": "Robbyanto",
            "prefix": null,
            "suffix": null,
            "nim": "12112092"
        }, {
            "id": "756",
            "name": "Luiz David Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": "12112093"
        }, {
            "id": "757",
            "name": "Dimas M A A",
            "prefix": null,
            "suffix": null,
            "nim": "12112094"
        }, {
            "id": "758",
            "name": "Rizky Hartawan",
            "prefix": null,
            "suffix": null,
            "nim": "12112095"
        }, {
            "id": "759",
            "name": "Rahmat Putra Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": "12112096"
        }, {
            "id": "760",
            "name": "Ahnaf Muhammad",
            "prefix": null,
            "suffix": null,
            "nim": "12112097"
        }, {
            "id": "761",
            "name": "Muhammad Iqbal",
            "prefix": null,
            "suffix": null,
            "nim": "12112098"
        }, {
            "id": "762",
            "name": "Rd Muh Rayhan Rafi Fauzan",
            "prefix": null,
            "suffix": null,
            "nim": "12112099"
        }, {
            "id": "763",
            "name": "Saeful Aziz",
            "prefix": null,
            "suffix": null,
            "nim": "12112100"
        }, {
            "id": "764",
            "name": "Baldus Ch. F. Ambrauw",
            "prefix": null,
            "suffix": null,
            "nim": "12113603"
        }, {
            "id": "765",
            "name": "Andrew F K",
            "prefix": null,
            "suffix": null,
            "nim": "12113604"
        }, {
            "id": "766",
            "name": "Oktavianus Max Irimi",
            "prefix": null,
            "suffix": null,
            "nim": "12113606"
        }, {
            "id": "767",
            "name": "Orlando Fonataba",
            "prefix": null,
            "suffix": null,
            "nim": "12113618"
        }, {
            "id": "768",
            "name": "Fandhia F.",
            "prefix": null,
            "suffix": null,
            "nim": "12109036"
        }, {
            "id": "769",
            "name": "M. Khalid Al-Hijri",
            "prefix": null,
            "suffix": null,
            "nim": "12110004"
        }, {
            "id": "770",
            "name": "Nugroho Nur F",
            "prefix": null,
            "suffix": null,
            "nim": "12110014"
        }, {
            "id": "771",
            "name": "Joshi Andhika Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12111013"
        }, {
            "id": "772",
            "name": "Ivan Tejantara",
            "prefix": null,
            "suffix": null,
            "nim": "12111039"
        }, {
            "id": "773",
            "name": "Raynardus Malvin Lymena",
            "prefix": null,
            "suffix": null,
            "nim": "12113001"
        }, {
            "id": "774",
            "name": "Hermansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12113002"
        }, {
            "id": "775",
            "name": "Fidelis Elizabet",
            "prefix": null,
            "suffix": null,
            "nim": "12113003"
        }, {
            "id": "776",
            "name": "Niki Rahma Rizkita",
            "prefix": null,
            "suffix": null,
            "nim": "12113004"
        }, {
            "id": "777",
            "name": "Yusef Pany",
            "prefix": null,
            "suffix": null,
            "nim": "12113005"
        }, {
            "id": "778",
            "name": "Kristian Edwin Salamba",
            "prefix": null,
            "suffix": null,
            "nim": "12113006"
        }, {
            "id": "779",
            "name": "Faiz Yudia Abyan",
            "prefix": null,
            "suffix": null,
            "nim": "12113007"
        }, {
            "id": "780",
            "name": "Rabbi Pandu Angkasa",
            "prefix": null,
            "suffix": null,
            "nim": "12113008"
        }, {
            "id": "781",
            "name": "Halim Fauzan Edher",
            "prefix": null,
            "suffix": null,
            "nim": "12113009"
        }, {
            "id": "782",
            "name": "Rafid Arsalan Permana",
            "prefix": null,
            "suffix": null,
            "nim": "12113010"
        }, {
            "id": "783",
            "name": "Muhammad Naufal Farhandio",
            "prefix": null,
            "suffix": null,
            "nim": "12113011"
        }, {
            "id": "784",
            "name": "Kevin Silvanus",
            "prefix": null,
            "suffix": null,
            "nim": "12113012"
        }, {
            "id": "785",
            "name": "Christian Natanael",
            "prefix": null,
            "suffix": null,
            "nim": "12113013"
        }, {
            "id": "786",
            "name": "Harun Nuruddin Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "12113014"
        }, {
            "id": "787",
            "name": "Harristio Adam",
            "prefix": null,
            "suffix": null,
            "nim": "12113015"
        }, {
            "id": "788",
            "name": "Putra Hanif Agson Gani",
            "prefix": null,
            "suffix": null,
            "nim": "12113016"
        }, {
            "id": "789",
            "name": "I K Dwika Paramananda",
            "prefix": null,
            "suffix": null,
            "nim": "12113017"
        }, {
            "id": "790",
            "name": "Destian Tony",
            "prefix": null,
            "suffix": null,
            "nim": "12113020"
        }, {
            "id": "791",
            "name": "Hizkia Christian",
            "prefix": null,
            "suffix": null,
            "nim": "12113021"
        }, {
            "id": "792",
            "name": "Sukuanto Tanoto",
            "prefix": null,
            "suffix": null,
            "nim": "12113022"
        }, {
            "id": "793",
            "name": "Putri Aprillia",
            "prefix": null,
            "suffix": null,
            "nim": "12113023"
        }, {
            "id": "794",
            "name": "Bayu Ajie Arrasyid",
            "prefix": null,
            "suffix": null,
            "nim": "12113024"
        }, {
            "id": "795",
            "name": "Muhammad Hanif Tri Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "12113025"
        }, {
            "id": "796",
            "name": "Najib Mahwan Najahah",
            "prefix": null,
            "suffix": null,
            "nim": "12113026"
        }, {
            "id": "797",
            "name": "Timothy Pardomuan Karre",
            "prefix": null,
            "suffix": null,
            "nim": "12113027"
        }, {
            "id": "798",
            "name": "Probo Wicaksono",
            "prefix": null,
            "suffix": null,
            "nim": "12113028"
        }, {
            "id": "799",
            "name": "Jaya Yosnanda",
            "prefix": null,
            "suffix": null,
            "nim": "12113029"
        }, {
            "id": "800",
            "name": "Steven Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "12113030"
        }, {
            "id": "801",
            "name": "Ardhi Rasy Wardhana",
            "prefix": null,
            "suffix": null,
            "nim": "12113031"
        }, {
            "id": "802",
            "name": "Ahmad Dzulfikar A",
            "prefix": null,
            "suffix": null,
            "nim": "12113033"
        }, {
            "id": "803",
            "name": "Mochamad Dinar",
            "prefix": null,
            "suffix": null,
            "nim": "12113034"
        }, {
            "id": "804",
            "name": "Sundharsen Liunardi",
            "prefix": null,
            "suffix": null,
            "nim": "12113035"
        }, {
            "id": "805",
            "name": "Rahmat Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "12113036"
        }, {
            "id": "806",
            "name": "Murni Naomi Hotmauli",
            "prefix": null,
            "suffix": null,
            "nim": "12113037"
        }, {
            "id": "807",
            "name": "Alrifki Arsa Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12113039"
        }, {
            "id": "808",
            "name": "Muhammad Alim",
            "prefix": null,
            "suffix": null,
            "nim": "12113040"
        }, {
            "id": "809",
            "name": "Feri Sandria",
            "prefix": null,
            "suffix": null,
            "nim": "12113041"
        }, {
            "id": "810",
            "name": "Achmad Juanzah",
            "prefix": null,
            "suffix": null,
            "nim": "12113043"
        }, {
            "id": "811",
            "name": "Farah Rizka Rahmatia",
            "prefix": null,
            "suffix": null,
            "nim": "12113044"
        }, {
            "id": "812",
            "name": "Gagah Arofat",
            "prefix": null,
            "suffix": null,
            "nim": "12113045"
        }, {
            "id": "813",
            "name": "Erika",
            "prefix": null,
            "suffix": null,
            "nim": "12113046"
        }, {
            "id": "814",
            "name": "Sobit Apriliana",
            "prefix": null,
            "suffix": null,
            "nim": "12113047"
        }, {
            "id": "815",
            "name": "Marshel Emmanuelle",
            "prefix": null,
            "suffix": null,
            "nim": "12113048"
        }, {
            "id": "816",
            "name": "Zulnio Rizqy Hafizh B",
            "prefix": null,
            "suffix": null,
            "nim": "12113049"
        }, {
            "id": "817",
            "name": "Achmad Ridho Hartono",
            "prefix": null,
            "suffix": null,
            "nim": "12113051"
        }, {
            "id": "818",
            "name": "Abed Nego Simamora",
            "prefix": null,
            "suffix": null,
            "nim": "12113052"
        }, {
            "id": "819",
            "name": "Karisma Adi Bawono Putro",
            "prefix": null,
            "suffix": null,
            "nim": "12113053"
        }, {
            "id": "820",
            "name": "Bima Saddha Prabawa",
            "prefix": null,
            "suffix": null,
            "nim": "12113054"
        }, {
            "id": "821",
            "name": "Nanda Andika Sembiring",
            "prefix": null,
            "suffix": null,
            "nim": "12113055"
        }, {
            "id": "822",
            "name": "Prayoga Wirya Alamsyah",
            "prefix": null,
            "suffix": null,
            "nim": "12113056"
        }, {
            "id": "823",
            "name": "Muhammad Sofyan",
            "prefix": null,
            "suffix": null,
            "nim": "12113057"
        }, {
            "id": "824",
            "name": "Dimas Alfiandri",
            "prefix": null,
            "suffix": null,
            "nim": "12113058"
        }, {
            "id": "825",
            "name": "Fathur Rozaq",
            "prefix": null,
            "suffix": null,
            "nim": "12113059"
        }, {
            "id": "826",
            "name": "Agung Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "12113060"
        }, {
            "id": "827",
            "name": "Rahadian Muslim",
            "prefix": null,
            "suffix": null,
            "nim": "12113061"
        }, {
            "id": "828",
            "name": "Jumadi",
            "prefix": null,
            "suffix": null,
            "nim": "12113062"
        }, {
            "id": "829",
            "name": "Dian Aris Sandi",
            "prefix": null,
            "suffix": null,
            "nim": "12113063"
        }, {
            "id": "830",
            "name": "Muhammad Taher S",
            "prefix": null,
            "suffix": null,
            "nim": "12113064"
        }, {
            "id": "831",
            "name": "Ridwan Syarif",
            "prefix": null,
            "suffix": null,
            "nim": "12113066"
        }, {
            "id": "832",
            "name": "Miqdam Furqany",
            "prefix": null,
            "suffix": null,
            "nim": "12113067"
        }, {
            "id": "833",
            "name": "Afdhalulhaq Sholihul Hadi",
            "prefix": null,
            "suffix": null,
            "nim": "12113068"
        }, {
            "id": "834",
            "name": "Samuelson Putra Sukarto Surbak",
            "prefix": null,
            "suffix": null,
            "nim": "12113070"
        }, {
            "id": "835",
            "name": "Viola Putri",
            "prefix": null,
            "suffix": null,
            "nim": "12113071"
        }, {
            "id": "836",
            "name": "Fajar Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "12113072"
        }, {
            "id": "837",
            "name": "Faisal Haris",
            "prefix": null,
            "suffix": null,
            "nim": "12113073"
        }, {
            "id": "838",
            "name": "Mirza Dian Rifaldi",
            "prefix": null,
            "suffix": null,
            "nim": "12113074"
        }, {
            "id": "839",
            "name": "Dwi Tama Nurcahya",
            "prefix": null,
            "suffix": null,
            "nim": "12113075"
        }, {
            "id": "840",
            "name": "Raymon Yohanes",
            "prefix": null,
            "suffix": null,
            "nim": "12113076"
        }, {
            "id": "841",
            "name": "Heru Anggara",
            "prefix": null,
            "suffix": null,
            "nim": "12113077"
        }, {
            "id": "842",
            "name": "Jovian Augustine Purawijaya",
            "prefix": null,
            "suffix": null,
            "nim": "12113078"
        }, {
            "id": "843",
            "name": "Fina Fitriana R",
            "prefix": null,
            "suffix": null,
            "nim": "12113079"
        }, {
            "id": "844",
            "name": "Kreshna Damar Segoro",
            "prefix": null,
            "suffix": null,
            "nim": "12113080"
        }, {
            "id": "845",
            "name": "Linda Permata",
            "prefix": null,
            "suffix": null,
            "nim": "12113081"
        }, {
            "id": "846",
            "name": "Ayu Bedtrik",
            "prefix": null,
            "suffix": null,
            "nim": "12113082"
        }, {
            "id": "847",
            "name": "Maulana Haqiqi",
            "prefix": null,
            "suffix": null,
            "nim": "12113083"
        }, {
            "id": "848",
            "name": "Pandu Radina Wibisono",
            "prefix": null,
            "suffix": null,
            "nim": "12113084"
        }, {
            "id": "849",
            "name": "Isti Chairianti Ahmad",
            "prefix": null,
            "suffix": null,
            "nim": "12113085"
        }, {
            "id": "850",
            "name": "Yoga Wishnu Pradana",
            "prefix": null,
            "suffix": null,
            "nim": "12113086"
        }, {
            "id": "851",
            "name": "Iqbal Ramanda Marli",
            "prefix": null,
            "suffix": null,
            "nim": "12113087"
        }, {
            "id": "852",
            "name": "Onria Muklis",
            "prefix": null,
            "suffix": null,
            "nim": "12113089"
        }, {
            "id": "853",
            "name": "Egar Agifatwa",
            "prefix": null,
            "suffix": null,
            "nim": "12113091"
        }, {
            "id": "854",
            "name": "Kevin Satrio Adiguna",
            "prefix": null,
            "suffix": null,
            "nim": "12113092"
        }, {
            "id": "855",
            "name": "Ridho Ilham Mury Nanda",
            "prefix": null,
            "suffix": null,
            "nim": "12113093"
        }, {
            "id": "856",
            "name": "Ghea Tiarasani Sondakh",
            "prefix": null,
            "suffix": null,
            "nim": "12113094"
        }, {
            "id": "857",
            "name": "Eksan Yuliyanto",
            "prefix": null,
            "suffix": null,
            "nim": "12113095"
        }, {
            "id": "858",
            "name": "Nindya Adita Sari",
            "prefix": null,
            "suffix": null,
            "nim": "12113096"
        }, {
            "id": "859",
            "name": "Muhammad Husni Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "12111001"
        }, {
            "id": "860",
            "name": "Yogie Reza Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12111002"
        }, {
            "id": "861",
            "name": "Hafidha Dwi Putri Aristien",
            "prefix": null,
            "suffix": null,
            "nim": "12111003"
        }, {
            "id": "862",
            "name": "Friska Martha",
            "prefix": null,
            "suffix": null,
            "nim": "12111004"
        }, {
            "id": "863",
            "name": "M Fachrizal Batubara",
            "prefix": null,
            "suffix": null,
            "nim": "12111005"
        }, {
            "id": "864",
            "name": "Nita Arsita Dewi",
            "prefix": null,
            "suffix": null,
            "nim": "12111006"
        }, {
            "id": "865",
            "name": "Gumilang Oktantyo Purbanagara",
            "prefix": null,
            "suffix": null,
            "nim": "12111007"
        }, {
            "id": "866",
            "name": "Jeihan Vanetharian",
            "prefix": null,
            "suffix": null,
            "nim": "12111009"
        }, {
            "id": "867",
            "name": "Tommy Setiawan Permadi",
            "prefix": null,
            "suffix": null,
            "nim": "12111010"
        }, {
            "id": "868",
            "name": "Elizzah Rahmatika",
            "prefix": null,
            "suffix": null,
            "nim": "12111011"
        }, {
            "id": "869",
            "name": "Muhammad Aditya Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "12111014"
        }, {
            "id": "870",
            "name": "Fedo Wilianto",
            "prefix": null,
            "suffix": null,
            "nim": "12111015"
        }, {
            "id": "871",
            "name": "Fitri Endro Prabowo",
            "prefix": null,
            "suffix": null,
            "nim": "12111017"
        }, {
            "id": "872",
            "name": "Franki Tinambunan",
            "prefix": null,
            "suffix": null,
            "nim": "12111018"
        }, {
            "id": "873",
            "name": "Dadang Kurnia",
            "prefix": null,
            "suffix": null,
            "nim": "12111019"
        }, {
            "id": "874",
            "name": "Okky Wicaksana Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12111020"
        }, {
            "id": "875",
            "name": "Dhia Huda Dhaifullah",
            "prefix": null,
            "suffix": null,
            "nim": "12111021"
        }, {
            "id": "876",
            "name": "Joel Goklasmartua Napitupulu",
            "prefix": null,
            "suffix": null,
            "nim": "12111022"
        }, {
            "id": "877",
            "name": "Khodijah Nurul Inayah",
            "prefix": null,
            "suffix": null,
            "nim": "12111023"
        }, {
            "id": "878",
            "name": "Briliant Prasetyo",
            "prefix": null,
            "suffix": null,
            "nim": "12111025"
        }, {
            "id": "879",
            "name": "Mohammad Rahman Ardhiansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12111026"
        }, {
            "id": "880",
            "name": "Kevin",
            "prefix": null,
            "suffix": null,
            "nim": "12111027"
        }, {
            "id": "881",
            "name": "Krisna Darmawan",
            "prefix": null,
            "suffix": null,
            "nim": "12111028"
        }, {
            "id": "882",
            "name": "Bhima",
            "prefix": null,
            "suffix": null,
            "nim": "12111029"
        }, {
            "id": "883",
            "name": "Anti Dwi Putri",
            "prefix": null,
            "suffix": null,
            "nim": "12111031"
        }, {
            "id": "884",
            "name": "Reziksufi Naszazuli",
            "prefix": null,
            "suffix": null,
            "nim": "12111032"
        }, {
            "id": "885",
            "name": "Aprida Suryana",
            "prefix": null,
            "suffix": null,
            "nim": "12111033"
        }, {
            "id": "886",
            "name": "Anthony",
            "prefix": null,
            "suffix": null,
            "nim": "12111034"
        }, {
            "id": "887",
            "name": "Danang Ardianto",
            "prefix": null,
            "suffix": null,
            "nim": "12111036"
        }, {
            "id": "888",
            "name": "Silvia Rahmawati",
            "prefix": null,
            "suffix": null,
            "nim": "12111037"
        }, {
            "id": "889",
            "name": "Siti Arna Arifah",
            "prefix": null,
            "suffix": null,
            "nim": "12111038"
        }, {
            "id": "890",
            "name": "Galih Pertiwi",
            "prefix": null,
            "suffix": null,
            "nim": "12111040"
        }, {
            "id": "891",
            "name": "Yessi Destianty",
            "prefix": null,
            "suffix": null,
            "nim": "12111041"
        }, {
            "id": "892",
            "name": "Albertus Dhisa Yoga Pradana",
            "prefix": null,
            "suffix": null,
            "nim": "12111043"
        }, {
            "id": "893",
            "name": "Adam Verdyansyah Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12111044"
        }, {
            "id": "894",
            "name": "Mochamad Rilga Syafidra",
            "prefix": null,
            "suffix": null,
            "nim": "12111045"
        }, {
            "id": "895",
            "name": "Iftikhor Faadhilah",
            "prefix": null,
            "suffix": null,
            "nim": "12111046"
        }, {
            "id": "896",
            "name": "Fauzan Yudho Pratomo",
            "prefix": null,
            "suffix": null,
            "nim": "12111047"
        }, {
            "id": "897",
            "name": "Ogi Bagja Waluya",
            "prefix": null,
            "suffix": null,
            "nim": "12111048"
        }, {
            "id": "898",
            "name": "Annisa Renaningtyas Tadiyana",
            "prefix": null,
            "suffix": null,
            "nim": "12111049"
        }, {
            "id": "899",
            "name": "Reza Amandra",
            "prefix": null,
            "suffix": null,
            "nim": "12111050"
        }, {
            "id": "900",
            "name": "Eric Widagda Palguna Gde",
            "prefix": null,
            "suffix": null,
            "nim": "12111052"
        }, {
            "id": "901",
            "name": "Randy Yoan Eksakta",
            "prefix": null,
            "suffix": null,
            "nim": "12111053"
        }, {
            "id": "902",
            "name": "Kurnia Candra Utama",
            "prefix": null,
            "suffix": null,
            "nim": "12111055"
        }, {
            "id": "903",
            "name": "Dahlan Iskan",
            "prefix": null,
            "suffix": null,
            "nim": "12111056"
        }, {
            "id": "904",
            "name": "Jamilla Mahadika Praconda",
            "prefix": null,
            "suffix": null,
            "nim": "12111057"
        }, {
            "id": "905",
            "name": "C Jimmy Chandra",
            "prefix": null,
            "suffix": null,
            "nim": "12111058"
        }, {
            "id": "906",
            "name": "Keisara Maudy Namina",
            "prefix": null,
            "suffix": null,
            "nim": "12111059"
        }, {
            "id": "907",
            "name": "Febrian Nugraha Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12111060"
        }, {
            "id": "908",
            "name": "Mary Agustina Putri",
            "prefix": null,
            "suffix": null,
            "nim": "12111061"
        }, {
            "id": "909",
            "name": "Cynthia",
            "prefix": null,
            "suffix": null,
            "nim": "12111062"
        }, {
            "id": "910",
            "name": "Dian Lazuardy B A",
            "prefix": null,
            "suffix": null,
            "nim": "12111063"
        }, {
            "id": "911",
            "name": "Tika Novira Tobing",
            "prefix": null,
            "suffix": null,
            "nim": "12111064"
        }, {
            "id": "912",
            "name": "Doandy Yonathan Wibisono",
            "prefix": null,
            "suffix": null,
            "nim": "12111065"
        }, {
            "id": "913",
            "name": "Randy Burhan Sanjaya",
            "prefix": null,
            "suffix": null,
            "nim": "12111066"
        }, {
            "id": "914",
            "name": "Bernando Parasian",
            "prefix": null,
            "suffix": null,
            "nim": "12111067"
        }, {
            "id": "915",
            "name": "Nirmana Fiqra Qaidahiyani",
            "prefix": null,
            "suffix": null,
            "nim": "12111068"
        }, {
            "id": "916",
            "name": "Alfonso Ceisar Krisman",
            "prefix": null,
            "suffix": null,
            "nim": "12111069"
        }, {
            "id": "917",
            "name": "Muamar Imam Fiqri",
            "prefix": null,
            "suffix": null,
            "nim": "12111070"
        }, {
            "id": "919",
            "name": "Hendy Farazi",
            "prefix": null,
            "suffix": null,
            "nim": "12109068"
        }, {
            "id": "920",
            "name": "Michael Sijabat",
            "prefix": null,
            "suffix": null,
            "nim": "12108056"
        }, {
            "id": "921",
            "name": "Nico Satrio Yudhanto",
            "prefix": null,
            "suffix": null,
            "nim": "12109013"
        }, {
            "id": "922",
            "name": "Romualdus Gervasius Sianturi",
            "prefix": null,
            "suffix": null,
            "nim": "12109018"
        }, {
            "id": "923",
            "name": "Barkah Tutuko Aji",
            "prefix": null,
            "suffix": null,
            "nim": "12109019"
        }, {
            "id": "924",
            "name": "Meiliza Fitri",
            "prefix": null,
            "suffix": null,
            "nim": "12109038"
        }, {
            "id": "925",
            "name": "Rivan Nur Aziz Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12109045"
        }, {
            "id": "926",
            "name": "Daniel Septian Morangin",
            "prefix": null,
            "suffix": null,
            "nim": "12109050"
        }, {
            "id": "927",
            "name": "Jackie",
            "prefix": null,
            "suffix": null,
            "nim": "12110001"
        }, {
            "id": "928",
            "name": "Zaenal Arifin",
            "prefix": null,
            "suffix": null,
            "nim": "12110002"
        }, {
            "id": "929",
            "name": "Indarto",
            "prefix": null,
            "suffix": null,
            "nim": "12110003"
        }, {
            "id": "930",
            "name": "Rudilanov Ahwan",
            "prefix": null,
            "suffix": null,
            "nim": "12110005"
        }, {
            "id": "931",
            "name": "Muhammad Afif Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "12110006"
        }, {
            "id": "932",
            "name": "Dede Wahyudin",
            "prefix": null,
            "suffix": null,
            "nim": "12110007"
        }, {
            "id": "933",
            "name": "Marthin W Silaban",
            "prefix": null,
            "suffix": null,
            "nim": "12110008"
        }, {
            "id": "934",
            "name": "Bagus Setiaji D",
            "prefix": null,
            "suffix": null,
            "nim": "12110009"
        }, {
            "id": "935",
            "name": "Alfonsus L Wahyu W",
            "prefix": null,
            "suffix": null,
            "nim": "12110010"
        }, {
            "id": "936",
            "name": "Denny Reza Kamarullah",
            "prefix": null,
            "suffix": null,
            "nim": "12110011"
        }, {
            "id": "937",
            "name": "Yosua Lukita",
            "prefix": null,
            "suffix": null,
            "nim": "12110012"
        }, {
            "id": "938",
            "name": "Dimas Agung Permadi",
            "prefix": null,
            "suffix": null,
            "nim": "12110013"
        }, {
            "id": "939",
            "name": "Abdillah",
            "prefix": null,
            "suffix": null,
            "nim": "12110015"
        }, {
            "id": "940",
            "name": "Muhammad Ihsan",
            "prefix": null,
            "suffix": null,
            "nim": "12110016"
        }, {
            "id": "941",
            "name": "Sapto Aji Pamungkas",
            "prefix": null,
            "suffix": null,
            "nim": "12110017"
        }, {
            "id": "942",
            "name": "Amiril Pratomo",
            "prefix": null,
            "suffix": null,
            "nim": "12110018"
        }, {
            "id": "943",
            "name": "Rizki Riadi Perdana",
            "prefix": null,
            "suffix": null,
            "nim": "12110019"
        }, {
            "id": "944",
            "name": "Bagas Irawandy Trihatmojo",
            "prefix": null,
            "suffix": null,
            "nim": "12110020"
        }, {
            "id": "945",
            "name": "Pakerti Lutzow Anjani",
            "prefix": null,
            "suffix": null,
            "nim": "12110021"
        }, {
            "id": "946",
            "name": "Fadhlan Adit Rachmawan",
            "prefix": null,
            "suffix": null,
            "nim": "12110022"
        }, {
            "id": "947",
            "name": "Pandu Prasetyo Adji Winamo ",
            "prefix": null,
            "suffix": null,
            "nim": "12110024"
        }, {
            "id": "948",
            "name": "Faisal Assidik ",
            "prefix": null,
            "suffix": null,
            "nim": "12110025"
        }, {
            "id": "949",
            "name": "Kamige Sulistyo",
            "prefix": null,
            "suffix": null,
            "nim": "12110026"
        }, {
            "id": "950",
            "name": "Andrew Kumala",
            "prefix": null,
            "suffix": null,
            "nim": "12110028"
        }, {
            "id": "951",
            "name": "Ahmad Ashari",
            "prefix": null,
            "suffix": null,
            "nim": "12110029"
        }, {
            "id": "952",
            "name": "Azharul Dwi Kumiawan ",
            "prefix": null,
            "suffix": null,
            "nim": "12110030"
        }, {
            "id": "953",
            "name": "Cecep Pumama Sidiq",
            "prefix": null,
            "suffix": null,
            "nim": "12110031"
        }, {
            "id": "954",
            "name": "David Ryan Hasiholan Purba",
            "prefix": null,
            "suffix": null,
            "nim": "12110033"
        }, {
            "id": "955",
            "name": "Ronald Widjojo",
            "prefix": null,
            "suffix": null,
            "nim": "12110034"
        }, {
            "id": "956",
            "name": "Luthfi Kamil",
            "prefix": null,
            "suffix": null,
            "nim": "12110035"
        }, {
            "id": "957",
            "name": "Fuad Riza Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "12110036"
        }, {
            "id": "958",
            "name": "Latif Muhammad Badra",
            "prefix": null,
            "suffix": null,
            "nim": "12110037"
        }, {
            "id": "959",
            "name": "Bagus Maheswara ",
            "prefix": null,
            "suffix": null,
            "nim": "12110038"
        }, {
            "id": "960",
            "name": "Aditiawarman",
            "prefix": null,
            "suffix": null,
            "nim": "12110039"
        }, {
            "id": "961",
            "name": "Ryan Paranindya P",
            "prefix": null,
            "suffix": null,
            "nim": "12110040"
        }, {
            "id": "962",
            "name": "Raisa Zahrah",
            "prefix": null,
            "suffix": null,
            "nim": "12110041"
        }, {
            "id": "963",
            "name": "Andre S Ginting",
            "prefix": null,
            "suffix": null,
            "nim": "12110042"
        }, {
            "id": "964",
            "name": "Dhenny Ajie W",
            "prefix": null,
            "suffix": null,
            "nim": "12110043"
        }, {
            "id": "965",
            "name": "Jayawijaya Parulian Nababan",
            "prefix": null,
            "suffix": null,
            "nim": "12110044"
        }, {
            "id": "966",
            "name": "Rizcha Sri Wibisono",
            "prefix": null,
            "suffix": null,
            "nim": "12110045"
        }, {
            "id": "967",
            "name": "David Parningotan Manurung",
            "prefix": null,
            "suffix": null,
            "nim": "12110046"
        }, {
            "id": "968",
            "name": "Samuel Billy S S",
            "prefix": null,
            "suffix": null,
            "nim": "12110047"
        }, {
            "id": "969",
            "name": "Halumi Nur Khamidah",
            "prefix": null,
            "suffix": null,
            "nim": "12110048"
        }, {
            "id": "970",
            "name": "Stefanus Jagad Gineung P",
            "prefix": null,
            "suffix": null,
            "nim": "12110049"
        }, {
            "id": "971",
            "name": "Arini Nisa Armenda",
            "prefix": null,
            "suffix": null,
            "nim": "12110050"
        }, {
            "id": "972",
            "name": "I Dewa Gede Oka Raghunatha",
            "prefix": null,
            "suffix": null,
            "nim": "12110051"
        }, {
            "id": "973",
            "name": "Abraham Mahesa Prana",
            "prefix": null,
            "suffix": null,
            "nim": "12110052"
        }, {
            "id": "974",
            "name": "Gigih Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "12110054"
        }, {
            "id": "975",
            "name": "Rendy Priana",
            "prefix": null,
            "suffix": null,
            "nim": "12110056"
        }, {
            "id": "976",
            "name": "Timotius Denis",
            "prefix": null,
            "suffix": null,
            "nim": "12110057"
        }, {
            "id": "977",
            "name": "Fira Resti Anindita",
            "prefix": null,
            "suffix": null,
            "nim": "12110058"
        }, {
            "id": "978",
            "name": "Hanif Ikhsan Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12110059"
        }, {
            "id": "979",
            "name": "Maulana Nata Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "12110060"
        }, {
            "id": "980",
            "name": "Adeodatus Palmadite",
            "prefix": null,
            "suffix": null,
            "nim": "12110061"
        }, {
            "id": "981",
            "name": "Herman F Sipayung",
            "prefix": null,
            "suffix": null,
            "nim": "12110062"
        }, {
            "id": "982",
            "name": "Yonatan A W",
            "prefix": null,
            "suffix": null,
            "nim": "12110063"
        }, {
            "id": "983",
            "name": "Evi Erwinton Simbolon",
            "prefix": null,
            "suffix": null,
            "nim": "12110064"
        }, {
            "id": "984",
            "name": "M Ihsan Novandika",
            "prefix": null,
            "suffix": null,
            "nim": "12110065"
        }, {
            "id": "985",
            "name": "Utomo Priyambodo",
            "prefix": null,
            "suffix": null,
            "nim": "12110066"
        }, {
            "id": "986",
            "name": "Dori Apriman Duha",
            "prefix": null,
            "suffix": null,
            "nim": "12110067"
        }, {
            "id": "987",
            "name": "Rahmat Septian Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12110068"
        }, {
            "id": "988",
            "name": "Wendy",
            "prefix": null,
            "suffix": null,
            "nim": "12110069"
        }, {
            "id": "989",
            "name": "Arief Cahyadi",
            "prefix": null,
            "suffix": null,
            "nim": "12110070"
        }, {
            "id": "990",
            "name": "Alex Samberi",
            "prefix": null,
            "suffix": null,
            "nim": "12113601"
        }, {
            "id": "991",
            "name": "Fedy Maniambo",
            "prefix": null,
            "suffix": null,
            "nim": "12113602"
        }, {
            "id": "992",
            "name": "Frans W. Krey",
            "prefix": null,
            "suffix": null,
            "nim": "12113605"
        }, {
            "id": "993",
            "name": "AdminKL",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "994",
            "name": "AdminSI",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "995",
            "name": "AdminTM",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "996",
            "name": "AdminMRI",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "997",
            "name": "AdminTMI",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "998",
            "name": "AdminTI",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "999",
            "name": "Marihot P. Siburian",
            "prefix": null,
            "suffix": null,
            "nim": "12109023"
        }, {
            "id": "1000",
            "name": "Nabyl An Najmy",
            "prefix": null,
            "suffix": null,
            "nim": "12110032"
        }, {
            "id": "1001",
            "name": "Adelia Kusuma Ayu",
            "prefix": null,
            "suffix": null,
            "nim": "12011002"
        }, {
            "id": "1002",
            "name": "Regina C Sekar Arum",
            "prefix": null,
            "suffix": null,
            "nim": "12011019"
        }, {
            "id": "1003",
            "name": "Abdullah Husna",
            "prefix": null,
            "suffix": null,
            "nim": "12011027"
        }, {
            "id": "1004",
            "name": "Niken Laras Shinta",
            "prefix": null,
            "suffix": null,
            "nim": "12011041"
        }, {
            "id": "1005",
            "name": "Lucy Kartikasari",
            "prefix": null,
            "suffix": null,
            "nim": "12011078"
        }, {
            "id": "1006",
            "name": "Ken Indiardi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1007",
            "name": "Daniel Septian Morangin",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1008",
            "name": "Hendy Farazi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1009",
            "name": "Agus Hendro",
            "prefix": null,
            "suffix": null,
            "nim": "12912017"
        }, {
            "id": "1010",
            "name": "Mirza Azmi",
            "prefix": null,
            "suffix": null,
            "nim": "12012018"
        }, {
            "id": "1011",
            "name": "Agung Cipta Putra,Agung Cipta Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12012067"
        }, {
            "id": "1012",
            "name": "Joshi Andika Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12111 013"
        }, {
            "id": "1013",
            "name": "R Malvin Lymena",
            "prefix": null,
            "suffix": null,
            "nim": "12113 001"
        }, {
            "id": "1014",
            "name": "Hermansyah",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1015",
            "name": "Kristian E Salamba",
            "prefix": null,
            "suffix": null,
            "nim": "12113 006"
        }, {
            "id": "1016",
            "name": "Harun N Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "12113 014"
        }, {
            "id": "1017",
            "name": "Hizkia Chistian",
            "prefix": null,
            "suffix": null,
            "nim": "12113 021"
        }, {
            "id": "1018",
            "name": "Putri Aprilia",
            "prefix": null,
            "suffix": null,
            "nim": "12113 023"
        }, {
            "id": "1019",
            "name": "Bayu Ajie Arrasyid",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1020",
            "name": "Najib M Najahah",
            "prefix": null,
            "suffix": null,
            "nim": "12113 026"
        }, {
            "id": "1021",
            "name": "Steven Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1022",
            "name": "Ahmad4 Dzulfikar A",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1023",
            "name": "Muhammad Alim",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1024",
            "name": "Farah Rizka Rahmatia",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1025",
            "name": "Gagah Arofat",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1026",
            "name": "Erika",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1027",
            "name": "Sobit Apriliana",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1028",
            "name": "Marshel Emmanuelle",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1029",
            "name": "Achmad3 Ridho Hartono",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1030",
            "name": "Abed Nego Simamora",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1031",
            "name": "Muhammad2 Sofyan",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1032",
            "name": "Dimas Alfiandri",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1033",
            "name": "Dian Aris Sandi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1034",
            "name": "Miqdam Furqany",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1035",
            "name": "Mirza Dian Rifaldi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1036",
            "name": "Dwi2 Tama Nurcahya",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1037",
            "name": "Fina Fitriana R",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1038",
            "name": "Linda Permata",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1039",
            "name": "Ayu2 Bedtrik",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1040",
            "name": "Maulana Haqiqi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1041",
            "name": "Iqbal Ramanda Marli",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1042",
            "name": "Onria Muklis",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1043",
            "name": "Kevin5 Satrio Adiguna",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1044",
            "name": "Eksan Yuliyanto",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1045",
            "name": "M Okky Anriansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12109062"
        }, {
            "id": "1046",
            "name": "Dita Nur Hanifah",
            "prefix": null,
            "suffix": null,
            "nim": "12012004"
        }, {
            "id": "1047",
            "name": "M. Taufik Abdullah",
            "prefix": null,
            "suffix": null,
            "nim": "12012013"
        }, {
            "id": "1048",
            "name": "Tabegra Disando",
            "prefix": null,
            "suffix": null,
            "nim": "12012035"
        }, {
            "id": "1049",
            "name": "Irfan Adriansyah N.",
            "prefix": null,
            "suffix": null,
            "nim": "12012045"
        }, {
            "id": "1050",
            "name": "Chandra Dewi",
            "prefix": null,
            "suffix": null,
            "nim": "12012049"
        }, {
            "id": "1051",
            "name": "Fachri Muhammad",
            "prefix": null,
            "suffix": null,
            "nim": "12012051"
        }, {
            "id": "1052",
            "name": "Firdaus Abdullah Rosyid",
            "prefix": null,
            "suffix": null,
            "nim": "12012053"
        }, {
            "id": "1053",
            "name": "Muchamad Ramses",
            "prefix": null,
            "suffix": null,
            "nim": "12012076"
        }, {
            "id": "1054",
            "name": "Bagus Prakosa Widyowaskito",
            "prefix": null,
            "suffix": null,
            "nim": "12113032"
        }, {
            "id": "1055",
            "name": "Rahmat Yusuf Lubis",
            "prefix": null,
            "suffix": null,
            "nim": "12113038"
        }, {
            "id": "1056",
            "name": "Fit Rahadian Syah Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": "12113042"
        }, {
            "id": "1057",
            "name": "Kristianus",
            "prefix": null,
            "suffix": null,
            "nim": "12113090"
        }, {
            "id": "1058",
            "name": "Melysa Yuni Sari Pane",
            "prefix": null,
            "suffix": null,
            "nim": "12114001"
        }, {
            "id": "1059",
            "name": "Mario Hartanto Tanda Reja",
            "prefix": null,
            "suffix": null,
            "nim": "12114002"
        }, {
            "id": "1060",
            "name": "Muhamad Alief Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "12114003"
        }, {
            "id": "1061",
            "name": "Putu Sandra Iswaramba",
            "prefix": null,
            "suffix": null,
            "nim": "12114004"
        }, {
            "id": "1062",
            "name": "Putri Sanjiwani Dewi",
            "prefix": null,
            "suffix": null,
            "nim": "12114005"
        }, {
            "id": "1063",
            "name": "Andhika Indra S",
            "prefix": null,
            "suffix": null,
            "nim": "12114006"
        }, {
            "id": "1064",
            "name": "Nurfahmi Yusuf H P",
            "prefix": null,
            "suffix": null,
            "nim": "12114007"
        }, {
            "id": "1065",
            "name": "Pandu Kharisma",
            "prefix": null,
            "suffix": null,
            "nim": "12114008"
        }, {
            "id": "1066",
            "name": "Fakhri Ihsan Fadilah",
            "prefix": null,
            "suffix": null,
            "nim": "12114009"
        }, {
            "id": "1067",
            "name": "Annisa Naila Dianiputri",
            "prefix": null,
            "suffix": null,
            "nim": "12114010"
        }, {
            "id": "1068",
            "name": "Rizaldy Hanafie Mauludy",
            "prefix": null,
            "suffix": null,
            "nim": "12114011"
        }, {
            "id": "1069",
            "name": "Farhan Muhammad D",
            "prefix": null,
            "suffix": null,
            "nim": "12114012"
        }, {
            "id": "1070",
            "name": "Maria Pradnya Paramita",
            "prefix": null,
            "suffix": null,
            "nim": "12114013"
        }, {
            "id": "1071",
            "name": "Idham Muhammad Fachri",
            "prefix": null,
            "suffix": null,
            "nim": "12114014"
        }, {
            "id": "1072",
            "name": "Patrick",
            "prefix": null,
            "suffix": null,
            "nim": "12114015"
        }, {
            "id": "1073",
            "name": "Muslim Syamsul Bahri",
            "prefix": null,
            "suffix": null,
            "nim": "12114016"
        }, {
            "id": "1074",
            "name": "Raihan Muflihhamim",
            "prefix": null,
            "suffix": null,
            "nim": "12114017"
        }, {
            "id": "1075",
            "name": "Ahmad Barra Vicra",
            "prefix": null,
            "suffix": null,
            "nim": "12114018"
        }, {
            "id": "1076",
            "name": "Muhammad Syukri",
            "prefix": null,
            "suffix": null,
            "nim": "12114019"
        }, {
            "id": "1077",
            "name": "Addien Wisnu Harnoko",
            "prefix": null,
            "suffix": null,
            "nim": "12114020"
        }, {
            "id": "1078",
            "name": "Muhammad Alen Irnawan B",
            "prefix": null,
            "suffix": null,
            "nim": "12114021"
        }, {
            "id": "1079",
            "name": "Calvin Leonard",
            "prefix": null,
            "suffix": null,
            "nim": "12114022"
        }, {
            "id": "1080",
            "name": "Fernando Agusto",
            "prefix": null,
            "suffix": null,
            "nim": "12114023"
        }, {
            "id": "1081",
            "name": "Muhammad Riski Afdol",
            "prefix": null,
            "suffix": null,
            "nim": "12114024"
        }, {
            "id": "1082",
            "name": "Aisyah Setia Wardani",
            "prefix": null,
            "suffix": null,
            "nim": "12114025"
        }, {
            "id": "1083",
            "name": "Rofikul Umam,Rofikul Umam",
            "prefix": null,
            "suffix": null,
            "nim": "12114026"
        }, {
            "id": "1084",
            "name": "Hazmi Andreanur,Hazmi Andreanur",
            "prefix": null,
            "suffix": null,
            "nim": "12114027"
        }, {
            "id": "1085",
            "name": "Boas Suhat",
            "prefix": null,
            "suffix": null,
            "nim": "12114028"
        }, {
            "id": "1086",
            "name": "Nabila Khairunnisa Ansory",
            "prefix": null,
            "suffix": null,
            "nim": "12114029"
        }, {
            "id": "1087",
            "name": "Ardhan Lazuardi F",
            "prefix": null,
            "suffix": null,
            "nim": "12114030"
        }, {
            "id": "1088",
            "name": "Olga Padmasari Anggraini",
            "prefix": null,
            "suffix": null,
            "nim": "12114031"
        }, {
            "id": "1089",
            "name": "Wahyuni Sri Lestari",
            "prefix": null,
            "suffix": null,
            "nim": "12114032"
        }, {
            "id": "1090",
            "name": "Alan Gassadesna Arisandi",
            "prefix": null,
            "suffix": null,
            "nim": "12114033"
        }, {
            "id": "1091",
            "name": "M Farhan Muzzammil",
            "prefix": null,
            "suffix": null,
            "nim": "12114034"
        }, {
            "id": "1092",
            "name": "Dhea Andhia Putrie Suwandi",
            "prefix": null,
            "suffix": null,
            "nim": "12114035"
        }, {
            "id": "1093",
            "name": "Agit Nuraeni",
            "prefix": null,
            "suffix": null,
            "nim": "12114036"
        }, {
            "id": "1094",
            "name": "Monika Atmajaya",
            "prefix": null,
            "suffix": null,
            "nim": "12114037"
        }, {
            "id": "1095",
            "name": "Sadam Saputra",
            "prefix": null,
            "suffix": null,
            "nim": "12114038"
        }, {
            "id": "1096",
            "name": "Ryan Yoga Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16414322"
        }, {
            "id": "1097",
            "name": "Adita Thifal Rahardini",
            "prefix": null,
            "suffix": null,
            "nim": "12114040"
        }, {
            "id": "1098",
            "name": "Hafizh Fauzan",
            "prefix": null,
            "suffix": null,
            "nim": "12114041"
        }, {
            "id": "1099",
            "name": "Rifqy Adwin Hafizsyah",
            "prefix": null,
            "suffix": null,
            "nim": "12114042"
        }, {
            "id": "1100",
            "name": "Erdian Christmas Ginting",
            "prefix": null,
            "suffix": null,
            "nim": "12114043"
        }, {
            "id": "1101",
            "name": "Kevin Kashikoi Naiborhu",
            "prefix": null,
            "suffix": null,
            "nim": "12114044"
        }, {
            "id": "1102",
            "name": "Julia Justina",
            "prefix": null,
            "suffix": null,
            "nim": "12114045"
        }, {
            "id": "1103",
            "name": "Harry Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": "12114046"
        }, {
            "id": "1104",
            "name": "Abdul Aziz",
            "prefix": null,
            "suffix": null,
            "nim": "12114047"
        }, {
            "id": "1105",
            "name": "Karina Purwitasari",
            "prefix": null,
            "suffix": null,
            "nim": "12114048"
        }, {
            "id": "1106",
            "name": "Ahmad Wali Radhi",
            "prefix": null,
            "suffix": null,
            "nim": "12114049"
        }, {
            "id": "1107",
            "name": "Chandra Riady",
            "prefix": null,
            "suffix": null,
            "nim": "12114050"
        }, {
            "id": "1108",
            "name": "Muhammad Naufal Fakhriyanto",
            "prefix": null,
            "suffix": null,
            "nim": "12114051"
        }, {
            "id": "1109",
            "name": "Darius Armando S",
            "prefix": null,
            "suffix": null,
            "nim": "12114052"
        }, {
            "id": "1110",
            "name": "Abdul Havidz",
            "prefix": null,
            "suffix": null,
            "nim": "12114053"
        }, {
            "id": "1111",
            "name": "Agus Purwanto",
            "prefix": null,
            "suffix": null,
            "nim": "12114054"
        }, {
            "id": "1112",
            "name": "Muh.wardiman.m",
            "prefix": null,
            "suffix": null,
            "nim": "12114055"
        }, {
            "id": "1113",
            "name": "Myeshia Faraz Hamzah",
            "prefix": null,
            "suffix": null,
            "nim": "12114056"
        }, {
            "id": "1114",
            "name": "Shobiya Bima Yurdhika",
            "prefix": null,
            "suffix": null,
            "nim": "12114057"
        }, {
            "id": "1115",
            "name": "Trisna Insani",
            "prefix": null,
            "suffix": null,
            "nim": "12114058"
        }, {
            "id": "1116",
            "name": "Dek Lanang Sanjivani",
            "prefix": null,
            "suffix": null,
            "nim": "12114059"
        }, {
            "id": "1117",
            "name": "Helen Yolanda Indriyani Ompusu",
            "prefix": null,
            "suffix": null,
            "nim": "12114060"
        }, {
            "id": "1118",
            "name": "Muhammad Archico Nar",
            "prefix": null,
            "suffix": null,
            "nim": "12114061"
        }, {
            "id": "1119",
            "name": "Riza Fathul Rahmi",
            "prefix": null,
            "suffix": null,
            "nim": "12114062"
        }, {
            "id": "1120",
            "name": "Andrew Harryanto Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": "12114063"
        }, {
            "id": "1121",
            "name": "Christ E W Pantow",
            "prefix": null,
            "suffix": null,
            "nim": "12114064"
        }, {
            "id": "1122",
            "name": "Jihan Syifa Hanunah",
            "prefix": null,
            "suffix": null,
            "nim": "12114065"
        }, {
            "id": "1123",
            "name": "Ahmad Noufal S",
            "prefix": null,
            "suffix": null,
            "nim": "12114066"
        }, {
            "id": "1124",
            "name": "M Dandy Resafahlevi Nasution",
            "prefix": null,
            "suffix": null,
            "nim": "12114067"
        }, {
            "id": "1125",
            "name": "K L Herdayatamma",
            "prefix": null,
            "suffix": null,
            "nim": "12114068"
        }, {
            "id": "1126",
            "name": "Anggraini Widiya Astuti",
            "prefix": null,
            "suffix": null,
            "nim": "12114069"
        }, {
            "id": "1127",
            "name": "Rafierdy Muhammad",
            "prefix": null,
            "suffix": null,
            "nim": "12114070"
        }, {
            "id": "1128",
            "name": "Agna Magistra",
            "prefix": null,
            "suffix": null,
            "nim": "12114071"
        }, {
            "id": "1129",
            "name": "Christomy",
            "prefix": null,
            "suffix": null,
            "nim": "12114072"
        }, {
            "id": "1130",
            "name": "Gianluca Abdillah Desrian",
            "prefix": null,
            "suffix": null,
            "nim": "12114073"
        }, {
            "id": "1131",
            "name": "Faisal Abdurahman",
            "prefix": null,
            "suffix": null,
            "nim": "12114074"
        }, {
            "id": "1132",
            "name": "Dendy Reza Fahlevi",
            "prefix": null,
            "suffix": null,
            "nim": "12114075"
        }, {
            "id": "1133",
            "name": "Nur Alim Kadir",
            "prefix": null,
            "suffix": null,
            "nim": "12114076"
        }, {
            "id": "1134",
            "name": "Yanuar Ayu Widowati",
            "prefix": null,
            "suffix": null,
            "nim": "12114077"
        }, {
            "id": "1135",
            "name": "Ucik Devi Mirnawati",
            "prefix": null,
            "suffix": null,
            "nim": "12114078"
        }, {
            "id": "1136",
            "name": "Ahmad Naufal Faiz",
            "prefix": null,
            "suffix": null,
            "nim": "12114079"
        }, {
            "id": "1137",
            "name": "Abi Ilmi",
            "prefix": null,
            "suffix": null,
            "nim": "12114080"
        }, {
            "id": "1138",
            "name": "Aji Widakdo Wicaksono",
            "prefix": null,
            "suffix": null,
            "nim": "12114081"
        }, {
            "id": "1139",
            "name": "Firas Ala Zagarino",
            "prefix": null,
            "suffix": null,
            "nim": "12114082"
        }, {
            "id": "1140",
            "name": "Satria Yudhistira",
            "prefix": null,
            "suffix": null,
            "nim": "12114083"
        }, {
            "id": "1141",
            "name": "Tri Tomi Yulio",
            "prefix": null,
            "suffix": null,
            "nim": "12114084"
        }, {
            "id": "1142",
            "name": "Raden Muhammad Imam Karnadiwij",
            "prefix": null,
            "suffix": null,
            "nim": "12114085"
        }, {
            "id": "1143",
            "name": "Ari Fakhtur Arvan",
            "prefix": null,
            "suffix": null,
            "nim": "12114086"
        }, {
            "id": "1144",
            "name": "Prastyo Tridinata",
            "prefix": null,
            "suffix": null,
            "nim": "12114087"
        }, {
            "id": "1145",
            "name": "Hanif Muslim Amari",
            "prefix": null,
            "suffix": null,
            "nim": "12114088"
        }, {
            "id": "1146",
            "name": "Samuel Alfa Kurnia Adi Nainggo",
            "prefix": null,
            "suffix": null,
            "nim": "12114089"
        }, {
            "id": "1147",
            "name": "Anastasia Hermanto W",
            "prefix": null,
            "suffix": null,
            "nim": "12114090"
        }, {
            "id": "1148",
            "name": "Nuradiva Iqbal Rahmansah",
            "prefix": null,
            "suffix": null,
            "nim": "12114091"
        }, {
            "id": "1149",
            "name": "Alifianty Delila Imani Sumantr",
            "prefix": null,
            "suffix": null,
            "nim": "12114092"
        }, {
            "id": "1150",
            "name": "Iffah Lutfiah",
            "prefix": null,
            "suffix": null,
            "nim": "12114093"
        }, {
            "id": "1151",
            "name": "Satrio Daruadji P",
            "prefix": null,
            "suffix": null,
            "nim": "12114094"
        }, {
            "id": "1152",
            "name": "Ulfah Indah Safitri",
            "prefix": null,
            "suffix": null,
            "nim": "12114095"
        }, {
            "id": "1153",
            "name": "Hartman Mesaroziduhu Zebua W",
            "prefix": null,
            "suffix": null,
            "nim": "12114096"
        }, {
            "id": "1154",
            "name": "Abi Gunawan,Abi Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "12114097"
        }, {
            "id": "1155",
            "name": "Aditya Herlambang",
            "prefix": null,
            "suffix": null,
            "nim": "12114098"
        }, {
            "id": "1156",
            "name": "Eigenia Saras Putri Indra Prak",
            "prefix": null,
            "suffix": null,
            "nim": "12114099"
        }, {
            "id": "1157",
            "name": "Jener Duwit",
            "prefix": null,
            "suffix": null,
            "nim": "12115601"
        }, {
            "id": "1158",
            "name": "Manfret Sedik",
            "prefix": null,
            "suffix": null,
            "nim": "12115602"
        }, {
            "id": "1159",
            "name": "Muhamad Indra N",
            "prefix": null,
            "suffix": null,
            "nim": "12011004"
        }, {
            "id": "1160",
            "name": "Faisal Siddiq",
            "prefix": null,
            "suffix": null,
            "nim": "12011032"
        }, {
            "id": "1161",
            "name": "Dirga David Suhendi",
            "prefix": null,
            "suffix": null,
            "nim": "12011081"
        }, {
            "id": "1162",
            "name": "Reni Hastari",
            "prefix": null,
            "suffix": null,
            "nim": "12012003"
        }, {
            "id": "1163",
            "name": "Enos Pebrian",
            "prefix": null,
            "suffix": null,
            "nim": "12012008"
        }, {
            "id": "1164",
            "name": "Syafiq Abdullah",
            "prefix": null,
            "suffix": null,
            "nim": "12012017"
        }, {
            "id": "1165",
            "name": "Mirza2 Azmi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1166",
            "name": "Alam Fajarudin Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": "12012019"
        }, {
            "id": "1167",
            "name": "Muhammad Ghufron",
            "prefix": null,
            "suffix": null,
            "nim": "12012025"
        }, {
            "id": "1168",
            "name": "Syahril Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "12012036"
        }, {
            "id": "1169",
            "name": "Dida Patera A",
            "prefix": null,
            "suffix": null,
            "nim": "12012038"
        }, {
            "id": "1170",
            "name": "Rizal Debrian Mahantara",
            "prefix": null,
            "suffix": null,
            "nim": "12012047"
        }, {
            "id": "1171",
            "name": "Benazir M Sabrina",
            "prefix": null,
            "suffix": null,
            "nim": "12012057"
        }, {
            "id": "1172",
            "name": "Extivonus Kiki Fr",
            "prefix": null,
            "suffix": null,
            "nim": "12012060"
        }, {
            "id": "1173",
            "name": "Tyto Baskara Adimedha",
            "prefix": null,
            "suffix": null,
            "nim": "12012065"
        }, {
            "id": "1174",
            "name": "Agung Cipta Putra",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1175",
            "name": "Cindytami Rachmawati",
            "prefix": null,
            "suffix": null,
            "nim": "12012085"
        }, {
            "id": "1176",
            "name": "Zulfahmi",
            "prefix": null,
            "suffix": null,
            "nim": "12113018"
        }, {
            "id": "1177",
            "name": "Florentina Rika",
            "prefix": null,
            "suffix": null,
            "nim": "12113019"
        }, {
            "id": "1178",
            "name": "Rizky Rachmadi",
            "prefix": null,
            "suffix": null,
            "nim": "12113069"
        }, {
            "id": "1179",
            "name": "Fidkya Allisha",
            "prefix": null,
            "suffix": null,
            "nim": "12113088"
        }, {
            "id": "1180",
            "name": "Rio Priandri Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "12010039"
        }, {
            "id": "1181",
            "name": "Tri Haryanta",
            "prefix": null,
            "suffix": null,
            "nim": "12011005"
        }, {
            "id": "1182",
            "name": "Hilman Syahri Fathoni",
            "prefix": null,
            "suffix": null,
            "nim": "12011006"
        }, {
            "id": "1183",
            "name": "Mayta Kamila",
            "prefix": null,
            "suffix": null,
            "nim": "12011010"
        }, {
            "id": "1184",
            "name": "Trisna Suntara",
            "prefix": null,
            "suffix": null,
            "nim": "12011012"
        }, {
            "id": "1185",
            "name": "Vini Apriliani",
            "prefix": null,
            "suffix": null,
            "nim": "12011013"
        }, {
            "id": "1186",
            "name": "Deniswara Nur Fadilah Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12011022"
        }, {
            "id": "1187",
            "name": "Riksan Herdiana",
            "prefix": null,
            "suffix": null,
            "nim": "12011037"
        }, {
            "id": "1188",
            "name": "Yulia Nur Annisa Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "12011039"
        }, {
            "id": "1189",
            "name": "Rumaisa Restiani",
            "prefix": null,
            "suffix": null,
            "nim": "12011042"
        }, {
            "id": "1190",
            "name": "Michael Jefry I T",
            "prefix": null,
            "suffix": null,
            "nim": "12011043"
        }, {
            "id": "1191",
            "name": "Hamdani Agusta Asri",
            "prefix": null,
            "suffix": null,
            "nim": "12011047"
        }, {
            "id": "1192",
            "name": "Hario Purbaseno",
            "prefix": null,
            "suffix": null,
            "nim": "12011060"
        }, {
            "id": "1193",
            "name": "Raden Muhammad Agung Triadi",
            "prefix": null,
            "suffix": null,
            "nim": "12011082"
        }, {
            "id": "1194",
            "name": "Sandy Hardian S H",
            "prefix": null,
            "suffix": null,
            "nim": "12811039"
        }, {
            "id": "1195",
            "name": "Abraham Paladan     ",
            "prefix": null,
            "suffix": null,
            "nim": "12108019"
        }, {
            "id": "1196",
            "name": "Muhamad Zuwanda",
            "prefix": null,
            "suffix": null,
            "nim": "12109022"
        }, {
            "id": "1197",
            "name": "Shojiro Matsuki",
            "prefix": null,
            "suffix": null,
            "nim": "72312701"
        }, {
            "id": "1198",
            "name": "Baldus Ch.F Ambrauw",
            "prefix": null,
            "suffix": null,
            "nim": "121113603"
        }, {
            "id": "1199",
            "name": "Adino Eleazar Manoppo",
            "prefix": null,
            "suffix": null,
            "nim": "12311030"
        }, {
            "id": "1200",
            "name": "Suryonegoro G",
            "prefix": null,
            "suffix": null,
            "nim": "12311034"
        }, {
            "id": "1201",
            "name": "Dantie Claudia",
            "prefix": null,
            "suffix": null,
            "nim": "12011007"
        }, {
            "id": "1202",
            "name": "Nur Fantatik",
            "prefix": null,
            "suffix": null,
            "nim": "12011014"
        }, {
            "id": "1203",
            "name": "Bioter Ryanto Silalahi",
            "prefix": null,
            "suffix": null,
            "nim": "12011026"
        }, {
            "id": "1204",
            "name": "Muhammad Chandra R M",
            "prefix": null,
            "suffix": null,
            "nim": "12012005"
        }, {
            "id": "1205",
            "name": "Nuresa Riana Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "12012075"
        }, {
            "id": "1206",
            "name": "Janres Yoseva",
            "prefix": null,
            "suffix": null,
            "nim": "12513002"
        }, {
            "id": "1207",
            "name": "Muhammad Arief",
            "prefix": null,
            "suffix": null,
            "nim": "12513004"
        }, {
            "id": "1208",
            "name": "Cheryl Livia",
            "prefix": null,
            "suffix": null,
            "nim": "12513006"
        }, {
            "id": "1209",
            "name": "Halimatul Husni",
            "prefix": null,
            "suffix": null,
            "nim": "12513008"
        }, {
            "id": "1210",
            "name": "M Dzikri",
            "prefix": null,
            "suffix": null,
            "nim": "12513010"
        }, {
            "id": "1211",
            "name": "Yohansen",
            "prefix": null,
            "suffix": null,
            "nim": "12513012"
        }, {
            "id": "1212",
            "name": "Edina Amadeka",
            "prefix": null,
            "suffix": null,
            "nim": "12513014"
        }, {
            "id": "1213",
            "name": "Deddy Chandra",
            "prefix": null,
            "suffix": null,
            "nim": "12513018"
        }, {
            "id": "1214",
            "name": "Titin Zulfiah",
            "prefix": null,
            "suffix": null,
            "nim": "12513020"
        }, {
            "id": "1215",
            "name": "Fify Kurnia",
            "prefix": null,
            "suffix": null,
            "nim": "12513022"
        }, {
            "id": "1216",
            "name": "Nanda TH",
            "prefix": null,
            "suffix": null,
            "nim": "12513024"
        }, {
            "id": "1217",
            "name": "Trimoko P",
            "prefix": null,
            "suffix": null,
            "nim": "12513026"
        }, {
            "id": "1218",
            "name": "Muhammad Rasyad",
            "prefix": null,
            "suffix": null,
            "nim": "12513028"
        }, {
            "id": "1219",
            "name": "Eric Dwi",
            "prefix": null,
            "suffix": null,
            "nim": "12513030"
        }, {
            "id": "1220",
            "name": "Anggia Magnalita",
            "prefix": null,
            "suffix": null,
            "nim": "12513032"
        }, {
            "id": "1221",
            "name": "Syakina M",
            "prefix": null,
            "suffix": null,
            "nim": "12513034"
        }, {
            "id": "1222",
            "name": "Zaki Adam",
            "prefix": null,
            "suffix": null,
            "nim": "12513038"
        }, {
            "id": "1223",
            "name": "Dyaksa A",
            "prefix": null,
            "suffix": null,
            "nim": "12513040"
        }, {
            "id": "1224",
            "name": "Brilliant",
            "prefix": null,
            "suffix": null,
            "nim": "12513042"
        }, {
            "id": "1225",
            "name": "M Akmal",
            "prefix": null,
            "suffix": null,
            "nim": "12513044"
        }, {
            "id": "1226",
            "name": "Fattaah Baqir",
            "prefix": null,
            "suffix": null,
            "nim": "12513046"
        }, {
            "id": "1227",
            "name": "Arsyad B",
            "prefix": null,
            "suffix": null,
            "nim": "12513050"
        }, {
            "id": "1228",
            "name": "Annisa Ikrima",
            "prefix": null,
            "suffix": null,
            "nim": "12513052"
        }, {
            "id": "1229",
            "name": "Muchammad",
            "prefix": null,
            "suffix": null,
            "nim": "12513054"
        }, {
            "id": "1230",
            "name": "Bobby JSM",
            "prefix": null,
            "suffix": null,
            "nim": "12513056"
        }, {
            "id": "1231",
            "name": "Erza Hasti",
            "prefix": null,
            "suffix": null,
            "nim": "12513058"
        }, {
            "id": "1232",
            "name": "Arwin Sanlia",
            "prefix": null,
            "suffix": null,
            "nim": "12513060"
        }, {
            "id": "1233",
            "name": "Zaiimatul Husna",
            "prefix": null,
            "suffix": null,
            "nim": "12513062"
        }, {
            "id": "1234",
            "name": "Una Fitria Sari",
            "prefix": null,
            "suffix": null,
            "nim": "12511007"
        }, {
            "id": "1235",
            "name": "R Riksa Suryadi S",
            "prefix": null,
            "suffix": null,
            "nim": "12512032"
        }, {
            "id": "1236",
            "name": "Fransiskus",
            "prefix": null,
            "suffix": null,
            "nim": "12513007"
        }, {
            "id": "1237",
            "name": "Ivan Marthin",
            "prefix": null,
            "suffix": null,
            "nim": "12513036"
        }, {
            "id": "1238",
            "name": "Dony H Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12513059"
        }, {
            "id": "1239",
            "name": "Aldo",
            "prefix": null,
            "suffix": null,
            "nim": "12514001"
        }, {
            "id": "1240",
            "name": "Erwin Siregar",
            "prefix": null,
            "suffix": null,
            "nim": "12514002"
        }, {
            "id": "1241",
            "name": "Sjioen Xavier Talla",
            "prefix": null,
            "suffix": null,
            "nim": "12514003"
        }, {
            "id": "1242",
            "name": "Muhammad Saifudin",
            "prefix": null,
            "suffix": null,
            "nim": "12514004"
        }, {
            "id": "1243",
            "name": "Nurah Anugrah",
            "prefix": null,
            "suffix": null,
            "nim": "12514005"
        }, {
            "id": "1244",
            "name": "Muhammad Amri Firdaus",
            "prefix": null,
            "suffix": null,
            "nim": "12514006"
        }, {
            "id": "1245",
            "name": "Faiz Arrizki L",
            "prefix": null,
            "suffix": null,
            "nim": "12514007"
        }, {
            "id": "1246",
            "name": "Alma Kenanga",
            "prefix": null,
            "suffix": null,
            "nim": "12514008"
        }, {
            "id": "1247",
            "name": "Anindityo Arifiadi",
            "prefix": null,
            "suffix": null,
            "nim": "12514009"
        }, {
            "id": "1248",
            "name": "Hardio Arif",
            "prefix": null,
            "suffix": null,
            "nim": "12514010"
        }, {
            "id": "1249",
            "name": "Jonathan Filbert",
            "prefix": null,
            "suffix": null,
            "nim": "12514011"
        }, {
            "id": "1250",
            "name": "Rendy Tandela,Rendy Tandela",
            "prefix": null,
            "suffix": null,
            "nim": "12514012"
        }, {
            "id": "1251",
            "name": "Ridho Wahyudi",
            "prefix": null,
            "suffix": null,
            "nim": "12514013"
        }, {
            "id": "1252",
            "name": "Nickolas Adrianto",
            "prefix": null,
            "suffix": null,
            "nim": "12514014"
        }, {
            "id": "1253",
            "name": "Fionna Clarissa M",
            "prefix": null,
            "suffix": null,
            "nim": "12514015"
        }, {
            "id": "1254",
            "name": "Satya Wira N",
            "prefix": null,
            "suffix": null,
            "nim": "12514016"
        }, {
            "id": "1255",
            "name": "Yoga Rafinika",
            "prefix": null,
            "suffix": null,
            "nim": "12514017"
        }, {
            "id": "1256",
            "name": "Rido Revalino",
            "prefix": null,
            "suffix": null,
            "nim": "12514018"
        }, {
            "id": "1257",
            "name": "Anggian ST",
            "prefix": null,
            "suffix": null,
            "nim": "12514019"
        }, {
            "id": "1258",
            "name": "Muhammad Ilham",
            "prefix": null,
            "suffix": null,
            "nim": "12514020"
        }, {
            "id": "1259",
            "name": "Muhammad Iqbal Hilmi",
            "prefix": null,
            "suffix": null,
            "nim": "12514021"
        }, {
            "id": "1260",
            "name": "Cornelius Ferian A",
            "prefix": null,
            "suffix": null,
            "nim": "12514022"
        }, {
            "id": "1261",
            "name": "Jeremy Anggakusuma",
            "prefix": null,
            "suffix": null,
            "nim": "12514023"
        }, {
            "id": "1262",
            "name": "Ahmad Ikhbal Z",
            "prefix": null,
            "suffix": null,
            "nim": "12514024"
        }, {
            "id": "1263",
            "name": "Hanajit Ranu S",
            "prefix": null,
            "suffix": null,
            "nim": "12514025"
        }, {
            "id": "1264",
            "name": "Rendi Ahmad Rustandi",
            "prefix": null,
            "suffix": null,
            "nim": "12514026"
        }, {
            "id": "1265",
            "name": "Tulustia Japanesa",
            "prefix": null,
            "suffix": null,
            "nim": "12514027"
        }, {
            "id": "1266",
            "name": "Ahmad Syafiq",
            "prefix": null,
            "suffix": null,
            "nim": "12514028"
        }, {
            "id": "1267",
            "name": "Ariza Mufid B",
            "prefix": null,
            "suffix": null,
            "nim": "12514029"
        }, {
            "id": "1268",
            "name": "Ihsanul Qalby",
            "prefix": null,
            "suffix": null,
            "nim": "12514030"
        }, {
            "id": "1269",
            "name": "Dean Kemal A",
            "prefix": null,
            "suffix": null,
            "nim": "12514031"
        }, {
            "id": "1270",
            "name": "Syaiful Bachri",
            "prefix": null,
            "suffix": null,
            "nim": "12514032"
        }, {
            "id": "1271",
            "name": "Zainal Muttakin",
            "prefix": null,
            "suffix": null,
            "nim": "12514033"
        }, {
            "id": "1272",
            "name": "Andhini CP Mandira",
            "prefix": null,
            "suffix": null,
            "nim": "12514034"
        }, {
            "id": "1273",
            "name": "Mohammad Andi S",
            "prefix": null,
            "suffix": null,
            "nim": "12514035"
        }, {
            "id": "1274",
            "name": "Rachmat Mukti",
            "prefix": null,
            "suffix": null,
            "nim": "12514036"
        }, {
            "id": "1275",
            "name": "Andrew Jonathan",
            "prefix": null,
            "suffix": null,
            "nim": "12514037"
        }, {
            "id": "1276",
            "name": "Lutfiyah Dea G",
            "prefix": null,
            "suffix": null,
            "nim": "12514038"
        }, {
            "id": "1277",
            "name": "Agung Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "12514039"
        }, {
            "id": "1278",
            "name": "Viktor Suganda",
            "prefix": null,
            "suffix": null,
            "nim": "12514040"
        }, {
            "id": "1279",
            "name": "Karuniawan Fitrianto",
            "prefix": null,
            "suffix": null,
            "nim": "12514041"
        }, {
            "id": "1280",
            "name": "Clarissa Christio",
            "prefix": null,
            "suffix": null,
            "nim": "12514042"
        }, {
            "id": "1281",
            "name": "Alfin Rizqiadi,Alfin Rizqiadi",
            "prefix": null,
            "suffix": null,
            "nim": "12514043"
        }, {
            "id": "1282",
            "name": "Michael Chandra",
            "prefix": null,
            "suffix": null,
            "nim": "12514044"
        }, {
            "id": "1283",
            "name": "Rizky Aisyah SM",
            "prefix": null,
            "suffix": null,
            "nim": "12514045"
        }, {
            "id": "1284",
            "name": "Rifqi",
            "prefix": null,
            "suffix": null,
            "nim": "12514046"
        }, {
            "id": "1285",
            "name": "Windu Shalat",
            "prefix": null,
            "suffix": null,
            "nim": "12514047"
        }, {
            "id": "1286",
            "name": "Guruh Diki P",
            "prefix": null,
            "suffix": null,
            "nim": "12514048"
        }, {
            "id": "1287",
            "name": "Ikra Setya Utama",
            "prefix": null,
            "suffix": null,
            "nim": "12514049"
        }, {
            "id": "1288",
            "name": "Supradona Sinaga,Supradona Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": "12514050"
        }, {
            "id": "1289",
            "name": "Rahmat Firdaus",
            "prefix": null,
            "suffix": null,
            "nim": "12514051"
        }, {
            "id": "1290",
            "name": "Kevin Fauzi",
            "prefix": null,
            "suffix": null,
            "nim": "12514052"
        }, {
            "id": "1291",
            "name": "Prawira Nusantara",
            "prefix": null,
            "suffix": null,
            "nim": "12514053"
        }, {
            "id": "1292",
            "name": "Adi Wirawan",
            "prefix": null,
            "suffix": null,
            "nim": "12514054"
        }, {
            "id": "1293",
            "name": "Nurdedani AP",
            "prefix": null,
            "suffix": null,
            "nim": "12514055"
        }, {
            "id": "1294",
            "name": "Nadia Agdika Islami",
            "prefix": null,
            "suffix": null,
            "nim": "12514056"
        }, {
            "id": "1295",
            "name": "Gilang Audi Pahlevi",
            "prefix": null,
            "suffix": null,
            "nim": "12514057"
        }, {
            "id": "1296",
            "name": "Diska Prini F",
            "prefix": null,
            "suffix": null,
            "nim": "12514058"
        }, {
            "id": "1297",
            "name": "Faqih Ahmad Jamil",
            "prefix": null,
            "suffix": null,
            "nim": "12514059"
        }, {
            "id": "1298",
            "name": "Dina Andini SH",
            "prefix": null,
            "suffix": null,
            "nim": "12514060"
        }, {
            "id": "1299",
            "name": "Wildan Luthfi",
            "prefix": null,
            "suffix": null,
            "nim": "12514061"
        }, {
            "id": "1300",
            "name": "Galih Dwiyan W",
            "prefix": null,
            "suffix": null,
            "nim": "12514062"
        }, {
            "id": "1301",
            "name": "Andreanta Ginting",
            "prefix": null,
            "suffix": null,
            "nim": "12514063"
        }, {
            "id": "1302",
            "name": "Christoforus Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "12514064"
        }, {
            "id": "1303",
            "name": "Safira Sabilla R",
            "prefix": null,
            "suffix": null,
            "nim": "12514065"
        }, {
            "id": "1304",
            "name": "Hussein Ali Ahmed",
            "prefix": null,
            "suffix": null,
            "nim": "12310701"
        }, {
            "id": "1305",
            "name": "Amir Husin",
            "prefix": null,
            "suffix": null,
            "nim": "12109002"
        }, {
            "id": "1306",
            "name": "Jener Duwit",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1307",
            "name": "Jener2 Duwit",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1308",
            "name": "Ricky Samuel ",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1309",
            "name": "Dian Tanuwijaya",
            "prefix": null,
            "suffix": null,
            "nim": "12513001"
        }, {
            "id": "1310",
            "name": "Fajril El Fakhri",
            "prefix": null,
            "suffix": null,
            "nim": "12513003"
        }, {
            "id": "1311",
            "name": "Wahyu Dwi Sulakso",
            "prefix": null,
            "suffix": null,
            "nim": "12513005"
        }, {
            "id": "1312",
            "name": "I Gede Ponky Aditya Ryanta",
            "prefix": null,
            "suffix": null,
            "nim": "12513009"
        }, {
            "id": "1313",
            "name": "Muhammad Mahdy Putra",
            "prefix": null,
            "suffix": null,
            "nim": "12513011"
        }, {
            "id": "1314",
            "name": "Elsa Dau Septianingrum",
            "prefix": null,
            "suffix": null,
            "nim": "12513013"
        }, {
            "id": "1315",
            "name": "Lidyana Utami",
            "prefix": null,
            "suffix": null,
            "nim": "12513015"
        }, {
            "id": "1316",
            "name": "Nicky Fernanda Diaz",
            "prefix": null,
            "suffix": null,
            "nim": "12513017"
        }, {
            "id": "1317",
            "name": "Muhammad Ilham Reyhan",
            "prefix": null,
            "suffix": null,
            "nim": "12513019"
        }, {
            "id": "1318",
            "name": "Safira Fitri",
            "prefix": null,
            "suffix": null,
            "nim": "12513021"
        }, {
            "id": "1319",
            "name": "Adib Muhammad Chafid",
            "prefix": null,
            "suffix": null,
            "nim": "12513023"
        }, {
            "id": "1320",
            "name": "Petrus Pardomuan Butarbutar",
            "prefix": null,
            "suffix": null,
            "nim": "12513025"
        }, {
            "id": "1321",
            "name": "Sarah Karinda Ramadhani",
            "prefix": null,
            "suffix": null,
            "nim": "12513027"
        }, {
            "id": "1322",
            "name": "Muhammad Rio Zulfahmi",
            "prefix": null,
            "suffix": null,
            "nim": "12513029"
        }, {
            "id": "1323",
            "name": "Naufal Azmi Rabbani",
            "prefix": null,
            "suffix": null,
            "nim": "12513031"
        }, {
            "id": "1324",
            "name": "Muhammad Iqbal Toynbee",
            "prefix": null,
            "suffix": null,
            "nim": "12513033"
        }, {
            "id": "1325",
            "name": "Fachri Fauzan",
            "prefix": null,
            "suffix": null,
            "nim": "12513035"
        }, {
            "id": "1326",
            "name": "Eka Widiastuti",
            "prefix": null,
            "suffix": null,
            "nim": "12513037"
        }, {
            "id": "1327",
            "name": "Kautsar Hanief Fitriansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12513039"
        }, {
            "id": "1328",
            "name": "Fathi Rifati Azkiah",
            "prefix": null,
            "suffix": null,
            "nim": "12513041"
        }, {
            "id": "1329",
            "name": "Muhammad Ghofry",
            "prefix": null,
            "suffix": null,
            "nim": "12513043"
        }, {
            "id": "1330",
            "name": "Erian Jeremy",
            "prefix": null,
            "suffix": null,
            "nim": "12513045"
        }, {
            "id": "1331",
            "name": "Cucu Hermawan",
            "prefix": null,
            "suffix": null,
            "nim": "12513047"
        }, {
            "id": "1332",
            "name": "M Dzikra Ulya S",
            "prefix": null,
            "suffix": null,
            "nim": "12513049"
        }, {
            "id": "1333",
            "name": "Moch Iqbal Nur Said",
            "prefix": null,
            "suffix": null,
            "nim": "12513051"
        }, {
            "id": "1334",
            "name": "Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "12513053"
        }, {
            "id": "1335",
            "name": "Vania Earlene Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "12513055"
        }, {
            "id": "1336",
            "name": "Fikri Wafi Irawan",
            "prefix": null,
            "suffix": null,
            "nim": "12513061"
        }, {
            "id": "1337",
            "name": "Kevin Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "12513063"
        }, {
            "id": "1338",
            "name": "Hanandi Rahmad S",
            "prefix": null,
            "suffix": null,
            "nim": "10212093"
        }, {
            "id": "1339",
            "name": "Vanessa Olivia Nainggolan",
            "prefix": null,
            "suffix": null,
            "nim": "12513048"
        }, {
            "id": "1340",
            "name": "Andriyo Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "12109025"
        }, {
            "id": "1341",
            "name": "Kurnia Ferdiansyah",
            "prefix": null,
            "suffix": null,
            "nim": "12012010"
        }, {
            "id": "1342",
            "name": "Safira Tyas Parasdya",
            "prefix": null,
            "suffix": null,
            "nim": "12012030"
        }, {
            "id": "1343",
            "name": "Ahmad Khoirul Basyar",
            "prefix": null,
            "suffix": null,
            "nim": "12012068"
        }, {
            "id": "1344",
            "name": "Yunita Cahyani Rahadiani",
            "prefix": null,
            "suffix": null,
            "nim": "12012078"
        }, {
            "id": "1345",
            "name": "Cahyo Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "15113070"
        }, {
            "id": "1346",
            "name": "Luciana",
            "prefix": null,
            "suffix": null,
            "nim": "13012004"
        }, {
            "id": "1347",
            "name": "Nur Huda Arif Indiarto",
            "prefix": null,
            "suffix": null,
            "nim": "13012008"
        }, {
            "id": "1348",
            "name": "Widi Suganda",
            "prefix": null,
            "suffix": null,
            "nim": "13012025"
        }, {
            "id": "1349",
            "name": "Ifan Murdiyadi",
            "prefix": null,
            "suffix": null,
            "nim": "13012036"
        }, {
            "id": "1350",
            "name": "Faridz Nizar",
            "prefix": null,
            "suffix": null,
            "nim": "12012009"
        }, {
            "id": "1351",
            "name": "Dyta Amelia",
            "prefix": null,
            "suffix": null,
            "nim": "12012014"
        }, {
            "id": "1352",
            "name": "Reyhan Wiyarta S.",
            "prefix": null,
            "suffix": null,
            "nim": "12012042"
        }, {
            "id": "1353",
            "name": "Lindawati Sumpena",
            "prefix": null,
            "suffix": null,
            "nim": "12012062"
        }, {
            "id": "1354",
            "name": "Setya Andini Larashati",
            "prefix": null,
            "suffix": null,
            "nim": "12012080"
        }, {
            "id": "1355",
            "name": "Tifally Gethiani Tanianawda",
            "prefix": null,
            "suffix": null,
            "nim": "12012084"
        }, {
            "id": "1356",
            "name": "Habibi Abdillah ",
            "prefix": null,
            "suffix": null,
            "nim": "10209101"
        }, {
            "id": "1357",
            "name": "Faisal Hendri",
            "prefix": null,
            "suffix": null,
            "nim": "13307025"
        }, {
            "id": "1358",
            "name": "Hizbulloh Husnul Khitaam",
            "prefix": null,
            "suffix": null,
            "nim": "13308035"
        }, {
            "id": "1359",
            "name": "AdminEE",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "1360",
            "name": "AdminTK",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "1361",
            "name": "AdminTL",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "1362",
            "name": "AdminTF",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "1364",
            "name": "AdminTA",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "1365",
            "name": "Qoriah Alibasyah Siregar",
            "prefix": "",
            "suffix": "MA",
            "nim": null
        }, {
            "id": "1366",
            "name": "Novriana Sumarti",
            "prefix": "Dr. ",
            "suffix": "M.Si.",
            "nim": null
        }, {
            "id": "1367",
            "name": "Nurhasan",
            "prefix": "Dr.",
            "suffix": "M.Si.",
            "nim": null
        }, {
            "id": "1368",
            "name": "Bambang2 Prijamboedi",
            "prefix": "",
            "suffix": "Ph.D",
            "nim": null
        }, {
            "id": "1369",
            "name": "Yedi Purwanto",
            "prefix": "Dr. H.",
            "suffix": "M.Ag",
            "nim": null
        }, {
            "id": "1370",
            "name": "Prima Roza",
            "prefix": "Dr.",
            "suffix": "SE. M.Ed. Admin",
            "nim": null
        }, {
            "id": "1371",
            "name": "Asep2 Wawan Jatnika",
            "prefix": "Drs.",
            "suffix": "M.Hum.",
            "nim": null
        }, {
            "id": "1372",
            "name": "Untari Gunta Pertiwi",
            "prefix": "",
            "suffix": "M.Pd. ",
            "nim": null
        }, {
            "id": "1373",
            "name": "Kusnaedi",
            "prefix": "Drs.",
            "suffix": "M.Kes.",
            "nim": null
        }, {
            "id": "1374",
            "name": "Taufiq Mulyanto",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "1375",
            "name": "Biemo W. Soemardi",
            "prefix": "Ir.",
            "suffix": "M.Sc., Ph.D.",
            "nim": null
        }, {
            "id": "1376",
            "name": "Fazat Nur Azizah",
            "prefix": "Dr.",
            "suffix": "ST., M.Sc. ",
            "nim": null
        }, {
            "id": "1377",
            "name": "Rizky Nandiwardhana",
            "prefix": null,
            "suffix": null,
            "nim": "16715001"
        }, {
            "id": "1378",
            "name": "Jane Marito",
            "prefix": null,
            "suffix": null,
            "nim": "16715005"
        }, {
            "id": "1379",
            "name": "Bagja Abdullah Gymnastiar Jazm",
            "prefix": null,
            "suffix": null,
            "nim": "16715009"
        }, {
            "id": "1380",
            "name": "Nurfitriana Muharami",
            "prefix": null,
            "suffix": null,
            "nim": "16715013"
        }, {
            "id": "1381",
            "name": "Nadhira Radhiyani Harryanto",
            "prefix": null,
            "suffix": null,
            "nim": "16715017"
        }, {
            "id": "1382",
            "name": "Natasya Trivani Silalahi",
            "prefix": null,
            "suffix": null,
            "nim": "16715021"
        }, {
            "id": "1383",
            "name": "Fawziah Ghina Saraswati",
            "prefix": null,
            "suffix": null,
            "nim": "16715025"
        }, {
            "id": "1384",
            "name": "Aji Wira Sumbaga",
            "prefix": null,
            "suffix": null,
            "nim": "16715029"
        }, {
            "id": "1385",
            "name": "Ghiffary Azka Nur Aulia",
            "prefix": null,
            "suffix": null,
            "nim": "16715033"
        }, {
            "id": "1386",
            "name": "Muhamad Aulia Ilmi Adinugroho",
            "prefix": null,
            "suffix": null,
            "nim": "16715037"
        }, {
            "id": "1387",
            "name": "Tiara Laksmidewi Muhardi",
            "prefix": null,
            "suffix": null,
            "nim": "16715041"
        }, {
            "id": "1388",
            "name": "Muhammad Akbar Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16715045"
        }, {
            "id": "1389",
            "name": "Adam Abrar Muhammad",
            "prefix": null,
            "suffix": null,
            "nim": "16715049"
        }, {
            "id": "1390",
            "name": "Mario Charly Gultom",
            "prefix": null,
            "suffix": null,
            "nim": "16715053"
        }, {
            "id": "1391",
            "name": "Muhammad Fauzan",
            "prefix": null,
            "suffix": null,
            "nim": "16715057"
        }, {
            "id": "1392",
            "name": "Citrawati Sukma Pratiwi",
            "prefix": null,
            "suffix": null,
            "nim": "16715061"
        }, {
            "id": "1393",
            "name": "Stephen Iskandar",
            "prefix": null,
            "suffix": null,
            "nim": "16715065"
        }, {
            "id": "1394",
            "name": "Diki Satria",
            "prefix": null,
            "suffix": null,
            "nim": "16715069"
        }, {
            "id": "1395",
            "name": "Kevin Adhapratama Kusnandar",
            "prefix": null,
            "suffix": null,
            "nim": "16715073"
        }, {
            "id": "1396",
            "name": "Ni\'ma Rosyidah",
            "prefix": null,
            "suffix": null,
            "nim": "16715077"
        }, {
            "id": "1397",
            "name": "Luca Cada Lora",
            "prefix": null,
            "suffix": null,
            "nim": "16715081"
        }, {
            "id": "1398",
            "name": "Marsya",
            "prefix": null,
            "suffix": null,
            "nim": "16715085"
        }, {
            "id": "1399",
            "name": "Sandy Fajar Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16715089"
        }, {
            "id": "1400",
            "name": "Alisya Hasna Fahrizi",
            "prefix": null,
            "suffix": null,
            "nim": "16715093"
        }, {
            "id": "1401",
            "name": "Airin Levina Salim",
            "prefix": null,
            "suffix": null,
            "nim": "16715097"
        }, {
            "id": "1402",
            "name": "Ilham Ratman Triwibowo",
            "prefix": null,
            "suffix": null,
            "nim": "16715101"
        }, {
            "id": "1403",
            "name": "Afiyah Amaturrahman Dalimunthe",
            "prefix": null,
            "suffix": null,
            "nim": "16715105"
        }, {
            "id": "1404",
            "name": "Vinson Halim",
            "prefix": null,
            "suffix": null,
            "nim": "16715109"
        }, {
            "id": "1405",
            "name": "Mohammad Luky Fidallah",
            "prefix": null,
            "suffix": null,
            "nim": "16715113"
        }, {
            "id": "1406",
            "name": "Utomo Putera",
            "prefix": null,
            "suffix": null,
            "nim": "16715117"
        }, {
            "id": "1407",
            "name": "Afif Hamzens",
            "prefix": null,
            "suffix": null,
            "nim": "16715121"
        }, {
            "id": "1408",
            "name": "Christian",
            "prefix": null,
            "suffix": null,
            "nim": "16715125"
        }, {
            "id": "1409",
            "name": "Bobby Efendy",
            "prefix": null,
            "suffix": null,
            "nim": "16715129"
        }, {
            "id": "1410",
            "name": "Muchammad Majapahit Nur Wisiso",
            "prefix": null,
            "suffix": null,
            "nim": "16715133"
        }, {
            "id": "1411",
            "name": "Yulio Sitio",
            "prefix": null,
            "suffix": null,
            "nim": "16715137"
        }, {
            "id": "1412",
            "name": "Nindya Putri Fadillah",
            "prefix": null,
            "suffix": null,
            "nim": "16715141"
        }, {
            "id": "1413",
            "name": "Ester Rosari Tambunan",
            "prefix": null,
            "suffix": null,
            "nim": "16715145"
        }, {
            "id": "1414",
            "name": "Lyonidhia Khairunnisa",
            "prefix": null,
            "suffix": null,
            "nim": "16715149"
        }, {
            "id": "1415",
            "name": "Fakhri Dwi Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "16715153"
        }, {
            "id": "1416",
            "name": "Mohammad Raka Febsya Putranto",
            "prefix": null,
            "suffix": null,
            "nim": "16715157"
        }, {
            "id": "1417",
            "name": "Zhafirah Ajrina",
            "prefix": null,
            "suffix": null,
            "nim": "16715161"
        }, {
            "id": "1418",
            "name": "Khalis Mohammad Aryadi Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": "16715165"
        }, {
            "id": "1419",
            "name": "Nadya Anjani Sudiro",
            "prefix": null,
            "suffix": null,
            "nim": "16715169"
        }, {
            "id": "1420",
            "name": "Vincent Mulianto",
            "prefix": null,
            "suffix": null,
            "nim": "16715173"
        }, {
            "id": "1421",
            "name": "Lintang Dewani Prabandari",
            "prefix": null,
            "suffix": null,
            "nim": "16715177"
        }, {
            "id": "1422",
            "name": "Hikam Fahri Ahmad",
            "prefix": null,
            "suffix": null,
            "nim": "16715181"
        }, {
            "id": "1423",
            "name": "Selli Widhi Astuti",
            "prefix": null,
            "suffix": null,
            "nim": "16715185"
        }, {
            "id": "1424",
            "name": "Rio Alfriza Faiz",
            "prefix": null,
            "suffix": null,
            "nim": "16715189"
        }, {
            "id": "1425",
            "name": "Muhamad Daris Al Husna",
            "prefix": null,
            "suffix": null,
            "nim": "16715193"
        }, {
            "id": "1426",
            "name": "Aya Anisa Dwinidasari",
            "prefix": null,
            "suffix": null,
            "nim": "16715197"
        }, {
            "id": "1427",
            "name": "Evita Febrina Julianti",
            "prefix": null,
            "suffix": null,
            "nim": "16715201"
        }, {
            "id": "1428",
            "name": "Muhammad Satria Akbar Abbas",
            "prefix": null,
            "suffix": null,
            "nim": "16715205"
        }, {
            "id": "1429",
            "name": "Muhammad Ghithrif Ashshiddiq",
            "prefix": null,
            "suffix": null,
            "nim": "16715209"
        }, {
            "id": "1430",
            "name": "Kelvin Johan",
            "prefix": null,
            "suffix": null,
            "nim": "16715213"
        }, {
            "id": "1431",
            "name": "Asyifa Rizki Aprilia Harahap",
            "prefix": null,
            "suffix": null,
            "nim": "16715217"
        }, {
            "id": "1432",
            "name": "Zein Radita Farizki",
            "prefix": null,
            "suffix": null,
            "nim": "16715221"
        }, {
            "id": "1433",
            "name": "Gumilang Paramarta Saniskara",
            "prefix": null,
            "suffix": null,
            "nim": "16715225"
        }, {
            "id": "1434",
            "name": "Jeremy Setiawan",
            "prefix": null,
            "suffix": null,
            "nim": "16715229"
        }, {
            "id": "1435",
            "name": "Muhammad Farhan",
            "prefix": null,
            "suffix": null,
            "nim": "16715233"
        }, {
            "id": "1436",
            "name": "Dionisius Dio Anugrahtama",
            "prefix": null,
            "suffix": null,
            "nim": "16715237"
        }, {
            "id": "1437",
            "name": "Muhammad Raditya Dwiprasta",
            "prefix": null,
            "suffix": null,
            "nim": "16715241"
        }, {
            "id": "1438",
            "name": "Faishal Mahdi",
            "prefix": null,
            "suffix": null,
            "nim": "16715245"
        }, {
            "id": "1439",
            "name": "Asep Suherman",
            "prefix": null,
            "suffix": null,
            "nim": "16715249"
        }, {
            "id": "1440",
            "name": "Gusnul Andria Gita Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16715253"
        }, {
            "id": "1441",
            "name": "Benaya Dwika Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16715257"
        }, {
            "id": "1442",
            "name": "Ayasha Nabila Priatno",
            "prefix": null,
            "suffix": null,
            "nim": "16715261"
        }, {
            "id": "1443",
            "name": "Alifdio Reyhan Sudrajat",
            "prefix": null,
            "suffix": null,
            "nim": "16715265"
        }, {
            "id": "1444",
            "name": "Arsya Mauly Adya",
            "prefix": null,
            "suffix": null,
            "nim": "16715269"
        }, {
            "id": "1445",
            "name": "Endah Triutami",
            "prefix": null,
            "suffix": null,
            "nim": "16715273"
        }, {
            "id": "1446",
            "name": "Beny Maulana Achsan",
            "prefix": null,
            "suffix": null,
            "nim": "16715277"
        }, {
            "id": "1447",
            "name": "Hanif Aulia Adiwiranto",
            "prefix": null,
            "suffix": null,
            "nim": "16715281"
        }, {
            "id": "1448",
            "name": "Afia Lestari Mahanani",
            "prefix": null,
            "suffix": null,
            "nim": "16715285"
        }, {
            "id": "1449",
            "name": "Ivana",
            "prefix": null,
            "suffix": null,
            "nim": "16715289"
        }, {
            "id": "1450",
            "name": "Angela Belinda Cecilia",
            "prefix": null,
            "suffix": null,
            "nim": "16715293"
        }, {
            "id": "1451",
            "name": "Ninditya Arifasuri",
            "prefix": null,
            "suffix": null,
            "nim": "16715297"
        }, {
            "id": "1452",
            "name": "Michael Soegiri Pringgomoeljo",
            "prefix": null,
            "suffix": null,
            "nim": "16715301"
        }, {
            "id": "1453",
            "name": "Dwita Assyifa Meihusna",
            "prefix": null,
            "suffix": null,
            "nim": "16715305"
        }, {
            "id": "1454",
            "name": "Andre Hartono Gundjojo",
            "prefix": null,
            "suffix": null,
            "nim": "16715309"
        }, {
            "id": "1455",
            "name": "Nur Rahmah Syah Ramdani",
            "prefix": null,
            "suffix": null,
            "nim": "16715313"
        }, {
            "id": "1456",
            "name": "Ulfa Hanifatul Hidayah",
            "prefix": null,
            "suffix": null,
            "nim": "16715317"
        }, {
            "id": "1457",
            "name": "Rahmanisa Vandyani Ardiyanto",
            "prefix": null,
            "suffix": null,
            "nim": "16715321"
        }, {
            "id": "1458",
            "name": "Salma Rifasari Wakana",
            "prefix": null,
            "suffix": null,
            "nim": "16715325"
        }, {
            "id": "1459",
            "name": "Ni Nyoman Redhita Rianti",
            "prefix": null,
            "suffix": null,
            "nim": "16715329"
        }, {
            "id": "1460",
            "name": "Rana Ro\'idah Fitriani",
            "prefix": null,
            "suffix": null,
            "nim": "16715333"
        }, {
            "id": "1461",
            "name": "Athaya Muhammad Hafizh",
            "prefix": null,
            "suffix": null,
            "nim": "16715337"
        }, {
            "id": "1462",
            "name": "Bima Sukmana Aulia",
            "prefix": null,
            "suffix": null,
            "nim": "16715341"
        }, {
            "id": "1463",
            "name": "Bartimeus Dominic Wibisana",
            "prefix": null,
            "suffix": null,
            "nim": "16715345"
        }, {
            "id": "1464",
            "name": "Yahya Abdulhakim Satwiko",
            "prefix": null,
            "suffix": null,
            "nim": "16715349"
        }, {
            "id": "1465",
            "name": "Muhammad Rijal Anshorullah",
            "prefix": null,
            "suffix": null,
            "nim": "16715353"
        }, {
            "id": "1466",
            "name": "Renaldo Valentin",
            "prefix": null,
            "suffix": null,
            "nim": "16715357"
        }, {
            "id": "1467",
            "name": "Melvin",
            "prefix": null,
            "suffix": null,
            "nim": "16715361"
        }, {
            "id": "1468",
            "name": "Gabriela Mega Rahayu",
            "prefix": null,
            "suffix": null,
            "nim": "19515001"
        }, {
            "id": "1469",
            "name": "Fariz Rizqi",
            "prefix": null,
            "suffix": null,
            "nim": "19515005"
        }, {
            "id": "1470",
            "name": "Abigail Marcia",
            "prefix": null,
            "suffix": null,
            "nim": "19515009"
        }, {
            "id": "1471",
            "name": "Azmi Rahmayani",
            "prefix": null,
            "suffix": null,
            "nim": "19515013"
        }, {
            "id": "1472",
            "name": "Ahmad Subagjadiputra",
            "prefix": null,
            "suffix": null,
            "nim": "19515017"
        }, {
            "id": "1473",
            "name": "Anita Devina",
            "prefix": null,
            "suffix": null,
            "nim": "19515021"
        }, {
            "id": "1474",
            "name": "Carla Nathania",
            "prefix": null,
            "suffix": null,
            "nim": "19515025"
        }, {
            "id": "1475",
            "name": "Frisilia Indriani",
            "prefix": null,
            "suffix": null,
            "nim": "19515029"
        }, {
            "id": "1476",
            "name": "Oky Syahroni",
            "prefix": null,
            "suffix": null,
            "nim": "19515033"
        }, {
            "id": "1477",
            "name": "Stefanie Gabriel",
            "prefix": null,
            "suffix": null,
            "nim": "19515037"
        }, {
            "id": "1478",
            "name": "Zisma Chendikia",
            "prefix": null,
            "suffix": null,
            "nim": "19515041"
        }, {
            "id": "1479",
            "name": "Cindy Laurensia",
            "prefix": null,
            "suffix": null,
            "nim": "19515045"
        }, {
            "id": "1480",
            "name": "Lindawaty",
            "prefix": null,
            "suffix": null,
            "nim": "19515049"
        }, {
            "id": "1481",
            "name": "Grace Evelina",
            "prefix": null,
            "suffix": null,
            "nim": "19515053"
        }, {
            "id": "1482",
            "name": "Indah Christina Chandra",
            "prefix": null,
            "suffix": null,
            "nim": "19515057"
        }, {
            "id": "1483",
            "name": "Benedict Ganawidagda Setjadini",
            "prefix": null,
            "suffix": null,
            "nim": "19515061"
        }, {
            "id": "1484",
            "name": "Handiani Dandrajati",
            "prefix": null,
            "suffix": null,
            "nim": "19515065"
        }, {
            "id": "1485",
            "name": "Priscilla Hermanto",
            "prefix": null,
            "suffix": null,
            "nim": "19515069"
        }, {
            "id": "1486",
            "name": "Priskila Dwi Nater",
            "prefix": null,
            "suffix": null,
            "nim": "19515073"
        }, {
            "id": "1487",
            "name": "Jovita Ramli",
            "prefix": null,
            "suffix": null,
            "nim": "16615003"
        }, {
            "id": "1488",
            "name": "Elicohen Dima Sagala",
            "prefix": null,
            "suffix": null,
            "nim": "16615008"
        }, {
            "id": "1489",
            "name": "Faisal Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "16615013"
        }, {
            "id": "1490",
            "name": "Muh. Fadel Hidayat Marzuki",
            "prefix": null,
            "suffix": null,
            "nim": "16615018"
        }, {
            "id": "1491",
            "name": "Baracha Augustinus Panggabean",
            "prefix": null,
            "suffix": null,
            "nim": "16615023"
        }, {
            "id": "1492",
            "name": "Samuel Dwima Halim",
            "prefix": null,
            "suffix": null,
            "nim": "16615028"
        }, {
            "id": "1493",
            "name": "Firza Aulia Syafina",
            "prefix": null,
            "suffix": null,
            "nim": "16615033"
        }, {
            "id": "1494",
            "name": "Nadya Hanggana Raras Rinandya",
            "prefix": null,
            "suffix": null,
            "nim": "16615038"
        }, {
            "id": "1495",
            "name": "Angelika Maria",
            "prefix": null,
            "suffix": null,
            "nim": "16615043"
        }, {
            "id": "1496",
            "name": "Akhmad Raynaldi",
            "prefix": null,
            "suffix": null,
            "nim": "16615048"
        }, {
            "id": "1497",
            "name": "Gde Pandu Brata Putra W",
            "prefix": null,
            "suffix": null,
            "nim": "16615053"
        }, {
            "id": "1498",
            "name": "Tita Kartika Dewi",
            "prefix": null,
            "suffix": null,
            "nim": "16615058"
        }, {
            "id": "1499",
            "name": "Gelasius Galvindy",
            "prefix": null,
            "suffix": null,
            "nim": "16615063"
        }, {
            "id": "1500",
            "name": "Wilson",
            "prefix": null,
            "suffix": null,
            "nim": "16615068"
        }, {
            "id": "1501",
            "name": "Farah Basellina Safira",
            "prefix": null,
            "suffix": null,
            "nim": "16615073"
        }, {
            "id": "1502",
            "name": "Michael Soritua Hasibuan",
            "prefix": null,
            "suffix": null,
            "nim": "16615078"
        }, {
            "id": "1503",
            "name": "Dimas Yoga Pratama Jubaedi",
            "prefix": null,
            "suffix": null,
            "nim": "16615083"
        }, {
            "id": "1504",
            "name": "Sesilia Javiera Aldisa",
            "prefix": null,
            "suffix": null,
            "nim": "16615088"
        }, {
            "id": "1505",
            "name": "Ferdian Pratama Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16615093"
        }, {
            "id": "1506",
            "name": "Harry Adhyatna",
            "prefix": null,
            "suffix": null,
            "nim": "16615098"
        }, {
            "id": "1507",
            "name": "Fransisca Eureka Valesta Mahar",
            "prefix": null,
            "suffix": null,
            "nim": "16615103"
        }, {
            "id": "1508",
            "name": "Mochammad Fathurridho Hermanto",
            "prefix": null,
            "suffix": null,
            "nim": "16615108"
        }, {
            "id": "1509",
            "name": "Edbert Rainer",
            "prefix": null,
            "suffix": null,
            "nim": "16615113"
        }, {
            "id": "1510",
            "name": "Febrita Rahmantia",
            "prefix": null,
            "suffix": null,
            "nim": "16615118"
        }, {
            "id": "1511",
            "name": "Andronikus Riansy Lumembang",
            "prefix": null,
            "suffix": null,
            "nim": "16615123"
        }, {
            "id": "1512",
            "name": "Christopher Patar Matius Sitor",
            "prefix": null,
            "suffix": null,
            "nim": "16615128"
        }, {
            "id": "1513",
            "name": "Muhammad Alfian Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16615133"
        }, {
            "id": "1514",
            "name": "Riesna Dhipta Prameswari",
            "prefix": null,
            "suffix": null,
            "nim": "16615138"
        }, {
            "id": "1515",
            "name": "Winston T Marco",
            "prefix": null,
            "suffix": null,
            "nim": "16615143"
        }, {
            "id": "1516",
            "name": "Keza Harsono",
            "prefix": null,
            "suffix": null,
            "nim": "16615148"
        }, {
            "id": "1517",
            "name": "Anugerah Tito Pambudi",
            "prefix": null,
            "suffix": null,
            "nim": "16615153"
        }, {
            "id": "1518",
            "name": "Made Ayu Priyanka",
            "prefix": null,
            "suffix": null,
            "nim": "16615158"
        }, {
            "id": "1519",
            "name": "Alya Rismayanti",
            "prefix": null,
            "suffix": null,
            "nim": "16615163"
        }, {
            "id": "1520",
            "name": "Dayana E R S",
            "prefix": null,
            "suffix": null,
            "nim": "16615168"
        }, {
            "id": "1521",
            "name": "Oscar Ricky Harviyanto",
            "prefix": null,
            "suffix": null,
            "nim": "16615173"
        }, {
            "id": "1522",
            "name": "Leonardo",
            "prefix": null,
            "suffix": null,
            "nim": "16615178"
        }, {
            "id": "1523",
            "name": "Kevin Reinaldo Jacques Pantouw",
            "prefix": null,
            "suffix": null,
            "nim": "16615183"
        }, {
            "id": "1524",
            "name": "Pandu Adijaya",
            "prefix": null,
            "suffix": null,
            "nim": "16615188"
        }, {
            "id": "1525",
            "name": "Regina Martha",
            "prefix": null,
            "suffix": null,
            "nim": "16615193"
        }, {
            "id": "1526",
            "name": "Mohammad Singgih Iman Suwangsa",
            "prefix": null,
            "suffix": null,
            "nim": "16615198"
        }, {
            "id": "1527",
            "name": "Nicolas Kevin Juandi",
            "prefix": null,
            "suffix": null,
            "nim": "16615203"
        }, {
            "id": "1528",
            "name": "Achmad Mawardi Nur El Fayed",
            "prefix": null,
            "suffix": null,
            "nim": "16615208"
        }, {
            "id": "1529",
            "name": "Eril Arioristanto",
            "prefix": null,
            "suffix": null,
            "nim": "16615213"
        }, {
            "id": "1530",
            "name": "Nisrina Aulia Is Marsa",
            "prefix": null,
            "suffix": null,
            "nim": "16615218"
        }, {
            "id": "1531",
            "name": "Lathifa Shofi Rahmawati",
            "prefix": null,
            "suffix": null,
            "nim": "16615223"
        }, {
            "id": "1532",
            "name": "Adicawida Suparman",
            "prefix": null,
            "suffix": null,
            "nim": "16615228"
        }, {
            "id": "1533",
            "name": "Dewinta Kirana Pratisti",
            "prefix": null,
            "suffix": null,
            "nim": "16615233"
        }, {
            "id": "1534",
            "name": "Azrul Adhim",
            "prefix": null,
            "suffix": null,
            "nim": "16615238"
        }, {
            "id": "1535",
            "name": "Kevin Daffa Arrahman",
            "prefix": null,
            "suffix": null,
            "nim": "16615243"
        }, {
            "id": "1536",
            "name": "Syafik Adiar Muzaki",
            "prefix": null,
            "suffix": null,
            "nim": "16615248"
        }, {
            "id": "1537",
            "name": "Josephine Amadea",
            "prefix": null,
            "suffix": null,
            "nim": "16615253"
        }, {
            "id": "1538",
            "name": "Chris Ryantonius",
            "prefix": null,
            "suffix": null,
            "nim": "16615258"
        }, {
            "id": "1539",
            "name": "Addina Shafiyya Ediansjah",
            "prefix": null,
            "suffix": null,
            "nim": "16615263"
        }, {
            "id": "1540",
            "name": "Satria Nugraha Nurmansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16615268"
        }, {
            "id": "1541",
            "name": "Muhammad Agus Fadhlan",
            "prefix": null,
            "suffix": null,
            "nim": "16615273"
        }, {
            "id": "1542",
            "name": "Luthfi Hibatullah",
            "prefix": null,
            "suffix": null,
            "nim": "16615278"
        }, {
            "id": "1543",
            "name": "Maria Theresia Simanjorang",
            "prefix": null,
            "suffix": null,
            "nim": "16615283"
        }, {
            "id": "1544",
            "name": "Deyza Achrizt Arisintani",
            "prefix": null,
            "suffix": null,
            "nim": "16615288"
        }, {
            "id": "1545",
            "name": "Malsha Oktyarouna",
            "prefix": null,
            "suffix": null,
            "nim": "16615293"
        }, {
            "id": "1546",
            "name": "Rahmandia Prasetia",
            "prefix": null,
            "suffix": null,
            "nim": "16615298"
        }, {
            "id": "1547",
            "name": "Malida Br Girsang",
            "prefix": null,
            "suffix": null,
            "nim": "16615303"
        }, {
            "id": "1548",
            "name": "Hilmi Wirasatya",
            "prefix": null,
            "suffix": null,
            "nim": "16615308"
        }, {
            "id": "1549",
            "name": "Grecencia Evangelistha Sirait",
            "prefix": null,
            "suffix": null,
            "nim": "16615313"
        }, {
            "id": "1550",
            "name": "Mhd. Reza Fahlevy",
            "prefix": null,
            "suffix": null,
            "nim": "16615318"
        }, {
            "id": "1551",
            "name": "Anantyanto Kurnia Wicaksono",
            "prefix": null,
            "suffix": null,
            "nim": "16615323"
        }, {
            "id": "1552",
            "name": "Panji Guntara",
            "prefix": null,
            "suffix": null,
            "nim": "16615328"
        }, {
            "id": "1553",
            "name": "Muhammad Noor Yuliansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16615333"
        }, {
            "id": "1554",
            "name": "Yoga Surya Nusantara",
            "prefix": null,
            "suffix": null,
            "nim": "19615003"
        }, {
            "id": "1555",
            "name": "Ihsan Hafiyyan",
            "prefix": null,
            "suffix": null,
            "nim": "19615008"
        }, {
            "id": "1556",
            "name": "Muhammad Nasrullah Ahmad",
            "prefix": null,
            "suffix": null,
            "nim": "19615014"
        }, {
            "id": "1557",
            "name": "Ilmiadin Rasyid",
            "prefix": null,
            "suffix": null,
            "nim": "19615020"
        }, {
            "id": "1558",
            "name": "Kevin Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "19615025"
        }, {
            "id": "1559",
            "name": "Saferi Dian Suryana",
            "prefix": null,
            "suffix": null,
            "nim": "19615031"
        }, {
            "id": "1560",
            "name": "Larasati Putri Defi",
            "prefix": null,
            "suffix": null,
            "nim": "19615036"
        }, {
            "id": "1561",
            "name": "Aprillia Eka Andriani",
            "prefix": null,
            "suffix": null,
            "nim": "19615041"
        }, {
            "id": "1562",
            "name": "Rasyid Muhammad Amirul Muttaqi",
            "prefix": null,
            "suffix": null,
            "nim": "19615046"
        }, {
            "id": "1563",
            "name": "Tities Bagus Sadewo",
            "prefix": null,
            "suffix": null,
            "nim": "19615051"
        }, {
            "id": "1564",
            "name": "Sakti Irianto Tandibua Batong",
            "prefix": null,
            "suffix": null,
            "nim": "19615056"
        }, {
            "id": "1565",
            "name": "Dhimas Boy Anggun Pribadi",
            "prefix": null,
            "suffix": null,
            "nim": "19615061"
        }, {
            "id": "1566",
            "name": "Rita Mutiara",
            "prefix": null,
            "suffix": null,
            "nim": "19615066"
        }, {
            "id": "1567",
            "name": "Neneng Rimayanti",
            "prefix": null,
            "suffix": null,
            "nim": "19615071"
        }, {
            "id": "1568",
            "name": "Rizka Legita Rachmawati",
            "prefix": null,
            "suffix": null,
            "nim": "19615077"
        }, {
            "id": "1569",
            "name": "Hisham Lazuardi Yusuf",
            "prefix": null,
            "suffix": null,
            "nim": "16515002"
        }, {
            "id": "1570",
            "name": "Winsya Hesaputri Suryawan",
            "prefix": null,
            "suffix": null,
            "nim": "16515007"
        }, {
            "id": "1571",
            "name": "Prajogo Atmaja",
            "prefix": null,
            "suffix": null,
            "nim": "16515012"
        }, {
            "id": "1572",
            "name": "William Aristea Tantiono",
            "prefix": null,
            "suffix": null,
            "nim": "16515017"
        }, {
            "id": "1573",
            "name": "Erfandi Suryo Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16515022"
        }, {
            "id": "1574",
            "name": "Devin Alvaro",
            "prefix": null,
            "suffix": null,
            "nim": "16515027"
        }, {
            "id": "1575",
            "name": "Holy Lovenia",
            "prefix": null,
            "suffix": null,
            "nim": "16515032"
        }, {
            "id": "1576",
            "name": "Nafisah Nurul Hakim",
            "prefix": null,
            "suffix": null,
            "nim": "16515037"
        }, {
            "id": "1577",
            "name": "Embrin Fernando Pakpahan",
            "prefix": null,
            "suffix": null,
            "nim": "16515042"
        }, {
            "id": "1578",
            "name": "Rahmadi Rizki S. Pandia",
            "prefix": null,
            "suffix": null,
            "nim": "16515047"
        }, {
            "id": "1579",
            "name": "Pradnya Astari",
            "prefix": null,
            "suffix": null,
            "nim": "16515052"
        }, {
            "id": "1580",
            "name": "Edwin Kumara Tandiono",
            "prefix": null,
            "suffix": null,
            "nim": "16515057"
        }, {
            "id": "1581",
            "name": "Lavita Nur\'aviana Rizalputri",
            "prefix": null,
            "suffix": null,
            "nim": "16515062"
        }, {
            "id": "1582",
            "name": "Sri Wahyu Ningsih",
            "prefix": null,
            "suffix": null,
            "nim": "16516215"
        }, {
            "id": "1583",
            "name": "Finiko Kasula Novenda",
            "prefix": null,
            "suffix": null,
            "nim": "16515072"
        }, {
            "id": "1584",
            "name": "Vigor Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "16515077"
        }, {
            "id": "1585",
            "name": "Aditya Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16515082"
        }, {
            "id": "1586",
            "name": "Yoga Sanyoto",
            "prefix": null,
            "suffix": null,
            "nim": "16515087"
        }, {
            "id": "1587",
            "name": "Nofandi Surya",
            "prefix": null,
            "suffix": null,
            "nim": "16515092"
        }, {
            "id": "1588",
            "name": "Qurrata Choir Marham",
            "prefix": null,
            "suffix": null,
            "nim": "16515097"
        }, {
            "id": "1589",
            "name": "Prama Legawa Halqavi",
            "prefix": null,
            "suffix": null,
            "nim": "16515102"
        }, {
            "id": "1590",
            "name": "Veren Iliana Kurniadi",
            "prefix": null,
            "suffix": null,
            "nim": "16515107"
        }, {
            "id": "1591",
            "name": "Drianhar Raffy",
            "prefix": null,
            "suffix": null,
            "nim": "16515112"
        }, {
            "id": "1592",
            "name": "Humam Kurniadhitama",
            "prefix": null,
            "suffix": null,
            "nim": "16515117"
        }, {
            "id": "1593",
            "name": "Muhammad Khalid Rahman",
            "prefix": null,
            "suffix": null,
            "nim": "16515122"
        }, {
            "id": "1594",
            "name": "Talitha Yusli Alifia",
            "prefix": null,
            "suffix": null,
            "nim": "16515127"
        }, {
            "id": "1595",
            "name": "Shafiyyah Novel",
            "prefix": null,
            "suffix": null,
            "nim": "16515132"
        }, {
            "id": "1596",
            "name": "William",
            "prefix": null,
            "suffix": null,
            "nim": "16515137"
        }, {
            "id": "1597",
            "name": "Aulia Izzuddin Laksono",
            "prefix": null,
            "suffix": null,
            "nim": "16515142"
        }, {
            "id": "1598",
            "name": "Fakhri Ekaputra Surya",
            "prefix": null,
            "suffix": null,
            "nim": "16515147"
        }, {
            "id": "1599",
            "name": "Adiyanti Rifda Hayati",
            "prefix": null,
            "suffix": null,
            "nim": "16515152"
        }, {
            "id": "1600",
            "name": "Muhammad Al Faruqi",
            "prefix": null,
            "suffix": null,
            "nim": "16515157"
        }, {
            "id": "1601",
            "name": "Muhammad Hilmi",
            "prefix": null,
            "suffix": null,
            "nim": "16515162"
        }, {
            "id": "1602",
            "name": "Muhamad Irfan Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16515167"
        }, {
            "id": "1603",
            "name": "Irni Nur Anisa",
            "prefix": null,
            "suffix": null,
            "nim": "16515172"
        }, {
            "id": "1604",
            "name": "Imam Nur Bani Yusuf",
            "prefix": null,
            "suffix": null,
            "nim": "16515177"
        }, {
            "id": "1605",
            "name": "Rahadian Irsyad",
            "prefix": null,
            "suffix": null,
            "nim": "16515182"
        }, {
            "id": "1606",
            "name": "Muhammad Alif Mi\'raj Jabbar",
            "prefix": null,
            "suffix": null,
            "nim": "16515187"
        }, {
            "id": "1607",
            "name": "Ilham Bintang Ramadhan",
            "prefix": null,
            "suffix": null,
            "nim": "16515192"
        }, {
            "id": "1608",
            "name": "Justin Panungkunan Sitohang",
            "prefix": null,
            "suffix": null,
            "nim": "16515197"
        }, {
            "id": "1609",
            "name": "Firdaus Wahyu Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "16515202"
        }, {
            "id": "1610",
            "name": "Gregorius Arthur Widianto Praj",
            "prefix": null,
            "suffix": null,
            "nim": "16515207"
        }, {
            "id": "1611",
            "name": "Yohanes Jhouma Parulian Napitu",
            "prefix": null,
            "suffix": null,
            "nim": "16515212"
        }, {
            "id": "1612",
            "name": "Gianfranco Fertino Hwandiano",
            "prefix": null,
            "suffix": null,
            "nim": "16515217"
        }, {
            "id": "1613",
            "name": "Gilang Ardyamandala Al Assyifa",
            "prefix": null,
            "suffix": null,
            "nim": "16515222"
        }, {
            "id": "1614",
            "name": "Taufik Rahmad",
            "prefix": null,
            "suffix": null,
            "nim": "16515227"
        }, {
            "id": "1615",
            "name": "Irene Edria Devina",
            "prefix": null,
            "suffix": null,
            "nim": "16515232"
        }, {
            "id": "1616",
            "name": "Vanny Fitria Cahya",
            "prefix": null,
            "suffix": null,
            "nim": "16515237"
        }, {
            "id": "1617",
            "name": "I Made Arisanto Jonathan Adiwi",
            "prefix": null,
            "suffix": null,
            "nim": "16515242"
        }, {
            "id": "1618",
            "name": "Kevin Iswara",
            "prefix": null,
            "suffix": null,
            "nim": "16515247"
        }, {
            "id": "1619",
            "name": "Farhan Iqbal Tawakal",
            "prefix": null,
            "suffix": null,
            "nim": "16515252"
        }, {
            "id": "1620",
            "name": "Navila Akhsanil Fitri",
            "prefix": null,
            "suffix": null,
            "nim": "16515257"
        }, {
            "id": "1621",
            "name": "Yana Tejasukmana",
            "prefix": null,
            "suffix": null,
            "nim": "16515262"
        }, {
            "id": "1622",
            "name": "Finadefisa",
            "prefix": null,
            "suffix": null,
            "nim": "16515267"
        }, {
            "id": "1623",
            "name": "Hugi Reyhandani Munggaran",
            "prefix": null,
            "suffix": null,
            "nim": "16515272"
        }, {
            "id": "1624",
            "name": "Angellina Trisno Putri",
            "prefix": null,
            "suffix": null,
            "nim": "16515277"
        }, {
            "id": "1625",
            "name": "Nidaul Muslimah",
            "prefix": null,
            "suffix": null,
            "nim": "16515282"
        }, {
            "id": "1626",
            "name": "Rio Dwi Putra Perkasa",
            "prefix": null,
            "suffix": null,
            "nim": "16515287"
        }, {
            "id": "1627",
            "name": "Sarmila Ramadhan",
            "prefix": null,
            "suffix": null,
            "nim": "16515292"
        }, {
            "id": "1628",
            "name": "Theo Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16515297"
        }, {
            "id": "1629",
            "name": "Eka Winata",
            "prefix": null,
            "suffix": null,
            "nim": "16515302"
        }, {
            "id": "1630",
            "name": "Adrian Mulyana Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16515307"
        }, {
            "id": "1631",
            "name": "I Made Dwi Darmantara",
            "prefix": null,
            "suffix": null,
            "nim": "16515312"
        }, {
            "id": "1632",
            "name": "Jauhar Arifin",
            "prefix": null,
            "suffix": null,
            "nim": "16515317"
        }, {
            "id": "1633",
            "name": "Kevin Jonathan Koswara",
            "prefix": null,
            "suffix": null,
            "nim": "16515322"
        }, {
            "id": "1634",
            "name": "Farhad Zamani",
            "prefix": null,
            "suffix": null,
            "nim": "16515327"
        }, {
            "id": "1635",
            "name": "Gisela Supardi",
            "prefix": null,
            "suffix": null,
            "nim": "16515332"
        }, {
            "id": "1636",
            "name": "Rizky Faramita",
            "prefix": null,
            "suffix": null,
            "nim": "16515337"
        }, {
            "id": "1637",
            "name": "David Valianto",
            "prefix": null,
            "suffix": null,
            "nim": "16515342"
        }, {
            "id": "1638",
            "name": "Condro Wiyono",
            "prefix": null,
            "suffix": null,
            "nim": "16515347"
        }, {
            "id": "1639",
            "name": "Lazuardi Firdaus",
            "prefix": null,
            "suffix": null,
            "nim": "16515352"
        }, {
            "id": "1640",
            "name": "Yashael Faith Arthanto",
            "prefix": null,
            "suffix": null,
            "nim": "16515357"
        }, {
            "id": "1641",
            "name": "Gafie Fandri Garcia",
            "prefix": null,
            "suffix": null,
            "nim": "16515362"
        }, {
            "id": "1642",
            "name": "Rizky Saputra",
            "prefix": null,
            "suffix": null,
            "nim": "16515367"
        }, {
            "id": "1643",
            "name": "Diyas Avicenna",
            "prefix": null,
            "suffix": null,
            "nim": "16515372"
        }, {
            "id": "1644",
            "name": "Fathia Andita Resapati",
            "prefix": null,
            "suffix": null,
            "nim": "16515377"
        }, {
            "id": "1645",
            "name": "Kanisius Kenneth Halim",
            "prefix": null,
            "suffix": null,
            "nim": "16515382"
        }, {
            "id": "1646",
            "name": "Edli Aliadi Herda",
            "prefix": null,
            "suffix": null,
            "nim": "16515387"
        }, {
            "id": "1647",
            "name": "Ryan Adiputra",
            "prefix": null,
            "suffix": null,
            "nim": "16515392"
        }, {
            "id": "1648",
            "name": "Dewita Sonya Tarabunga",
            "prefix": null,
            "suffix": null,
            "nim": "16515397"
        }, {
            "id": "1649",
            "name": "Edwin Rachman",
            "prefix": null,
            "suffix": null,
            "nim": "16515402"
        }, {
            "id": "1650",
            "name": "Ahmad Fauzan Muzakki",
            "prefix": null,
            "suffix": null,
            "nim": "16515407"
        }, {
            "id": "1651",
            "name": "Irfan Ariq",
            "prefix": null,
            "suffix": null,
            "nim": "16515412"
        }, {
            "id": "1652",
            "name": "Henry Harianto",
            "prefix": null,
            "suffix": null,
            "nim": "16415004"
        }, {
            "id": "1653",
            "name": "Fransiskus Xaverius Bimantara",
            "prefix": null,
            "suffix": null,
            "nim": "16415008"
        }, {
            "id": "1654",
            "name": "Nandira Erninzafitri",
            "prefix": null,
            "suffix": null,
            "nim": "16415012"
        }, {
            "id": "1655",
            "name": "Grha Gandana Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415016"
        }, {
            "id": "1656",
            "name": "Muhamad Solihin",
            "prefix": null,
            "suffix": null,
            "nim": "16415020"
        }, {
            "id": "1657",
            "name": "Renaldy Herdin Tandry",
            "prefix": null,
            "suffix": null,
            "nim": "16415024"
        }, {
            "id": "1658",
            "name": "Dhimas Satria Buana",
            "prefix": null,
            "suffix": null,
            "nim": "16415028"
        }, {
            "id": "1659",
            "name": "Muhammad Al-kahfi",
            "prefix": null,
            "suffix": null,
            "nim": "16415032"
        }, {
            "id": "1660",
            "name": "Khobita Fara Daniella",
            "prefix": null,
            "suffix": null,
            "nim": "16415036"
        }, {
            "id": "1661",
            "name": "Silvia Oktovia",
            "prefix": null,
            "suffix": null,
            "nim": "16415040"
        }, {
            "id": "1662",
            "name": "Samuel",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1663",
            "name": "Mira Permata Sari Frm",
            "prefix": null,
            "suffix": null,
            "nim": "16415048"
        }, {
            "id": "1664",
            "name": "Ariel Paramasatya",
            "prefix": null,
            "suffix": null,
            "nim": "16415052"
        }, {
            "id": "1665",
            "name": "Clara Ester Veronica Sirait",
            "prefix": null,
            "suffix": null,
            "nim": "16415056"
        }, {
            "id": "1666",
            "name": "Piyan Rahmadi",
            "prefix": null,
            "suffix": null,
            "nim": "16415060"
        }, {
            "id": "1667",
            "name": "Bianca Edgina Handoko",
            "prefix": null,
            "suffix": null,
            "nim": "16415064"
        }, {
            "id": "1668",
            "name": "Marshel",
            "prefix": null,
            "suffix": null,
            "nim": "16415068"
        }, {
            "id": "1669",
            "name": "Bianca Adindanestya Putri",
            "prefix": null,
            "suffix": null,
            "nim": "16415072"
        }, {
            "id": "1670",
            "name": "Irfan Fadhilah",
            "prefix": null,
            "suffix": null,
            "nim": "16415076"
        }, {
            "id": "1671",
            "name": "I Putu Raditya Ambara Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415080"
        }, {
            "id": "1672",
            "name": "Zahra Hasina Nizami",
            "prefix": null,
            "suffix": null,
            "nim": "16415084"
        }, {
            "id": "1673",
            "name": "Keithcar Llang Mayo",
            "prefix": null,
            "suffix": null,
            "nim": "16415088"
        }, {
            "id": "1674",
            "name": "George Vincent Bungaran Parded",
            "prefix": null,
            "suffix": null,
            "nim": "16415092"
        }, {
            "id": "1675",
            "name": "Tegar Mukti Aji",
            "prefix": null,
            "suffix": null,
            "nim": "16415096"
        }, {
            "id": "1676",
            "name": "Anthony Eldrian Hendra",
            "prefix": null,
            "suffix": null,
            "nim": "16415100"
        }, {
            "id": "1677",
            "name": "Nabiel Husein Shihab",
            "prefix": null,
            "suffix": null,
            "nim": "16415104"
        }, {
            "id": "1678",
            "name": "Bintang Kusuma Yuda",
            "prefix": null,
            "suffix": null,
            "nim": "16415108"
        }, {
            "id": "1679",
            "name": "Irfani Sakinah",
            "prefix": null,
            "suffix": null,
            "nim": "16415112"
        }, {
            "id": "1680",
            "name": "Yarga Puritza",
            "prefix": null,
            "suffix": null,
            "nim": "16415116"
        }, {
            "id": "1681",
            "name": "Rifdah Fadhilah",
            "prefix": null,
            "suffix": null,
            "nim": "16415120"
        }, {
            "id": "1682",
            "name": "Yulio Adhitya Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "16415124"
        }, {
            "id": "1683",
            "name": "Devara Adhika Jala Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415128"
        }, {
            "id": "1684",
            "name": "Satrio Arif Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415132"
        }, {
            "id": "1685",
            "name": "Aldi Damora Siregar",
            "prefix": null,
            "suffix": null,
            "nim": "16415136"
        }, {
            "id": "1686",
            "name": "Tubagus Muhammad Ibrahim",
            "prefix": null,
            "suffix": null,
            "nim": "16415140"
        }, {
            "id": "1687",
            "name": "Afdhal Baravanni",
            "prefix": null,
            "suffix": null,
            "nim": "16415144"
        }, {
            "id": "1688",
            "name": "Fatur Rahman Abdillah",
            "prefix": null,
            "suffix": null,
            "nim": "16415148"
        }, {
            "id": "1689",
            "name": "Muhammad Ichsan",
            "prefix": null,
            "suffix": null,
            "nim": "16415152"
        }, {
            "id": "1690",
            "name": "Kevin Joshua Reynaldi",
            "prefix": null,
            "suffix": null,
            "nim": "16415156"
        }, {
            "id": "1691",
            "name": "Ari Febiansyah Prima Nasution",
            "prefix": null,
            "suffix": null,
            "nim": "16415160"
        }, {
            "id": "1692",
            "name": "Raymond Daniel Andriawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415164"
        }, {
            "id": "1693",
            "name": "Vicky Aji Pangestu",
            "prefix": null,
            "suffix": null,
            "nim": "16415168"
        }, {
            "id": "1694",
            "name": "M Eka Putra Abdillah",
            "prefix": null,
            "suffix": null,
            "nim": "16415172"
        }, {
            "id": "1695",
            "name": "Aulia Padlan",
            "prefix": null,
            "suffix": null,
            "nim": "16415176"
        }, {
            "id": "1696",
            "name": "Paul Joy Saut",
            "prefix": null,
            "suffix": null,
            "nim": "16415180"
        }, {
            "id": "1697",
            "name": "Anisa Fitri",
            "prefix": null,
            "suffix": null,
            "nim": "16415184"
        }, {
            "id": "1698",
            "name": "Susantry",
            "prefix": null,
            "suffix": null,
            "nim": "16415188"
        }, {
            "id": "1699",
            "name": "Citra Laksmi Utaminingsih",
            "prefix": null,
            "suffix": null,
            "nim": "16415192"
        }, {
            "id": "1700",
            "name": "Muhammad Satria Adhi Jatikusum",
            "prefix": null,
            "suffix": null,
            "nim": "16415196"
        }, {
            "id": "1701",
            "name": "Hyang Iman Kinasih Gusti",
            "prefix": null,
            "suffix": null,
            "nim": "16415200"
        }, {
            "id": "1702",
            "name": "Syani Liffa Suci",
            "prefix": null,
            "suffix": null,
            "nim": "16415204"
        }, {
            "id": "1703",
            "name": "Yasmine Safitri",
            "prefix": null,
            "suffix": null,
            "nim": "16415208"
        }, {
            "id": "1704",
            "name": "Raisha Pradisti",
            "prefix": null,
            "suffix": null,
            "nim": "16415212"
        }, {
            "id": "1705",
            "name": "Muhammad Denis Syahputra",
            "prefix": null,
            "suffix": null,
            "nim": "16415216"
        }, {
            "id": "1706",
            "name": "Andika Satria Pradana",
            "prefix": null,
            "suffix": null,
            "nim": "16415220"
        }, {
            "id": "1707",
            "name": "Jacky Jonathan",
            "prefix": null,
            "suffix": null,
            "nim": "16415224"
        }, {
            "id": "1708",
            "name": "Prajaka Trimandiri Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415228"
        }, {
            "id": "1709",
            "name": "Alexander",
            "prefix": null,
            "suffix": null,
            "nim": "16415232"
        }, {
            "id": "1710",
            "name": "Bagas Arya Wichaksana",
            "prefix": null,
            "suffix": null,
            "nim": "16415236"
        }, {
            "id": "1711",
            "name": "Muhammad Arief Sasmita",
            "prefix": null,
            "suffix": null,
            "nim": "16415240"
        }, {
            "id": "1712",
            "name": "I Ketut Mahendra Dharma Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415244"
        }, {
            "id": "1713",
            "name": "Nurul Aghni Febrianti",
            "prefix": null,
            "suffix": null,
            "nim": "16415248"
        }, {
            "id": "1714",
            "name": "Jarister Edwins Silalahi",
            "prefix": null,
            "suffix": null,
            "nim": "16415252"
        }, {
            "id": "1715",
            "name": "Atrie Ghina Al\'aliyya",
            "prefix": null,
            "suffix": null,
            "nim": "16415256"
        }, {
            "id": "1716",
            "name": "Giovanni Piere",
            "prefix": null,
            "suffix": null,
            "nim": "16415260"
        }, {
            "id": "1717",
            "name": "Fathoni Amirul Maarif",
            "prefix": null,
            "suffix": null,
            "nim": "16415264"
        }, {
            "id": "1718",
            "name": "Jordi Fatah",
            "prefix": null,
            "suffix": null,
            "nim": "16415268"
        }, {
            "id": "1719",
            "name": "Zahra Rizky Dwita Anissa",
            "prefix": null,
            "suffix": null,
            "nim": "16415272"
        }, {
            "id": "1720",
            "name": "Putu Billy Suryanata",
            "prefix": null,
            "suffix": null,
            "nim": "16415276"
        }, {
            "id": "1721",
            "name": "Fairuuz Ryanhilmi Hibatullah",
            "prefix": null,
            "suffix": null,
            "nim": "16415280"
        }, {
            "id": "1722",
            "name": "Fadlilah Mulyana",
            "prefix": null,
            "suffix": null,
            "nim": "16415284"
        }, {
            "id": "1723",
            "name": "Mochamad Redza Yuliawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415288"
        }, {
            "id": "1724",
            "name": "Vieri Kristianto Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "16415292"
        }, {
            "id": "1725",
            "name": "Wismoyo Dwi Juniawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415296"
        }, {
            "id": "1726",
            "name": "Farrel Alvian Purnama",
            "prefix": null,
            "suffix": null,
            "nim": "16415300"
        }, {
            "id": "1727",
            "name": "M Afif Habiburrahman",
            "prefix": null,
            "suffix": null,
            "nim": "16415304"
        }, {
            "id": "1728",
            "name": "Pransiska Marlina",
            "prefix": null,
            "suffix": null,
            "nim": "16415308"
        }, {
            "id": "1729",
            "name": "Dita Pertiwi",
            "prefix": null,
            "suffix": null,
            "nim": "16415312"
        }, {
            "id": "1730",
            "name": "Yosafat Abednego",
            "prefix": null,
            "suffix": null,
            "nim": "16415316"
        }, {
            "id": "1731",
            "name": "Gabriel Powericho Luo Daely",
            "prefix": null,
            "suffix": null,
            "nim": "16415320"
        }, {
            "id": "1732",
            "name": "Dicky Putra Alviansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16415324"
        }, {
            "id": "1733",
            "name": "Fadhillah Amroe",
            "prefix": null,
            "suffix": null,
            "nim": "16415328"
        }, {
            "id": "1734",
            "name": "Evander Franklin Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415332"
        }, {
            "id": "1735",
            "name": "Josaphat Manik",
            "prefix": null,
            "suffix": null,
            "nim": "16415336"
        }, {
            "id": "1736",
            "name": "Rendy Setyawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415340"
        }, {
            "id": "1737",
            "name": "Hans J. C. Makabori",
            "prefix": null,
            "suffix": null,
            "nim": "16415344"
        }, {
            "id": "1738",
            "name": "Betty Elisabeth Manurung",
            "prefix": null,
            "suffix": null,
            "nim": "16515004"
        }, {
            "id": "1739",
            "name": "Rizki Ihza Parama",
            "prefix": null,
            "suffix": null,
            "nim": "16515009"
        }, {
            "id": "1740",
            "name": "Wahyu Setyawan",
            "prefix": null,
            "suffix": null,
            "nim": "16515014"
        }, {
            "id": "1741",
            "name": "Stevanno Hero Leadervand",
            "prefix": null,
            "suffix": null,
            "nim": "16515019"
        }, {
            "id": "1742",
            "name": "Juleo Nobel Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16515024"
        }, {
            "id": "1743",
            "name": "Dini Nur Farida Putri",
            "prefix": null,
            "suffix": null,
            "nim": "16515029"
        }, {
            "id": "1744",
            "name": "Mikhael Artur Darmakesuma",
            "prefix": null,
            "suffix": null,
            "nim": "16515034"
        }, {
            "id": "1745",
            "name": "Christopher Clement Andreas",
            "prefix": null,
            "suffix": null,
            "nim": "16515039"
        }, {
            "id": "1746",
            "name": "Ray Andrew Obaja Sinurat",
            "prefix": null,
            "suffix": null,
            "nim": "16515044"
        }, {
            "id": "1747",
            "name": "Tegar Satria Nurhuda",
            "prefix": null,
            "suffix": null,
            "nim": "16515049"
        }, {
            "id": "1748",
            "name": "Mahardhika Michael Hamonangan",
            "prefix": null,
            "suffix": null,
            "nim": "16515054"
        }, {
            "id": "1749",
            "name": "Akbar Bagas Pramantya",
            "prefix": null,
            "suffix": null,
            "nim": "16515059"
        }, {
            "id": "1750",
            "name": "Saskia Amalia Aryono",
            "prefix": null,
            "suffix": null,
            "nim": "16515064"
        }, {
            "id": "1751",
            "name": "Jessika",
            "prefix": null,
            "suffix": null,
            "nim": "16515069"
        }, {
            "id": "1752",
            "name": "Azis Adi Kuncoro",
            "prefix": null,
            "suffix": null,
            "nim": "16515074"
        }, {
            "id": "1753",
            "name": "Mohammad Ali Rido",
            "prefix": null,
            "suffix": null,
            "nim": "16515079"
        }, {
            "id": "1754",
            "name": "Stevani Halim",
            "prefix": null,
            "suffix": null,
            "nim": "16515084"
        }, {
            "id": "1755",
            "name": "Khoerudin Alfi Syahri",
            "prefix": null,
            "suffix": null,
            "nim": "16515089"
        }, {
            "id": "1756",
            "name": "Judhistira Natha Junior",
            "prefix": null,
            "suffix": null,
            "nim": "16515094"
        }, {
            "id": "1757",
            "name": "Mahdiar Naufal",
            "prefix": null,
            "suffix": null,
            "nim": "16515099"
        }, {
            "id": "1758",
            "name": "Sean",
            "prefix": null,
            "suffix": null,
            "nim": "16515104"
        }, {
            "id": "1759",
            "name": "Andrew Anastasius Kintadjaja",
            "prefix": null,
            "suffix": null,
            "nim": "16515109"
        }, {
            "id": "1760",
            "name": "Adrian Cahyadi",
            "prefix": null,
            "suffix": null,
            "nim": "16515114"
        }, {
            "id": "1761",
            "name": "Jonathan Christopher",
            "prefix": null,
            "suffix": null,
            "nim": "16515119"
        }, {
            "id": "1762",
            "name": "Muhammad Rifqi Febrian",
            "prefix": null,
            "suffix": null,
            "nim": "16515124"
        }, {
            "id": "1763",
            "name": "Fakhri Pradana Suhandi",
            "prefix": null,
            "suffix": null,
            "nim": "16515129"
        }, {
            "id": "1764",
            "name": "Shalahuddin Al Ayyubi",
            "prefix": null,
            "suffix": null,
            "nim": "16515134"
        }, {
            "id": "1765",
            "name": "Albertus Djauhari Djohan",
            "prefix": null,
            "suffix": null,
            "nim": "16515139"
        }, {
            "id": "1766",
            "name": "Vatresya Rumiris",
            "prefix": null,
            "suffix": null,
            "nim": "16515144"
        }, {
            "id": "1767",
            "name": "Dhinda Ariecintya Janita",
            "prefix": null,
            "suffix": null,
            "nim": "16515149"
        }, {
            "id": "1768",
            "name": "Muthahhari Aulia Padmanagara",
            "prefix": null,
            "suffix": null,
            "nim": "16515154"
        }, {
            "id": "1769",
            "name": "Emilia Andari Razak",
            "prefix": null,
            "suffix": null,
            "nim": "16515159"
        }, {
            "id": "1770",
            "name": "Audry Nyonata",
            "prefix": null,
            "suffix": null,
            "nim": "16515164"
        }, {
            "id": "1771",
            "name": "Aries Tri Sutrisno K A",
            "prefix": null,
            "suffix": null,
            "nim": "16515169"
        }, {
            "id": "1772",
            "name": "Trevin Matthew Robertsen",
            "prefix": null,
            "suffix": null,
            "nim": "16515174"
        }, {
            "id": "1773",
            "name": "Fauzan Linnas",
            "prefix": null,
            "suffix": null,
            "nim": "16515179"
        }, {
            "id": "1774",
            "name": "Lutfiatul Mar\'Ah",
            "prefix": null,
            "suffix": null,
            "nim": "16515184"
        }, {
            "id": "1775",
            "name": "Sukmawening Nastitisari",
            "prefix": null,
            "suffix": null,
            "nim": "16515189"
        }, {
            "id": "1776",
            "name": "Royyan Abdullah Dzakiy",
            "prefix": null,
            "suffix": null,
            "nim": "16515194"
        }, {
            "id": "1777",
            "name": "Wakhidati Hidayah",
            "prefix": null,
            "suffix": null,
            "nim": "16515199"
        }, {
            "id": "1778",
            "name": "Rifaldhy Anand Caesara",
            "prefix": null,
            "suffix": null,
            "nim": "16515204"
        }, {
            "id": "1779",
            "name": "Devin Indra Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16515209"
        }, {
            "id": "1780",
            "name": "Francisco Kenandi Cahyono",
            "prefix": null,
            "suffix": null,
            "nim": "16515214"
        }, {
            "id": "1781",
            "name": "Arfinda Ilmania",
            "prefix": null,
            "suffix": null,
            "nim": "16515219"
        }, {
            "id": "1782",
            "name": "Edo Bintang O. Napitupulu",
            "prefix": null,
            "suffix": null,
            "nim": "16515224"
        }, {
            "id": "1783",
            "name": "Daniel Christian Pradipta Baso",
            "prefix": null,
            "suffix": null,
            "nim": "16515229"
        }, {
            "id": "1784",
            "name": "Vincent Giovanni",
            "prefix": null,
            "suffix": null,
            "nim": "16515234"
        }, {
            "id": "1785",
            "name": "Aufa Ariayuda",
            "prefix": null,
            "suffix": null,
            "nim": "16515239"
        }, {
            "id": "1786",
            "name": "Adela Teresa",
            "prefix": null,
            "suffix": null,
            "nim": "16515244"
        }, {
            "id": "1787",
            "name": "Faiz Ghifari Haznitrama",
            "prefix": null,
            "suffix": null,
            "nim": "16515249"
        }, {
            "id": "1788",
            "name": "Syahadatul Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "16515254"
        }, {
            "id": "1789",
            "name": "Afna Okta Sekar Rima",
            "prefix": null,
            "suffix": null,
            "nim": "16515259"
        }, {
            "id": "1790",
            "name": "Ferdinandus Richard",
            "prefix": null,
            "suffix": null,
            "nim": "16515264"
        }, {
            "id": "1791",
            "name": "Intan Qonitah",
            "prefix": null,
            "suffix": null,
            "nim": "16515269"
        }, {
            "id": "1792",
            "name": "Septasia Dwi Angfika",
            "prefix": null,
            "suffix": null,
            "nim": "16515274"
        }, {
            "id": "1793",
            "name": "Ratna Adriani Djohan",
            "prefix": null,
            "suffix": null,
            "nim": "16515279"
        }, {
            "id": "1794",
            "name": "Sylvia Juliana",
            "prefix": null,
            "suffix": null,
            "nim": "16515284"
        }, {
            "id": "1795",
            "name": "Fildah Ananda Amalia",
            "prefix": null,
            "suffix": null,
            "nim": "16515289"
        }, {
            "id": "1796",
            "name": "Laurence Hasan",
            "prefix": null,
            "suffix": null,
            "nim": "16515294"
        }, {
            "id": "1797",
            "name": "Kevin2",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1798",
            "name": "Ikhsan Widi Adyatma",
            "prefix": null,
            "suffix": null,
            "nim": "16515304"
        }, {
            "id": "1799",
            "name": "Tri Fuad Aziz",
            "prefix": null,
            "suffix": null,
            "nim": "16515309"
        }, {
            "id": "1800",
            "name": "Jungman Berliansyah Nurdin",
            "prefix": null,
            "suffix": null,
            "nim": "16515314"
        }, {
            "id": "1801",
            "name": "Naufal Nashirul M",
            "prefix": null,
            "suffix": null,
            "nim": "16515319"
        }, {
            "id": "1802",
            "name": "Chessa Nur Triejunita",
            "prefix": null,
            "suffix": null,
            "nim": "16515324"
        }, {
            "id": "1803",
            "name": "Nancy Silvia",
            "prefix": null,
            "suffix": null,
            "nim": "16515329"
        }, {
            "id": "1804",
            "name": "Muhammad Yusreza Irsyan",
            "prefix": null,
            "suffix": null,
            "nim": "16515334"
        }, {
            "id": "1805",
            "name": "Muhammad Faiq Nauval",
            "prefix": null,
            "suffix": null,
            "nim": "16515339"
        }, {
            "id": "1806",
            "name": "Tafriyana",
            "prefix": null,
            "suffix": null,
            "nim": "16515344"
        }, {
            "id": "1807",
            "name": "Fadhil Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "16515349"
        }, {
            "id": "1808",
            "name": "Harum Lokawati",
            "prefix": null,
            "suffix": null,
            "nim": "16515354"
        }, {
            "id": "1809",
            "name": "Nyayu Fitria Romadhona",
            "prefix": null,
            "suffix": null,
            "nim": "16515359"
        }, {
            "id": "1810",
            "name": "Reinaldo Ignatius Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "16515364"
        }, {
            "id": "1811",
            "name": "Jabal Logian",
            "prefix": null,
            "suffix": null,
            "nim": "16515369"
        }, {
            "id": "1812",
            "name": "Abdul Hafizh Firdaus",
            "prefix": null,
            "suffix": null,
            "nim": "16515374"
        }, {
            "id": "1813",
            "name": "Abraham Gavrillo",
            "prefix": null,
            "suffix": null,
            "nim": "16515379"
        }, {
            "id": "1814",
            "name": "Agus Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16515384"
        }, {
            "id": "1815",
            "name": "Muhammad Rafid Amrullah",
            "prefix": null,
            "suffix": null,
            "nim": "16515389"
        }, {
            "id": "1816",
            "name": "Roselina",
            "prefix": null,
            "suffix": null,
            "nim": "16515395"
        }, {
            "id": "1817",
            "name": "Robby Syaifullah",
            "prefix": null,
            "suffix": null,
            "nim": "16515400"
        }, {
            "id": "1818",
            "name": "Rangga Mahendra Santoso",
            "prefix": null,
            "suffix": null,
            "nim": "16515405"
        }, {
            "id": "1819",
            "name": "Abdurrahman",
            "prefix": null,
            "suffix": null,
            "nim": "16515410"
        }, {
            "id": "1820",
            "name": "Mofu William Imanuel",
            "prefix": null,
            "suffix": null,
            "nim": "16515415"
        }, {
            "id": "1821",
            "name": "Dinda Nurianti",
            "prefix": null,
            "suffix": null,
            "nim": "16315119"
        }, {
            "id": "1822",
            "name": "Rizky Inayati",
            "prefix": null,
            "suffix": null,
            "nim": "13309070"
        }, {
            "id": "1823",
            "name": "Riza Nuari",
            "prefix": null,
            "suffix": null,
            "nim": "13307038"
        }, {
            "id": "1824",
            "name": "Ari Angga Rochim",
            "prefix": null,
            "suffix": null,
            "nim": "13307054"
        }, {
            "id": "1825",
            "name": "Hanifan Prafiadi",
            "prefix": null,
            "suffix": null,
            "nim": "13308032"
        }, {
            "id": "1826",
            "name": "Gentry Rafa",
            "prefix": null,
            "suffix": null,
            "nim": "13308042"
        }, {
            "id": "1827",
            "name": "Kevin Kristianto",
            "prefix": null,
            "suffix": null,
            "nim": "13308090"
        }, {
            "id": "1828",
            "name": "Maruli Asvan H Pasaribu",
            "prefix": null,
            "suffix": null,
            "nim": "13308104"
        }, {
            "id": "1829",
            "name": "Stefio Yosse Andrean",
            "prefix": null,
            "suffix": null,
            "nim": "13309002"
        }, {
            "id": "1830",
            "name": "Anita Ayuningtiyas",
            "prefix": null,
            "suffix": null,
            "nim": "13309004"
        }, {
            "id": "1831",
            "name": "Enggar Dwi Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": "13309010"
        }, {
            "id": "1832",
            "name": "Anggi Bagus Satrio Sumantri",
            "prefix": null,
            "suffix": null,
            "nim": "13309012"
        }, {
            "id": "1833",
            "name": "Budi Pratomo",
            "prefix": null,
            "suffix": null,
            "nim": "13309016"
        }, {
            "id": "1834",
            "name": "Ridwan Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "13309018"
        }, {
            "id": "1835",
            "name": "Ananta Adhi Wardana",
            "prefix": null,
            "suffix": null,
            "nim": "13309020"
        }, {
            "id": "1836",
            "name": "Kurniadi Budiono",
            "prefix": null,
            "suffix": null,
            "nim": "13309024"
        }, {
            "id": "1837",
            "name": "Zaid Cahya Dinul Haq",
            "prefix": null,
            "suffix": null,
            "nim": "13309026"
        }, {
            "id": "1838",
            "name": "Felicia Angelina Sarwono",
            "prefix": null,
            "suffix": null,
            "nim": "13309028"
        }, {
            "id": "1839",
            "name": "Vernida Mufidah",
            "prefix": null,
            "suffix": null,
            "nim": "13309032"
        }, {
            "id": "1840",
            "name": "Dietia Ayuningtyas",
            "prefix": null,
            "suffix": null,
            "nim": "13309034"
        }, {
            "id": "1841",
            "name": "Gea Linggar Galih",
            "prefix": null,
            "suffix": null,
            "nim": "13309036"
        }, {
            "id": "1842",
            "name": "Yusal Sunjaya",
            "prefix": null,
            "suffix": null,
            "nim": "13309040"
        }, {
            "id": "1843",
            "name": "Ivan Tamusi",
            "prefix": null,
            "suffix": null,
            "nim": "13309042"
        }, {
            "id": "1844",
            "name": "Annisa Nurul Hidayah",
            "prefix": null,
            "suffix": null,
            "nim": "13309044"
        }, {
            "id": "1845",
            "name": "Sandra Rahmawati",
            "prefix": null,
            "suffix": null,
            "nim": "13309046"
        }, {
            "id": "1846",
            "name": "Prima Patriana",
            "prefix": null,
            "suffix": null,
            "nim": "13309050"
        }, {
            "id": "1847",
            "name": "Rere Nugrahita",
            "prefix": null,
            "suffix": null,
            "nim": "13309052"
        }, {
            "id": "1848",
            "name": "Abdul Somat Budiaji",
            "prefix": null,
            "suffix": null,
            "nim": "13309054"
        }, {
            "id": "1849",
            "name": "Bharata Dewanto",
            "prefix": null,
            "suffix": null,
            "nim": "13309060"
        }, {
            "id": "1850",
            "name": "Triannisa Julia Dian Anggreani",
            "prefix": null,
            "suffix": null,
            "nim": "13309064"
        }, {
            "id": "1851",
            "name": "Thio Ariyanto W S B",
            "prefix": null,
            "suffix": null,
            "nim": "13309068"
        }, {
            "id": "1852",
            "name": "Irvan Winarto",
            "prefix": null,
            "suffix": null,
            "nim": "13309072"
        }, {
            "id": "1853",
            "name": "Paulus Himawan",
            "prefix": null,
            "suffix": null,
            "nim": "13309074"
        }, {
            "id": "1854",
            "name": "Rifa Wibisana",
            "prefix": null,
            "suffix": null,
            "nim": "13309076"
        }, {
            "id": "1855",
            "name": "Timotius Yudha Adiruswanto",
            "prefix": null,
            "suffix": null,
            "nim": "13309078"
        }, {
            "id": "1856",
            "name": "Teuku Fawzul Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "13309080"
        }, {
            "id": "1857",
            "name": "Muhammad Artono",
            "prefix": null,
            "suffix": null,
            "nim": "13309082"
        }, {
            "id": "1858",
            "name": "Bintang Dwita Widhana",
            "prefix": null,
            "suffix": null,
            "nim": "13309084"
        }, {
            "id": "1859",
            "name": "Evan Clearesta",
            "prefix": null,
            "suffix": null,
            "nim": "13309086"
        }, {
            "id": "1860",
            "name": "Albert Soputra",
            "prefix": null,
            "suffix": null,
            "nim": "13309094"
        }, {
            "id": "1861",
            "name": "Manoressy Tobias",
            "prefix": null,
            "suffix": null,
            "nim": "13309096"
        }, {
            "id": "1862",
            "name": "Alkindi Rizky D",
            "prefix": null,
            "suffix": null,
            "nim": "13309098"
        }, {
            "id": "1863",
            "name": "Gede Indera Wirakusuma",
            "prefix": null,
            "suffix": null,
            "nim": "13309106"
        }, {
            "id": "1864",
            "name": "M Suryo Kusumo",
            "prefix": null,
            "suffix": null,
            "nim": "13309110"
        }, {
            "id": "1865",
            "name": "Pradita O H",
            "prefix": null,
            "suffix": null,
            "nim": "18008026"
        }, {
            "id": "1866",
            "name": "Ayu3 Purwarianti",
            "prefix": "Dr.",
            "suffix": "",
            "nim": null
        }, {
            "id": "1867",
            "name": "Adisti Ratri Wibowo",
            "prefix": null,
            "suffix": null,
            "nim": "16415002"
        }, {
            "id": "1868",
            "name": "Vincent Livanrio",
            "prefix": null,
            "suffix": null,
            "nim": "16415006"
        }, {
            "id": "1869",
            "name": "Syiaudi Maghfira",
            "prefix": null,
            "suffix": null,
            "nim": "16415010"
        }, {
            "id": "1870",
            "name": "Achmad Ridwan Chaniago",
            "prefix": null,
            "suffix": null,
            "nim": "16415014"
        }, {
            "id": "1871",
            "name": "lrfandy Rayhan Caesario",
            "prefix": null,
            "suffix": null,
            "nim": "16415018"
        }, {
            "id": "1872",
            "name": "Enrico Gilrandy Wahyu Suharjo",
            "prefix": null,
            "suffix": null,
            "nim": "16415022"
        }, {
            "id": "1873",
            "name": "Komang Widhi Widantha",
            "prefix": null,
            "suffix": null,
            "nim": "16415026"
        }, {
            "id": "1874",
            "name": "Brian Dwijayanto",
            "prefix": null,
            "suffix": null,
            "nim": "16415030"
        }, {
            "id": "1875",
            "name": "Sarifah Ulfa Sahubawa",
            "prefix": null,
            "suffix": null,
            "nim": "16415034"
        }, {
            "id": "1876",
            "name": "Prihita Eksi Cahyandari",
            "prefix": null,
            "suffix": null,
            "nim": "16415038"
        }, {
            "id": "1877",
            "name": "Faradilla Thelma Ariesti",
            "prefix": null,
            "suffix": null,
            "nim": "16415042"
        }, {
            "id": "1878",
            "name": "Alfredo Marciano Zola",
            "prefix": null,
            "suffix": null,
            "nim": "16415046"
        }, {
            "id": "1879",
            "name": "Axel Perwira lndro",
            "prefix": null,
            "suffix": null,
            "nim": "16415050"
        }, {
            "id": "1880",
            "name": "Talitha Sheilla Chrisnawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415054"
        }, {
            "id": "1881",
            "name": "Natasya Emeralda Odysse",
            "prefix": null,
            "suffix": null,
            "nim": "16415058"
        }, {
            "id": "1882",
            "name": "Fahrizal Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16415062"
        }, {
            "id": "1883",
            "name": "Krisnatin Nasiroh",
            "prefix": null,
            "suffix": null,
            "nim": "16415066"
        }, {
            "id": "1884",
            "name": "Niko Fani",
            "prefix": null,
            "suffix": null,
            "nim": "16415070"
        }, {
            "id": "1885",
            "name": "Muhammad Ulil Albab",
            "prefix": null,
            "suffix": null,
            "nim": "16415074"
        }, {
            "id": "1886",
            "name": "Bayu Aji Purbantanu",
            "prefix": null,
            "suffix": null,
            "nim": "16415078"
        }, {
            "id": "1887",
            "name": "Samuel Edwa rd lvander",
            "prefix": null,
            "suffix": null,
            "nim": "16415082"
        }, {
            "id": "1888",
            "name": "I Putu Arya Aditya Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16415086"
        }, {
            "id": "1889",
            "name": "Janet Jessica Devira Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415090"
        }, {
            "id": "1890",
            "name": "Utari Julia Azwar",
            "prefix": null,
            "suffix": null,
            "nim": "16415094"
        }, {
            "id": "1891",
            "name": "Richardus Adi Sanjaya",
            "prefix": null,
            "suffix": null,
            "nim": "16415098"
        }, {
            "id": "1892",
            "name": "Nikolas Nico Calvin Alexandra",
            "prefix": null,
            "suffix": null,
            "nim": "16415102"
        }, {
            "id": "1893",
            "name": "Widya Fath Mamerda",
            "prefix": null,
            "suffix": null,
            "nim": "16415106"
        }, {
            "id": "1894",
            "name": "Rizki Adi Pamungkas",
            "prefix": null,
            "suffix": null,
            "nim": "16415110"
        }, {
            "id": "1895",
            "name": "Alamkelana lndraputra",
            "prefix": null,
            "suffix": null,
            "nim": "16415114"
        }, {
            "id": "1896",
            "name": "Muhammad Amin Tahta",
            "prefix": null,
            "suffix": null,
            "nim": "16415118"
        }, {
            "id": "1897",
            "name": "Ryan Setyabudi",
            "prefix": null,
            "suffix": null,
            "nim": "16415122"
        }, {
            "id": "1898",
            "name": "M.alfiza Farhan",
            "prefix": null,
            "suffix": null,
            "nim": "16415126"
        }, {
            "id": "1899",
            "name": "Joshua Febrian",
            "prefix": null,
            "suffix": null,
            "nim": "16415130"
        }, {
            "id": "1900",
            "name": "Andira Wulandari",
            "prefix": null,
            "suffix": null,
            "nim": "16415134"
        }, {
            "id": "1901",
            "name": "Feri Vernando Salim",
            "prefix": null,
            "suffix": null,
            "nim": "16415138"
        }, {
            "id": "1902",
            "name": "Akhlish Muhib Mushhafi",
            "prefix": null,
            "suffix": null,
            "nim": "16415142"
        }, {
            "id": "1903",
            "name": "Fadhlan Hafizh Permana",
            "prefix": null,
            "suffix": null,
            "nim": "16415146"
        }, {
            "id": "1904",
            "name": "Abraham Lungguh Prayoga",
            "prefix": null,
            "suffix": null,
            "nim": "16415150"
        }, {
            "id": "1905",
            "name": "Fashli Adli Wallkhsan",
            "prefix": null,
            "suffix": null,
            "nim": "16415154"
        }, {
            "id": "1906",
            "name": "Faridh Afdhal Aziz",
            "prefix": null,
            "suffix": null,
            "nim": "16415158"
        }, {
            "id": "1907",
            "name": "Ainu Athifah Riezkie Aldhiada",
            "prefix": null,
            "suffix": null,
            "nim": "16415162"
        }, {
            "id": "1908",
            "name": "Abiliansyah Fatwa Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415166"
        }, {
            "id": "1909",
            "name": "Saul Sorrenio Ambrosius",
            "prefix": null,
            "suffix": null,
            "nim": "16415170"
        }, {
            "id": "1910",
            "name": "Ahmad Abyan llmanto",
            "prefix": null,
            "suffix": null,
            "nim": "16415174"
        }, {
            "id": "1911",
            "name": "Michelle Santoso",
            "prefix": null,
            "suffix": null,
            "nim": "16415178"
        }, {
            "id": "1912",
            "name": "Alit Azfar Badaruddin",
            "prefix": null,
            "suffix": null,
            "nim": "16415182"
        }, {
            "id": "1913",
            "name": "Kandaka Wusko",
            "prefix": null,
            "suffix": null,
            "nim": "16415186"
        }, {
            "id": "1914",
            "name": "Silvia Ayu Agatha",
            "prefix": null,
            "suffix": null,
            "nim": "16415190"
        }, {
            "id": "1915",
            "name": "Prima Widianto Mozef",
            "prefix": null,
            "suffix": null,
            "nim": "16415194"
        }, {
            "id": "1916",
            "name": "Yessy  Dewandari Kusuma Wardani",
            "prefix": null,
            "suffix": null,
            "nim": "16415198"
        }, {
            "id": "1917",
            "name": "Rayhan Yazid Thalib",
            "prefix": null,
            "suffix": null,
            "nim": "16415202"
        }, {
            "id": "1918",
            "name": "Sherly",
            "prefix": null,
            "suffix": null,
            "nim": "16415206"
        }, {
            "id": "1919",
            "name": "Sherwin Kooswara",
            "prefix": null,
            "suffix": null,
            "nim": "16415210"
        }, {
            "id": "1920",
            "name": "Alief Gendis Mawardani",
            "prefix": null,
            "suffix": null,
            "nim": "16415214"
        }, {
            "id": "1921",
            "name": "Mohamad Choirul Anwar",
            "prefix": null,
            "suffix": null,
            "nim": "16415218"
        }, {
            "id": "1922",
            "name": "Rizky Arif Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415222"
        }, {
            "id": "1923",
            "name": "Iqbal Ridalta Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415226"
        }, {
            "id": "1924",
            "name": "Ahmad Rafi",
            "prefix": null,
            "suffix": null,
            "nim": "16415230"
        }, {
            "id": "1925",
            "name": "Ganesha Gajah",
            "prefix": null,
            "suffix": null,
            "nim": "16415234"
        }, {
            "id": "1926",
            "name": "Jefri Mycell Anju lumban Gaol",
            "prefix": null,
            "suffix": null,
            "nim": "16415238"
        }, {
            "id": "1927",
            "name": "Tio Gefien lmami",
            "prefix": null,
            "suffix": null,
            "nim": "16415242"
        }, {
            "id": "1928",
            "name": "Muhammad Ramadi",
            "prefix": null,
            "suffix": null,
            "nim": "16415246"
        }, {
            "id": "1929",
            "name": "Faqrin Rukyat Fauzi",
            "prefix": null,
            "suffix": null,
            "nim": "16415250"
        }, {
            "id": "1930",
            "name": "Raden Bagus Arif lndradi Edija",
            "prefix": null,
            "suffix": null,
            "nim": "16415254"
        }, {
            "id": "1931",
            "name": "Meilani Sri Suryani",
            "prefix": null,
            "suffix": null,
            "nim": "16415258"
        }, {
            "id": "1932",
            "name": "Abed Nego Silaban",
            "prefix": null,
            "suffix": null,
            "nim": "16415262"
        }, {
            "id": "1933",
            "name": "Alief Zaky Taftazanni",
            "prefix": null,
            "suffix": null,
            "nim": "16415266"
        }, {
            "id": "1934",
            "name": "Dave Deraldy",
            "prefix": null,
            "suffix": null,
            "nim": "16415270"
        }, {
            "id": "1935",
            "name": "Annisa Nurmala",
            "prefix": null,
            "suffix": null,
            "nim": "16415274"
        }, {
            "id": "1936",
            "name": "Florencia Pauli Siahaan",
            "prefix": null,
            "suffix": null,
            "nim": "16415278"
        }, {
            "id": "1937",
            "name": "Ghalib Sima Gema Ramadhan",
            "prefix": null,
            "suffix": null,
            "nim": "16415282"
        }, {
            "id": "1938",
            "name": "Maulana lnsan Kamil",
            "prefix": null,
            "suffix": null,
            "nim": "16415286"
        }, {
            "id": "1939",
            "name": "Rayhan Aulia Rashqi",
            "prefix": null,
            "suffix": null,
            "nim": "16415290"
        }, {
            "id": "1940",
            "name": "Wisam Badar",
            "prefix": null,
            "suffix": null,
            "nim": "16415294"
        }, {
            "id": "1941",
            "name": "Kevin Mega Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415298"
        }, {
            "id": "1942",
            "name": "Asriya Helfindo Rioardiansa",
            "prefix": null,
            "suffix": null,
            "nim": "16415302"
        }, {
            "id": "1943",
            "name": "Oei, Airy Utomo",
            "prefix": null,
            "suffix": null,
            "nim": "16415306"
        }, {
            "id": "1944",
            "name": "Azhar Harisandi",
            "prefix": null,
            "suffix": null,
            "nim": "16415310"
        }, {
            "id": "1945",
            "name": "Khalid Umar",
            "prefix": null,
            "suffix": null,
            "nim": "16415314"
        }, {
            "id": "1946",
            "name": "Andika Febrian",
            "prefix": null,
            "suffix": null,
            "nim": "16415318"
        }, {
            "id": "1947",
            "name": "Mhd Managor Satiafdi Hasibuan",
            "prefix": null,
            "suffix": null,
            "nim": "16415322"
        }, {
            "id": "1948",
            "name": "Aidil Hidayatul Azhar",
            "prefix": null,
            "suffix": null,
            "nim": "16415326"
        }, {
            "id": "1949",
            "name": "Jeremy Bona Carlo",
            "prefix": null,
            "suffix": null,
            "nim": "16415330"
        }, {
            "id": "1950",
            "name": "Thanthawi",
            "prefix": null,
            "suffix": null,
            "nim": "16415334"
        }, {
            "id": "1951",
            "name": "Novriwanda",
            "prefix": null,
            "suffix": null,
            "nim": "16415338"
        }, {
            "id": "1952",
            "name": "Hilman Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "16415342"
        }, {
            "id": "1953",
            "name": "Rahman Nur Hakim",
            "prefix": null,
            "suffix": null,
            "nim": "16414002"
        }, {
            "id": "1954",
            "name": "Aldo",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1955",
            "name": "Faqih Ahmad Jamil",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1956",
            "name": "Ikra Setya Utama",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1957",
            "name": "Parakarsa Mulyo",
            "prefix": null,
            "suffix": null,
            "nim": "16414018"
        }, {
            "id": "1958",
            "name": "Yafet",
            "prefix": null,
            "suffix": null,
            "nim": "16414022"
        }, {
            "id": "1959",
            "name": "Rai Sudha Prabawa",
            "prefix": null,
            "suffix": null,
            "nim": "16414026"
        }, {
            "id": "1960",
            "name": "Pandu Kharisma",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1961",
            "name": "Muhammad5 Riski Afdol",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1962",
            "name": "Karuniawan Firianto",
            "prefix": null,
            "suffix": null,
            "nim": "16414038"
        }, {
            "id": "1963",
            "name": "Usman Faiz",
            "prefix": null,
            "suffix": null,
            "nim": "16414042"
        }, {
            "id": "1964",
            "name": "Rofikul Umam",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1965",
            "name": "Rafierdy Muhammad",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1966",
            "name": "Joreinhard RM",
            "prefix": null,
            "suffix": null,
            "nim": "16414054"
        }, {
            "id": "1967",
            "name": "Radifan Taufiqul Hafizh",
            "prefix": null,
            "suffix": null,
            "nim": "16414058"
        }, {
            "id": "1968",
            "name": "Ahmad Ikhbal Zamzammi",
            "prefix": null,
            "suffix": null,
            "nim": "16414062"
        }, {
            "id": "1969",
            "name": "Ihsanul Qolby",
            "prefix": null,
            "suffix": null,
            "nim": "16414066"
        }, {
            "id": "1970",
            "name": "Jeremiah Haposan",
            "prefix": null,
            "suffix": null,
            "nim": "16414070"
        }, {
            "id": "1971",
            "name": "Abdel Jawad Shodiq",
            "prefix": null,
            "suffix": null,
            "nim": "16414074"
        }, {
            "id": "1972",
            "name": "Alfin Rizqiadi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1973",
            "name": "Kevin3 Kashikoi Naiborhu",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1974",
            "name": "Guruh Diki Prawoto",
            "prefix": null,
            "suffix": null,
            "nim": "16414086"
        }, {
            "id": "1975",
            "name": "Yanuar Ayu Widowati",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1976",
            "name": "Brigitha Amelinda Deviana",
            "prefix": null,
            "suffix": null,
            "nim": "16414094"
        }, {
            "id": "1977",
            "name": "Agus Dwi Sasongko",
            "prefix": null,
            "suffix": null,
            "nim": "16414098"
        }, {
            "id": "1978",
            "name": "Aulia Ahmad Naufal",
            "prefix": null,
            "suffix": null,
            "nim": "16414102"
        }, {
            "id": "1979",
            "name": "Sri Devi",
            "prefix": null,
            "suffix": null,
            "nim": "16414106"
        }, {
            "id": "1980",
            "name": "Ahmad Nizhom",
            "prefix": null,
            "suffix": null,
            "nim": "16414110"
        }, {
            "id": "1981",
            "name": "Wingky Suganda",
            "prefix": null,
            "suffix": null,
            "nim": "16414114"
        }, {
            "id": "1982",
            "name": "Gilang Audi Pahlevi",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1983",
            "name": "Sulfa Ratna Sari",
            "prefix": null,
            "suffix": null,
            "nim": "16414122"
        }, {
            "id": "1984",
            "name": "Valentina Maria Cecilia",
            "prefix": null,
            "suffix": null,
            "nim": "16414126"
        }, {
            "id": "1985",
            "name": "Novrisal Prasetya",
            "prefix": null,
            "suffix": null,
            "nim": "16414130"
        }, {
            "id": "1986",
            "name": "Nuradiva Iqbal Rahmansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16414134"
        }, {
            "id": "1987",
            "name": "Andre Yudhistika",
            "prefix": null,
            "suffix": null,
            "nim": "16414138"
        }, {
            "id": "1988",
            "name": "Jonathan Filbert",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1989",
            "name": "Harris Pramana",
            "prefix": null,
            "suffix": null,
            "nim": "16414146"
        }, {
            "id": "1990",
            "name": "Kharullah Sastradinata",
            "prefix": null,
            "suffix": null,
            "nim": "16414150"
        }, {
            "id": "1991",
            "name": "Supradona Sinaga",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1992",
            "name": "Aldy Kurnia Ramadhan",
            "prefix": null,
            "suffix": null,
            "nim": "16414158"
        }, {
            "id": "1993",
            "name": "Ebenezer Solin",
            "prefix": null,
            "suffix": null,
            "nim": "16414162"
        }, {
            "id": "1994",
            "name": "Genting Sholata Sya",
            "prefix": null,
            "suffix": null,
            "nim": "16414166"
        }, {
            "id": "1995",
            "name": "Rendy Tandela",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1996",
            "name": "Yeremia Immanuel",
            "prefix": null,
            "suffix": null,
            "nim": "16414174"
        }, {
            "id": "1997",
            "name": "Mohammad Galang Merdeka",
            "prefix": null,
            "suffix": null,
            "nim": "16414178"
        }, {
            "id": "1998",
            "name": "Muhammad6 Alen Irnawan B",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "1999",
            "name": "Aria Widhi Baskara",
            "prefix": null,
            "suffix": null,
            "nim": "16414186"
        }, {
            "id": "2000",
            "name": "Ari Fakhtur Arvan",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2001",
            "name": "Ramadhani",
            "prefix": null,
            "suffix": null,
            "nim": "16414194"
        }, {
            "id": "2002",
            "name": "Victor Suganda Rulie",
            "prefix": null,
            "suffix": null,
            "nim": "16414198"
        }, {
            "id": "2003",
            "name": "Muhammad Ihsan Rezqa",
            "prefix": null,
            "suffix": null,
            "nim": "16414202"
        }, {
            "id": "2004",
            "name": "Firah Risallah",
            "prefix": null,
            "suffix": null,
            "nim": "16414206"
        }, {
            "id": "2005",
            "name": "Brantyo L",
            "prefix": null,
            "suffix": null,
            "nim": "16414210"
        }, {
            "id": "2006",
            "name": "Maria Pradnya Paramita",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2007",
            "name": "Fionna Clarissa Muharlie",
            "prefix": null,
            "suffix": null,
            "nim": "16414218"
        }, {
            "id": "2008",
            "name": "Alvian Firman Apriliawan",
            "prefix": null,
            "suffix": null,
            "nim": "16414222"
        }, {
            "id": "2009",
            "name": "Eigenia Saras ",
            "prefix": null,
            "suffix": null,
            "nim": "16414226"
        }, {
            "id": "2010",
            "name": "Satria Yudhistira",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2011",
            "name": "Firas Ala Zagarino",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2012",
            "name": "Safira Sabilla R",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2013",
            "name": "Aldo Kho",
            "prefix": null,
            "suffix": null,
            "nim": "16414242"
        }, {
            "id": "2014",
            "name": "Ardianto",
            "prefix": null,
            "suffix": null,
            "nim": "16414246"
        }, {
            "id": "2015",
            "name": "M. Farhan Muzzammil",
            "prefix": null,
            "suffix": null,
            "nim": "16414250"
        }, {
            "id": "2016",
            "name": "Waskito Aji",
            "prefix": null,
            "suffix": null,
            "nim": "16414254"
        }, {
            "id": "2017",
            "name": "Okta Indah Sulistyorini",
            "prefix": null,
            "suffix": null,
            "nim": "16414258"
        }, {
            "id": "2018",
            "name": "Harry Kusuma",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2019",
            "name": "Enrico B Purba",
            "prefix": null,
            "suffix": null,
            "nim": "16414266"
        }, {
            "id": "2020",
            "name": "Nur Alim Kadir",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2021",
            "name": "Chairul Rajib",
            "prefix": null,
            "suffix": null,
            "nim": "16414274"
        }, {
            "id": "2022",
            "name": "Abi Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2023",
            "name": "Dek Lanang Sanjivani",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2024",
            "name": "Regidestyoko Wasistha Harseno",
            "prefix": null,
            "suffix": null,
            "nim": "16414286"
        }, {
            "id": "2025",
            "name": "Janet Ramot Esmeralda",
            "prefix": null,
            "suffix": null,
            "nim": "16414290"
        }, {
            "id": "2026",
            "name": "Prawira Nusantara",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2027",
            "name": "Nandito Davy",
            "prefix": null,
            "suffix": null,
            "nim": "16414298"
        }, {
            "id": "2028",
            "name": "Aisyah Setia Wardani",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2029",
            "name": "Wilson Wiranda",
            "prefix": null,
            "suffix": null,
            "nim": "16414306"
        }, {
            "id": "2030",
            "name": "Hazmi Andreanur ",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2031",
            "name": "Wildan Luthfi Irsyad",
            "prefix": null,
            "suffix": null,
            "nim": "16414314"
        }, {
            "id": "2032",
            "name": "Ulfah Indah Safitri",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2033",
            "name": "Ryan Yoga Pratama",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2034",
            "name": "Deny Candra Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16414326"
        }, {
            "id": "2035",
            "name": "Galih Whisnu Romantyo",
            "prefix": null,
            "suffix": null,
            "nim": "16414330"
        }, {
            "id": "2036",
            "name": "Nur Eko Abdurrahman",
            "prefix": null,
            "suffix": null,
            "nim": "16414334"
        }, {
            "id": "2078",
            "name": "Rizkia Nur Aulia",
            "prefix": null,
            "suffix": null,
            "nim": "15311012"
        }, {
            "id": "2079",
            "name": "Marissa Rakhma Sofiani",
            "prefix": null,
            "suffix": null,
            "nim": "15311066"
        }, {
            "id": "2080",
            "name": "Mega Senoputri",
            "prefix": null,
            "suffix": null,
            "nim": "11512020"
        }, {
            "id": "2081",
            "name": "Sisca Tri Wijayanti",
            "prefix": null,
            "suffix": null,
            "nim": "11512021"
        }, {
            "id": "2082",
            "name": "Edinatha Silitonga",
            "prefix": null,
            "suffix": null,
            "nim": "11512050"
        }, {
            "id": "2083",
            "name": "Amelia Mustika K",
            "prefix": null,
            "suffix": null,
            "nim": "12910013"
        }, {
            "id": "2084",
            "name": "Fuad Asyifa",
            "prefix": null,
            "suffix": null,
            "nim": "12910022"
        }, {
            "id": "2085",
            "name": "Adrian Hendra",
            "prefix": null,
            "suffix": null,
            "nim": "12910027"
        }, {
            "id": "2086",
            "name": "Glorious Christian",
            "prefix": null,
            "suffix": null,
            "nim": "12910030"
        }, {
            "id": "2087",
            "name": "Yasmin Nabilla",
            "prefix": null,
            "suffix": null,
            "nim": "12910034"
        }, {
            "id": "2088",
            "name": "Joshua",
            "prefix": null,
            "suffix": null,
            "nim": "12011084"
        }, {
            "id": "2089",
            "name": "Yoga  Prasetio",
            "prefix": null,
            "suffix": null,
            "nim": "12011045"
        }, {
            "id": "2090",
            "name": "Song Cheor Vi",
            "prefix": null,
            "suffix": null,
            "nim": "12114666"
        }, {
            "id": "2091",
            "name": "Park Ji Won",
            "prefix": null,
            "suffix": null,
            "nim": "12114667"
        }, {
            "id": "2092",
            "name": "Kim In Cheon",
            "prefix": null,
            "suffix": null,
            "nim": "12114668"
        }, {
            "id": "2093",
            "name": "Dini Andriani",
            "prefix": null,
            "suffix": null,
            "nim": "16415001"
        }, {
            "id": "2094",
            "name": "Daniel Roberto Simamora",
            "prefix": null,
            "suffix": null,
            "nim": "16415005"
        }, {
            "id": "2095",
            "name": "Achmad Rafiq Alfaruqi",
            "prefix": null,
            "suffix": null,
            "nim": "16415009"
        }, {
            "id": "2096",
            "name": "Maria Yohana Togimarito Harli",
            "prefix": null,
            "suffix": null,
            "nim": "16415013"
        }, {
            "id": "2097",
            "name": "Fauzia Ayu Ramadhanti",
            "prefix": null,
            "suffix": null,
            "nim": "16415017"
        }, {
            "id": "2098",
            "name": "Yongki Alek Sander",
            "prefix": null,
            "suffix": null,
            "nim": "16415021"
        }, {
            "id": "2099",
            "name": "Fajar Nurbehaqi",
            "prefix": null,
            "suffix": null,
            "nim": "16415025"
        }, {
            "id": "2100",
            "name": "Rifa Khoirul Muqtafa",
            "prefix": null,
            "suffix": null,
            "nim": "16415029"
        }, {
            "id": "2101",
            "name": "Herianto",
            "prefix": null,
            "suffix": null,
            "nim": "16415033"
        }, {
            "id": "2102",
            "name": "Yohanes Nuwara",
            "prefix": null,
            "suffix": null,
            "nim": "16415037"
        }, {
            "id": "2103",
            "name": "Christofer Devlin",
            "prefix": null,
            "suffix": null,
            "nim": "16415041"
        }, {
            "id": "2104",
            "name": "Three Sebastianus Sihaloho",
            "prefix": null,
            "suffix": null,
            "nim": "16415045"
        }, {
            "id": "2105",
            "name": "David Kristianto",
            "prefix": null,
            "suffix": null,
            "nim": "16415049"
        }, {
            "id": "2106",
            "name": "Alvin Adi Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16415053"
        }, {
            "id": "2107",
            "name": "Dennas Rizky Fauzan",
            "prefix": null,
            "suffix": null,
            "nim": "16415057"
        }, {
            "id": "2108",
            "name": "Nur Fasya Febriarti",
            "prefix": null,
            "suffix": null,
            "nim": "16415061"
        }, {
            "id": "2109",
            "name": "Saleh Parningotan Siahaan",
            "prefix": null,
            "suffix": null,
            "nim": "16415065"
        }, {
            "id": "2110",
            "name": "Michael Muhammad Somantri",
            "prefix": null,
            "suffix": null,
            "nim": "16415069"
        }, {
            "id": "2111",
            "name": "Muhammad Ghani Hidayatullah",
            "prefix": null,
            "suffix": null,
            "nim": "16415073"
        }, {
            "id": "2112",
            "name": "Mochammad Randy Caesario Harsu",
            "prefix": null,
            "suffix": null,
            "nim": "16415077"
        }, {
            "id": "2113",
            "name": "Muhammad Hafiyyan Fikri",
            "prefix": null,
            "suffix": null,
            "nim": "16415081"
        }, {
            "id": "2114",
            "name": "Batara Pande",
            "prefix": null,
            "suffix": null,
            "nim": "16415085"
        }, {
            "id": "2115",
            "name": "Dofi Kurniawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415089"
        }, {
            "id": "2116",
            "name": "Almira Putri Oldia",
            "prefix": null,
            "suffix": null,
            "nim": "16415093"
        }, {
            "id": "2117",
            "name": "Fitriah Nuraeni",
            "prefix": null,
            "suffix": null,
            "nim": "16415097"
        }, {
            "id": "2118",
            "name": "Alandika Kharis Prasetya Adhi",
            "prefix": null,
            "suffix": null,
            "nim": "16415101"
        }, {
            "id": "2119",
            "name": "Feri Irawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415105"
        }, {
            "id": "2120",
            "name": "Yesaya Arison Haratua",
            "prefix": null,
            "suffix": null,
            "nim": "16415109"
        }, {
            "id": "2121",
            "name": "Agung Aji Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "16415113"
        }, {
            "id": "2122",
            "name": "Muhammad Harits Hafidza",
            "prefix": null,
            "suffix": null,
            "nim": "16415117"
        }, {
            "id": "2123",
            "name": "Rafif Abdus Salam",
            "prefix": null,
            "suffix": null,
            "nim": "16415121"
        }, {
            "id": "2124",
            "name": "Michael Aria Santoso",
            "prefix": null,
            "suffix": null,
            "nim": "16415125"
        }, {
            "id": "2125",
            "name": "Mas Rochmatan",
            "prefix": null,
            "suffix": null,
            "nim": "16415129"
        }, {
            "id": "2126",
            "name": "Jefri Bambang Irawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415133"
        }, {
            "id": "2127",
            "name": "David Jonathan",
            "prefix": null,
            "suffix": null,
            "nim": "16415137"
        }, {
            "id": "2128",
            "name": "Rifky Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16415141"
        }, {
            "id": "2129",
            "name": "Arnold Sahat Ferry Manullang",
            "prefix": null,
            "suffix": null,
            "nim": "16415145"
        }, {
            "id": "2130",
            "name": "Rafli Herdiansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16415149"
        }, {
            "id": "2131",
            "name": "Moh. Fairuzzabaady",
            "prefix": null,
            "suffix": null,
            "nim": "16415153"
        }, {
            "id": "2132",
            "name": "Rifki Muhammad Rashad",
            "prefix": null,
            "suffix": null,
            "nim": "16415157"
        }, {
            "id": "2133",
            "name": "Abdel Mohammad Deghati",
            "prefix": null,
            "suffix": null,
            "nim": "16415161"
        }, {
            "id": "2134",
            "name": "David Djabar Immanuel",
            "prefix": null,
            "suffix": null,
            "nim": "16415165"
        }, {
            "id": "2135",
            "name": "Muhammad Fikri Fauzan Hasrul",
            "prefix": null,
            "suffix": null,
            "nim": "16415169"
        }, {
            "id": "2136",
            "name": "Luna Laurenzha",
            "prefix": null,
            "suffix": null,
            "nim": "16415173"
        }, {
            "id": "2137",
            "name": "Isna Nuraini",
            "prefix": null,
            "suffix": null,
            "nim": "16415177"
        }, {
            "id": "2138",
            "name": "R. Aulia Muhammad Rizky",
            "prefix": null,
            "suffix": null,
            "nim": "16415181"
        }, {
            "id": "2139",
            "name": "Febriani Fitria Rahmawati",
            "prefix": null,
            "suffix": null,
            "nim": "16415185"
        }, {
            "id": "2140",
            "name": "Sindi Nabila Rusfa",
            "prefix": null,
            "suffix": null,
            "nim": "16415189"
        }, {
            "id": "2141",
            "name": "Dimas Muhammad Zulfikar",
            "prefix": null,
            "suffix": null,
            "nim": "16415193"
        }, {
            "id": "2142",
            "name": "Amelia Gabriella Zebua",
            "prefix": null,
            "suffix": null,
            "nim": "16415197"
        }, {
            "id": "2143",
            "name": "Raihan Irfan Kosasih",
            "prefix": null,
            "suffix": null,
            "nim": "16415201"
        }, {
            "id": "2144",
            "name": "Budi Prayoga",
            "prefix": null,
            "suffix": null,
            "nim": "16415205"
        }, {
            "id": "2145",
            "name": "Mahendra Dwi Syaifullah",
            "prefix": null,
            "suffix": null,
            "nim": "16415209"
        }, {
            "id": "2146",
            "name": "Ronaldo Hamonangan Tua",
            "prefix": null,
            "suffix": null,
            "nim": "16415213"
        }, {
            "id": "2147",
            "name": "Adrian Bonaventura",
            "prefix": null,
            "suffix": null,
            "nim": "16415217"
        }, {
            "id": "2148",
            "name": "Andrean Firnanto",
            "prefix": null,
            "suffix": null,
            "nim": "16415221"
        }, {
            "id": "2149",
            "name": "Indyra Prameswari",
            "prefix": null,
            "suffix": null,
            "nim": "16415225"
        }, {
            "id": "2150",
            "name": "Naura Tsabita Fadjar",
            "prefix": null,
            "suffix": null,
            "nim": "16415229"
        }, {
            "id": "2151",
            "name": "Teja Nugraha Mahaputra Perdana",
            "prefix": null,
            "suffix": null,
            "nim": "16415233"
        }, {
            "id": "2152",
            "name": "Philip Angsetya",
            "prefix": null,
            "suffix": null,
            "nim": "16415237"
        }, {
            "id": "2153",
            "name": "Kevyn Augusta Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16415241"
        }, {
            "id": "2154",
            "name": "Atrian Rahadi",
            "prefix": null,
            "suffix": null,
            "nim": "16415245"
        }, {
            "id": "2155",
            "name": "Michael Jhonson Arnold",
            "prefix": null,
            "suffix": null,
            "nim": "16415249"
        }, {
            "id": "2156",
            "name": "Jonathan Edwin",
            "prefix": null,
            "suffix": null,
            "nim": "16415253"
        }, {
            "id": "2157",
            "name": "Alya Tathira",
            "prefix": null,
            "suffix": null,
            "nim": "16415257"
        }, {
            "id": "2158",
            "name": "Christian Abdiwijoyo Henry",
            "prefix": null,
            "suffix": null,
            "nim": "16415261"
        }, {
            "id": "2159",
            "name": "Albert Agatha Putra Gultom",
            "prefix": null,
            "suffix": null,
            "nim": "16415265"
        }, {
            "id": "2160",
            "name": "Made Ray Yuda Suyatna",
            "prefix": null,
            "suffix": null,
            "nim": "16415269"
        }, {
            "id": "2161",
            "name": "Ali Husain Taherdito",
            "prefix": null,
            "suffix": null,
            "nim": "16415273"
        }, {
            "id": "2162",
            "name": "Fithratullah Habibie",
            "prefix": null,
            "suffix": null,
            "nim": "16415277"
        }, {
            "id": "2163",
            "name": "Safira Andriani",
            "prefix": null,
            "suffix": null,
            "nim": "16415281"
        }, {
            "id": "2164",
            "name": "Denny Van Anggara",
            "prefix": null,
            "suffix": null,
            "nim": "16415285"
        }, {
            "id": "2165",
            "name": "Ayu Chasanah",
            "prefix": null,
            "suffix": null,
            "nim": "16415289"
        }, {
            "id": "2166",
            "name": "Oktandi Miftahul Syamsiar",
            "prefix": null,
            "suffix": null,
            "nim": "16415293"
        }, {
            "id": "2167",
            "name": "Yuyun Wahyudin",
            "prefix": null,
            "suffix": null,
            "nim": "16415297"
        }, {
            "id": "2168",
            "name": "Fransiskus Ondihon Sitompul",
            "prefix": null,
            "suffix": null,
            "nim": "16415301"
        }, {
            "id": "2169",
            "name": "Maria Indira Puspita Sari",
            "prefix": null,
            "suffix": null,
            "nim": "16415305"
        }, {
            "id": "2170",
            "name": "Ryan Riyadi",
            "prefix": null,
            "suffix": null,
            "nim": "16415309"
        }, {
            "id": "2171",
            "name": "Muhammad Irfan Ibrahim",
            "prefix": null,
            "suffix": null,
            "nim": "16415313"
        }, {
            "id": "2172",
            "name": "Willy Barimbing",
            "prefix": null,
            "suffix": null,
            "nim": "16415317"
        }, {
            "id": "2173",
            "name": "Muhammad Adrian Firdaus",
            "prefix": null,
            "suffix": null,
            "nim": "16415321"
        }, {
            "id": "2174",
            "name": "M Mufid Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16415325"
        }, {
            "id": "2175",
            "name": "Muhammad Arlis",
            "prefix": null,
            "suffix": null,
            "nim": "16415329"
        }, {
            "id": "2176",
            "name": "Septian Hadi Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16415333"
        }, {
            "id": "2177",
            "name": "Hanafi Kristoper Ginting",
            "prefix": null,
            "suffix": null,
            "nim": "16415337"
        }, {
            "id": "2178",
            "name": "Amru Mufid",
            "prefix": null,
            "suffix": null,
            "nim": "16415341"
        }, {
            "id": "2179",
            "name": "Kaleb Eka Satya Yaroseray",
            "prefix": null,
            "suffix": null,
            "nim": "16415345"
        }, {
            "id": "2180",
            "name": "Muhammad Firhan Qinthara ",
            "prefix": null,
            "suffix": null,
            "nim": "16415211"
        }, {
            "id": "2181",
            "name": "Ebbryl Nur Rochman ",
            "prefix": null,
            "suffix": null,
            "nim": "16415215"
        }, {
            "id": "2182",
            "name": "Nursyamhadi Nugroho ",
            "prefix": null,
            "suffix": null,
            "nim": "16415223"
        }, {
            "id": "2183",
            "name": "Dean Ananta Wirya ",
            "prefix": null,
            "suffix": null,
            "nim": "16415227"
        }, {
            "id": "2184",
            "name": "Andang Taruna Putera Wardhana ",
            "prefix": null,
            "suffix": null,
            "nim": "16415231"
        }, {
            "id": "2185",
            "name": "Rifda Dinillah ",
            "prefix": null,
            "suffix": null,
            "nim": "16415235"
        }, {
            "id": "2186",
            "name": "Amos Melanthony Pasaribu ",
            "prefix": null,
            "suffix": null,
            "nim": "16415239"
        }, {
            "id": "2187",
            "name": "Ivan Lesmana ",
            "prefix": null,
            "suffix": null,
            "nim": "16415243"
        }, {
            "id": "2188",
            "name": "Erica Caesariaty",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "2189",
            "name": "Irfan Ibrahim ",
            "prefix": null,
            "suffix": null,
            "nim": "16316005"
        }, {
            "id": "2190",
            "name": "Arlif Nabilatur Rosyidah ",
            "prefix": null,
            "suffix": null,
            "nim": "16316012"
        }, {
            "id": "2191",
            "name": "Emmanuel Yoshua Eleison ",
            "prefix": null,
            "suffix": null,
            "nim": "16316014"
        }, {
            "id": "2192",
            "name": "Muhammad Fachry Pribadi ",
            "prefix": null,
            "suffix": null,
            "nim": "16316015"
        }, {
            "id": "2193",
            "name": "Aulia Dyan Yohanlis ",
            "prefix": null,
            "suffix": null,
            "nim": "16316018"
        }, {
            "id": "2194",
            "name": "Fara Ayu Zaura ",
            "prefix": null,
            "suffix": null,
            "nim": "16316021"
        }, {
            "id": "2195",
            "name": "Venno Akbar Fathurrochman ",
            "prefix": null,
            "suffix": null,
            "nim": "16316026"
        }, {
            "id": "2196",
            "name": "Venry Romual Silaban ",
            "prefix": null,
            "suffix": null,
            "nim": "16316030"
        }, {
            "id": "2197",
            "name": "Aulia Syafitri ",
            "prefix": null,
            "suffix": null,
            "nim": "16316035"
        }, {
            "id": "2198",
            "name": "Endah Nuraswiputri ",
            "prefix": null,
            "suffix": null,
            "nim": "16316041"
        }, {
            "id": "2199",
            "name": "Fadhil Abryanto Nugraha ",
            "prefix": null,
            "suffix": null,
            "nim": "16316044"
        }, {
            "id": "2200",
            "name": "Hafidz Muhammad Yudhaifha ",
            "prefix": null,
            "suffix": null,
            "nim": "16316046"
        }, {
            "id": "2201",
            "name": "Muhammad Sutan Syauqi F G ",
            "prefix": null,
            "suffix": null,
            "nim": "16316055"
        }, {
            "id": "2202",
            "name": "Budianto Santoso ",
            "prefix": null,
            "suffix": null,
            "nim": "16316061"
        }, {
            "id": "2203",
            "name": "Muhammad Rahaldi Taher ",
            "prefix": null,
            "suffix": null,
            "nim": "16316070"
        }, {
            "id": "2204",
            "name": "Virgiawan Putra Mahendra ",
            "prefix": null,
            "suffix": null,
            "nim": "16316076"
        }, {
            "id": "2205",
            "name": "Fitria Maharani ",
            "prefix": null,
            "suffix": null,
            "nim": "16316078"
        }, {
            "id": "2206",
            "name": "Rizky Sergie Harriz ",
            "prefix": null,
            "suffix": null,
            "nim": "16316081"
        }, {
            "id": "2207",
            "name": "Nafandra Syabana Lubis ",
            "prefix": null,
            "suffix": null,
            "nim": "16316086"
        }, {
            "id": "2208",
            "name": "Yehezkiel Festian Prakoso ",
            "prefix": null,
            "suffix": null,
            "nim": "16316097"
        }, {
            "id": "2209",
            "name": "Felicia Indraswari ",
            "prefix": null,
            "suffix": null,
            "nim": "16316098"
        }, {
            "id": "2210",
            "name": "Sayyid Abdullah Marzuqi ",
            "prefix": null,
            "suffix": null,
            "nim": "16316100"
        }, {
            "id": "2211",
            "name": "Muhammad Rizaldi Utomo ",
            "prefix": null,
            "suffix": null,
            "nim": "16316111"
        }, {
            "id": "2212",
            "name": "Shafira Akhiria Hanani ",
            "prefix": null,
            "suffix": null,
            "nim": "16316112"
        }, {
            "id": "2213",
            "name": "Nuruddin Arief Indrajaya ",
            "prefix": null,
            "suffix": null,
            "nim": "16316114"
        }, {
            "id": "2214",
            "name": "Muhammad Ariq Naufal ",
            "prefix": null,
            "suffix": null,
            "nim": "16316117"
        }, {
            "id": "2215",
            "name": "Farizan Muhammad Hashfi ",
            "prefix": null,
            "suffix": null,
            "nim": "16316118"
        }, {
            "id": "2216",
            "name": "Gistya Chairuniza ",
            "prefix": null,
            "suffix": null,
            "nim": "16316121"
        }, {
            "id": "2217",
            "name": "Devito Pradipta ",
            "prefix": null,
            "suffix": null,
            "nim": "16316127"
        }, {
            "id": "2218",
            "name": "Nindy Heryati ",
            "prefix": null,
            "suffix": null,
            "nim": "16316128"
        }, {
            "id": "2219",
            "name": "Thalia Helena Imelda ",
            "prefix": null,
            "suffix": null,
            "nim": "16316131"
        }, {
            "id": "2220",
            "name": "Leonardus Berlianto Setiawan ",
            "prefix": null,
            "suffix": null,
            "nim": "16316136"
        }, {
            "id": "2221",
            "name": "Muhammad Farhan Rizaldi ",
            "prefix": null,
            "suffix": null,
            "nim": "16316137"
        }, {
            "id": "2222",
            "name": "Gregory R N Tampubolon ",
            "prefix": null,
            "suffix": null,
            "nim": "16316141"
        }, {
            "id": "2223",
            "name": "Andrika Brema Ginting ",
            "prefix": null,
            "suffix": null,
            "nim": "16316149"
        }, {
            "id": "2224",
            "name": "Desty Aulia ",
            "prefix": null,
            "suffix": null,
            "nim": "16316152"
        }, {
            "id": "2225",
            "name": "Desy Bunga Sari ",
            "prefix": null,
            "suffix": null,
            "nim": "16316153"
        }, {
            "id": "2226",
            "name": "Dephita Vega Koswara ",
            "prefix": null,
            "suffix": null,
            "nim": "16316158"
        }, {
            "id": "2227",
            "name": "Dede Nurheliza ",
            "prefix": null,
            "suffix": null,
            "nim": "16316168"
        }, {
            "id": "2228",
            "name": "Obedh Try Samekto Sidabutar ",
            "prefix": null,
            "suffix": null,
            "nim": "16316176"
        }, {
            "id": "2229",
            "name": "Ajeng Siti Noorrahmah ",
            "prefix": null,
            "suffix": null,
            "nim": "16316179"
        }, {
            "id": "2230",
            "name": "Ahmad Mujaddid ",
            "prefix": null,
            "suffix": null,
            "nim": "16316183"
        }, {
            "id": "2231",
            "name": "Firdaus El Afghani ",
            "prefix": null,
            "suffix": null,
            "nim": "16316186"
        }, {
            "id": "2232",
            "name": "Saefulloh ",
            "prefix": null,
            "suffix": null,
            "nim": "16316187"
        }, {
            "id": "2233",
            "name": "Saga Maulana ",
            "prefix": null,
            "suffix": null,
            "nim": "16316188"
        }, {
            "id": "2234",
            "name": "Salsabila Syakura ",
            "prefix": null,
            "suffix": null,
            "nim": "16316195"
        }, {
            "id": "2235",
            "name": "Muhammad Refri Ansyari ",
            "prefix": null,
            "suffix": null,
            "nim": "16316198"
        }, {
            "id": "2236",
            "name": "Rafiq Naufal Shidiq ",
            "prefix": null,
            "suffix": null,
            "nim": "16316203"
        }, {
            "id": "2237",
            "name": "Andrianto Mahardika ",
            "prefix": null,
            "suffix": null,
            "nim": "16316204"
        }, {
            "id": "2238",
            "name": "Zenal Mutakin ",
            "prefix": null,
            "suffix": null,
            "nim": "16316205"
        }, {
            "id": "2239",
            "name": "Winda Lestari Siadari ",
            "prefix": null,
            "suffix": null,
            "nim": "16316213"
        }, {
            "id": "2240",
            "name": "Muhammad Marvin Fauzaan ",
            "prefix": null,
            "suffix": null,
            "nim": "16316214"
        }, {
            "id": "2241",
            "name": "Regaska Pudawara ",
            "prefix": null,
            "suffix": null,
            "nim": "16316218"
        }, {
            "id": "2242",
            "name": "Zefanya Abigail Artanauli ",
            "prefix": null,
            "suffix": null,
            "nim": "16316220"
        }, {
            "id": "2243",
            "name": "Angga Pratama ",
            "prefix": null,
            "suffix": null,
            "nim": "16316221"
        }, {
            "id": "2244",
            "name": "Enggartiasto ",
            "prefix": null,
            "suffix": null,
            "nim": "16316230"
        }, {
            "id": "2245",
            "name": "A R Khairun Nisa\' ",
            "prefix": null,
            "suffix": null,
            "nim": "16316231"
        }, {
            "id": "2246",
            "name": "Raihan Arkyandi Djulian ",
            "prefix": null,
            "suffix": null,
            "nim": "16316233"
        }, {
            "id": "2247",
            "name": "Nabila Alfi Al Halimy ",
            "prefix": null,
            "suffix": null,
            "nim": "16316240"
        }, {
            "id": "2248",
            "name": "Gusti Ayu Isma Yanti ",
            "prefix": null,
            "suffix": null,
            "nim": "16316244"
        }, {
            "id": "2249",
            "name": "Farid Wisnu Setyawardana ",
            "prefix": null,
            "suffix": null,
            "nim": "16316248"
        }, {
            "id": "2250",
            "name": "Akhmad Abrar Al-arsyi Hendana ",
            "prefix": null,
            "suffix": null,
            "nim": "16316252"
        }, {
            "id": "2251",
            "name": "Yusron Ma\'ruf ",
            "prefix": null,
            "suffix": null,
            "nim": "16316256"
        }, {
            "id": "2252",
            "name": "Abednego Stanley ",
            "prefix": null,
            "suffix": null,
            "nim": "16316260"
        }, {
            "id": "2253",
            "name": "Muhammad Irham ",
            "prefix": null,
            "suffix": null,
            "nim": "16316264"
        }, {
            "id": "2254",
            "name": "Junaid Daffa Izzudin ",
            "prefix": null,
            "suffix": null,
            "nim": "16316268"
        }, {
            "id": "2255",
            "name": "Adnan Kholif Ma\'ruf",
            "prefix": null,
            "suffix": null,
            "nim": "16416004"
        }, {
            "id": "2256",
            "name": "Muhammad Satrio Dwi Cahya",
            "prefix": null,
            "suffix": null,
            "nim": "16416010"
        }, {
            "id": "2257",
            "name": "Caesar Meilanton L Tobing",
            "prefix": null,
            "suffix": null,
            "nim": "16416013"
        }, {
            "id": "2258",
            "name": "Muhammad Prasetya Dwi Aurianto",
            "prefix": null,
            "suffix": null,
            "nim": "16416017"
        }, {
            "id": "2259",
            "name": "Dinda Nuroktaviani",
            "prefix": null,
            "suffix": null,
            "nim": "16416020"
        }, {
            "id": "2260",
            "name": "Afif Andria Ma\'fu",
            "prefix": null,
            "suffix": null,
            "nim": "16416023"
        }, {
            "id": "2261",
            "name": "Guslinda Permata Sari",
            "prefix": null,
            "suffix": null,
            "nim": "16416026"
        }, {
            "id": "2262",
            "name": "Baskoro Adi Nugroho",
            "prefix": null,
            "suffix": null,
            "nim": "16416030"
        }, {
            "id": "2263",
            "name": "Abdiel Falah Fawwaz",
            "prefix": null,
            "suffix": null,
            "nim": "16416032"
        }, {
            "id": "2264",
            "name": "Sarah Fauziah Nur Aini",
            "prefix": null,
            "suffix": null,
            "nim": "16416034"
        }, {
            "id": "2265",
            "name": "Rahmad Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "16416040"
        }, {
            "id": "2266",
            "name": "Maulana Syifa Nahla Alghifari",
            "prefix": null,
            "suffix": null,
            "nim": "16416045"
        }, {
            "id": "2267",
            "name": "Sylvester Solaiman",
            "prefix": null,
            "suffix": null,
            "nim": "16416047"
        }, {
            "id": "2268",
            "name": "Patrick Ivan",
            "prefix": null,
            "suffix": null,
            "nim": "16416057"
        }, {
            "id": "2269",
            "name": "Shiva Rinelly",
            "prefix": null,
            "suffix": null,
            "nim": "16416058"
        }, {
            "id": "2270",
            "name": "Rahmad Aji Rahadian",
            "prefix": null,
            "suffix": null,
            "nim": "16416062"
        }, {
            "id": "2271",
            "name": "Anthony Kurnia Jaya",
            "prefix": null,
            "suffix": null,
            "nim": "16416066"
        }, {
            "id": "2272",
            "name": "Hadisurya Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16416068"
        }, {
            "id": "2273",
            "name": "Asril Fitrabuana",
            "prefix": null,
            "suffix": null,
            "nim": "16416074"
        }, {
            "id": "2274",
            "name": "Dzaky Ilham Muyasar",
            "prefix": null,
            "suffix": null,
            "nim": "16416077"
        }, {
            "id": "2275",
            "name": "Jay Bianco Sembiring Meliala",
            "prefix": null,
            "suffix": null,
            "nim": "16416086"
        }, {
            "id": "2276",
            "name": "Fandi Akhmad",
            "prefix": null,
            "suffix": null,
            "nim": "16416092"
        }, {
            "id": "2277",
            "name": "Luthfi Hafizh Azhar",
            "prefix": null,
            "suffix": null,
            "nim": "16416094"
        }, {
            "id": "2278",
            "name": "Mancho Resvana",
            "prefix": null,
            "suffix": null,
            "nim": "16416100"
        }, {
            "id": "2279",
            "name": "Muhamad Ababil Akram",
            "prefix": null,
            "suffix": null,
            "nim": "16416101"
        }, {
            "id": "2280",
            "name": "Muhammad Yusril Haidar Hamdani",
            "prefix": null,
            "suffix": null,
            "nim": "16416104"
        }, {
            "id": "2281",
            "name": "Fajar Tri Anggoro",
            "prefix": null,
            "suffix": null,
            "nim": "16416110"
        }, {
            "id": "2282",
            "name": "Johan Iswara Lumban Tungkup",
            "prefix": null,
            "suffix": null,
            "nim": "16416115"
        }, {
            "id": "2283",
            "name": "Vanessa Antovie",
            "prefix": null,
            "suffix": null,
            "nim": "16416120"
        }, {
            "id": "2284",
            "name": "Arzy Maulidianta",
            "prefix": null,
            "suffix": null,
            "nim": "16416121"
        }, {
            "id": "2285",
            "name": "Muhammad Akmal Bil Haq",
            "prefix": null,
            "suffix": null,
            "nim": "16416123"
        }, {
            "id": "2286",
            "name": "Tuah Kudratzat",
            "prefix": null,
            "suffix": null,
            "nim": "16416130"
        }, {
            "id": "2287",
            "name": "Kristoforus Arief Sutandi",
            "prefix": null,
            "suffix": null,
            "nim": "16416136"
        }, {
            "id": "2288",
            "name": "Daryl Gabriel",
            "prefix": null,
            "suffix": null,
            "nim": "16416141"
        }, {
            "id": "2289",
            "name": "Rizka Amalia",
            "prefix": null,
            "suffix": null,
            "nim": "16416145"
        }, {
            "id": "2290",
            "name": "Mohammad Edwin Alif Utama",
            "prefix": null,
            "suffix": null,
            "nim": "16416150"
        }, {
            "id": "2291",
            "name": "Glenn Anderson",
            "prefix": null,
            "suffix": null,
            "nim": "16416153"
        }, {
            "id": "2292",
            "name": "Steffani Kurnia Viona",
            "prefix": null,
            "suffix": null,
            "nim": "16416154"
        }, {
            "id": "2293",
            "name": "Dimas Rezki Al Kafi",
            "prefix": null,
            "suffix": null,
            "nim": "16416160"
        }, {
            "id": "2294",
            "name": "Muhamad Adilko",
            "prefix": null,
            "suffix": null,
            "nim": "16416162"
        }, {
            "id": "2295",
            "name": "Zaid Suhail",
            "prefix": null,
            "suffix": null,
            "nim": "16416164"
        }, {
            "id": "2296",
            "name": "Muhammad Baharuddin Yusup",
            "prefix": null,
            "suffix": null,
            "nim": "16416168"
        }, {
            "id": "2297",
            "name": "Helmi Fairuz Kalahari",
            "prefix": null,
            "suffix": null,
            "nim": "16416182"
        }, {
            "id": "2298",
            "name": "Billy Akbar Prabowo",
            "prefix": null,
            "suffix": null,
            "nim": "16416188"
        }, {
            "id": "2299",
            "name": "Muhammad Ilham Rafii Ramadhan",
            "prefix": null,
            "suffix": null,
            "nim": "16416192"
        }, {
            "id": "2300",
            "name": "Garindra Harvianto",
            "prefix": null,
            "suffix": null,
            "nim": "16416193"
        }, {
            "id": "2301",
            "name": "Erick Wicaksono Ramas",
            "prefix": null,
            "suffix": null,
            "nim": "16416194"
        }, {
            "id": "2302",
            "name": "Muhammad Ardhya Wirananggapati",
            "prefix": null,
            "suffix": null,
            "nim": "16416196"
        }, {
            "id": "2303",
            "name": "Faiz Alfianto",
            "prefix": null,
            "suffix": null,
            "nim": "16416202"
        }, {
            "id": "2304",
            "name": "Wahyu Setiawan",
            "prefix": null,
            "suffix": null,
            "nim": "16416204"
        }, {
            "id": "2305",
            "name": "Reynaldo Billy Towidjojo",
            "prefix": null,
            "suffix": null,
            "nim": "16416218"
        }, {
            "id": "2306",
            "name": "Achmad Wisam Mahmuda",
            "prefix": null,
            "suffix": null,
            "nim": "16416225"
        }, {
            "id": "2307",
            "name": "Vincent Chandra",
            "prefix": null,
            "suffix": null,
            "nim": "16416226"
        }, {
            "id": "2308",
            "name": "Muhammad Zidane Al Bana",
            "prefix": null,
            "suffix": null,
            "nim": "16416227"
        }, {
            "id": "2309",
            "name": "Andre Kurnia Triputra",
            "prefix": null,
            "suffix": null,
            "nim": "16416231"
        }, {
            "id": "2310",
            "name": "Muhammad10 Iqbal",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2311",
            "name": "Geraldo Giovanni Harjono",
            "prefix": null,
            "suffix": null,
            "nim": "16416233"
        }, {
            "id": "2312",
            "name": "Muhamad Iqbal",
            "prefix": null,
            "suffix": null,
            "nim": "16416238"
        }, {
            "id": "2313",
            "name": "Baiq Ramadhani Karina",
            "prefix": null,
            "suffix": null,
            "nim": "16416240"
        }, {
            "id": "2314",
            "name": "Dzaky Irfansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16416243"
        }, {
            "id": "2315",
            "name": "Nurussyifaail Fuadah",
            "prefix": null,
            "suffix": null,
            "nim": "16416244"
        }, {
            "id": "2316",
            "name": "Francis Valavani Setio",
            "prefix": null,
            "suffix": null,
            "nim": "16416245"
        }, {
            "id": "2317",
            "name": "Fatimah Az Zahra",
            "prefix": null,
            "suffix": null,
            "nim": "16416266"
        }, {
            "id": "2318",
            "name": "Tania Galuh Pitaloka",
            "prefix": null,
            "suffix": null,
            "nim": "16416272"
        }, {
            "id": "2319",
            "name": "Arivia Dian Pertiwi",
            "prefix": null,
            "suffix": null,
            "nim": "16416273"
        }, {
            "id": "2320",
            "name": "Luke Baskoro",
            "prefix": null,
            "suffix": null,
            "nim": "16416275"
        }, {
            "id": "2321",
            "name": "Yasmin Meidiana Syarif",
            "prefix": null,
            "suffix": null,
            "nim": "16416276"
        }, {
            "id": "2322",
            "name": "Rosadi Agung Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16416277"
        }, {
            "id": "2323",
            "name": "Azhar Nurhafidh",
            "prefix": null,
            "suffix": null,
            "nim": "16416278"
        }, {
            "id": "2324",
            "name": "Dian Rizqi Khusnul Khotimah",
            "prefix": null,
            "suffix": null,
            "nim": "16416280"
        }, {
            "id": "2325",
            "name": "Mohammad Rizky Tsaury",
            "prefix": null,
            "suffix": null,
            "nim": "16416283"
        }, {
            "id": "2326",
            "name": "Muharrir Azzaki",
            "prefix": null,
            "suffix": null,
            "nim": "16416284"
        }, {
            "id": "2327",
            "name": "Ricky S M Simanjuntak",
            "prefix": null,
            "suffix": null,
            "nim": "16416289"
        }, {
            "id": "2328",
            "name": "Mohammad Hamdan Abdillah",
            "prefix": null,
            "suffix": null,
            "nim": "16416293"
        }, {
            "id": "2329",
            "name": "Muhammad Marwan Aziz",
            "prefix": null,
            "suffix": null,
            "nim": "16416297"
        }, {
            "id": "2330",
            "name": "Aulia Ikhsan Nur Alam",
            "prefix": null,
            "suffix": null,
            "nim": "16416301"
        }, {
            "id": "2331",
            "name": "Muhammad12 Iqbal",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2332",
            "name": "Muhammad Irfan Althoriq",
            "prefix": null,
            "suffix": null,
            "nim": "16416309"
        }, {
            "id": "2333",
            "name": "Fakhmi Imanuddin Prakasa",
            "prefix": null,
            "suffix": null,
            "nim": "16416313"
        }, {
            "id": "2334",
            "name": "Ananda Putra Pamungkas",
            "prefix": null,
            "suffix": null,
            "nim": "16416317"
        }, {
            "id": "2335",
            "name": "Robert Frans Horik",
            "prefix": null,
            "suffix": null,
            "nim": "16416321"
        }, {
            "id": "2336",
            "name": "Cindi Pratiwi",
            "prefix": null,
            "suffix": null,
            "nim": "16716014"
        }, {
            "id": "2337",
            "name": "Haekal Hafidz Nurhadi",
            "prefix": null,
            "suffix": null,
            "nim": "16716021"
        }, {
            "id": "2338",
            "name": "Elvin Buntoro",
            "prefix": null,
            "suffix": null,
            "nim": "16716023"
        }, {
            "id": "2339",
            "name": "Dimas Apeco Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16716025"
        }, {
            "id": "2340",
            "name": "Arya Putra Rizkya Akbar",
            "prefix": null,
            "suffix": null,
            "nim": "16716033"
        }, {
            "id": "2341",
            "name": "Safikri Aji Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16716037"
        }, {
            "id": "2342",
            "name": "Irsyad Ashshiddieqy",
            "prefix": null,
            "suffix": null,
            "nim": "16716048"
        }, {
            "id": "2343",
            "name": "Qusairi Wardana Harahap",
            "prefix": null,
            "suffix": null,
            "nim": "16716052"
        }, {
            "id": "2344",
            "name": "Jakfal Karim",
            "prefix": null,
            "suffix": null,
            "nim": "16716059"
        }, {
            "id": "2345",
            "name": "Jonathan Bryan Cheon",
            "prefix": null,
            "suffix": null,
            "nim": "16716061"
        }, {
            "id": "2346",
            "name": "Lalitya Putri Gitaanjali",
            "prefix": null,
            "suffix": null,
            "nim": "16716068"
        }, {
            "id": "2347",
            "name": "Valentina",
            "prefix": null,
            "suffix": null,
            "nim": "16716073"
        }, {
            "id": "2348",
            "name": "Nurazmi Fauziah",
            "prefix": null,
            "suffix": null,
            "nim": "16716084"
        }, {
            "id": "2349",
            "name": "Bjorka Tosca Azzahra",
            "prefix": null,
            "suffix": null,
            "nim": "16716086"
        }, {
            "id": "2350",
            "name": "Salsabila",
            "prefix": null,
            "suffix": null,
            "nim": "16716087"
        }, {
            "id": "2351",
            "name": "Genta Muhammad Rahayu",
            "prefix": null,
            "suffix": null,
            "nim": "16716095"
        }, {
            "id": "2352",
            "name": "Ravanny Waraney Moesa Komalig",
            "prefix": null,
            "suffix": null,
            "nim": "16716102"
        }, {
            "id": "2353",
            "name": "Lydia Puspa Melati",
            "prefix": null,
            "suffix": null,
            "nim": "16716108"
        }, {
            "id": "2354",
            "name": "Nusatio Edwin Wirya",
            "prefix": null,
            "suffix": null,
            "nim": "16716113"
        }, {
            "id": "2355",
            "name": "Karin Adriana Widyanti",
            "prefix": null,
            "suffix": null,
            "nim": "16716115"
        }, {
            "id": "2356",
            "name": "Steven Ariya Cahyadi",
            "prefix": null,
            "suffix": null,
            "nim": "16716122"
        }, {
            "id": "2357",
            "name": "Shella Novelinda Purba",
            "prefix": null,
            "suffix": null,
            "nim": "16716129"
        }, {
            "id": "2358",
            "name": "Diandra Rizkiyani",
            "prefix": null,
            "suffix": null,
            "nim": "16716133"
        }, {
            "id": "2359",
            "name": "Shavira Hana Benitaputri",
            "prefix": null,
            "suffix": null,
            "nim": "16716134"
        }, {
            "id": "2360",
            "name": "Muhammad Reza Aditya H",
            "prefix": null,
            "suffix": null,
            "nim": "16716140"
        }, {
            "id": "2361",
            "name": "Della Silvia",
            "prefix": null,
            "suffix": null,
            "nim": "16716163"
        }, {
            "id": "2362",
            "name": "Vania",
            "prefix": null,
            "suffix": null,
            "nim": "16716164"
        }, {
            "id": "2363",
            "name": "Yovita",
            "prefix": null,
            "suffix": null,
            "nim": "16716166"
        }, {
            "id": "2364",
            "name": "Daffa Rifqi Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16716168"
        }, {
            "id": "2365",
            "name": "Nathania",
            "prefix": null,
            "suffix": null,
            "nim": "16716170"
        }, {
            "id": "2366",
            "name": "Muhammad Wildan Hakim",
            "prefix": null,
            "suffix": null,
            "nim": "16716176"
        }, {
            "id": "2367",
            "name": "Faisa Maheswari Millenianya",
            "prefix": null,
            "suffix": null,
            "nim": "16716184"
        }, {
            "id": "2368",
            "name": "Aswin Nobby Susanto",
            "prefix": null,
            "suffix": null,
            "nim": "16716185"
        }, {
            "id": "2369",
            "name": "Muhammad Rizqi Nurkhalish",
            "prefix": null,
            "suffix": null,
            "nim": "16716189"
        }, {
            "id": "2370",
            "name": "Putri Bunga Addini",
            "prefix": null,
            "suffix": null,
            "nim": "16716194"
        }, {
            "id": "2371",
            "name": "Abiseno Aryo Makarim",
            "prefix": null,
            "suffix": null,
            "nim": "16716198"
        }, {
            "id": "2372",
            "name": "Aisyah Kamilah Abidin",
            "prefix": null,
            "suffix": null,
            "nim": "16716199"
        }, {
            "id": "2373",
            "name": "Teza Novianti",
            "prefix": null,
            "suffix": null,
            "nim": "16716204"
        }, {
            "id": "2374",
            "name": "Natalia Karin Prasetya",
            "prefix": null,
            "suffix": null,
            "nim": "16716208"
        }, {
            "id": "2375",
            "name": "David Alvin Christopher",
            "prefix": null,
            "suffix": null,
            "nim": "16716210"
        }, {
            "id": "2376",
            "name": "Tsaqila Rizmadanty Singgih",
            "prefix": null,
            "suffix": null,
            "nim": "16716211"
        }, {
            "id": "2377",
            "name": "Holifah",
            "prefix": null,
            "suffix": null,
            "nim": "16716214"
        }, {
            "id": "2378",
            "name": "Rafki Islamy",
            "prefix": null,
            "suffix": null,
            "nim": "16716219"
        }, {
            "id": "2379",
            "name": "Putri Nurul Hikmah",
            "prefix": null,
            "suffix": null,
            "nim": "16716232"
        }, {
            "id": "2380",
            "name": "Adi Dwi Yulianto",
            "prefix": null,
            "suffix": null,
            "nim": "16716233"
        }, {
            "id": "2381",
            "name": "Louis Mari Agung Wicaksono",
            "prefix": null,
            "suffix": null,
            "nim": "16716235"
        }, {
            "id": "2382",
            "name": "Muhammad Fadhil",
            "prefix": null,
            "suffix": null,
            "nim": "16716249"
        }, {
            "id": "2383",
            "name": "Galang Aulia",
            "prefix": null,
            "suffix": null,
            "nim": "16716253"
        }, {
            "id": "2384",
            "name": "Valentino Henry Anjalie",
            "prefix": null,
            "suffix": null,
            "nim": "16716255"
        }, {
            "id": "2385",
            "name": "Novia Calista Phang",
            "prefix": null,
            "suffix": null,
            "nim": "16716258"
        }, {
            "id": "2386",
            "name": "Andre",
            "prefix": null,
            "suffix": null,
            "nim": "16716259"
        }, {
            "id": "2387",
            "name": "Rizki Basuki",
            "prefix": null,
            "suffix": null,
            "nim": "16716264"
        }, {
            "id": "2388",
            "name": "Ferisyal Ramli",
            "prefix": null,
            "suffix": null,
            "nim": "16716278"
        }, {
            "id": "2389",
            "name": "Muhammad Aditya Said",
            "prefix": null,
            "suffix": null,
            "nim": "16716283"
        }, {
            "id": "2390",
            "name": "Johanna Muliany",
            "prefix": null,
            "suffix": null,
            "nim": "16716292"
        }, {
            "id": "2391",
            "name": "Achmad Zaka Wali",
            "prefix": null,
            "suffix": null,
            "nim": "16716293"
        }, {
            "id": "2392",
            "name": "Thoriq Fauzan Ariandi",
            "prefix": null,
            "suffix": null,
            "nim": "16716295"
        }, {
            "id": "2393",
            "name": "Daniel Immanuel Poluan",
            "prefix": null,
            "suffix": null,
            "nim": "16716299"
        }, {
            "id": "2394",
            "name": "Kinan Maulana Makmoen",
            "prefix": null,
            "suffix": null,
            "nim": "16716302"
        }, {
            "id": "2395",
            "name": "Fian Adinata",
            "prefix": null,
            "suffix": null,
            "nim": "16716314"
        }, {
            "id": "2396",
            "name": "Raisa Putri Avianny",
            "prefix": null,
            "suffix": null,
            "nim": "16716317"
        }, {
            "id": "2397",
            "name": "Avyandra Rizka Putri",
            "prefix": null,
            "suffix": null,
            "nim": "16716318"
        }, {
            "id": "2398",
            "name": "Jonatan Kevin Daniel",
            "prefix": null,
            "suffix": null,
            "nim": "16716328"
        }, {
            "id": "2399",
            "name": "Fauzatu Arabica Yatasya",
            "prefix": null,
            "suffix": null,
            "nim": "16716331"
        }, {
            "id": "2400",
            "name": "Sabrinna Wulandari",
            "prefix": null,
            "suffix": null,
            "nim": "16716338"
        }, {
            "id": "2401",
            "name": "Jeremy Nathaniel Gunawan",
            "prefix": null,
            "suffix": null,
            "nim": "16716344"
        }, {
            "id": "2402",
            "name": "Qori Lestari",
            "prefix": null,
            "suffix": null,
            "nim": "16716347"
        }, {
            "id": "2403",
            "name": "Muhammad Arief Shafarifky",
            "prefix": null,
            "suffix": null,
            "nim": "16716352"
        }, {
            "id": "2404",
            "name": "Jonathan Ariel Sugih",
            "prefix": null,
            "suffix": null,
            "nim": "16716360"
        }, {
            "id": "2405",
            "name": "Jihan Camilia Nabila",
            "prefix": null,
            "suffix": null,
            "nim": "16716370"
        }, {
            "id": "2406",
            "name": "Bernadeta Adinda Inka P",
            "prefix": null,
            "suffix": null,
            "nim": "16716371"
        }, {
            "id": "2407",
            "name": "Rauzatul Muna",
            "prefix": null,
            "suffix": null,
            "nim": "16716377"
        }, {
            "id": "2408",
            "name": "Sola Gratia Malau",
            "prefix": null,
            "suffix": null,
            "nim": "16716378"
        }, {
            "id": "2409",
            "name": "Severinus Viery Utomo",
            "prefix": null,
            "suffix": null,
            "nim": "16716380"
        }, {
            "id": "2410",
            "name": "Ghiffari Emir Muchamad",
            "prefix": null,
            "suffix": null,
            "nim": "16716382"
        }, {
            "id": "2411",
            "name": "Rama Dikky Larosa",
            "prefix": null,
            "suffix": null,
            "nim": "16716388"
        }, {
            "id": "2412",
            "name": "Fadel Hilmy",
            "prefix": null,
            "suffix": null,
            "nim": "16716389"
        }, {
            "id": "2413",
            "name": "Shannen Limenta",
            "prefix": null,
            "suffix": null,
            "nim": "16716390"
        }, {
            "id": "2414",
            "name": "Timotius Pratama Tjahja",
            "prefix": null,
            "suffix": null,
            "nim": "16716414"
        }, {
            "id": "2415",
            "name": "Nadhila Nur Fauziyyah",
            "prefix": null,
            "suffix": null,
            "nim": "16716422"
        }, {
            "id": "2416",
            "name": "Edrick Hansel",
            "prefix": null,
            "suffix": null,
            "nim": "16716424"
        }, {
            "id": "2417",
            "name": "David Silaban",
            "prefix": null,
            "suffix": null,
            "nim": "16716425"
        }, {
            "id": "2418",
            "name": "Pranisfia Putri Karima",
            "prefix": null,
            "suffix": null,
            "nim": "16716429"
        }, {
            "id": "2419",
            "name": "Veren",
            "prefix": null,
            "suffix": null,
            "nim": "16716432"
        }, {
            "id": "2420",
            "name": "Zulfa Wahyu Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "16716433"
        }, {
            "id": "2421",
            "name": "Muhammad Rady Irawan",
            "prefix": null,
            "suffix": null,
            "nim": "16716460"
        }, {
            "id": "2422",
            "name": "mkdu",
            "prefix": "",
            "suffix": "",
            "nim": null
        }, {
            "id": "2423",
            "name": "Ivan Fadillah",
            "prefix": null,
            "suffix": null,
            "nim": "16516004"
        }, {
            "id": "2424",
            "name": "Raditya Priyongga",
            "prefix": null,
            "suffix": null,
            "nim": "16516011"
        }, {
            "id": "2425",
            "name": "Agung Daryodi",
            "prefix": null,
            "suffix": null,
            "nim": "16516012"
        }, {
            "id": "2426",
            "name": "Muh Habibi Haidir",
            "prefix": null,
            "suffix": null,
            "nim": "16516015"
        }, {
            "id": "2427",
            "name": "Yoshiro Bimaputra Fathurahman",
            "prefix": null,
            "suffix": null,
            "nim": "16516020"
        }, {
            "id": "2428",
            "name": "Dandy Arif Rahman",
            "prefix": null,
            "suffix": null,
            "nim": "16516031"
        }, {
            "id": "2429",
            "name": "Ilham Firdausi Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16516033"
        }, {
            "id": "2430",
            "name": "Muhamad Aditya Farizki",
            "prefix": null,
            "suffix": null,
            "nim": "16516050"
        }, {
            "id": "2431",
            "name": "Amaldi Tri Septyanto",
            "prefix": null,
            "suffix": null,
            "nim": "16516055"
        }, {
            "id": "2432",
            "name": "Antonio Setya",
            "prefix": null,
            "suffix": null,
            "nim": "16516058"
        }, {
            "id": "2433",
            "name": "Gusna Naufal Taris",
            "prefix": null,
            "suffix": null,
            "nim": "16516062"
        }, {
            "id": "2434",
            "name": "Stacia Janice",
            "prefix": null,
            "suffix": null,
            "nim": "16516065"
        }, {
            "id": "2435",
            "name": "M Rivandi Fadli",
            "prefix": null,
            "suffix": null,
            "nim": "16516075"
        }, {
            "id": "2436",
            "name": "Harso Adjie Brotosukmono",
            "prefix": null,
            "suffix": null,
            "nim": "16516076"
        }, {
            "id": "2437",
            "name": "Yusuf Rahmat Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16516077"
        }, {
            "id": "2438",
            "name": "Alvin Tafarrel",
            "prefix": null,
            "suffix": null,
            "nim": "16516081"
        }, {
            "id": "2439",
            "name": "Muhammad Fajar Fadilah",
            "prefix": null,
            "suffix": null,
            "nim": "16516087"
        }, {
            "id": "2440",
            "name": "Jonathan Alvaro",
            "prefix": null,
            "suffix": null,
            "nim": "16516099"
        }, {
            "id": "2441",
            "name": "Pramadithya Herdian",
            "prefix": null,
            "suffix": null,
            "nim": "16516106"
        }, {
            "id": "2442",
            "name": "Dimas Aji Pangestu",
            "prefix": null,
            "suffix": null,
            "nim": "16516108"
        }, {
            "id": "2443",
            "name": "Rhesa Aditya Sugondo",
            "prefix": null,
            "suffix": null,
            "nim": "16516112"
        }, {
            "id": "2444",
            "name": "Alvin Limassa",
            "prefix": null,
            "suffix": null,
            "nim": "16516118"
        }, {
            "id": "2445",
            "name": "Hauzan Adi Wafi",
            "prefix": null,
            "suffix": null,
            "nim": "16516121"
        }, {
            "id": "2446",
            "name": "Senapati Sang Diwangkara",
            "prefix": null,
            "suffix": null,
            "nim": "16516129"
        }, {
            "id": "2447",
            "name": "Shinta Ayu Chandra Kemala",
            "prefix": null,
            "suffix": null,
            "nim": "16516143"
        }, {
            "id": "2448",
            "name": "Ahmad Izzan",
            "prefix": null,
            "suffix": null,
            "nim": "16516144"
        }, {
            "id": "2449",
            "name": "Christian Chandra Setiadi",
            "prefix": null,
            "suffix": null,
            "nim": "16516158"
        }, {
            "id": "2450",
            "name": "Jonathan Tjandra",
            "prefix": null,
            "suffix": null,
            "nim": "16516160"
        }, {
            "id": "2451",
            "name": "Naila Fadhilah Fithriah",
            "prefix": null,
            "suffix": null,
            "nim": "16516161"
        }, {
            "id": "2452",
            "name": "Haifa Fadhila Ilma",
            "prefix": null,
            "suffix": null,
            "nim": "16516164"
        }, {
            "id": "2453",
            "name": "Kevin Caesar Hagata Ginting",
            "prefix": null,
            "suffix": null,
            "nim": "16516167"
        }, {
            "id": "2454",
            "name": "Nadia Hasanah",
            "prefix": null,
            "suffix": null,
            "nim": "16516173"
        }, {
            "id": "2455",
            "name": "Dafi Ihsandiya Faraz",
            "prefix": null,
            "suffix": null,
            "nim": "16516178"
        }, {
            "id": "2456",
            "name": "Udinar Fatahila",
            "prefix": null,
            "suffix": null,
            "nim": "16516181"
        }, {
            "id": "2457",
            "name": "Ardji Naufal Setiawan",
            "prefix": null,
            "suffix": null,
            "nim": "16516190"
        }, {
            "id": "2458",
            "name": "Blestro Bellarmino Hutabalian",
            "prefix": null,
            "suffix": null,
            "nim": "16516193"
        }, {
            "id": "2459",
            "name": "Ihsan Muhammad Asnadi",
            "prefix": null,
            "suffix": null,
            "nim": "16516199"
        }, {
            "id": "2460",
            "name": "Qonita Salamah Auliya",
            "prefix": null,
            "suffix": null,
            "nim": "16516201"
        }, {
            "id": "2461",
            "name": "Thomas Robin",
            "prefix": null,
            "suffix": null,
            "nim": "16516204"
        }, {
            "id": "2462",
            "name": "Bariq Sufi Firmansyah",
            "prefix": null,
            "suffix": null,
            "nim": "16516206"
        }, {
            "id": "2463",
            "name": "Zefanya Chandra",
            "prefix": null,
            "suffix": null,
            "nim": "16516207"
        }, {
            "id": "2464",
            "name": "Sri3 Wahyu Ningsih",
            "prefix": null,
            "suffix": null,
            "nim": null
        }, {
            "id": "2465",
            "name": "Dion Saputra",
            "prefix": null,
            "suffix": null,
            "nim": "16516220"
        }, {
            "id": "2466",
            "name": "Arba Robbani",
            "prefix": null,
            "suffix": null,
            "nim": "16516223"
        }, {
            "id": "2467",
            "name": "Christian Wibisono",
            "prefix": null,
            "suffix": null,
            "nim": "16516229"
        }, {
            "id": "2468",
            "name": "Maurizfa",
            "prefix": null,
            "suffix": null,
            "nim": "16516233"
        }, {
            "id": "2469",
            "name": "Hani\'ah Wafa",
            "prefix": null,
            "suffix": null,
            "nim": "16516235"
        }, {
            "id": "2470",
            "name": "Rahma Rizky Alifia",
            "prefix": null,
            "suffix": null,
            "nim": "16516240"
        }, {
            "id": "2471",
            "name": "Wilson Rustiandy",
            "prefix": null,
            "suffix": null,
            "nim": "16516241"
        }, {
            "id": "2472",
            "name": "Rakha Patria Indrasepta Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16516243"
        }, {
            "id": "2473",
            "name": "Cheryl Josephine Ovani",
            "prefix": null,
            "suffix": null,
            "nim": "16516246"
        }, {
            "id": "2474",
            "name": "Fahri Kusuma Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16516261"
        }, {
            "id": "2475",
            "name": "Putu Gery Wahyu Nugraha",
            "prefix": null,
            "suffix": null,
            "nim": "16516266"
        }, {
            "id": "2476",
            "name": "Aldo Azali",
            "prefix": null,
            "suffix": null,
            "nim": "16516271"
        }, {
            "id": "2477",
            "name": "M Alif Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16516281"
        }, {
            "id": "2478",
            "name": "Akbar Ghifari",
            "prefix": null,
            "suffix": null,
            "nim": "16516282"
        }, {
            "id": "2479",
            "name": "Yonas Adiel Wiguna",
            "prefix": null,
            "suffix": null,
            "nim": "16516283"
        }, {
            "id": "2480",
            "name": "Mathias Novianto",
            "prefix": null,
            "suffix": null,
            "nim": "16516290"
        }, {
            "id": "2481",
            "name": "Renjira Naufhal Dhiaegana",
            "prefix": null,
            "suffix": null,
            "nim": "16516295"
        }, {
            "id": "2482",
            "name": "Zahid Abdurrohman",
            "prefix": null,
            "suffix": null,
            "nim": "16516298"
        }, {
            "id": "2483",
            "name": "Gabby Aprilia",
            "prefix": null,
            "suffix": null,
            "nim": "16516300"
        }, {
            "id": "2484",
            "name": "I Putu Eka Surya Aditya",
            "prefix": null,
            "suffix": null,
            "nim": "16516302"
        }, {
            "id": "2485",
            "name": "Nadia Saraswati",
            "prefix": null,
            "suffix": null,
            "nim": "16516304"
        }, {
            "id": "2486",
            "name": "Ahmad Rasyid A",
            "prefix": null,
            "suffix": null,
            "nim": "16516308"
        }, {
            "id": "2487",
            "name": "Jeremy Maringan Tua Napitupulu",
            "prefix": null,
            "suffix": null,
            "nim": "16516311"
        }, {
            "id": "2488",
            "name": "Muhammad Choirudin Malik",
            "prefix": null,
            "suffix": null,
            "nim": "16516313"
        }, {
            "id": "2489",
            "name": "Sarah Az Zahra",
            "prefix": null,
            "suffix": null,
            "nim": "16516320"
        }, {
            "id": "2490",
            "name": "Steven Sukma Limanus",
            "prefix": null,
            "suffix": null,
            "nim": "16516328"
        }, {
            "id": "2491",
            "name": "Kevin Fernaldy",
            "prefix": null,
            "suffix": null,
            "nim": "16516332"
        }, {
            "id": "2492",
            "name": "Nuel Yosia",
            "prefix": null,
            "suffix": null,
            "nim": "16516350"
        }, {
            "id": "2493",
            "name": "Julian Rifky Santika",
            "prefix": null,
            "suffix": null,
            "nim": "16516353"
        }, {
            "id": "2494",
            "name": "Muhammad Ivan Taftazani",
            "prefix": null,
            "suffix": null,
            "nim": "16516360"
        }, {
            "id": "2495",
            "name": "Rizca Shafira S M",
            "prefix": null,
            "suffix": null,
            "nim": "16516366"
        }, {
            "id": "2496",
            "name": "Radifa Akbar Abhesa",
            "prefix": null,
            "suffix": null,
            "nim": "16516367"
        }, {
            "id": "2497",
            "name": "Rinda Nur Hafizha",
            "prefix": null,
            "suffix": null,
            "nim": "16516373"
        }, {
            "id": "2498",
            "name": "Joseph Salimin",
            "prefix": null,
            "suffix": null,
            "nim": "16516378"
        }, {
            "id": "2499",
            "name": "Ulfa Hamida",
            "prefix": null,
            "suffix": null,
            "nim": "16516383"
        }, {
            "id": "2500",
            "name": "Isro Syaeful Iman",
            "prefix": null,
            "suffix": null,
            "nim": "16516388"
        }, {
            "id": "2501",
            "name": "Haykal Hutama Kahum",
            "prefix": null,
            "suffix": null,
            "nim": "16516393"
        }, {
            "id": "2502",
            "name": "Ardiyanto Hidayat",
            "prefix": null,
            "suffix": null,
            "nim": "16516398"
        }, {
            "id": "2503",
            "name": "Ricky Kennedy",
            "prefix": null,
            "suffix": null,
            "nim": "16516403"
        }, {
            "id": "2504",
            "name": "Gloryanson Ginting",
            "prefix": null,
            "suffix": null,
            "nim": "16516408"
        }, {
            "id": "2505",
            "name": "Shidqie Taufiqurrahman",
            "prefix": null,
            "suffix": null,
            "nim": "16516413"
        }, {
            "id": "2506",
            "name": "Muhammad Ammar Robbani",
            "prefix": null,
            "suffix": null,
            "nim": "16516418"
        }, {
            "id": "2507",
            "name": "Adam Rifky",
            "prefix": null,
            "suffix": null,
            "nim": "16516423"
        }, {
            "id": "2508",
            "name": "Cyknoris Mayor",
            "prefix": null,
            "suffix": null,
            "nim": "16516428"
        }, {
            "id": "2509",
            "name": "Hazim Ali Tajuddin",
            "prefix": null,
            "suffix": null,
            "nim": "16616001"
        }, {
            "id": "2510",
            "name": "Fajrul Mutaqin",
            "prefix": null,
            "suffix": null,
            "nim": "16616008"
        }, {
            "id": "2511",
            "name": "Rafael Ivan Kristoni",
            "prefix": null,
            "suffix": null,
            "nim": "16616010"
        }, {
            "id": "2512",
            "name": "Rizkia Iqlil Inshirah Otslan",
            "prefix": null,
            "suffix": null,
            "nim": "16616014"
        }, {
            "id": "2513",
            "name": "Muhammad Fadhl \'abbas",
            "prefix": null,
            "suffix": null,
            "nim": "16616016"
        }, {
            "id": "2514",
            "name": "Fadhlurrahman",
            "prefix": null,
            "suffix": null,
            "nim": "16616023"
        }, {
            "id": "2515",
            "name": "Silmi Fatharani Rahmah",
            "prefix": null,
            "suffix": null,
            "nim": "16616025"
        }, {
            "id": "2516",
            "name": "Puteri Nugrahaning Jati",
            "prefix": null,
            "suffix": null,
            "nim": "16616029"
        }, {
            "id": "2517",
            "name": "Malikhey Almantha Siregar",
            "prefix": null,
            "suffix": null,
            "nim": "16616031"
        }, {
            "id": "2518",
            "name": "Mutiara Ramadhani Nur Irbah",
            "prefix": null,
            "suffix": null,
            "nim": "16616032"
        }, {
            "id": "2519",
            "name": "Jan Davin Tengdyantono",
            "prefix": null,
            "suffix": null,
            "nim": "16616034"
        }, {
            "id": "2520",
            "name": "Felia Puti Salsabila",
            "prefix": null,
            "suffix": null,
            "nim": "16616042"
        }, {
            "id": "2521",
            "name": "Fharadita Rahma Y",
            "prefix": null,
            "suffix": null,
            "nim": "16616044"
        }, {
            "id": "2522",
            "name": "Yosafat Setiawan Susilo Juwono",
            "prefix": null,
            "suffix": null,
            "nim": "16616049"
        }, {
            "id": "2523",
            "name": "Kelvin Tanadi",
            "prefix": null,
            "suffix": null,
            "nim": "16616050"
        }, {
            "id": "2524",
            "name": "Mohammad Fajri Raazaq R",
            "prefix": null,
            "suffix": null,
            "nim": "16616051"
        }, {
            "id": "2525",
            "name": "Muhamad Abdurahman",
            "prefix": null,
            "suffix": null,
            "nim": "16616055"
        }, {
            "id": "2526",
            "name": "Suwandi Luhur",
            "prefix": null,
            "suffix": null,
            "nim": "16616062"
        }, {
            "id": "2527",
            "name": "Alexandra Ayurinaras Darminto",
            "prefix": null,
            "suffix": null,
            "nim": "16616067"
        }, {
            "id": "2528",
            "name": "Muhamad Pringadi Lesmana Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16616069"
        }, {
            "id": "2529",
            "name": "Gumilang Nuralam",
            "prefix": null,
            "suffix": null,
            "nim": "16616080"
        }, {
            "id": "2530",
            "name": "Syaviera Aninda Putri Said",
            "prefix": null,
            "suffix": null,
            "nim": "16616086"
        }, {
            "id": "2531",
            "name": "Rizky Novitasari",
            "prefix": null,
            "suffix": null,
            "nim": "16616095"
        }, {
            "id": "2532",
            "name": "Kenneth Algian",
            "prefix": null,
            "suffix": null,
            "nim": "16616108"
        }, {
            "id": "2533",
            "name": "Hira Pradhana",
            "prefix": null,
            "suffix": null,
            "nim": "16616111"
        }, {
            "id": "2534",
            "name": "Mohamad Fakhry H A",
            "prefix": null,
            "suffix": null,
            "nim": "16616124"
        }, {
            "id": "2535",
            "name": "Muhammad Rizal Arvandy Harahap",
            "prefix": null,
            "suffix": null,
            "nim": "16616127"
        }, {
            "id": "2536",
            "name": "Chrisnathan Triparma Gurning",
            "prefix": null,
            "suffix": null,
            "nim": "16616129"
        }, {
            "id": "2537",
            "name": "Vernanda Yunan Hilmi",
            "prefix": null,
            "suffix": null,
            "nim": "16616130"
        }, {
            "id": "2538",
            "name": "Marcel Renaldy Soetyoso",
            "prefix": null,
            "suffix": null,
            "nim": "16616135"
        }, {
            "id": "2539",
            "name": "Indradi Khalid Maulana",
            "prefix": null,
            "suffix": null,
            "nim": "16616140"
        }, {
            "id": "2540",
            "name": "Muhammad Fakhri Aldin",
            "prefix": null,
            "suffix": null,
            "nim": "16616145"
        }, {
            "id": "2541",
            "name": "Syakal Hammas Kh Hujaemi",
            "prefix": null,
            "suffix": null,
            "nim": "16616149"
        }, {
            "id": "2542",
            "name": "Muhammad Ulwan Rais El Irfan",
            "prefix": null,
            "suffix": null,
            "nim": "16616159"
        }, {
            "id": "2543",
            "name": "Benediktus Brian Prasetya",
            "prefix": null,
            "suffix": null,
            "nim": "16616168"
        }, {
            "id": "2544",
            "name": "Rismaya Fayi Dienta",
            "prefix": null,
            "suffix": null,
            "nim": "16616171"
        }, {
            "id": "2545",
            "name": "Gary Nata Permana",
            "prefix": null,
            "suffix": null,
            "nim": "16616174"
        }, {
            "id": "2546",
            "name": "Muhammad Rizki Purnama",
            "prefix": null,
            "suffix": null,
            "nim": "16616180"
        }, {
            "id": "2547",
            "name": "Julian Adiputra",
            "prefix": null,
            "suffix": null,
            "nim": "16616181"
        }, {
            "id": "2548",
            "name": "Anwar Irwandi Hamids",
            "prefix": null,
            "suffix": null,
            "nim": "16616182"
        }, {
            "id": "2549",
            "name": "Muhammad Dafa Sultan Pasha",
            "prefix": null,
            "suffix": null,
            "nim": "16616192"
        }, {
            "id": "2550",
            "name": "Kesia Naduma",
            "prefix": null,
            "suffix": null,
            "nim": "16616193"
        }, {
            "id": "2551",
            "name": "Inka Permata Purba",
            "prefix": null,
            "suffix": null,
            "nim": "16616199"
        }, {
            "id": "2552",
            "name": "Pranaja Abel Amanta",
            "prefix": null,
            "suffix": null,
            "nim": "16616202"
        }, {
            "id": "2553",
            "name": "Nada Zharfania Zuhaira",
            "prefix": null,
            "suffix": null,
            "nim": "16616206"
        }, {
            "id": "2554",
            "name": "Anisa Rahma Widiyanti",
            "prefix": null,
            "suffix": null,
            "nim": "16616208"
        }, {
            "id": "2555",
            "name": "Muhammad Hafidh",
            "prefix": null,
            "suffix": null,
            "nim": "16616235"
        }, {
            "id": "2556",
            "name": "Ridho Yuneldi Pratama",
            "prefix": null,
            "suffix": null,
            "nim": "16616236"
        }, {
            "id": "2557",
            "name": "Bernardus Sena Pasereng",
            "prefix": null,
            "suffix": null,
            "nim": "16616245"
        }, {
            "id": "2558",
            "name": "Feliciana Ajeng Setyorini",
            "prefix": null,
            "suffix": null,
            "nim": "16616246"
        }, {
            "id": "2559",
            "name": "Mujahid Bina Alfikri",
            "prefix": null,
            "suffix": null,
            "nim": "16616253"
        }, {
            "id": "2560",
            "name": "Muhammad Ilham Dirgantara",
            "prefix": null,
            "suffix": null,
            "nim": "16616256"
        }, {
            "id": "2561",
            "name": "Nurrohmah Samrotul Fauziah",
            "prefix": null,
            "suffix": null,
            "nim": "16616257"
        }, {
            "id": "2562",
            "name": "Jayanti Ramadhany",
            "prefix": null,
            "suffix": null,
            "nim": "16616266"
        }, {
            "id": "2563",
            "name": "Dwi Fauzi Ashidqi",
            "prefix": null,
            "suffix": null,
            "nim": "16616281"
        }, {
            "id": "2564",
            "name": "I Gede Andika Putra",
            "prefix": null,
            "suffix": null,
            "nim": "16616285"
        }, {
            "id": "2565",
            "name": "Intan Tiara Indrajati",
            "prefix": null,
            "suffix": null,
            "nim": "16616293"
        }, {
            "id": "2566",
            "name": "Julius Alfredo Panamotan M",
            "prefix": null,
            "suffix": null,
            "nim": "16616296"
        }, {
            "id": "2567",
            "name": "Aldo Bungkulan Hutagalung",
            "prefix": null,
            "suffix": null,
            "nim": "16616299"
        }, {
            "id": "2568",
            "name": "Arisa F Pangaribuan",
            "prefix": null,
            "suffix": null,
            "nim": "16616304"
        }, {
            "id": "2569",
            "name": "Yuanda Eka Putri",
            "prefix": null,
            "suffix": null,
            "nim": "16616305"
        }, {
            "id": "2570",
            "name": "Ario Arianto",
            "prefix": null,
            "suffix": null,
            "nim": "16616306"
        }, {
            "id": "2571",
            "name": "Rizki Jatmika Z R",
            "prefix": null,
            "suffix": null,
            "nim": "16616310"
        }, {
            "id": "2572",
            "name": "Nubella Citresna Zakiyyah",
            "prefix": null,
            "suffix": null,
            "nim": "16616316"
        }, {
            "id": "2573",
            "name": "Ivan Geraldo Situmorang",
            "prefix": null,
            "suffix": null,
            "nim": "16616318"
        }, {
            "id": "2574",
            "name": "Ferdy Tjahyadi T L",
            "prefix": null,
            "suffix": null,
            "nim": "16616324"
        }, {
            "id": "2575",
            "name": "Muhammad Faqih Nashiruddin",
            "prefix": null,
            "suffix": null,
            "nim": "16616335"
        }, {
            "id": "2576",
            "name": "Billy Dwianto",
            "prefix": null,
            "suffix": null,
            "nim": "16616336"
        }, {
            "id": "2577",
            "name": "Mochammad Iqbal",
            "prefix": null,
            "suffix": null,
            "nim": "16616345"
        }, {
            "id": "2578",
            "name": "Viedi Ananda Rizqia",
            "prefix": null,
            "suffix": null,
            "nim": "16616348"
        }, {
            "id": "2579",
            "name": "Bleddian Citra Wijaya",
            "prefix": null,
            "suffix": null,
            "nim": "16616358"
        }, {
            "id": "2580",
            "name": "Naufal Sutan Panglima",
            "prefix": null,
            "suffix": null,
            "nim": "16616360"
        }, {
            "id": "2581",
            "name": "Annisa Sanya Putri",
            "prefix": null,
            "suffix": null,
            "nim": "16616364"
        }, {
            "id": "2582",
            "name": "Rr Indah Kusumasari",
            "prefix": null,
            "suffix": null,
            "nim": "16616368"
        }, {
            "id": "2583",
            "name": "Rahadian Archie Danisworo",
            "prefix": null,
            "suffix": null,
            "nim": "16616378"
        }, {
            "id": "2584",
            "name": "Elyada Eben Ezer",
            "prefix": null,
            "suffix": null,
            "nim": "16616379"
        }, {
            "id": "2585",
            "name": "Irham Dhafin",
            "prefix": null,
            "suffix": null,
            "nim": "16616381"
        }, {
            "id": "2586",
            "name": "Ainunnisa Aulia Alfatihah",
            "prefix": null,
            "suffix": null,
            "nim": "16616387"
        }, {
            "id": "2587",
            "name": "Muhammad Fadhli Mubarak",
            "prefix": null,
            "suffix": null,
            "nim": "16616393"
        }, {
            "id": "2588",
            "name": "Deananda Factria Utami",
            "prefix": null,
            "suffix": null,
            "nim": "16616399"
        }, {
            "id": "2589",
            "name": "Muhammad Iqbal Wiratama F",
            "prefix": null,
            "suffix": null,
            "nim": "16616405"
        }, {
            "id": "2590",
            "name": "Puti Fauzia Imani",
            "prefix": null,
            "suffix": null,
            "nim": "16616410"
        }, {
            "id": "2591",
            "name": "Yahya Nashihur Rodhi",
            "prefix": null,
            "suffix": null,
            "nim": "16616411"
        }, {
            "id": "2592",
            "name": "Adriel Joshua Tataming",
            "prefix": null,
            "suffix": null,
            "nim": "16616416"
        }, {
            "id": "2593",
            "name": "M Qisthi Muallim",
            "prefix": null,
            "suffix": null,
            "nim": "16616423"
        }, {
            "id": "2594",
            "name": "Yesaya Elaby",
            "prefix": null,
            "suffix": null,
            "nim": "16616428"
        }]');

        foreach($data as $v) {

            if($v->nim == null) {
                $username = explode(" ", $v->name);
                $username = strtolower($username[0]);
            } else {
                $username = $v->nim;
            }

            $user = new User();
            $user->id = $v->id+10;
            $user->name = $v->name;
            $user->username = $username;
            $user->email = $username.'@itb.ac.id';
            $user->password = bcrypt($v->name);
            $user->idnumber = $v->nim;
            $user->prefix = $v->prefix;
            $user->suffix = $v->suffix;
            $user->save();

            if($v->nim != null) {
                $user->attachRole('student');
            } else {
                $user->attachRole('lecturer');
            }
        }

    }
}
