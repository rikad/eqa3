<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',20)->unique();
            $table->string('idnumber',20)->unique()->nullable();
            $table->string('name',60);
            $table->string('email',60)->unique();
            $table->string('password');
            $table->string('prefix',20)->nullable();
            $table->string('suffix',20)->nullable();
            $table->string('birth_place',20)->nullable();
            $table->string('birth_date',20)->nullable();
            $table->integer('active_role')->nullable()->unsigned();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
