<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('po_forms', function (Blueprint $table) {
        //     $table->bigIncrements('id')->unsigned();
        //     $table->string('name',100);
        //     $table->string('abbr',10);
        //     $table->integer('layer')->unsigned();
        //     $table->timestamps();
        // });

        // Schema::create('po_organizations', function (Blueprint $table) {
        //     $table->bigIncrements('id')->unsigned();
        //     $table->unsignedBigInteger('form_id');
        //     $table->unsignedBigInteger('parent_id')->default(0);
        //     $table->string('name',100);
        //     $table->string('abbr',10);
        //     $table->timestamps();

        //     $table->foreign('form_id')->references('id')->on('po_forms')
        //         ->onUpdate('cascade')->onDelete('restrict');
        // });

        // Schema::table('role_user', function (Blueprint $table) {
        //     $table->unsignedBigInteger('organization_id')->nullable();

        //     $table->foreign('organization_id')->references('id')->on('po_organizations')
        //         ->onUpdate('cascade')->onDelete('cascade');
        // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('role_user', function (Blueprint $table) {
        //     $table->dropForeign(['organization_id']);
        //     $table->dropColumn('organization_id');
        // });

        // Schema::dropIfExists('po_organizations');
        // Schema::dropIfExists('po_forms');
    }
}
