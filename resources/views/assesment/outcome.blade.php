@extends('layouts.app')


@section('submenu')
<div class="navbar-header col-md-2" style="margin: 0px">
    <select class="form-control selectpicker" data-live-search="true" onchange="drawChart(this.value)">
      @foreach($outcomes as $v)
      <option value="{{ $v->id }}-{{ $v->code }}. {{ $v->outcome }}">{{ $v->code }}. {{ $v->outcome }}</option>
      @endforeach
    </select>
</div>
@endsection

@section('content')

<div class="container-fluid">
  <div class="block-header">
    <ol class="breadcrumb breadcrumb-bg-indigo">
        <li><a href="/"><i class="material-icons">home</i> Home</a></li>
        <li><a href="/"><i class="material-icons">pageview</i> Assesment</a></li>
        <li class="active"><i class="material-icons">library_books</i>Test Outcome Analysis</li>
    </ol>
  </div>
  <div class="row clearfix">

    <!-- Basic Tabs -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>Test Outcome Analysis</h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                  </li>
              </ul>
          </div>
          <div class="body">
            <!-- INI TEMPAT CHART -->
            <div class="panel-body">
                <canvas id="canvas" height="280" width="600"></canvas>
            </div>

          </div>
      </div>
  </div>
  <!-- #END# Basic Table -->

  </div>

</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('js')

<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>
<script src="{{ asset('plugins') }}/chartjs/Chart.bundle.js"></script>
<script src="{{ asset('plugins') }}/chartjs/scale-hierarchical.js"></script>


<script>

var zLabels = new Array();
var zData = [new Array(),new Array(),new Array(),new Array()];
var zIdx = 0;

/*
Menghasilkan :
label:
      children: ['2013', '2014', '2015']

data:
      children: [3.0, 3.1, 3,05]  rerata year yg sama (kelas)
*/

function rYear(response, ySemester, xCourse) {

  var wYear = response[zIdx].year;
  var wSum = [0,0,0,0];
  var wTot = 0;
  var wN = 0;
  do {
    wSum[0] += response[zIdx].a1;
    wSum[1] += response[zIdx].a2;
    wSum[2] += response[zIdx].a3;
    wSum[3] += response[zIdx].a4;
    wN++;
    zIdx++;
  } while ((zIdx < response.length) &&
        (wYear == response[zIdx].year) &&
        (xCourse == response[zIdx].code) &&
        (ySemester == response[zIdx].smt));

    wTot = 0;
    for (i=0; i<4; i++) {
        wTot += wSum[i];
    }
    var wData = new Array();
    for (i=0; i<4; i++) {
      wData.push(Math.round(wSum[i] / wTot * 10000)/100);
    }
    return {
      label: wYear.toString(),
      data: wData
    };
}

function rCourse(response, ySemester) {
    var xCourse = response[zIdx].code;
    var xChildren = new Array();
    var xTree = [new Array(), new Array(), new Array(), new Array()];
    var xSum = [0,0,0,0];
    var xN = 0;

    do {
      result = rYear(response, ySemester, xCourse);
      xChildren.push(result.label);
      for (i=0; i<4; i++) {
        xTree[i].push(result.data[i]);
        xSum[i] = xSum[i]+ result.data[i];
      }
      xN++;
    } while ((zIdx < response.length) &&
        (xCourse == response[zIdx].code) &&
        (ySemester == response[zIdx].smt));

    var xLabel = {
        label : xCourse,
        expand : false,
        children : xChildren
    };

    var xData = new Array();
    for (i=0; i<4; i++) {
      xData.push({
        value : Math.round(100*xSum[i] / xN)/100,
        children: xTree[i]
      });
    }

    return {
      label: xLabel,
      data: xData
    };
}


function rSemester(response) {
    var ySemester = response[zIdx].smt;
    var yChildren = new Array();
    var yTree = [new Array(), new Array(), new Array(), new Array()];
    var ySum = [0,0,0,0];
    var yN = 0;

    do {
      result = rCourse(response, ySemester);
      yChildren.push(result.label);
      for (i=0; i<4; i++) {
        yTree[i].push(result.data[i]);
        ySum[i] = ySum[i]+ result.data[i].value;
      }
      yN++;
    } while ((zIdx < response.length) &&
        (ySemester == response[zIdx].smt));

    var yLabel = {
        label : ySemester.toString(),
        expand : false,
        children : yChildren
    };

    var yData = new Array();
    for (i=0; i<4; i++) {
      yData.push({
        value : Math.round(100*ySum[i] / yN)/100,
        children: yTree[i]
      });
    }

    return {
      label: yLabel,
      data: yData
    };
}


function rOutcome(response) {
    var yOutcome = response[zIdx].outcome;
    var yChildren = new Array();
    var yTree = [new Array(), new Array(), new Array(), new Array()];
    var ySum = [0,0,0,0];
    var yN = 0;

    do {
      result = rYear(response, yOutcome);
      yChildren.push(result.label);
      for (i=0; i<4; i++) {
        yTree[i].push(result.data[i]);
        ySum[i] = ySum[i]+ result.data[i];
      }
      yN++;
    } while ((zIdx < response.length) &&
        (yOutcome == response[zIdx].outcome));

    var yLabel = {
        label : yOutcome,
        expand : false,
        children : yChildren
    };

    var yData = new Array();
    for (i=0; i<4; i++) {
      yData.push({
        value : Math.round(100*ySum[i] / yN)/100,
        children: yTree[i]
      });
    }

    return {
      label: yLabel,
      data: yData
    };
}


function drawChart(data) {
  data = data.split('-');

  var id = data[0];
  var title = data[1];

  var url = "{{ route('outcome.index') }}"+'/'+id;

  zLabels = new Array();
  zData = [new Array(),new Array(),new Array(),new Array()];
  zIdx = 0;

  $.get(url, function(response) {
    while (zIdx < response.length) {
      result = rSemester(response);
      zLabels.push(result.label);
      for (i=0; i<4; i++) {
        zData[i].push(result.data[i]);
      }
    }

    var chartdata = {
        labels: zLabels,
        datasets: [{
          label: 'Exemplary',
          backgroundColor: 'blue',
          tree: zData[3]
        },{
          label: 'Satisfactory',
          backgroundColor: 'green',
          tree: zData[2]
        },{
          label: 'Developing',
          backgroundColor: 'orange',
          tree: zData[1]
        },{
          label: 'Unsatisfactory',
          backgroundColor: 'red',
          tree: zData[0]
        }]
      };

      if (window.myBar != undefined) {
        window.myBar.destroy();
      }

    var ctx = document.getElementById("canvas").getContext('2d');
    window.myBar = new Chart(ctx, {
      type: 'bar',
      data: chartdata,
      options: {
        responsive: true,
        title: {
          display: true,
          text: title + ' Chart'
        },
        layout: {
          padding: {
            bottom: 45
          }
        },
        scales: {
          xAxes: [{
            type: 'hierarchical',
            stacked: true,
            offset: true,
            gridLines: {
              offsetGridLines: true
            }
          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true
            }
          }]
      }
    }
});


  });
}

drawChart('{{ $outcomes[0]->id }}-{{ $outcomes[0]->code }}. {{ $outcomes[0]->outcome }}');

</script>


@endsection
