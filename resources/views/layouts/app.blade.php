<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  <link href="{{ asset('plugins') }}/bootstrap/css/bootstrap.css" rel="stylesheet">

  <!-- Waves Effect Css -->
  <link href="{{ asset('plugins') }}/node-waves/waves.css" rel="stylesheet" />

  <!-- Animation Css -->
  <link href="{{ asset('plugins') }}/animate-css/animate.css" rel="stylesheet" />

  <!-- Morris Chart Css-->
  <link href="{{ asset('plugins') }}/morrisjs/morris.css" rel="stylesheet" />

  <!-- Custom Css -->
  <link href="{{ asset('css') }}/style.css" rel="stylesheet">

  <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
  <link href="{{ asset('css') }}/themes/all-themes.css" rel="stylesheet" />

  <!-- Select picker -->
  <link href="{{ asset('plugins') }}/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />

  @yield('css')


</head>
<body class="theme-blue-grey">
  <!-- Top Bar -->
  <nav class="navbar">
    <div class="container-fluid">
      <div class="navbar-header col-md-1" style="margin: 0px">
      	<a href="/">
          <h1 style="font-size: 2em; vertical-align: sub;padding: 0; margin: 0; color: white">EQA</h1>
        </a>
      </div>

      @if(!Request::is('/') && !is_null(Request::segment(2)) )
      <div class="navbar-header col-md-2" style="margin: 0px">
        <select class="selectpicker form-control" onchange="Menu(this)">
          @foreach(config('eqa.menu') as $key => $value)
            @if(Request::segment(2) == strtolower($key))
              <optgroup label="{{ $key }}">
                @foreach($value as $k => $v)
                <option value="{{ route($k.'.index') }}" {{ Request::segment(3) == $k ? 'selected' : '' }}>{{ $v }}</option>
                @endforeach
              </optgroup>
            @endif
          @endforeach
        </select>
      </div>

      @php $submenu = config('eqa.submenu'); @endphp

      {{-- @if(\Laratrust::can("manage-".Request::segment(3))) --}}
      @if(Request::segment(3) != null && isset($submenu[Request::segment(3)]) && count($submenu[Request::segment(3)]) > 0)
      <div class="navbar-header col-md-2" style="margin: 0px">
        <select class="selectpicker form-control" onchange="Menu(this)" title="Action">
        @isset($submenu[Request::segment(3)]['menu'])
          <optgroup label="Menu">
            @foreach($submenu[Request::segment(3)]['menu'] as $k => $v)
            <option value="{{ route($k) }}">{{ $v }}</option>
            @endforeach
          </optgroup>
        @endif
          <optgroup label="Action">
            @foreach($submenu[Request::segment(3)]['action'] as $k => $v)
            <option value="{{ route($k) }}">{{ $v }}</option>
            @endforeach
          </optgroup>
        </select>
      </div>
      @endif
      {{-- @endif --}}

      @yield('submenu')

      @endif

      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">

          <li class="pull-right" style="margin-top:8px">
            <a href="javascript:void(0);" class="dropdown-toggle" style="margin:0px;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              @if(!Request::is('/') && session()->exists('organization_id'))
              Edit : {{ session('organization_name') }}
              @endif
              <i class="material-icons">more_vert</i>
            </a>
            <ul class="dropdown-menu pull-right">
                <li class="header">Current Role : Admin<br>
                @if(session()->exists('organization_id'))
                Active Organization : {{ session('organization_name') }}
                @endif
                </li>
                <li><a href="{{ route('selectOrganization') }}?redirect={{ Request::url() }}">Change Organization</a></li>
                <li><a href="javascript:void(0);">Change Profil</a></li>
                <li><a href="{{ route('logout') }}" class="list-group-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form></li>
            </ul>
          </li>
        </ul>
      </div>

    </div>
  </nav>

  <section style="padding-top: 7em">
    @yield('content')
  </section>

  <!-- Jquery Core Js -->
  <script src="{{ asset('plugins') }}/jquery/jquery.min.js"></script>

  <!-- Bootstrap Core Js -->
  <script src="{{ asset('plugins') }}/bootstrap/js/bootstrap.js"></script>

  <!-- Bootstrap Select -->
  <script src="{{ asset('plugins') }}/bootstrap-select/js/bootstrap-select.min.js"></script>
  <script src="{{ asset('plugins') }}/bootstrap-notify/bootstrap-notify.min.js"></script>

  <script type="text/javascript">
    $('.selectpicker').selectpicker();

  	function Aplikasi(x) {
  	}
  	function Menu(x) {
  		window.location.href = x.value;
  	}

    @if(session()->has('status.message'))
      $.notify({ message: '{!! session()->get('status.message') !!}' },{ type: '{{ session()->get('status.level') }}' });
    @endif


  </script>

  @yield('js')

</body>

</html>
