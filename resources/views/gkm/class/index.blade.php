@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="block-header">
    <ol class="breadcrumb breadcrumb-bg-indigo">
        <li><a href="/"><i class="material-icons">home</i> Home</a></li>
        <li><a href="/"><i class="material-icons">pageview</i> Analysis</a></li>
        <li class="active"><i class="material-icons">library_books</i>Test Outcome Analysis</li>
    </ol>
  </div>
  <div class="row clearfix">

    <!-- Basic Tabs -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>Test Outcome Analysis</h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                  </li>
              </ul>
          </div>
          <div class="body">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                @foreach($tests as $v)
                  <li role="presentation" @if($loop->first) class="active" @endif>
                      <a href="#home" data-toggle="tab" onclick="getAjax({{ $v->id }},'{{ $v->title }}')">{{ $v->title }}</a>
                  </li>
                @endforeach
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="home">
                      <p>
                        <div id="chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                      </p>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- #END# Basic Table -->

  </div>

</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>

<script>

  var path = '{{ route("outcomes.index") }}';

  function generateChart(title,categories,items) {
    $('#chart').highcharts({
        chart: {
          type: 'column'
        },
        title: {
            text: title+' Chart'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Attainments'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '<span style=\"color:{series.color}\">{series.name}</span>: <b> {point.y}</b> of {point.stackTotal} / ({point.percentage:.0f}%) of 100 %<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent',

                  dataLabels: {
                  enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    },
                  format: '{point.percentage:.0f} %',
                },
            },

        },
        series: items
    });
  }

  function getAjax(id,title) {
    $.ajax({
      url: path+'/api/tests/'+id,
      type: 'get',
      error: function() {
        swal({
          title: "Error 404",
          text: "Failed When Try To Get Data",
          type: "danger"
        });
      },
      success: function(res) {
        generateChart(title,res.outcomes,res.items_data);
      }
    });
  }

  getAjax({{ $tests[0]->id }},'{{ $tests[0]->title }}')


</script>
@endsection
