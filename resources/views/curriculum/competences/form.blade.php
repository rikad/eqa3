@extends('layouts.app')

@section('content')

<div class="container-fluid">

  <!-- Horizontal Layout -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li><a href="{{ route('outcomes.index') }}">Outcomes</a></li>
          <li class="active">Add New Competences</li>
        </ol>

        <div class="body">
            @if(isset($data))
              {!! Form::model($data, ['url' => route('competences.update', $data->id), 'files'=>true,'method'=>'put','class'=>'form-horizontal']) !!}
            @else
              {!! Form::open(['url' => route('competences.store'), 'method' => 'post','files'=>true, 'class'=>'form-horizontal']) !!}
            @endif

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="curriculum_id">Curriculum</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('curriculum_id') ? ' error' : '' }}">
                    {!! Form::select('curriculum_id', $curriculum_id, null, ['class' => 'form-control selectpicker curriculum-selection', 'required'=>'true', 'onchange'=>'getOutcomes(this.value)'] ) !!}
                  </div>
                  @if ($errors->has('curriculum_id'))
                      <label class="error" for="curriculum_id">
                          {{ $errors->first('curriculum_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="outcome_id">Outcome</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="outcomes_selection form-line{{ $errors->has('outcome_id') ? ' error' : '' }}">
                    {!! Form::select('outcome_id', [], null, ['class' => 'form-control selectpicker', 'required'=>'true'] ) !!}
                  </div>
                  @if ($errors->has('outcome_id'))
                      <label class="error" for="outcome_id">
                          {{ $errors->first('outcome_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="level_id">Level</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('level_id') ? ' error' : '' }}">
                    <select name="level_id" class="selectpicker form-control" title="Choose Level" required="true">
                      @foreach($levels as $k => $level)
                        <optgroup label="{{ $k }}">
                          @foreach($level as $k => $v)
                          <option value="{{ $v->id }}" @if(old('level_id') == $v->id || isset($data) && $data->level_id == $v->id ) selected="selected" @endif>{{ $v->level }}. {{ $v->title }}</option>
                          @endforeach
                        </optgroup>
                      @endforeach
                    </select>
                  </div>
                  @if ($errors->has('level_id'))
                      <label class="error" for="level_id">
                          {{ $errors->first('level_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="code">Code</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('code') ? ' error' : '' }}">
                    {!! Form::text('code', null, 
                      [ 
                        'class'=>'form-control', 'pattern'=>'[A-Z0-9]{2,5}', 
                        'title'=>'Only One Capital Letter A - Z and Number', 
                        'required'=>'true',
                        'placeholder'=> 'A2',
                      ]) 
                    !!}
                  </div>
                  @if ($errors->has('code'))
                      <label class="error" for="code">
                          {{ $errors->first('code') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="competence">Competence</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('competence') ? ' error' : '' }}">
                    {!! Form::text('competence', null, 
                      [ 
                        'class'=>'form-control', 'pattern'=>'[A-Z a-z.,-]{4,250}', 
                        'title'=>'Only Letters minimum 4 - 30 character', 'required'=>'true',
                        'placeholder' => 'The ability to apply mathematical principles for engineering tasks.'
                      ]) 
                    !!}
                  </div>
                  @if ($errors->has('competence'))
                      <label class="error" for="competence">
                          {{ $errors->first('competence') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="description">Description</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('description') ? ' error' : '' }}">
                    {!! Form::textarea('description', null, ['class'=>'form-control', 'required'=>'true']) !!}
                  </div>
                  @if ($errors->has('description'))
                      <label class="error" for="description">
                          {{ $errors->first('description') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                <input type="submit" class="btn btn-primary m-t-15 waves-effect" value="Simpan">
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Horizontal Layout -->

</div>
@endsection

@section('css')
<link href="{{ asset('plugins') }}/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
@endsection

@section('js')
<script>

  var outcomesCt = document.querySelector('.outcomes_selection');

  @if(isset($data))
  var oldOutcome_id = '{{ $data->outcome_id }}';
  @else
  var oldOutcome_id = '{{ old("outcome_id") }}';
  @endif
  
  function sendAjax(path,cb) {
    $.ajax({
      url: path,
      error: function() {
        alert('error');
      },
      success: function(res) {
        cb(res);
      }
    });
  }

  function getOutcomes(id) {
    var path = '{{ route("outcomes.index") }}/?id='+id;
    sendAjax(path, function(data) {
      var output = '<select class="form-control selectpicker" required="true" name="outcome_id">';

      data.forEach(function(v,i){
        output += '<option value="'+v.id+'" ';
        output += oldOutcome_id == v.id ? 'selected="selected"' : ''; 
        output += '>'+v.code+'. '+v.outcome+'</option>';
      });

      output += '</select>';
      outcomesCt.innerHTML = output;

      $('.selectpicker').selectpicker(); //refresh list

    })
  }

  getOutcomes(document.querySelector('.curriculum-selection select').value);

</script>
@endsection
