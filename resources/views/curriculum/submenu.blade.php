<div class="navbar-header col-md-2" style="margin: 0px">
    <select class="form-control selectpicker" data-live-search="true" onchange="submenu(this)">
      @foreach($curriculums as $v)
      <option value="{{ $v->id }}">{{ $v->title }}</option>
      @endforeach
    </select>
</div>
