@extends('layouts.app')

@section('content')

<div class="container-fluid">

  <!-- Horizontal Layout -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li><a href="{{ route('outcomes.index') }}">Outcomes</a></li>
          <li class="active">Add New Outcomes</li>
        </ol>

        <div class="body">
            @if(isset($data))
              {!! Form::model($data, ['url' => route('outcomes.update', $data->id), 'files'=>true,'method'=>'put','class'=>'form-horizontal']) !!}
            @else
              {!! Form::open(['url' => route('outcomes.store'), 'method' => 'post','files'=>true, 'class'=>'form-horizontal']) !!}
            @endif

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="curriculum_id">Curriculum</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('curriculum_id') ? ' error' : '' }}">
                    {!! Form::select('curriculum_id', $curriculum_id, null, ['class' => 'form-control selectpicker', 'required'=>'true'] ) !!}
                  </div>
                  @if ($errors->has('curriculum_id'))
                      <label class="error" for="curriculum_id">
                          {{ $errors->first('curriculum_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="code">Code</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('code') ? ' error' : '' }}">
                    {!! Form::text('code', null, ['class'=>'form-control', 'pattern'=>'[A-Z]{0,1}', 'title'=>'Only One Capital Letter A - Z', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('code'))
                      <label class="error" for="code">
                          {{ $errors->first('code') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="outcome">Outcome Name</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('outcome') ? ' error' : '' }}">
                    {!! Form::text('outcome', null, ['class'=>'form-control', 'pattern'=>'[A-Z a-z]{4,30}', 'title'=>'Only Letters minimum 4 - 30 character', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('outcome'))
                      <label class="error" for="outcome">
                          {{ $errors->first('outcome') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="description">Description</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('description') ? ' error' : '' }}">
                    {!! Form::textarea('description', null, ['class'=>'form-control', 'required'=>'true']) !!}
                  </div>
                  @if ($errors->has('description'))
                      <label class="error" for="description">
                          {{ $errors->first('description') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                <input type="submit" class="btn btn-primary m-t-15 waves-effect" value="Simpan">
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Horizontal Layout -->

</div>
@endsection

@section('css')
<link href="{{ asset('plugins') }}/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
@endsection
