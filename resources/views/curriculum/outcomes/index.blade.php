@extends('layouts.app')

@section('submenu')
  @include('curriculum.submenu')
@endsection

@section('content')

<div class="container-fluid outcomes-section">

  <div class="row clearfix">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <div class="card">

        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li class="active">Outcomes</li>
        </ol>

        <div class="body">
          <table class="table table-hover" id="konten"></table>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="container-fluid competences-section" style='display:none'>

  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">

        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li><a href="#" onclick="backToOutcomes()">Outcomes</a></li>
          <li class="active">A</li>
        </ol>

        <div class="body">
          <table class="table table-striped" id="competences"></table>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />

<style>
#konten > tbody {
  cursor:pointer;
}

</style>
@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>

<script>

  var path = '{{ route("outcomes.index") }}';
  var path2 = '{{ route("competences.index") }}';

  function submenu(x) {
    table.ajax.url('?id='+x.value).load();
  }

  var table = $('#konten').DataTable({
    processing: true,
    serverSide: true,
    paging: false,
    ajax: '',
    columns: [
      { title: '#', data: 'no', name: 'no', searchable:false },
      { title: 'Code', data: 'code', name: 'code' },
      { title: 'Outcome', data: 'outcome', name: 'outcome' },
      { title: 'Description', data: 'description', name: 'description' },
      @if(\Laratrust::can("manage-outcomes"))
      { title: '', data: 'id', name: 'id', sortable: false, searchable: false,
          render: function(data) {
            return '<a href="'+path+'/'+data+'/edit"><button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i></button></a> <button onclick="deleteData('+data+')" class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>';
          }
      }
      @endif
    ],
    responsive: true
  });

  $('#konten tbody').on('click', 'td', function () {

    var data = table.row($(this).closest('tr')).data();
    var indexColumn = table.cell(this).index().column;
    var lengthColumns = table.columns().header().length - 1;

    if (indexColumn != lengthColumns) {
      showCompetences(data.id);
    }    

  } );

  $.notify({ message: 'Click Row For More Information of Outcomes' });

  var tableCompetences;
  function showCompetences(id) {

    document.querySelector('.outcomes-section').style.display = 'none';
    document.querySelector('.competences-section').style.display = '';

    tableCompetences = $('#competences').DataTable({
      processing: true,
      serverSide: true,
      paging: false,
      ajax: path+'?outcome_id='+id,
      columns: [
        { title: '#', data: 'no', name: 'no', searchable:false },
        { title: 'Code', data: 'code', name: 'code' },
        { title: 'Competence', data: 'competence', name: 'competence' },
        { title: 'Level', data: 'title', name: 'title', render: function(data,type,full) {
          return '<a href="#" onclick="getLevel('+full.level_id+')">' + full.level + '. ' + data + '</a>';
        } },
        { title: 'Domain', data: 'domain', name: 'domain', render: function(data,type,full) {
          return '<a href="#" onclick="getDomain('+full.domain_id+')">' + data + '</a>';
        } },
        @if(\Laratrust::can("manage-competences"))
        { title: '', data: 'id', name: 'id', sortable: false, searchable: false,
            render: function(data) {
              return '<a href="'+path2+'/'+data+'/edit"><button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i></button></a> <button onclick="deleteData2('+data+')" class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>';
            }
        }
        @endif
      ],
      responsive: true
    });
  }

  function getDomain(id) {
    $.ajax({
        url: '{{ route("domains.index") }}/'+id,
        type: 'GET',
        error: function() {
          alert('error');
        },
        success: function(res) {
          document.querySelector('.modal-title').innerHTML = res.domain;
          document.querySelector('.modal-body').innerHTML = '<p>'+res.description+'</p>';
          $('#myModal').modal();
        }
      });
  }

  function getLevel(id) {
    $.ajax({
        url: '{{ route("levels.index") }}/'+id,
        type: 'GET',
        error: function() {
          alert('error');
        },
        success: function(res) {
          document.querySelector('.modal-title').innerHTML = res.level+'. '+res.title;
          document.querySelector('.modal-body').innerHTML = '<p>'+res.description+'</p>';
          $('#myModal').modal();
        }
      });
  } 

  function backToOutcomes() {
    document.querySelector('.outcomes-section').style.display = '';
    document.querySelector('.competences-section').style.display = 'none';
    tableCompetences.destroy();
  }

  @if(\Laratrust::can("manage-outcomes"))
  function deleteData(id) {
    swal({
        title: "Are You Sure ?",
        text: "Data will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus!",
        closeOnConfirm: false
    }, function() {
        sendAjax();
    } );

    function sendAjax() {
      $.ajax({
        url: path+'/'+id,
        data: { '_token': '{{ csrf_token() }}' },
        type: 'DELETE',
        error: function() {
          swal({
              title: "Failed !",
              text: "Please delete data which related to this outcome to continue",
              type: "error"
            });
        },
        success: function(res) {
            swal({
              title: "Deleted !",
              text: "Data has been deleted",
              type: "success"
            }, function() {
              location.reload();
            });
        }
      });
    }
  }
  @endif

  @if(\Laratrust::can("manage-competences"))
  function deleteData2(id) {
    swal({
        title: "Are You Sure ?",
        text: "Data will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus!",
        closeOnConfirm: false
    }, function() {
        sendAjax();
    } );

    function sendAjax() {
      $.ajax({
        url: path2+'/'+id,
        data: { '_token': '{{ csrf_token() }}' },
        type: 'DELETE',
        error: function() {
          swal({
              title: "Failed !",
              text: "Please delete data which related to this competence to continue",
              type: "error"
            });
        },
        success: function(res) {
            swal({
              title: "Deleted !",
              text: "Data has been deleted",
              type: "success"
            }, function() {
              location.reload();
            });
        }
      });
    }
  }
  @endif

</script>
@endsection
