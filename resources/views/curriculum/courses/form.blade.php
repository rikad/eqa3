@extends('layouts.app')

@section('content')

<div class="container-fluid">

  <!-- Horizontal Layout -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li><a href="{{ route('courses.index') }}">Courses</a></li>
          <li class="active">Add New Course</li>
        </ol>

        <div class="body">
            @if(isset($data))
              {!! Form::model($data, ['url' => route('courses.update', $data->id), 'files'=>true,'method'=>'put','class'=>'form-horizontal']) !!}
            @else
              {!! Form::open(['url' => route('courses.store'), 'method' => 'post','files'=>true, 'class'=>'form-horizontal']) !!}
            @endif

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="curriculum_id">Curriculum</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group curriculum-selection">
                  <div class="form-line{{ $errors->has('curriculum_id') ? ' error' : '' }}">
                    {!! Form::select('curriculum_id', $curriculum_id, null, ['class' => 'form-control selectpicker','onchange'=>'updateCurriculum(this.value)', 'required'=>'true'] ) !!}
                  </div>
                  @if ($errors->has('curriculum_id'))
                      <label class="error" for="curriculum_id">
                          {{ $errors->first('curriculum_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="code">Code</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('code') ? ' error' : '' }}">
                    {!! Form::text('code', null, ['class'=>'form-control', 'pattern'=>'[A-Z0-9]{2,10}', 'title'=>'Only Capital Letters minimum 2 - 10 character', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('code'))
                      <label class="error" for="code">
                          {{ $errors->first('code') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="title_en">Title (english)</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('title_en') ? ' error' : '' }}">
                  {!! Form::text('title_en', null, ['class'=>'form-control','placeholder'=>"Introduction to Engineering and Design II", 'pattern'=>'[A-Z a-z]{2,80}', 'title'=>'Only Letters minimum 2 - 80 character', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('title_en'))
                      <label class="error" for="title_en">
                          {{ $errors->first('title_en') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="title">Title (indonesia)</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('title') ? ' error' : '' }}">
                    {!! Form::text('title', null, ['class'=>'form-control','placeholder'=>"Pengantar Rekayasa Desain II", 'pattern'=>'[A-Z a-z]{2,80}', 'title'=>'Only Letters minimum 2 - 80 character', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('title'))
                      <label class="error" for="title">
                          {{ $errors->first('title') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="credit">Credit</label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('credit') ? ' error' : '' }}">
                  {!! Form::number('credit', null, ['class'=>'form-control', 'placeholder'=>'2 sks','min'=>'1', 'max'=>'8', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('credit'))
                      <label class="error" for="credit">
                          {{ $errors->first('credit') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="semester">Semester</label>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('semester') ? ' error' : '' }}">
                  {!! Form::number('semester', null, ['class'=>'form-control', 'placeholder'=>'Semester 7','min'=>'1', 'max'=>'20', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('semester'))
                      <label class="error" for="semester">
                          {{ $errors->first('semester') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="component_id">Component</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="components_selection form-line{{ $errors->has('component_id') ? ' error' : '' }}">
                    {!! Form::select('component_id', [], null, ['class' => 'form-control selectpicker', 'required'=>'true'] ) !!}
                  </div>
                  @if ($errors->has('component_id'))
                      <label class="error" for="component_id">
                          {{ $errors->first('component_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="sylabus">Sylabus</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('sylabus') ? ' error' : '' }}">
                    {!! Form::textarea('sylabus', null, ['class'=>'form-control']) !!}
                  </div>
                  @if ($errors->has('sylabus'))
                      <label class="error" for="sylabus">
                          {{ $errors->first('sylabus') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="checkbox">
                      <input name="compulsory" type="checkbox" id="Compulsory" class="filled-in" {{ !is_null(old('compulsory')) || isset($data->compulsory) && $data->compulsory ? 'checked' : '' }}>
                      <label for="Compulsory">Compulsory </label>
                      <input name="class" type="checkbox" id="Class" class="filled-in" {{ !is_null(old('class')) || isset($data->class) && $data->class ? 'checked' : '' }}>
                      <label for="Class">Class </label>
                      <input name="lab" type="checkbox" id="Lab" class="filled-in" {{ !is_null(old('lab')) || isset($data->lab) && $data->lab? 'checked' : '' }}>
                      <label for="Lab">Lab </label>
                      <input name="field" type="checkbox" id="Field" class="filled-in" {{ !is_null(old('field')) || isset($data->field) && $data->field ? 'checked' : '' }}>
                      <label for="Field">Field</label>
                  </div>
                  @if ($errors->has('compulsory'))
                      <label class="error" for="compulsory">
                          {{ $errors->first('compulsory') }}
                      </label>
                  @endif

                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="competences">Competences</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="competences_selection form-line{{ $errors->has('competences') ? ' error' : '' }}">
                    <select class="form-control selectpicker" multiple data-live-search="true" required="true"></select>
                  </div>
                  @if ($errors->has('competences'))
                      <label class="error" for="competences">
                          {{ $errors->first('competences') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                <input type="submit" class="btn btn-primary m-t-15 waves-effect" value="Save">
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Horizontal Layout -->

</div>
@endsection

@section('js')
<script>

  var componentCt = document.querySelector('.components_selection');
  var competenceCt = document.querySelector('.competences_selection');

  @if(isset($data))
  var oldComponent_id = '{{ $data->component_id }}';
  var oldCompetence_id = {!! $data->competence_id !!};
  @else
  var oldComponent_id = '{{ old("component_id") }}';
  var oldCompetence_id = {!! old("competence_id") == null ? '[]' : json_encode(old("competence_id")) !!};
  @endif
  
  function sendAjax(path,cb) {
    $.ajax({
      url: path,
      error: function() {
        alert('error');
      },
      success: function(res) {
        cb(res);
      }
    });
  }

  function getComponents(id) {
    var path = '{{ route("components.index") }}/?id='+id;
    sendAjax(path, function(data) {
      var output = '<select class="form-control selectpicker" required="true" name="component_id">';

      data.forEach(function(v,i){
        output += '<option value="'+v.id+'" ';
        output += oldComponent_id == v.id ? 'selected="selected"' : ''; 
        output += '>'+v.code+'. '+v.component+'</option>';
      });

      output += '</select>';
      componentCt.innerHTML = output;

      $('.selectpicker').selectpicker(); //refresh list

    })
  }

  function getCompetences(id) {
    var path = '{{ route("competences.index") }}/?id='+id;
    sendAjax(path, function(data) {
      var output = '<select class="form-control selectpicker" multiple data-live-search="true" required="true" name="competence_id[]">';

      data.forEach(function(v,i){
        output += '<option value="'+v.id+'" ';

        oldCompetence_id.forEach(function(v2,i) {
          output += v2 == v.id ? 'selected="selected"' : '';
        })

        output += '>'+v.code+'. '+v.competence+'</option>';
      });

      output += '</select>';
      competenceCt.innerHTML = output;

      $('.selectpicker').selectpicker(); //refresh list

    })
  }

  function updateCurriculum(id) {
    getComponents(id);
    getCompetences(id);
  }

  updateCurriculum(document.querySelector('.curriculum-selection select').value);

</script>
@endsection
