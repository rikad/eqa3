@extends('layouts.app')

@section('content')

<div class="container-fluid">

  <!-- Horizontal Layout -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li><a href="{{ route('attainments.index') }}">Attainments</a></li>
          <li class="active">Add New Attainment</li>
        </ol>

        <div class="body">
            @if(isset($data))
              {!! Form::model($data, ['url' => route('attainments.update', $data->id), 'files'=>true,'method'=>'put','class'=>'form-horizontal']) !!}
            @else
              {!! Form::open(['url' => route('attainments.store'), 'method' => 'post','files'=>true, 'class'=>'form-horizontal']) !!}
            @endif

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="curriculum_id">Curriculum</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('curriculum_id') ? ' error' : '' }}">
                    {!! Form::select('curriculum_id', $curriculum_id, null, ['class' => 'form-control selectpicker', 'required'=>'true'] ) !!}
                  </div>
                  @if ($errors->has('curriculum_id'))
                      <label class="error" for="curriculum_id">
                          {{ $errors->first('curriculum_id') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="attainment">Attainment</label>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('attainment') ? ' error' : '' }}">
                    {!! Form::number('attainment', null, ['class'=>'form-control', 'placeholder'=>'1','min'=>'0', 'max'=>'4', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('attainment'))
                      <label class="error" for="attainment">
                          {{ $errors->first('attainment') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="title">Title</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('title') ? ' error' : '' }}">
                    {!! Form::text('title', null, ['class'=>'form-control', 'pattern'=>'[A-Z a-z]{4,30}', 'title'=>'Only Letters minimum 4 - 30 character', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('title'))
                      <label class="error" for="title">
                          {{ $errors->first('title') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="treshold">Treshold</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('treshold') ? ' error' : '' }}">
                  {!! Form::number('treshold', null, ['class'=>'form-control', 'placeholder'=>'80','min'=>'0', 'max'=>'100', 'required'=>'true' ]) !!}
                  </div>
                  @if ($errors->has('treshold'))
                      <label class="error" for="treshold">
                          {{ $errors->first('treshold') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                <label for="description">Description</label>
              </div>
              <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                <div class="form-group">
                  <div class="form-line{{ $errors->has('description') ? ' error' : '' }}">
                    {!! Form::textarea('description', null, ['class'=>'form-control', 'required'=>'true']) !!}
                  </div>
                  @if ($errors->has('description'))
                      <label class="error" for="description">
                          {{ $errors->first('description') }}
                      </label>
                  @endif
                </div>
              </div>
            </div>

            <div class="row clearfix">
              <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                <input type="submit" class="btn btn-primary m-t-15 waves-effect" value="Save">
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Horizontal Layout -->

</div>
@endsection

@section('css')
<link href="{{ asset('plugins') }}/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
@endsection
