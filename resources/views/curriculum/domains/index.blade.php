@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="block-header">
    <ol class="breadcrumb breadcrumb-bg-indigo">
        <li><a href="/"><i class="material-icons">home</i> Home</a></li>
        <li><a href="/"><i class="material-icons">dashboard</i> Setting</a></li>
        <li class="active"><i class="material-icons">library_books</i>Learning Domains</li>
    </ol>
  </div>

  <div class="row clearfix">

    <!-- Basic Table -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            Learning Domains
          </h2>
          <ul class="header-dropdown m-r--5">
            <li class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">more_vert</i>
              </a>
              <ul class="dropdown-menu pull-right">
              @if(\Laratrust::can("create-domains"))
                <li><a href="{{ route('domains.create') }}">Tambah</a></li>
              @endif
              </ul>
            </li>
          </ul>
        </div>

        <div class="body table-responsive">
          <table class="table" id="konten">
            <thead>
              <tr>
                <th>#</th>
                <th>Domain</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
    <!-- #END# Basic Table -->

  </div>

</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>
<script>

  var showPath = '{{ route("domains.show",'') }}';
  var editPath = '{{ route("domains.edit",'=') }}';
  var deletePath =  '{{ route("domains.destroy",'') }}';
  var addPath =  '{{ route("domains.create") }}';

  $(function() {

    function genEditPath(id) {
      return editPath.replace('=',id);
    }

    function genShowPath(id) {
      return showPath+'/'+id;
    }


    $('#konten').DataTable({
      processing: true,
      serverSide: true,
      ajax: '',
      columns: [
        { data: 'no', name: 'no', searchable:false },
        { data: 'domain', name: 'domain' },
        { data: 'description', name: 'description' },
        { data: 'id', name: 'id', sortable: false,
            render: function(data) {
              return '<a href="'+genShowPath(data)+'"><button class="btn btn-success btn-xs"><i class="material-icons">pageview</i></button></a> @if(\Laratrust::can("update-domains")) <a href="'+genEditPath(data)+'"><button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i></button></a>@endif @if(\Laratrust::can("delete-domains")) <button onclick="deleteData('+data+')" class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button> @endif';
            }
        }
      ],
      responsive: true
    });
  });

  @if(\Laratrust::can("delete-domains"))
  function deleteData(id) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Data akan Hilang Setelah Di Hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus!",
        closeOnConfirm: false
    }, function() {
        sendAjax();
    } );

    function sendAjax() {
      $.ajax({
        url: deletePath+'/'+id,
        data: { '_token': '{{ csrf_token() }}' },
        type: 'DELETE',
        error: function() {
          alert('error');
        },
        success: function(res) {
            swal({
              title: "Berhasil Di Hapus!",
              text: "Data Telah Berhasil Di Hapus",
              type: "success"
            }, function() {
              location.reload();
            });
        }
      });
    }
  }
  @endif

</script>
@endsection
