@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="block-header">
    <ol class="breadcrumb breadcrumb-bg-indigo">
        <li><a href="/"><i class="material-icons">home</i> Home</a></li>
        <li><a href="/"><i class="material-icons">dashboard</i> Curriculum</a></li>
        <li class="active"><i class="material-icons">library_books</i>Imports Excell</li>
    </ol>
  </div>

  <div class="row clearfix">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uploadcard">
      <div class="card">
          <div class="header">
              <h2>
                  IMPORT FILE
              </h2>
          </div>
          <div class="body">
              <div class="dz" style="text-align:center; padding:2em; border: 2px solid transparent !important; background-color: #eee !important; cursor: pointer">
                  <div class="dz-message">
                      <div class="drag-icon-cph">
                          <i class="material-icons" style="font-size:4em">touch_app</i>
                      </div>
                      <h3>Drop files here or click to upload.</h3>
                  </div>                              
              </div>

              <div style="display:none" class="loader"></div>

          </div>
      </div>
    </div>

    <!-- Basic Table -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 preview">
      <div class="card">
        <div class="header">
          <h2>
            PREVIEW
          </h2>
        </div>

        <div class="body">

          <div style="display:none" align="right">
            <button type="btn btn-primary" onclick="save()" id="save">Save</button>
          </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
              <li role="presentation" class="active"><a href="#information" data-toggle="tab">Informasi</a></li>
              <li role="presentation"><a href="#mahasiswa" data-toggle="tab">Mahasiswa</a></li>
              <li role="presentation"><a href="#pi-tab" data-toggle="tab">PI</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="information">
                <table class="table" style="width:50%">
                  <tr><th>FAKULTAS</th><td>:</td> <td class="fakultas"></td> <td>OK</td></tr>
                  <tr><th>PROGRAM STUDI</th><td>:</td> <td class="prodi"></td> <td>OK</td></tr>
                  <tr><th>KODE KULIAH</th><td>:</td> <td class="kode"></td> <td>OK</td></tr>
                  <tr><th>NAMA KULIAH</th><td>:</td> <td class="nama"></td> <td>OK</td></tr>
                  <tr><th>KELAS</th><td>:</td> <td class="kelas"></td> <td>OK</td></tr>
                  <tr><th>SKS</th><td>:</td> <td class="sks"></td> <td>OK</td></tr>
                  <tr><th>SEMESTER</th><td>:</td> <td class="semester"></td> <td>OK</td></tr>
                  <tr><th>TAHUN</th><td>:</td> <td class="tahun"></td> <td>OK</td></tr>
                  <tr><th>JUMLAH PESERTA</th><td>:</td> <td class="peserta"></td> <td>OK</td></tr>
                  <tr><th>NAMA DOSEN</th><td>:</td> <td class="dosen"></td> <td>OK</td></tr>
                  <tr><th>GRADES</th><td>:</td> <td class="grades"></td> <td>OK</td></tr>
                  <tr><th>TESTS</th><td>:</td> <td class="tests"></td> <td>OK</td></tr>
                  <tr><th>OUTCOME</th><td>:</td> <td class="so"></td> <td>OK</td></tr>
                  <tr><th>PI</th><td>:</td> <td class="pi"></td> <td>OK</td></tr>
                  <tr><th>ATTAINMENTS</th><td>:</td> <td class="attainments"></td> <td>OK</td></tr>
                </table>
              </div>              

              <div role="tabpanel" class="tab-pane fade" id="mahasiswa"></div>
              <div role="tabpanel" class="tab-pane fade" id="pi-tab"></div>
            </div>

        </div>
      </div>
    </div>
    <!-- #END# Basic Table -->


  </div>

</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />

<!-- handsontable Css -->
<link href="{{ asset('plugins') }}/handsontable/handsontable.min.css" rel="stylesheet" />

<!--loader -->
<style>
.loader {
  margin: auto;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}
/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
.dz-preview {
  display:none;
}
@endsection

@section('js')

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>

<!-- xlsx Js -->
<script src="{{ asset('plugins') }}/js-xlsx/shim.js"></script>
<script src="{{ asset('plugins') }}/js-xlsx/xlsx.full.min.js"></script>

<!-- HandsonTable Plugin Js -->
<script src="{{ asset('plugins') }}/handsontable/handsontable.min.js"></script>

<script src="{{ asset('plugins') }}/dropzone/min/dropzone.min.js"></script>

<script>
var myDropzone = new Dropzone(".dz", { url: '/', createImageThumbnails: false, autoProcessQueue: false, addedfile: function(file) { console.log(file); handleFile(file); } });

var rABS = true; // true: readAsBinaryString ; false: readAsArrayBuffer
var finalData = {};
var loader = document.querySelector('.loader');
var preview = document.querySelector('.preview');
var uploadcard = document.querySelector('.uploadcard');

function handleFile(file) {

  //show loader
  loader.style.display = "";

  var reader = new FileReader();
  reader.onload = function(e) {
    var data = e.target.result;
    if(!rABS) data = new Uint8Array(data);
    var workbook = XLSX.read(data, {type: rABS ? 'binary' : 'array'});

    /* DO SOMETHING WITH workbook HERE */
    wb = workbook; // for debug send to global variable

    getInformation(workbook);
    display();

    //show preview
    loader.style.display = "none"; //hide loader
    uploadcard.style.display = "none"; // hide upload
    preview.style.display = ""; // show

  };
  if(rABS) reader.readAsBinaryString(file); else reader.readAsArrayBuffer(file);
}

function display(){

  document.querySelector('.fakultas').innerHTML = finalData.fakultas;
  document.querySelector('.prodi').innerHTML = finalData.prodi;
  document.querySelector('.kode').innerHTML = finalData.kode;
  document.querySelector('.nama').innerHTML = finalData.nama;
  document.querySelector('.kelas').innerHTML = finalData.kelas;
  document.querySelector('.sks').innerHTML = finalData.sks;
  document.querySelector('.semester').innerHTML = finalData.semester;
  document.querySelector('.tahun').innerHTML = finalData.tahun;
  document.querySelector('.peserta').innerHTML = finalData.peserta;
  document.querySelector('.dosen').innerHTML = finalData.dosen;
  document.querySelector('.grades').innerHTML = finalData.grades.html;
  document.querySelector('.tests').innerHTML = finalData.tests.html;
  document.querySelector('.so').innerHTML = finalData.so.toString();
  document.querySelector('.pi').innerHTML = finalData.pi.toString();
  document.querySelector('.attainments').innerHTML = finalData.attainment.html;

}

function getInformation(workbook) {
  var finalSheet = workbook.Sheets['Final'];
  var gradeSheet = workbook.Sheets['Grade'];
  var soSheet = workbook.Sheets['SO'];
  var piSheet = workbook.Sheets['PI'];

  finalData.fakultas = finalSheet['C1'] ? finalSheet['C1'].v : '';
  finalData.prodi = finalSheet['C2'] ? finalSheet['C2'].v : '';
  finalData.kode = finalSheet['C3'] ? finalSheet['C3'].v : '';
  finalData.nama = finalSheet['C4'] ? finalSheet['C4'].v : '';
  finalData.kelas = finalSheet['C5'] ? finalSheet['C5'].v : '';
  finalData.sks = finalSheet['C6'] ? finalSheet['C6'].v : '';
  finalData.semester = finalSheet['C7'] ? finalSheet['C7'].v : '';
  finalData.tahun = finalSheet['C8'] ? finalSheet['C8'].v : '';
  finalData.peserta = finalSheet['C9'] ? finalSheet['C9'].v : '';
  finalData.dosen = finalSheet['C10'] ? finalSheet['C10'].v : '';
  finalData.grades = getGrade(gradeSheet);
  finalData.tests = getTests(gradeSheet);
  finalData.so = getPI(soSheet);
  finalData.pi = getSO(piSheet);
  finalData.attainment = getAttainment(piSheet);
  finalData.mahasiswa = getMahasiswa(finalSheet);

}

function getGrade(gradeSheet) {
  var json = [];
  var html = '';
  for (let index = 8; index <= 14; index++) {
    var grade = gradeSheet['F'+index] ? gradeSheet['F'+index].v : '';
    var treshold = gradeSheet['E'+index] ? gradeSheet['E'+index].v : '';
    json.push({ grade:grade, treshold: treshold });
    html += grade + ' = ' + treshold +'<br>';
  }

  return { json: json, html: html };
}

function getTests(gradeSheet) {

  var column = 'E';
  var html = '';
  var data = {};

  while(true) {

    if(gradeSheet[column+'18'] == undefined || gradeSheet[column+'19'] == undefined || gradeSheet[column+'18'].v == 'BONUS') {
      break;
    }

    var bobot = gradeSheet[column+'18'] ? gradeSheet[column+'18'].v : '';
    var test = gradeSheet[column+'19'] ? gradeSheet[column+'19'].v : '';
    html += test + ' = ' + bobot +'<br>';
    
    data[test] = {bobot : bobot, items : getItems(wb.Sheets['T_'+test]) };

    column = (parseInt(column, 36)+1).toString(36).toUpperCase();
  }

  return { data: data, html: html }

}

function getItems(sheet) {
  var column = 'E';
  var items = [];
  var weights = [];
  
  while(true) {

    if(sheet[column+'18'] == undefined || sheet[column+'19'] == undefined || sheet[column+'19'].v == 'GROUP') {
      break;
    }

    var bobot = sheet[column+'18'] ? sheet[column+'18'].v : '';
    var item = sheet[column+'19'] ? sheet[column+'19'].v : '';

    items.push(item);
    weights.push(bobot);

    column = (parseInt(column, 36)+1).toString(36).toUpperCase();
  }

  if(items.length > 0) {

    //get data to rows array
    var csv = XLSX.utils.sheet_to_csv(sheet);
    var rows = csv.split('\n');
    var scores = [];
    var pi = [];

    for (let index = 20; index < finalData.peserta + 20; index++) {
      var row = rows[index].split(',');
      
      scores.push(row.splice(4,items.length));
    }

    for (let index = 150; index < 170; index++) {
      var row = rows[index].split(',');

      if(row[3] == ' ' || row[3] == 0 || row[3] == null || row[3] == '' || row[3] == 'TOTAL') {
        break;
      }
      
      pi.push(row.splice(4,items.length));
    }

  }
  
  return { itemsName: items, weights : weights, scores : scores, pi : pi }
}

function getSO(soSheet) {
  var so = [];
  var column = 'C';

  while(true) {

    if(soSheet[column+'20'] == undefined) {
      break;
    }

    var code = soSheet[column+'20'] ? soSheet[column+'20'].v : '';
    so.push(code);

    column = (parseInt(column, 36)+1).toString(36).toUpperCase();
  }

  return so;
}

function getPI(piSheet) {
  var pi = [];
  var column = 'C';

  while(true) {

    if(piSheet[column+'20'] == undefined) {
      break;
    }

    var code = piSheet[column+'20'] ? piSheet[column+'20'].v : '';
    pi.push(code);

    column = (parseInt(column, 36)+1).toString(36).toUpperCase();
  }

  return pi;
}

function getAttainment(piSheet) {
  var json = [
    { attainment : 1, treshold : piSheet['J13'] ? piSheet['J13'].v : '' },
    { attainment : 2, treshold : piSheet['J14'] ? piSheet['J14'].v : '' },
    { attainment : 3, treshold : piSheet['J15'] ? piSheet['J15'].v : '' },
    { attainment : 4, treshold : piSheet['J16'] ? piSheet['J16'].v : '' }
  ];

  var html = '';
  json.forEach(function(v){
    html += v.attainment + ' = ' + v.treshold + '<br>';
  })

  return { json: json, html: html };
}


function getMahasiswa(worksheet) {

  //get data to rows array
  var csv = XLSX.utils.sheet_to_csv(worksheet);
  var rows = csv.split('\n');
  var tmpRows = [];
  var start = 13;

  for (let index = start; index < finalData.peserta; index++) {
    var row = rows[index].split(',');
    var tmp = [row[1],row[2]];
    tmpRows.push(tmp);
  }

  return tmpRows;
}


function save() {
  swal({
      title: "Are you sure ?",
      text: "make sure data is valid !",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes !",
      closeOnConfirm: false
  }, function() {
      sendAjax();
  } );

  function sendAjax() {
    $.ajax({
      url: '{{ route('imports.store') }}',
      data: { '_token': '{{ csrf_token() }}', 'data' : finalData },
      type: 'POST',
      error: function() {
        alert('error');
      },
      success: function(res) {
          swal({
            title: "Berhasil Di kirim",
            text: "Data Telah Berhasil Di Kirim",
            type: "success"
          }, function() {
            alert('data yang di kirim : ' + res);
            location.reload();
          });
      }
    });
  }
}
</script>
@endsection
