@extends('layouts.app')

@section('submenu')
<div class="navbar-header col-md-3" style="margin: 0px">
    <select class="form-control selectpicker" data-live-search="true" onchange="submenu(this)">
      @foreach($curriculums as $v)
      <option value="{{ $v->id }}">{{ $v->title }} - {{ $v->organization }}</option>
      @endforeach
    </select>
</div>
@endsection

@section('content')

<div class="container-fluid">
  <div class="block-header">
    <ol class="breadcrumb breadcrumb-bg-indigo">
        <li><a href="/"><i class="material-icons">home</i> Home</a></li>
        <li><a href="/"><i class="material-icons">dashboard</i> Curriculum</a></li>
        <li class="active"><i class="material-icons">library_books</i>Performace Indicator</li>
    </ol>
  </div>

  <div class="row clearfix">

    <!-- Basic Table -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            Performace Indicator
          </h2>
          <ul class="header-dropdown m-r--5">
            <li class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">more_vert</i>
              </a>
            </li>
          </ul>
        </div>

        <div class="body">

        <table class="table table-bordered" id="konten">


        		<thead>
        			<tr>
                <th>Courses</th>
                @foreach($competences as $v)
                  <th>{{ $v->code }}</th>
                @endforeach
							</tr>
        		</thead>

        		<tbody>

              @foreach($courses as $v)
              <tr>
                @php $competence = explode(',',$v->competences) @endphp
                <td class="info">{{ $v->code }}</td>

                @foreach($competences as $o)
                  <td>{!! in_array($o->code,$competence) ? '<i class="material-icons">close</i>' : '' !!}</td>
                @endforeach

              </tr>
              @endforeach
        		</tbody>

        	</table>

          <!--body -->
        </div>
      </div>
    </div>
    <!-- #END# Basic Table -->

  </div>

</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.fixedColumns.min.css" rel="stylesheet">

<!-- Sweetalert Css -->
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />

@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.fixedColumns.min.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>
<script>

  var table = $('#konten').DataTable({
    scrollY : '80vh',
    scrollX : true,
    scrollCollapse: true,
    paging:         false,
    fixedColumns:   {
         leftColumns: 1
    }
  });

</script>
@endsection
