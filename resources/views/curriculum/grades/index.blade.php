@extends('layouts.app')

@section('submenu')
  @include('curriculum.submenu')
@endsection

@section('content')

<div class="container-fluid">

  <div class="row clearfix">

    <!-- Basic Table -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li class="active">Grades</li>
        </ol>
        <div class="body table-responsive">
          <table class="table" id="konten"></table>
        </div>
      </div>
    </div>
    <!-- #END# Basic Table -->

  </div>

</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Sweetalert Css -->
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>
<script>

  var path = '{{ route("grades.index") }}';

  function submenu(x) {
    table.ajax.url('?id='+x.value).load();
  }

  function genEditPath(id) {
    return path+'/'+id+'/edit';
  }

  function genShowPath(id) {
    return path+'/'+id;
  }


  var table = $('#konten').DataTable({
    processing: true,
    serverSide: true,
    paging: false,
    ajax: '',
    columns: [
      { title: '#', data: 'no', name: 'no', searchable:false },
      { title: 'Grade', data: 'grade', name: 'grade' },
      { title: 'Description', data: 'description', name: 'description' },
      { title: 'Numeric', data: 'numeric', name: 'numeric' },
      { title: 'Treshold', data: 'treshold', name: 'treshold' },
      @if(\Laratrust::can("manage-grades")) 
      { title: '', data: 'id', name: 'id', sortable: false,
          render: function(data) {
            return '<a href="'+genEditPath(data)+'"><button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i></button></a> <button onclick="deleteData('+data+')" class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>';
          }
      }
      @endif
    ],
    responsive: true
  });

  @if(\Laratrust::can("manage-grades"))
  function deleteData(id) {
    swal({
        title: "Apakah Anda Yakin ?",
        text: "Data akan Hilang Setelah Di Hapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus!",
        closeOnConfirm: false
    }, function() {
        sendAjax();
    } );

    function sendAjax() {
      $.ajax({
        url: path+'/'+id,
        data: { '_token': '{{ csrf_token() }}' },
        type: 'DELETE',
        error: function() {
          swal({
              title: "Failed !",
              text: "Please delete data which related to this grades to continue",
              type: "error"
            });
        },
        success: function(res) {
            swal({
              title: "Berhasil Di Hapus!",
              text: "Data Telah Berhasil Di Hapus",
              type: "success"
            }, function() {
              location.reload();
            });
        }
      });
    }
  }
  @endif

</script>
@endsection
