@extends('layouts.app')

@section('content')

<div class="container-fluid">

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Dashboard</h2>
                </div>
                <div class="body">
                    <table class="table" style="font-weight: bold;">
                        <tr><td>Nama <td>:</td> </td><td>{{ Auth::user()->name }}</td></tr>
                        <tr><td>Email <td>:</td> </td><td>{{ Auth::user()->email }}</td></tr>
                        <tr>
                            <td>
                                Active Role <td>:</td> </td><td>{{ Auth::user()->roles[0]->display_name }}  ({{ session('organization_name') }})&nbsp;&nbsp;&nbsp; 
                                <a href="{{ route('selectOrganization') }}?redirect={{ Request::url() }}"><span class="label label-primary">Change Role</span></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>    

    <div class="row clearfix">

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="card card-clickable" url="{{ route('outcomes.index') }}">
                <div class="header bg-blue-grey">
                    <h2>Curriculums</h2>
                </div>
                <div class="body">
                    <i class="material-icons menu-icon">assignment</i>
                    <p>Manage an Objectives & Outcomes</p>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="card card-clickable" url="{{ route('classes.index') }}">
                <div class="header bg-blue-grey">
                    <h2>
                        Learning
                    </h2>
                </div>
                <div class="body">
                    <i class="material-icons menu-icon">school</i>
                    <p>Manage a classes,students and grades</p>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="card card-clickable" url="{{ route('outcome.index') }}">
                <div class="header bg-blue-grey">
                    <h2>Assesments</h2>
                </div>
                <div class="body">
                    <i class="material-icons menu-icon">assessment</i>
                    <p>Review progress with interactive charts</p>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="card card-clickable" url="{{ route('outcomes.index') }}">
                <div class="header bg-blue-grey">
                    <h2>
                        People and Organizations
                    </h2>
                </div>
                <div class="body">
                    <i class="material-icons menu-icon">group</i>
                    <p>Manage Study Program, etc</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        @permission("read-settings")
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class="card card-clickable" url="{{ route('users.index') }}">
                <div class="header bg-blue-grey">
                    <h2>
                        Settings
                    </h2>
                </div>
                <div class="body">
                    <i class="material-icons menu-icon">settings</i>
                    <p>Manage Users, Role & Configuration</p>
                </div>
            </div>
        </div>
        @endpermission

    </div>
</div>

</div>
@endsection

@section('css')
<style>
.card-clickable {
    text-align: center;
}
.card-clickable:hover {
    cursor: pointer;
    opacity: 0.8;
}
.menu-icon {
    font-size: 10em;
}
</style>
@endsection

@section('js')
<script>
  $(document).ready(function(){
    var card = $(".card-clickable");

    card.click(function(){
      window.location.href = $(this).attr('url');
    });

  });
</script>    
@endsection
