@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <div class="card">
        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li>Setting</li>
          <li class="active">Users</li>
        </ol>

        <div class="body">
          <table class="table table-hover" id="konten"></table>
        </div>

      </div>
    </div>
  </div>
</div>

@endsection


@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="{{ asset('plugins') }}/sweetalert/sweetalert.min.js"></script>

<script>

  var path = '{{ route("users.index") }}';

  var table = $('#konten').DataTable({
    processing: true,
    serverSide: true,
    ajax: '',
    columns: [
      { title: '#', data: 'no', name: 'no', searchable:false },
      { title: 'Prefix', data: 'prefix', name: 'prefix' },
      { title: 'Name', data: 'name', name: 'name' },
      { title: 'Suffix', data: 'suffix', name: 'suffix' },
      { title: 'Email', data: 'email', name: 'email' },
      @if(\Laratrust::can("manage-users"))
      { title: '', data: 'id', name: 'id', sortable: false, searchable: false,
          render: function(data) {
            return '<a href="'+path+'/'+data+'/edit"><button class="btn btn-primary btn-xs"><i class="material-icons">mode_edit</i></button></a> <button onclick="deleteData('+data+')" class="btn btn-danger btn-xs"><i class="material-icons">delete</i></button>';
          }
      }
      @endif
    ],
    responsive: true
  });

  @if(\Laratrust::can("manage-users"))
  function deleteData(id) {
    swal({
        title: "Are You Sure ?",
        text: "Data will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus!",
        closeOnConfirm: false
    }, function() {
        sendAjax();
    } );

    function sendAjax() {
      $.ajax({
        url: path+'/'+id,
        data: { '_token': '{{ csrf_token() }}' },
        type: 'DELETE',
        error: function() {
          swal({
              title: "Failed !",
              text: "Please delete data which related to this outcome to continue",
              type: "error"
            });
        },
        success: function(res) {
            swal({
              title: "Deleted !",
              text: "Data has been deleted",
              type: "success"
            }, function() {
              location.reload();
            });
        }
      });
    }
  }
  @endif

</script>
@endsection
