@extends('layouts.app')

@section('content')

<div class="container-fluid">

  <div class="row clearfix">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

      <div class="card">

        <ol class="breadcrumb breadcrumb-bg-blue-grey">
          <li><a href="/"><i class="material-icons">home</i> Home</a></li>
          <li class="active">Select Role & Organization</li>
        </ol>

        <div class="body">
          <table class="table table-hover" id="konten"></table>
        </div>

      </div>
    </div>
  </div>
</div>

</div>
@endsection

@section('css')
<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="{{ asset('plugins') }}/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins') }}/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ asset('plugins') }}/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<script>

  var urlParams = new URLSearchParams(window.location.search);
  var redirectUrl = urlParams.get('redirect');

  var table = $('#konten').DataTable({
    processing: true,
    serverSide: true,
    ajax: '',
    columns: [
      { data: 'no', name: 'no', searchable:false },
      { data: 'abbreviation', name: 'abbreviation', title:'Abbreviation' },
      { data: 'organization', name: 'organization', title:'Organizations' },
      @if(!\Auth::user()->hasRole('admin'))
      { data: 'display_name', name: 'display_name', title:'Role' },
      @endif
      { data: 'id', name: 'id', sortable: false,
          render: function(data) {
            return '<a href="{{ route('selectOrganization') }}/'+data+'?redirect='+redirectUrl+'"><button class="btn btn-primary btn-xs">Select</button></a>';
          }
      }
    ],
    responsive: true
  });
</script>
@endsection

