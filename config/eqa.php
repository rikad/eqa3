<?php

return [
	'menu' => [
		'Curriculum' => [
//			'domains' => 'Curriculums',
			'attainments' => 'Attainment',
			'components' => 'Components',
			'courses' => 'Courses',
			'grades' => 'Grades',
			'objectives' => 'Objectives',
			'outcomes' => 'Outcomes',
			'performances' => 'Performace Indicator',
		],
		'Learning' => [
			'classes' => 'Classes',
		],
		'Assesment' => [
			'outcome' => 'Outcomes Analysis',
			'course' => 'Courses Analysis',
			'cohort' => 'Cohort Analysis',
			'curriculum' => 'Curriculum Analysis',
			'student' => 'Student Analysis',
		],
		'Settings' => [
			'users' => 'Users Management',
			'roles' => 'Roles & Permissions',
		],
	],

	'submenu' => [
		'attainments' => [
			'menu' => [],
			'action' => [
				'attainments.create' => 'Add New Attainment',
			]
		],
		'classes' => [
			'menu' => [
				'classes.index' => 'List Test',
				'classes.index' => 'List Outcomes Assesment'
			],
			'action' => [
				'classes.index' => 'Tambah Test',
				'classes.index' => 'Edit Bobot',
				'classes.index' => 'Atur Bobot Assesment',
				'imports.index' => 'Import Excel'
			]
		],
		'cohort' => [
			'menu' => [
				'cohort.index' => 'Normal Cohort',
				'cohort.create' => 'Cummulative Cohort'
			],
			'action' => []
		],
		'courses' => [
			'menu' => [
			],
			'action' => [
				'courses.create' => 'Add New Course',
			]
		],
		'grades' => [
			'menu' => [],
			'action' => [
				'grades.create' => 'Add New Grades',
			]
		],
		'objectives' => [
			'menu' => [],
			'action' => [
				'objectives.create' => 'Add New Objective',
			]
		],
		'outcomes' => [
			'action' => [
				'outcomes.create' => 'Add New Outcome',
				'competences.create' => 'Add New Competence',
			]
		],
		'roles' => [
			'action' => [
				'roles.create' => 'Add New Role',
				'permissions.create' => 'Add New Permission',
			]
		]
	],


];
