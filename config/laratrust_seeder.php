<?php

return [
    'role_structure' => [
        'admin' => [
            'attainments' => 'r,m',
            'curriculums' => 'r,m',
            'competences' => 'r,m',
            'components' => 'r,m',
            'courses' => 'r,m',
            'classes' => 'r,m',
            'domains' => 'r,m',
            'grades' => 'r,m',
            'levels' => 'r,m',
            'lecturers' => 'r,m',
            'objectives' => 'r,m',
            'outcomes' => 'r,m',
            'students' => 'r,m',
            'semester' => 'r,m',
            'settings' => 'r,m',
            'users' => 'r,m',
            'roles' => 'r,m',
            'permissions' => 'r,m'

        ],
        'adminprodi' => [
            'attainments' => 'r,m',
            'curriculums' => 'r,m',
            'components' => 'r,m',
            'domains' => 'r,m',
            'course' => 'r,m',
            'competences' => 'r,m',
            'grades' => 'r,m',
            'levels' => 'r,m',
            'objectives' => 'r,m',
            'outcomes' => 'r,m',
        ],
        'lecturer' => [
            'assesment' => 'r,m',
            'classes' => 'r',
            'tests' => 'r,m',
        ],
        'student' => [
            'analisys' => 'r,m',
        ],

    ],
    'permissions_map' => [
        'r' => 'read',
        'm' => 'manage',
    ]
];
