<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
	return view('home');
})->name('home')->middleware('auth');

Route::get('/selectOrganization','SelectOrganizationController@index')->name('selectOrganization')->middleware('web','auth');
Route::get('/selectOrganization/{id}','SelectOrganizationController@show')->middleware('web','auth');

Route::group(['prefix'=>'apps/settings', 'middleware' => ['web','auth','organization'] ], function () {

	Route::post('roles/editRolePermission', 'Admin\RolesController@editRolePermission');

	Route::resource('users', 'Admin\UsersController');
	Route::resource('roles', 'Admin\RolesController');
	Route::resource('permissions', 'Admin\PermissionsController');

});

Route::group(['prefix'=>'apps', 'middleware' => ['web','auth','organization'] ], function () {

	Route::get('assesment/cohort/cummulative', 'Assesment\CohortController@cummulative');
	Route::get('assesment/student/cummulative', 'Assesment\StudentController@cummulative');

	Route::resource('assesment/course', 'Assesment\CourseController');
	Route::resource('assesment/outcome', 'Assesment\OutcomeController');
	Route::resource('assesment/cohort', 'Assesment\CohortController');
	Route::resource('assesment/curriculum', 'Assesment\CurriculumController');
	Route::resource('assesment/student', 'Assesment\StudentController');

	Route::resource('curriculum/attainments', 'Curriculum\AttainmentsController');
	Route::resource('curriculum/curriculums', 'Curriculum\CurriculumsController');
	Route::resource('curriculum/competences', 'Curriculum\CompetencesController');
	Route::resource('curriculum/components', 'Curriculum\ComponentsController');
	Route::resource('curriculum/courses', 'Curriculum\CoursesController');
	Route::resource('curriculum/domains', 'Curriculum\DomainsController');
	Route::resource('curriculum/grades', 'Curriculum\GradesController');
	Route::resource('curriculum/imports', 'Curriculum\ImportsController');
	Route::resource('curriculum/levels', 'Curriculum\LevelsController');
	Route::resource('curriculum/objectives', 'Curriculum\ObjectivesController');
	Route::resource('curriculum/outcomes', 'Curriculum\OutcomesController');
	Route::resource('curriculum/performances', 'Curriculum\PerformancesController');

	Route::resource('learning/classes', 'Learning\ClassesController');

});
