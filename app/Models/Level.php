<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'pc_competence_levels';

    protected $fillable = [
        'level','title','description','created_at','updated_at'
    ];
}
