<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'pl_tests';

    protected $fillable = [
        'class_id','title','date','weight','graded','rubric','grading','created_at','updated_at','deleted'
    ];
}
