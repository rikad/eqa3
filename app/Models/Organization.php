<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'po_organizations';

    protected $fillable = [
        'form_id','parent_id','name','abbr','created_at','updated_at'
    ];
}
