<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    protected $table = 'pc_competences';

    protected $fillable = [
        'outcome_id','level_id','code','competence','description','created_at','updated_at'
    ];
}
