<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'po_forms';

    protected $fillable = [
        'name','abbr','layer','created_at','updated_at'
    ];
}
