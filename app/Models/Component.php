<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $table = 'pc_components';

    protected $fillable = [
        'curriculum_id','code','component','description','created_at','updated_at'
    ];
}
