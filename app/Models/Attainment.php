<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attainment extends Model
{
    protected $table = 'pc_attainments';

    protected $fillable = [
        'curriculum_id','title','attainment','description','treshold','created_at','updated_at'
    ];
}
