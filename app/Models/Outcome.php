<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    protected $table = 'pc_outcomes';

    protected $fillable = [
        'curriculum_id','code','outcome','description','created_at','updated_at'
    ];
}
