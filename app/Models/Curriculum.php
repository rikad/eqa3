<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $table = 'pc_curriculums';

    protected $fillable = [
        'domain','description','created_at','updated_at'
    ];
}
