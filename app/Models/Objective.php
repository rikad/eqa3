<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objective extends Model
{
    protected $table = 'pc_objectives';

    protected $fillable = [
        'curriculum_id','number','objective','description','created_at','updated_at'
    ];
}
