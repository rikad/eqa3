<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'pc_courses';

    protected $fillable = [
        'curriculum_id','component_id','code','title','title_en','credit','semester','compulsory','lab','class','field','sylabus','created_at','updated_at'
    ];
}
