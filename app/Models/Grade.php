<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'pc_grades';

    protected $fillable = [
        'curriculum_id','grade','description','numeric','treshold','created_at','updated_at'
    ];
}
