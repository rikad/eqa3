<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $table = 'pc_competence_domains';

    protected $fillable = [
        'domain','description','created_at','updated_at'
    ];
}
