<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\Curriculum;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use DB;

class GradesController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'curriculum_id' => 'required|exists:pc_curriculums,id',
        'grade' => 'required|between:1,2',
        'numeric' => 'required|between:1,4',
        'treshold' => 'required|integer|between:1,100',
        'description' => 'required|between:1,240'
    ];

    public function index(Request $request)
    {
        $organization_id = $request->session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->orderBy('po_organizations.organization','ASC')
                        ->get();

        if ($request->ajax()) {
            if(count($curriculums) > 0) {
                $curriculum_id = isset($_GET['id']) ? e($_GET['id']) : $curriculums[0]->id;
            } else {
                $curriculum_id = 0;
            }

            DB::statement(DB::raw('set @rownum=0'));

            $data = Grade::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_grades.*')
                            ->where('pc_grades.curriculum_id',$curriculum_id);
            return Datatables::of($data)->make(true);
        }

        return view('curriculum.grades.index',[ 'curriculums' => $curriculums ]);
    }

    public function create()
    {
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        return view('curriculum.grades.form',[ 
            'curriculum_id' => $curriculums, 
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Grade::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been saved"
        ]);

        return redirect()->route('grades.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Grade::find($id);

        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');
                               
        return view('curriculum.gradess.form', [ 
            'curriculum_id' => $curriculums,
            'data' => $data,
        ]);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Grade::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been updated"
        ]);

        return redirect()->route('grades.index');
    }

    public function destroy($id)
    {
        $db = Grade::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Data has been deleted"
        ]);

        return 'ok';
    }

}
