<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use App\Models\Curriculum;
use App\Models\Outcome;
use App\Models\Competence;
use DB;


class OutcomesController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'curriculum_id' => 'required|exists:pc_curriculums,id',
        'code' => 'required|between:1,2',
        'outcome' => 'required|between:1,30',
        'description' => 'required|between:1,250'
    ];

    public function index(Request $request)
    {

        $organization_id = $request->session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->get();

        if ($request->ajax()) {
            if(count($curriculums) > 0) {
                $curriculum_id = $request->has('id') ? $request->input('id') : $curriculums[0]->id;
            } else {
                $curriculum_id = 0;
            }

            DB::statement(DB::raw('set @rownum=0'));

            if($request->has('outcome_id')) {
                $outcome_id = $request->input('outcome_id');
                $data = Competence::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_competences.*','pc_competence_levels.title','pc_competence_levels.level','pc_competence_domains.domain','pc_competences.level_id','pc_competence_levels.domain_id')
                                ->join('pc_competence_levels','pc_competence_levels.id','pc_competences.level_id')
                                ->join('pc_competence_domains','pc_competence_domains.id','pc_competence_levels.domain_id')
                                ->where('outcome_id',$outcome_id);
            } else {
                $data = Outcome::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_outcomes.*')->where('curriculum_id',$curriculum_id);
            }

            if($request->has('draw')) {
                return Datatables::of($data)->make(true);
            }

            return $data->get();

        }

        return view('curriculum.outcomes.index',[ 'curriculums' => $curriculums ]);
    }

    public function create()
    {
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        return view('curriculum.outcomes.form',[ 'curriculum_id' => $curriculums ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Outcome::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been saved"
        ]);

        return redirect()->route('outcomes.index');
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        $data = Outcome::find($id);

        return view('curriculum.outcomes.form', [ 
            'curriculum_id' => $curriculums,
            'data' => $data,
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Outcome::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been updated"
        ]);

        return redirect()->route('outcomes.index');
    }

    public function destroy($id)
    {
        $db = Outcome::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Data has been deleted"
        ]);

        return 'ok';
    }

}