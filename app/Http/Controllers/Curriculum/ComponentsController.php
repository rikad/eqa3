<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use App\Models\Component;
use App\Models\Curriculum;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use DB;

class ComponentsController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'curriculum_id' => 'required|exists:pc_curriculums,id',
        'code' => 'required|between:1,10',
        'component' => 'required|between:1,50',
        'description' => 'required|between:1,240'
    ];

    public function index(Request $request)
    {
        $organization_id = $request->session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->orderBy('po_organizations.organization','ASC')
                        ->get();

        if ($request->ajax()) {
            if(count($curriculums) > 0) {
                $curriculum_id = isset($_GET['id']) ? e($_GET['id']) : $curriculums[0]->id;
            } else {
                $curriculum_id = 0;
            }

            DB::statement(DB::raw('set @rownum=0'));

            $data = Component::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_components.*')
                            ->where('pc_components.curriculum_id',$curriculum_id);

            if($request->has('draw')) {
                return Datatables::of($data)->make(true);
            }

            return $data->get();
        }

        return view('curriculum.components.index',[ 'curriculums' => $curriculums ]);
    }

    public function create()
    {
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        return view('curriculum.components.form',[ 
            'curriculum_id' => $curriculums, 
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Component::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been saved"
        ]);

        return redirect()->route('components.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Component::find($id);

        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id'); 
                               
        return view('curriculum.components.form', [ 
            'curriculum_id' => $curriculums,
            'data' => $data,
        ]);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Component::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been updated"
        ]);

        return redirect()->route('components.index');
    }

    public function destroy($id)
    {
        $db = Component::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Data has been deleted"
        ]);

        return 'ok';
    }

}
