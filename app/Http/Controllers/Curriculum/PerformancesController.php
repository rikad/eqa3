<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use DB;
use App\Models\Course;
use App\Models\Curriculum;


class PerformancesController extends \App\Http\Controllers\Controller
{

    private $validationRule = [
          'domain' => 'required|unique:pc_competence_performances,domain',
          'description' => 'required'
    ];

    public function index(Request $request)
    {

        $program_id = 1;
        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                          ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                          ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                          ->where('program_id',$program_id)
                          ->orderBy('pc_curriculums.id','DESC')
                          ->orderBy('po_organizations.organization','ASC')
                          ->get();

        $curriculum_id = isset($_GET['id']) ? e($_GET['id']) : $curriculums[0]->id;

        $courses = Course::select('pc_courses.*',DB::raw('GROUP_CONCAT(pc_competences.code) AS competences'))
                        ->leftJoin('pc_courses_competences','pc_courses_competences.course_id','pc_courses.id')
                        ->join('pc_competences','pc_courses_competences.competence_id','pc_competences.id')
                        ->where('curriculum_id',$curriculum_id)
                        ->groupBy('pc_courses.id')
                        ->get();

        $competences = DB::table('pc_competences')->select('pc_competences.*')
                          ->join('pc_outcomes','pc_outcomes.id','pc_competences.outcome_id')
                          ->where('pc_outcomes.curriculum_id',$curriculum_id)
                          ->get();

        return view('curriculum.performances.index',[
          'curriculums' => $curriculums,
          'competences' => $competences,
          'courses' => $courses
        ]);
    }

    public function create()
    {
        return view('curriculum.domain.form');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Course::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('curriculum.domain.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Role::find($id);

        return view('roles.form')->with(compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validationRule['name'] = $this->validationRule['name'].','.$id; //exception unique rule for name
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Role::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('roles.index');
    }

    public function destroy($id)
    {
        $db = Role::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Berhasil Di Hapus"
        ]);

        return 'ok';
    }

}
