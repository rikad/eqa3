<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use App\Models\Domain;
use DB;


class DomainsController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'domain' => 'required|unique:pc_competence_domains,domain',
        'description' => 'required'
    ];

    public function index(Request $request)
    {

        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));

            $data = Domain::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_competence_domains.*');
            return Datatables::of($data)->make(true);
        }

        return view('curriculum.domains.index');
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
    }

    public function show($id)
    {
        $data = Domain::find($id);

        return $data;
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
    }

}
