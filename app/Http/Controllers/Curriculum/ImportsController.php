<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;


class ImportsController extends \App\Http\Controllers\Controller
{

    public function index(Request $request)
    {
        return view('curriculum.imports.index');
    }

    public function create()
    {
        return view('curriculum.domain.form');
    }

    public function store(Request $request)
    {
      //get from excel

      $data = $request->all();

      return $data;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
    }

}
