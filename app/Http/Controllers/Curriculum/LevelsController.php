<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use App\Models\Level;
use App\Models\Curriculum;
use DB;


class LevelsController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'level' => 'required|unique:pc_competence_Levels,level',
        'title' => 'required|unique:pc_competence_Levels,title',
        'description' => 'required'
    ];

    public function index(Request $request)
    {

        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));

            $data = Level::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_competence_levels.*');

            if($request->has('draw')) {
                return Datatables::of($data)->make(true);
            }

            return $data->get();

        }

        return view('curriculum.Levels.index');
    }

    public function create()
    {
        return view('curriculum.Levels.form');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Level::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('curriculum.Levels.index');
    }

    public function show($id)
    {
        $data = Level::find($id);

        return $data;
    }

    public function edit($id)
    {
        $data = Level::find($id);

        return view('Levels.form')->with(compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validationRule['domain'] = $this->validationRule['domain'].','.$id; //exception unique rule for name
        $this->validationRule['title'] = $this->validationRule['title'].','.$id; //exception unique rule for name
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Level::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('Levels.index');
    }

    public function destroy($id)
    {
        $db = Level::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Berhasil Di Hapus"
        ]);

        return 'ok';
    }

}
