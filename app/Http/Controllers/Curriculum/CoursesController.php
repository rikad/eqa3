<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use DB;
use App\Models\Course;
use App\Models\Component;
use App\Models\Curriculum;


class CoursesController extends \App\Http\Controllers\Controller
{   

    private $validationRule = [
        'curriculum_id' => 'required|exists:pc_curriculums,id',
        'component_id' => 'required|exists:pc_components,id',
        'code' => 'required|between:1,10',
        'credit' => 'required|integer|between:1,8',
        'semester' => 'required|integer|between:1,20',
        'title_en' => 'required|between:1,100',
        'title' => 'required|between:1,100',
        'sylabus' => 'nullable|between:1,500',
    ];

    public function index(Request $request)
    {

        $organization_id = $request->session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                          ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                          ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                          ->where('po_organizations.id',$organization_id)
                          ->orderBy('pc_curriculums.id','DESC')
                          ->orderBy('po_organizations.organization','ASC')
                          ->get();

        if ($request->ajax()) {

            if(count($curriculums) > 0) {
                $curriculum_id = isset($_GET['id']) ? e($_GET['id']) : $curriculums[0]->id;
            } else {
                $curriculum_id = 0;
            }

            DB::statement(DB::raw('set @rownum=0'));

            $data = Course::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_courses.*')->where('curriculum_id',$curriculum_id);

            if($request->has('draw')) {
                return Datatables::of($data)->make(true);
            }

            return $data->get();
        }

        return view('curriculum.courses.index',[ 'curriculums' => $curriculums ]);
    }

    public function create()
    {
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        return view('curriculum.courses.form',[ 
            'curriculum_id' => $curriculums, 
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();
        $data['compulsory'] = $request->has('compulsory') ? 1 : 0;
        $data['class'] = $request->has('class') ? 1 : 0;
        $data['lab'] = $request->has('lab') ? 1 : 0;
        $data['field'] = $request->has('field') ? 1 : 0;

        $db = Course::create($data);

        foreach( $data['competence_id'] as $v ) {
            //bugs level_id inconsisten database soon fix
            DB::table('pc_courses_competences')->insert(['course_id'=>$db->id, 'competence_id'=>$v, 'level_id'=>1]);
        }

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been saved"
        ]);

        return redirect()->route('courses.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Course::find($id);

        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id'); 
        
        $competence_id = DB::table('pc_courses_competences')->select('pc_competences.*')
                        ->join('pc_competences','pc_competences.id','pc_courses_competences.competence_id')
                        ->where('pc_courses_competences.course_id',$id)
                        ->pluck('id');

        $data['competence_id'] = $competence_id;
                               
        return view('curriculum.courses.form', [ 
            'curriculum_id' => $curriculums,
            'data' => $data,
        ]);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRule);

        $data = $request->all();
        $data['compulsory'] = $request->has('compulsory') ? 1 : 0;
        $data['class'] = $request->has('class') ? 1 : 0;
        $data['lab'] = $request->has('lab') ? 1 : 0;
        $data['field'] = $request->has('field') ? 1 : 0;

        $db = Course::find($id);

        $db->update($data);

        DB::table('pc_courses_competences')->where('course_id',$id)->delete();
        foreach( $data['competence_id'] as $v ) {
            //bugs level_id inconsisten database soon fix            
            DB::table('pc_courses_competences')->insert(['course_id'=>$id, 'competence_id'=>$v, 'level_id'=>1]);
        }

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been updated"
        ]);

        return redirect()->route('courses.index');
    }

    public function destroy($id)
    {
        $db = Course::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Data has been deleted"
        ]);

        return 'ok';
    }

}