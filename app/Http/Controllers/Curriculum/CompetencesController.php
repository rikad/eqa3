<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use App\Models\Curriculum;
use App\Models\Competence;
use App\Models\Domain;
use App\Models\Level;
use App\Models\Outcome;
use DB;


class CompetencesController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'curriculum_id' => 'required|exists:pc_curriculums,id',
        'outcome_id' => 'required|exists:pc_outcomes,id',
        'level_id' => 'required|exists:pc_competence_levels,id',
        'code' => 'required|between:1,2',
        'competence' => 'required|between:1,250',
        'description' => 'required|between:1,250'
    ];

    public function index(Request $request)
    {
        $organization_id = $request->session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                          ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                          ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                          ->where('po_organizations.id',$organization_id)
                          ->orderBy('pc_curriculums.id','DESC')
                          ->orderBy('po_organizations.organization','ASC')
                          ->get();

        if ($request->ajax()) {

            if(count($curriculums) > 0) {
                $curriculum_id = isset($_GET['id']) ? e($_GET['id']) : $curriculums[0]->id;
            } else {
                $curriculum_id = 0;
            }

            DB::statement(DB::raw('set @rownum=0'));

            $data = Competence::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_competences.*')
                        ->join('pc_outcomes','pc_competences.outcome_id','pc_outcomes.id')
                        ->where('pc_outcomes.curriculum_id',$curriculum_id);

            if($request->has('draw')) {
                return Datatables::of($data)->make(true);
            }

            return $data->get();
        }
    }

    public function create()
    {
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        $levels = [];

        foreach(Domain::get() as $v) {
            $levels[$v->domain] = Level::where('domain_id',$v->id)->get();
        }

        return view('curriculum.competences.form',[ 
            'curriculum_id' => $curriculums, 
            'levels' => $levels,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Competence::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been saved"
        ]);
        
        return redirect()->route('outcomes.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {

        $data = Competence::find($id);
        $outcome = Outcome::find($data->outcome_id);
        $data['curriculum_id'] = $outcome->curriculum_id;
        
        $organization_id = session()->get('organization_id');

        $curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                        ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                        ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                        ->where('po_organizations.id',$organization_id)
                        ->orderBy('pc_curriculums.id','DESC')
                        ->pluck('title','id');

        $levels = [];

        foreach(Domain::get() as $v) {
            $levels[$v->domain] = Level::where('domain_id',$v->id)->get();
        }

        return view('curriculum.competences.form', [ 
            'curriculum_id' => $curriculums,
            'data' => $data,
            'levels' => $levels,
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Competence::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Data has been updated"
        ]);

        return redirect()->route('outcomes.index');
    }

    public function destroy($id)
    {
        $db = Competence::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Data has been deleted"
        ]);

        return 'ok';
    }

}