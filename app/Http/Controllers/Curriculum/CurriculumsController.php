<?php

namespace App\Http\Controllers\Curriculum;

use Illuminate\Http\Request;
use App\Models\Objective;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use DB;


class CurriculumsController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
        'curriculum_id' => 'required|exists:pc_curriculums,id',
        'objective' => 'required|unique:pc_competence_objectives,objective',
        'description' => 'required'
    ];

    public function index(Request $request)
    {

        if ($request->ajax()) {

            DB::statement(DB::raw('set @rownum=0'));

            $data = Objective::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pc_competence_objectives.*')
                              ->where('curriculum_id',$curriculum_id);
            return Datatables::of($data)->make(true);
        }

        return view('curriculum.objectives.index');
    }

    public function create()
    {
        return view('curriculum.objective.form');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = objective::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('curriculum.objectives.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Role::find($id);

        return view('curriculum.objectives.form')->with(compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validationRule['name'] = $this->validationRule['name'].','.$id; //exception unique rule for name
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Role::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('curriculum.objectives.index');
    }

    public function destroy($id)
    {
        $db = Role::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Berhasil Di Hapus"
        ]);

        return 'ok';
    }

}