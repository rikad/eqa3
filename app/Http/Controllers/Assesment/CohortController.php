<?php

namespace App\Http\Controllers\Assesment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class CohortController extends Controller
{


    private $curriculum_id = 2;

    private $cohorts;

    public function __construct () {

      $this->cohorts = DB::table('pl_semesters')->select(DB::raw('ANY_VALUE(year) as year'))->groupBy('year')->get();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('assesment/cohort', ['cohorts' => $this->cohorts ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cummulative()
    {
      return view('assesment/cohort-cummulative', ['cohorts' => $this->cohorts ]);
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('assesment/cohort-cummulative', ['cohorts' => $this->cohorts ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cohort = $id;

        $cohort1 = $cohort+1;
        $cohort2 = $cohort+2;
        $sqlWhere = 'pc_outcomes.curriculum_id = 2 AND ((pa_directs.year="'.$cohort.
        '") AND ((pa_directs.smt=3) OR (pa_directs.smt=4))) '.
        'OR ((pa_directs.year="'.$cohort1.
        '") AND ((pa_directs.smt=5) OR (pa_directs.smt=6))) '.
        'OR ((pa_directs.year="'.$cohort2.
        '") AND ((pa_directs.smt=7) OR (pa_directs.smt=8)))';

          $result = DB::table('pc_outcomes')
            ->select(DB::raw('pc_outcomes.code as outcome, smt, pc_courses.code as code, pc_courses.title_en as course, pa_directs.year, a1, a2, a3, a4'))
            ->join('pa_directs','pc_outcomes.id','pa_directs.outcome_id')
            ->join('pc_courses','pa_directs.course_id','pc_courses.id')
            ->orderBy('outcome', 'ASC')
            ->orderBy('smt', 'ASC')
            ->orderBy('pa_directs.code', 'ASC')
            ->orderBy('pa_directs.year', 'ASC')
            ->where(DB::raw($sqlWhere))
            ->get();
          return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
