<?php

namespace App\Http\Controllers\Assesment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Curriculum;

class CurriculumController extends Controller
{

    private $program_id = 1;

    private $curriculums;

    public function __construct()
    {
      $this->curriculums = Curriculum::select('po_organizations.organization','pc_curriculums.id','pc_curriculums.title')
                          ->join('pc_programs','pc_programs.id','pc_curriculums.program_id')
                          ->join('po_organizations','po_organizations.id','pc_programs.organization_id')
                          ->where('program_id',$this->program_id)
                          ->orderBy('pc_curriculums.id','DESC')
                          ->orderBy('po_organizations.organization','ASC')
                          ->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('assesment/curriculum', ['curriculums' => $this->curriculums ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = DB::table('pc_outcomes')
          ->select(DB::raw('pc_outcomes.code as outcome, smt, pc_courses.code as code, pc_courses.title_en as course, pa_directs.year, a1, a2, a3, a4'))
          ->join('pa_directs','pc_outcomes.id','pa_directs.outcome_id')
          ->join('pc_courses','pa_directs.course_id','pc_courses.id')
          ->orderBy('outcome', 'ASC')
          ->orderBy('smt', 'ASC')
          ->orderBy('pa_directs.code', 'ASC')
          ->orderBy('pa_directs.year', 'ASC')
          ->where('pc_outcomes.curriculum_id','=',$id)
          ->get();
        return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
