<?php

namespace App\Http\Controllers\Assesment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $curriculum_id = 2;

    private $courses;

    public function __construct()
    {
      $this->courses = DB::table('pc_courses')->select('pc_courses.*')
                              ->join('pc_curriculums','pc_curriculums.id', 'pc_courses.curriculum_id')
                             ->join('pc_programs','pc_programs.id', 'pc_curriculums.program_id')
                             ->where('pc_curriculums.id',$this->curriculum_id)
                             ->orderBy('pc_courses.code','ASC')
                             ->get();
    }

    public function index()
    {
       return view('assesment/course', ['courses' => $this->courses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * SELECT pc_outcomes.curriculum_id, pc_outcomes.code as outcome, pc_courses.code, pl_semester.year, a1, a2, a3, a4
     *   FROM pc_curriculums
     *   INNER JOIN pc_outcomes ON pc_curriculums.id = pc_outcomes.curriculum_id
     *   INNER JOIN pa_direct ON pc_outcomes.id = pa_direct.outcome_id
     *   INNER JOIN pc_courses ON pa_direct.course_id = pc_courses.id
     *   INNER JOIN pl_semester ON pa_direct.semester_id = pl_semester.id
     *   ORDER BY id, outcome, code, year
     */

    public function show($id)
    {

      $course_id = $id != null ? $id : $this->courses[0]->id;

      $result = DB::table('pc_outcomes')
        ->select(DB::raw('pc_outcomes.curriculum_id, pc_outcomes.code as outcome, pc_courses.code as code, pc_courses.title_en as course, pa_directs.year, smt, a1, a2, a3, a4'))
        ->join('pa_directs','pc_outcomes.id','pa_directs.outcome_id')
        ->join('pc_courses','pa_directs.course_id','pc_courses.id')
        ->orderBy('curriculum_id', 'ASC')
        ->orderBy('outcome', 'ASC')
        ->orderBy('smt', 'ASC')
        ->orderBy('pa_directs.code', 'ASC')
        ->orderBy('pa_directs.year', 'ASC')
        ->where('pc_courses.id',$course_id)
        ->get();

        return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
