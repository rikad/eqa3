<?php

namespace App\Http\Controllers\Assesment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;




class OutcomeController extends Controller
{

    private $curriculum_id = 2;

    private $outcomes;

    public function __construct () {
      $this->outcomes = DB::table('pc_outcomes')
        ->select('pc_outcomes.*')
        ->where('pc_outcomes.curriculum_id','=', $this->curriculum_id)
        ->get();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('assesment/outcome', [ 'outcomes' => $this->outcomes ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $result = DB::table('pc_outcomes')
          ->select(DB::raw('pc_outcomes.code as outcome, smt, pc_courses.code as code, pc_courses.title_en as course, pa_directs.year, a1, a2, a3, a4'))
          ->join('pa_directs','pc_outcomes.id','pa_directs.outcome_id')
          ->join('pc_courses','pa_directs.course_id','pc_courses.id')
          ->orderBy('outcome', 'ASC')
          ->orderBy('smt', 'ASC')
          ->orderBy('pa_directs.code', 'ASC')
          ->orderBy('pa_directs.year', 'ASC')
          ->where('pc_outcomes.id','=', $id)
          ->get();
        return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
