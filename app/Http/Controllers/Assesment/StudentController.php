<?php

namespace App\Http\Controllers\Assesment;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;
use DB;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
          DB::statement(DB::raw('set @rownum=0'));

          $program_id = 1;

          $data = DB::table('pl_students')->select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pl_students.*')
                              ->where('pl_students.program_id',$program_id);

          return Datatables::of($data)->make(true);
      }

      return view('assesment/student-index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

      if ($request->ajax()) {
        //$student_number = "13312058";

        $result = DB::table('pa_student_directs')
            ->select(DB::raw('pl_students.student_number, pc_outcomes.code as outcome, pc_courses.semester, pc_courses.code, pa_student_directs.attainment'))
            ->join('pl_students','pa_student_directs.student_id','pl_students.id')
            ->join('pc_outcomes','pa_student_directs.outcome_id','pc_outcomes.id')
            ->join('pl_classes','pa_student_directs.class_id','pl_classes.id')
            ->join('pc_courses','pl_classes.course_id','pc_courses.id')
            ->orderBy('pa_student_directs.student_id', 'ASC')
            ->orderBy('pc_outcomes.code', 'ASC')
            ->orderBy('pc_courses.semester', 'ASC')
            ->orderBy('pc_courses.code', 'ASC')
            ->where('pl_students.id','=',$id)
            ->get();

          return response()->json($result);
      }

      $student = DB::table('pl_students')->where('pl_students.id',$id)->first();

      return view('assesment/student-chart', [ 'student' => $student ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


/*
SELECT student_id, pc_courses.code, pc_courses.semester, attainment FROM `pa_student_directs`
	JOIN pl_students ON pl_students.id = pa_student_directs.student_id
    JOIN pl_classes ON pl_classes.id = pa_student_directs.class_id
    JOIN pc_courses ON pc_courses.id = pl_classes.course_id
    WHERE (attainment >= 1) AND (attainment<=4)
    ORDER BY student_id, pc_courses.semester, pc_courses.code
    */



}
