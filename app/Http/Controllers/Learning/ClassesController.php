<?php

namespace App\Http\Controllers\Learning;

use Illuminate\Http\Request;
use App\Models\Test;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Session;
use DB;


class ClassesController extends \App\Http\Controllers\Controller
{


    private $validationRule = [
                'domain' => 'required|unique:pc_competence_domains,domain',
                'description' => 'required'
    ];

    public function index(Request $request)
    {
      $studyProgramId = 1;
      $classes = DB::table('pl_classes')->select('pl_classes.id','pc_courses.code','pl_semesters.title')
                             ->join('pc_courses','pc_courses.id', 'pl_classes.course_id')
                             ->join('pc_curriculums','pc_curriculums.id', 'pc_courses.curriculum_id')
                             ->join('pc_programs','pc_programs.id', 'pc_curriculums.program_id')
                             ->join('pl_semesters','pl_semesters.id', 'pl_classes.semester_id')
                             ->where('pc_programs.id',$studyProgramId)
                             ->orderBy('pl_semesters.title','DESC')
                             ->get();

        if ($request->ajax()) {
            $classes_id = isset($_GET['id']) ? e($_GET['id']) : $classes[0]->id;
            DB::statement(DB::raw('set @rownum=0'));

            $data = Test::select(DB::raw('@rownum  := @rownum  + 1 AS no'),'pl_tests.*')->where('class_id',$classes_id);

            return Datatables::of($data)->make(true);
        }

        return view('dosen.classes.index', [ 'classes' => $classes ]);
    }

    public function create()
    {
        return view('dosen.classes.form');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Domain::create($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('dosen.classes.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Role::find($id);

        return view('roles.form')->with(compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validationRule['name'] = $this->validationRule['name'].','.$id; //exception unique rule for name
        $this->validate($request, $this->validationRule);

        $data = $request->all();

        $db = Role::find($id);

        $db->update($data);

        Session::flash("status", [
            "level"=>"success",
            "message"=>"Berhasil Di Simpan"
        ]);

        return redirect()->route('roles.index');
    }

    public function destroy($id)
    {
        $db = Role::find($id);
        $db->delete();

        Session::flash("status", [
            "level"=>"danger",
            "message"=>"Berhasil Di Hapus"
        ]);

        return 'ok';
    }

}
