<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use DB;
use Auth;
use App\Role;

class SelectOrganizationController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));

            if(Auth::user()->hasRole('admin')) {           
                $data = DB::table('po_organizations')->select(DB::raw('@rownum  := @rownum  + 1 AS no'),'po_organizations.*')
                ->where('form_id',4);
            } else {
                $data = DB::table('positions')->select(DB::raw('@rownum  := @rownum  + 1 AS no'),'roles.display_name','positions.id','po_organizations.abbreviation','po_organizations.organization')
                        ->join('po_organizations','po_organizations.id','positions.organization_id')
                        ->join('roles','roles.id','positions.role_id');
            }
                
            return Datatables::of($data)->make(true);

        }

        return view('selectOrganization');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        if(Auth::user()->hasRole('admin')) {
            $data = DB::table('po_organizations')->where('id',$id)->first();
        } else {
            $positions = DB::table('positions')->where('id',$id)->where('user_id',Auth::id())->first();
            $data = $positions ? DB::table('po_organizations')->where('id',$positions->organization_id)->first() : false;

            $currentRole = Auth::user()->roles[0];
            $selectedRole = Role::find($positions->role_id);

            if(!Auth::user()->hasRole($selectedRole->name)) { //switch role if different
                Auth::user()->attachRole($selectedRole->id);
                Auth::user()->detachRole($currentRole->id);
            }
        }

        if ($data) {
            $request->session()->put('organization_id', $id);
            $request->session()->put('organization_name', $data->organization);    
        }

        return redirect($request->get('redirect'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
