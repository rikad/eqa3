(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('chart.js')) :
	typeof define === 'function' && define.amd ? define(['exports', 'chart.js'], factory) :
	(factory((global.ChartHierarchical = global.ChartHierarchical || {}),global.Chart));
}(this, (function (exports,Chart) { 'use strict';

/**
 * bulids up recursivly the label tree
 * @param {string|ILabelNode} label
 * @param {ILabelNode|null} parent
 * @returns the node itself
 */
function asNode(label, parent) {
  var node = Object.assign({
    label: '',
    children: [],
    expand: false,
    level: parent ? parent.level + 1 : 0,
    center: NaN,
    width: 0,
    hidden: false,
    major: !parent // for ticks
  }, typeof label === 'string' ? { label: label } : label);

  node.children = node.children.map(function (d) {
    return asNode(d, node);
  });

  return node;
}

/**
 * pushses a node into the given flat array and updates the index information to avoid parent links
 *
 * @param {ILabelNode} node
 * @param {number} i relative index of this node to its parent
 * @param {ILabelNode[]} flat flat array to push
 * @param {ILabelNode|null} parent
 */
function push(node, i, flat, parent) {
  node.relIndex = i;
  node.index = flat.length; // absolute index
  node.parent = parent ? parent.index : -1;
  // node is hidden if parent is visible or not expanded
  node.hidden = Boolean(parent ? parent.expand === false || node.expand : node.expand);

  flat.push(node);

  node.children.forEach(function (d, j) {
    return push(d, j, flat, node);
  });
}

/**
 * converts the given labels to a flat array of linked nodes
 * @param {Partial<ILabelNode>|string} labels
 */
function toNodes(labels) {
  var nodes = labels.map(function (d) {
    return asNode(d);
  });

  var flat = [];
  nodes.forEach(function (d, i) {
    return push(d, i, flat);
  });

  return flat;
}

/**
 * computes the parents (including itself) of the given node
 * @param {ILabelNode} node
 * @param {ILabelNode[]} flat flatArray for lookup
 */
function parentsOf(node, flat) {
  var parents = [node];
  while (parents[0].parent >= 0) {
    parents.unshift(flat[parents[0].parent]);
  }
  return parents;
}

/**
 * based on the visibility of the nodes computes the last node in the same level that is visible also considering expanded children
 * @param {ILabelNode} node
 * @param {ILabelNode[]} flat flatArray for lookup
 */
function lastOfLevel(node, flat) {
  var last = flat[node.index];
  while (last && last.level >= node.level && (last.level !== node.level || last.parent === node.parent)) {
    last = flat[last.index + 1];
  }
  return flat[(last ? last.index : flat.length) - 1];
}

/**
 * resolves for the given label node its value node
 * @param {ILabelNode} label
 * @param {ILabelNode[]} flat
 * @param {(IValueNode|any)[]} dataTree
 */
function resolve(label, flat, dataTree) {
  var parents = parentsOf(label, flat);

  var dataItem = { children: dataTree };
  var dataParents = parents.map(function (p) {
    dataItem = dataItem && !(typeof dataItem === 'number' && isNaN(dataItem)) && dataItem.children ? dataItem.children[p.relIndex] : NaN;
    return dataItem;
  });

  var value = dataParents[dataParents.length - 1];
  // convert to value
  if (typeof value !== 'number' && value.hasOwnProperty('value')) {
    return value.value;
  }
  return value;
}

/**
 * counts the number of nodes that are visible when the given node is expanded
 * @param {ILabelNode} node
 */
function countExpanded(node) {
  if (!node.expand) {
    return 1;
  }
  return node.children.reduce(function (acc, d) {
    return acc + countExpanded(d);
  }, 0);
}

var defaultConfig = Object.assign({}, Chart.scaleService.getScaleDefaults('category'), {
  /**
   * reduce the space between items at level X by this factor
   */
  levelPercentage: 0.75,
  /**
   * additional attributes to copy, e.g. backgroundColor
   * object where the key is the attribute and the value the default value if not explicity specified in the label tree
   */
  attributes: {},

  /**
   * top/left padding for showing the hierarchy marker
   */
  padding: 25,
  /**
   * size of the box to draw
   */
  hierarchyBoxSize: 14,
  /**
   * distance between two hierarhy indicators
   */
  hierarchyBoxLineHeight: 22,
  /**
   * color of the line indicator hierarchy children
   */
  hierarchySpanColor: 'gray',
  /**
   * storke width of the line
   */
  hierarchySpanWidth: 2,
  /**
   * color of the box to toggle collapse/expand
   */
  hierarchyBoxColor: 'gray',
  /**
   * stroke width of the toggle box
   */
  hierarchyBoxWidth: 1
});

var HierarchicalScale = Chart.Scale.extend({
  determineDataLimits: function determineDataLimits() {
    var data = this.chart.data;
    var labels = this.options.labels || (this.isHorizontal() ? data.xLabels : data.yLabels) || data.labels;

    // labels are already prepared by the plugin just use them as ticks
    this._nodes = labels.slice();

    // not really used
    this.minIndex = 0;
    this.maxIndex = this._nodes.length;
    this.min = this._nodes[this.minIndex];
    this.max = this._nodes[this.maxIndex];

    // this.options.barThickness = 'flex';
  },
  buildTicks: function buildTicks() {
    var hor = this.isHorizontal();
    var total = hor ? this.width : this.height;
    var nodes = this._nodes.slice(this.minIndex, this.maxIndex);
    var flat = this.chart.data.flatLabels;

    if (nodes.length === 0) {
      this.ticks = [];
      return this.ticks;
    }

    // optimize such that the distance between two points on the same level is same
    // creaiing a grouping effect of nodes
    var ratio = this.options.levelPercentage;

    // max 5 levels for now
    var ratios = [1, Math.pow(ratio, 1), Math.pow(ratio, 2), Math.pow(ratio, 3), Math.pow(ratio, 4)];

    var distances = [];
    {
      var prev = nodes[0];
      var prevParents = parentsOf(prev, flat);
      distances.push(0.5); // half top level distance before and after

      for (var i = 1; i < nodes.length; ++i) {
        var n = nodes[i];
        var parents = parentsOf(n, flat);
        if (prev.parent === n.parent) {
          // same parent -> can use the level distance
          distances.push(ratios[n.level]);
        } else {
          // differnt level -> use the distance of the common parent
          // find level of common parent
          var common = 0;
          while (parents[common] === prevParents[common]) {
            common++;
          }
          distances.push(ratios[common]);
        }
        prev = n;
        prevParents = parents;
      }
      distances.push(0.5);
    }

    var distance = distances.reduce(function (acc, s) {
      return acc + s;
    }, 0);
    var factor = total / distance;

    var offset = distances[0] * factor;
    nodes.forEach(function (node, i) {
      var previous = distances[i] * factor;
      var next = distances[i + 1] * factor;
      node.center = offset;
      offset += next;

      node.width = Math.min(next, previous) / 2;
    });

    this.ticks = nodes.map(function (d) {
      return Object.assign({}, d);
    }); // copy since mutated during auto skip
    return this.ticks;
  },
  convertTicksToLabels: function convertTicksToLabels(ticks) {
    return ticks.map(function (d) {
      return d.label;
    });
  },
  getLabelForIndex: function getLabelForIndex(index, datasetIndex) {
    var data = this.chart.data;
    var isHorizontal = this.isHorizontal();

    if (data.yLabels && !isHorizontal) {
      return this.getRightValue(data.datasets[datasetIndex].data[index]);
    }
    return this._nodes[index - this.minIndex].label;
  },


  // Used to get data value locations.  Value can either be an index or a numerical value
  getPixelForValue: function getPixelForValue(value, index) {
    // If value is a data object, then index is the index in the data array,
    // not the index of the scale. We need to change that.
    {
      var valueCategory = void 0;
      if (value !== undefined && value !== null) {
        valueCategory = this.isHorizontal() ? value.x : value.y;
      }
      if (valueCategory !== undefined || value !== undefined && isNaN(index)) {
        value = valueCategory || value;
        var idx = this._flatTree.findIndex(function (d) {
          return d.label === value;
        });
        index = idx !== -1 ? idx : index;
      }
    }

    var node = this._nodes[index];
    var centerTick = this.options.offset;
    var base = this.isHorizontal() ? this.left : this.top;

    return base + node.center - (centerTick ? 0 : node.width / 2);
  },
  getPixelForTick: function getPixelForTick(index) {
    var node = this._nodes[index + this.minIndex];
    var centerTick = this.options.offset;
    var base = this.isHorizontal() ? this.left : this.top;
    return base + node.center - (centerTick ? 0 : node.width / 2);
  },
  getValueForPixel: function getValueForPixel(pixel) {
    return this._nodes.findIndex(function (d) {
      return pixel >= d.center - d.width / 2 && pixel <= d.center + d.width / 2;
    });
  },
  getBasePixel: function getBasePixel() {
    return this.bottom;
  }
});

Chart.scaleService.registerScaleType('hierarchical', HierarchicalScale, defaultConfig);

function parseFontOptions(options) {
  var valueOrDefault = Chart.helpers.valueOrDefault;
  var globalDefaults = Chart.defaults.global;
  var size = valueOrDefault(options.fontSize, globalDefaults.defaultFontSize);
  var style = valueOrDefault(options.fontStyle, globalDefaults.defaultFontStyle);
  var family = valueOrDefault(options.fontFamily, globalDefaults.defaultFontFamily);

  return {
    size: size,
    style: style,
    family: family,
    font: Chart.helpers.fontString(size, style, family)
  };
}

function generateCode(labels) {
  // label, expand, children
  var code = '';
  var encode = function encode(label) {
    if (typeof label === 'string') {
      code += label;
      return;
    }
    code += '(l=' + label.label + ',e=' + label.expand + ',c=[';
    (label.children || []).forEach(encode);
    code += '])';
  };

  labels.forEach(encode);
  return code;
}

var HierarchicalPlugin = {
  id: 'chartJsPluginHierarchical',

  /**
   * checks whether this plugin needs ot be enabled based on wehther one is a hierarchical axis
   */
  _enabled: function _enabled(chart) {
    if (chart.config.options.scales.xAxes[0].type === 'hierarchical') {
      return 'x';
    }
    if (chart.config.options.scales.yAxes[0].type === 'hierarchical') {
      return 'y';
    }
    return null;
  },
  _check: function _check(chart) {
    if (chart.data.labels && chart.data._verify === generateCode(chart.data.labels)) {
      return;
    }

    // convert labels to nodes
    var flat = chart.data.flatLabels = toNodes(chart.data.labels);

    var focus = flat.find(function (d) {
      return d.expand === 'focus';
    });
    var labels = void 0;

    if (focus) {
      labels = flat.slice(focus.index + 1).filter(function (d) {
        return !d.hidden && parentsOf(d, flat).includes(focus);
      });
    } else {
      // the real labels are the one not hidden in the tree
      labels = chart.data.labels = chart.data.flatLabels.filter(function (d) {
        return !d.hidden;
      });
    }

    chart.data.labels = labels;
    this._updateVerifyCode(chart);

    // convert the data tree to the flat visible counterpart
    chart.data.datasets.forEach(function (dataset) {
      if (dataset.tree == null) {
        dataset.tree = dataset.data.slice();
      }
      dataset.data = labels.map(function (l) {
        return resolve(l, flat, dataset.tree);
      });
    });

    this._updateAttributes(chart);
  },
  _updateVerifyCode: function _updateVerifyCode(chart) {
    chart.data._verify = generateCode(chart.data.labels);
  },


  /**
   * updates the attributes according to config, similar to data sync
   */
  _updateAttributes: function _updateAttributes(chart) {
    var scale = this._findScale(chart);
    if (!scale) {
      return;
    }
    var attributes = scale.options.attributes;

    var nodes = chart.data.labels;
    var flat = chart.data.flatLabels;

    Object.keys(attributes).forEach(function (attr) {
      chart.data.datasets.forEach(function (d) {
        d[attr] = nodes.map(function (n) {
          while (n) {
            if (n.hasOwnProperty(attr)) {
              return n[attr];
            }
            // walk up the hierarchy
            n = n.parent >= 0 ? flat[n.parent] : null;
          }
          return attributes[attr]; // default value
        });
      });
    });
  },
  _findScale: function _findScale(chart) {
    var scales = Object.keys(chart.scales).map(function (d) {
      return chart.scales[d];
    });
    return scales.find(function (d) {
      return d.type === 'hierarchical';
    });
  },
  beforeUpdate: function beforeUpdate(chart) {
    if (!this._enabled(chart)) {
      return;
    }
    this._check(chart);
  },


  /**
   * draw the hierarchy indicators
   */
  beforeDatasetsDraw: function beforeDatasetsDraw(chart) {
    if (!this._enabled(chart)) {
      return;
    }
    var scale = this._findScale(chart);
    var flat = chart.data.flatLabels;
    var ctx = chart.ctx;
    var hor = scale.isHorizontal();

    var boxSize = scale.options.hierarchyBoxSize;
    var boxSize5 = boxSize * 0.5;
    var boxSize1 = boxSize * 0.1;
    var boxRow = scale.options.hierarchyBoxLineHeight;
    var boxColor = scale.options.hierarchyBoxColor;
    var boxWidth = scale.options.hierarchyBoxWidth;
    var boxSpanColor = scale.options.hierarchySpanColor;
    var boxSpanWidth = scale.options.hierarchySpanWidth;

    var scaleLabel = scale.options.scaleLabel;
    var scaleLabelFontColor = Chart.helpers.valueOrDefault(scaleLabel.fontColor, Chart.defaults.global.defaultFontColor);
    var scaleLabelFont = parseFontOptions(scaleLabel);

    ctx.save();
    ctx.strokeStyle = boxColor;
    ctx.lineeWidth = boxWidth;
    ctx.fillStyle = scaleLabelFontColor; // render in correct colour
    ctx.font = scaleLabelFont.font;

    if (hor) {
      ctx.textAlign = 'center';
      ctx.textBaseline = 'top';
      ctx.translate(scale.left, scale.top + scale.options.padding);

      chart.data.labels.forEach(function (tick) {
        var center = tick.center;
        var offset = 0;
        var parents = parentsOf(tick, flat);

        parents.slice(1).forEach(function (d, i) {
          var pp = parents[i];
          if (d.relIndex === 0) {
            var last = lastOfLevel(d, flat);
            var next = flat.slice(d.index + 1, last.index + 1).find(function (n) {
              return !n.hidden;
            });

            if (pp.expand !== 'focus') {
              ctx.strokeRect(center - boxSize5, offset + 0, boxSize, boxSize);
              ctx.fillRect(center - boxSize5 + 2, offset + boxSize5 - 1, boxSize - 4, 2);
            }
            ctx.strokeRect(last.center - boxSize5, offset + 0, boxSize, boxSize);
            ctx.fillRect(last.center - 2, offset + boxSize5 - 2, 4, 4);

            ctx.fillText(pp.label, (next.center + center) / 2, offset + boxSize);

            // render helper indicator line
            ctx.strokeStyle = boxSpanColor;
            ctx.lineWidth = boxSpanWidth;
            ctx.beginPath();
            ctx.moveTo(last.center - boxSize5, offset + boxSize5);
            if (pp.expand === 'focus') {
              ctx.lineTo(center, offset + boxSize5);
              ctx.lineTo(center, offset + boxSize1);
            } else {
              ctx.lineTo(center + boxSize5, offset + boxSize5);
            }
            ctx.stroke();
            ctx.strokeStyle = boxColor;
            ctx.lineWidth = boxWidth;
          }
          offset += boxRow;
        });

        if (tick.children.length > 0) {
          ctx.strokeRect(center - boxSize5, offset + 0, boxSize, boxSize);
          ctx.fillRect(center - boxSize5 + 2, offset + boxSize5 - 1, boxSize - 4, 2);
          ctx.fillRect(center - 1, offset + 2, 2, boxSize - 4);
        }
      });
    } else {
      ctx.textAlign = 'right';
      ctx.textBaseline = 'center';
      ctx.translate(scale.left - scale.options.padding, scale.top);

      chart.data.labels.forEach(function (tick) {
        var center = tick.center;
        var offset = 0;
        var parents = parentsOf(tick, flat);

        parents.slice(1).forEach(function (d, i) {
          var pp = parents[i];
          if (d.relIndex === 0) {
            var last = lastOfLevel(d, flat);
            var next = flat.slice(d.index + 1, last.index + 1).find(function (n) {
              return !n.hidden;
            });

            if (pp.expand !== 'focus') {
              ctx.strokeRect(offset - boxSize, center - boxSize5, boxSize, boxSize);
              ctx.fillRect(offset - boxSize + 2, center - 1, boxSize - 4, 2);
            }
            ctx.strokeRect(offset - boxSize, last.center - boxSize5, boxSize, boxSize);
            ctx.fillRect(offset - boxSize5 - 2, last.center - 2, 4, 4);

            ctx.fillText(pp.label, offset - boxSize, (next.center + center) / 2);

            // render helper indicator line
            ctx.strokeStyle = boxSpanColor;
            ctx.lineWidth = boxSpanWidth;
            ctx.beginPath();

            ctx.moveTo(offset - boxSize5, last.center - boxSize5);
            if (pp.expand === 'focus') {
              ctx.lineTo(offset - boxSize5, center);
              ctx.lineTo(offset - boxSize1, center);
            } else {
              ctx.lineTo(offset - boxSize5, center + boxSize5);
            }

            ctx.stroke();
            ctx.strokeStyle = boxColor;
            ctx.lineWidth = boxWidth;
          }
          offset -= boxRow;
        });

        // render expand hint
        if (tick.children.length > 0) {
          ctx.strokeRect(offset - boxSize, center - boxSize5, boxSize, boxSize);
          ctx.fillRect(offset - boxSize + 2, center - 1, boxSize - 4, 2);
          ctx.fillRect(offset - boxSize5 - 1, center - boxSize5 + 2, 2, boxSize - 4);
        }
      });
    }

    ctx.restore();
  },
  _expandCollapse: function _expandCollapse(chart, index, count, toAdd) {
    var labels = chart.data.labels;
    var flatLabels = chart.data.flatLabels;
    var data = chart.data.datasets;

    var removed = labels.splice.apply(labels, [index, count].concat(toAdd));
    removed.forEach(function (d) {
      d.hidden = true;
      d.expand = false;
    });
    toAdd.forEach(function (d) {
      d.hidden = false;
    });

    data.forEach(function (dataset) {
      var toAddData = toAdd.map(function (d) {
        return resolve(d, flatLabels, dataset.tree);
      });
      dataset.data.splice.apply(dataset.data, [index, count].concat(toAddData));
    });

    this._updateVerifyCode(chart);
    this._updateAttributes(chart);

    chart.update();
  },
  _collapse: function _collapse(chart, index, parent) {
    var count = countExpanded(parent);
    this._expandCollapse(chart, index, count, [parent]);
    parent.expand = false;
  },
  _expand: function _expand(chart, index, node) {
    this._expandCollapse(chart, index, 1, node.children);
    node.expand = true;
  },
  _zoomIn: function _zoomIn(chart, lastIndex, parent) {
    var count = countExpanded(parent);
    parent.expand = 'focus';
    var index = lastIndex - count + 1;

    var labels = chart.data.labels;
    labels.splice(lastIndex + 1, labels.length);
    labels.splice(0, index);

    var data = chart.data.datasets;
    data.forEach(function (dataset) {
      dataset.data.splice(lastIndex + 1, dataset.data.length);
      dataset.data.splice(0, index);
    });

    this._updateVerifyCode(chart);
    this._updateAttributes(chart);

    chart.update();
  },
  _zoomOut: function _zoomOut(chart, parent) {
    var labels = chart.data.labels;
    var flatLabels = chart.data.flatLabels;

    parent.expand = true;
    var nextLabels = flatLabels.filter(function (d) {
      return !d.hidden;
    });
    var index = nextLabels.indexOf(labels[0]);
    var count = labels.length;

    labels.splice.apply(labels, [labels.length, 0].concat(nextLabels.slice(index + count)));
    labels.splice.apply(labels, [0, 0].concat(nextLabels.slice(0, index)));

    var data = chart.data.datasets;
    data.forEach(function (dataset) {
      var toAddBefore = nextLabels.slice(0, index).map(function (d) {
        return resolve(d, flatLabels, dataset.tree);
      });
      var toAddAfter = nextLabels.slice(index + count).map(function (d) {
        return resolve(d, flatLabels, dataset.tree);
      });

      dataset.data.splice.apply(dataset.data, [labels.length, 0].concat(toAddAfter));
      dataset.data.splice.apply(dataset.data, [0, 0].concat(toAddBefore));
    });

    this._updateVerifyCode(chart);
    this._updateAttributes(chart);

    chart.update();
  },
  _resolveElement: function _resolveElement(event, chart, scale) {
    var hor = scale.isHorizontal();
    var offset = hor ? scale.top + scale.options.padding : scale.left - scale.options.padding;
    if (hor && event.y <= offset || !hor && event.x > offset) {
      return null;
    }

    var elem = chart.getElementsAtEventForMode(event, 'index', { axis: hor ? 'x' : 'y' })[0];
    if (!elem) {
      return null;
    }
    return { offset: offset, index: elem._index };
  },
  beforeEvent: function beforeEvent(chart, event) {
    if (event.type !== 'click' || !this._enabled(chart)) {
      return;
    }

    var scale = this._findScale(chart);
    var hor = scale.isHorizontal();

    var elem = this._resolveElement(event, chart, scale);
    if (!elem) {
      return;
    }
    var offset = elem.offset;

    var index = elem.index;
    var flat = chart.data.flatLabels;
    var label = chart.data.labels[index];
    var parents = parentsOf(label, flat);
    var boxRow = scale.options.hierarchyBoxLineHeight;

    var inRange = hor ? function (o) {
      return event.y >= o && event.y <= o + boxRow;
    } : function (o) {
      return event.x <= o && event.x >= o - boxRow;
    };

    for (var i = 1; i < parents.length; ++i) {
      var parent = parents[i];
      // out of box
      if (inRange(offset) && (parent.children[0] === parents[i + 1] || i === parents.length - 1)) {
        var pp = flat[parent.parent];
        if (parent.relIndex === 0 && pp.expand === true) {
          this._collapse(chart, index, pp);
          return;
        }
        var isLastIndex = pp.children.length === parent.relIndex + 1;

        if (isLastIndex && pp.expand === 'focus') {
          this._zoomOut(chart, pp);
          return;
        } else if (isLastIndex && pp.expand === true) {
          this._zoomIn(chart, index, pp);
          return;
        }
      }
      offset += hor ? boxRow : -boxRow;
    }

    if (label.children.length > 0 && inRange(offset)) {
      // expand
      this._expand(chart, index, label);
      return;
    }
  }
};

Chart.pluginService.register(HierarchicalPlugin);

exports.HierarchialScale = HierarchicalScale;
exports.HierarchialPlugin = HierarchicalPlugin;

Object.defineProperty(exports, '__esModule', { value: true });

})));
